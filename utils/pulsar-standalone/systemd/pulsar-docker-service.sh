#!/bin/bash

sudo /usr/bin/docker start pulsar-service || sudo /usr/bin/docker run --name pulsar-service -d -p 6650:6650 -p 8080:8080 --mount source=pulsardata,target=/pulsar/data --mount source=pulsarconf,target=/pulsar/conf apachepulsar/pulsar:3.1.1 bin/pulsar standalone