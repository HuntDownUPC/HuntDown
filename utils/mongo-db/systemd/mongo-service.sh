#!/bin/bash

sudo /usr/bin/docker start huntdown-db || sudo docker run --name huntdown-db -d -p 27017:27017 -v ~/hd-mongo-db/data:/data/db -v ~/hd-mongo-db/mongo_init.sh:/docker-entrypoint-initdb.d/mongo_init.sh mongo