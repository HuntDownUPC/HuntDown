<div id="top"></div>

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="">
    <img src="images/logo.png" alt="Logo" width="auto" height="auto">
  </a>

  <h3 align="center">HUNTDOWN</h3>

  <p align="center">
    A platform that helps with your penetration tests
  </p>
</div>

> [!IMPORTANT]
> ⚠️⚠️Legal Disclaimer⚠️⚠️: Usage of Huntdown for attacking targets without prior mutual consent is illegal. It is the end user's responsibility to obey all applicable local, state and federal laws. Developers assume no liability and are not responsible for any misuse or damage caused by this program.

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#simple-single-host-installation">Simple single host installation</a></li>
        <li><a href="#running-the-whole-application">Running the whole application</a></li>
      </ul>
    </li>
    <li>
      <a href="#how-to">How to</a>
      <ul>
        <li><a href="#create-new-attacks">Create new attacks</a></li>
      </ul>
    </li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

[![Product Name Screen Shot][product-screenshot]](https://www.upc.edu/ca)

The initial work of this project is being developed by 4 students of the **Barcelona School of Informatics FIB** for their final degree project.

The main ideas of this project are:

- Develop a web application that facilitates pentests.
- Develop in a way that is easy to implement new attacks and functionalities
- Use microservices
- Use virtual machines for the execution of the attacks in order to avoid maintenance.

<p align="right">(<a href="#top">back to top</a>)</p>

### Built With

These are the main technologies that the project is going to have.

- [Golang](https://go.dev/)
- [Ansible](https://www.ansible.com/)
- [Vagrant](https://www.vagrantup.com/)
- [React](https://es.reactjs.org/)
- [Apache Pulsar](https://pulsar.apache.org/)
- [MongoDB](https://www.mongodb.com/)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- GETTING STARTED -->

## Getting Started

At this moment, this project has four modules and one Data Base:

<ul>
<li>Web Application</li>
<li>Message Broker</li>
<li>Microservice that interacts with the Data Base</li>
<li>Microservice that manage attacks configuration</li>
<li>Microservice that instantiate virtual machines that throw attacks</li>
</ul>

All this modules can run in the same server or they can be distributed among different instances. In this document both configurations will be approached.

### Prerequisites

The different modules require to be executed in a Debian distribution, like Ubuntu or Kali.

### Simple single host installation

Huntdown internals are currently composed by 3 different blocks, a message broker, the user interface, and a database.

<ul>
<li><b>Database Manager</b>: in charge of interfacing with the database</li>
<li><b>Attack Manager</b>: in charge of handling the internal logic of the attacks and process their results</li>
<li><b>Instantiation Manager</b>: in charge of handling the instantiation of the virtual machines</li>
<li><b>User Interface</b>: is in charge of interfacing with the user, it is composed by two different parts on its own, the fronted and the backend, both written in ReactJS and leveraging the NodeJS server</li>
</ul>

The database is a MongoDB instance, and as the rest of the components may be installed and executed independently, as the message broker handles the message passing transparently.

In this section we describe the simplest deployment mechanism, which is to deploy all the components in the same host. This is the recommended deployment mechanism for testing purposes. To this end we provide an script (`setup.sh`) which will assist in the configuration and initial deployment of the system.

#### Guided steps

First choose the default location for all the source code, for example `~/` or `~/Desktop`

```
mkdir Huntdown
cd Huntdown
git clone git@gitlab.com:HuntDownUPC/HuntDown.git
git clone git@gitlab.com:HuntDownUPC/go-modules.git
cd Huntdown && git checkout develop && cd ..
cd go-modules && git checkout develop && cd ..
```

Now create the `go.work` file with the following contents:
```
go 1.22.7

use (
        ./HuntDown/src
        ./HuntDown/src/attack-manager
        ./HuntDown/src/attacks
        ./HuntDown/src/confManager
        ./HuntDown/src/database
        ./HuntDown/src/helpers
        ./HuntDown/src/information-gathering
        ./HuntDown/src/instantiation
        ./go-modules/PulsarLib
        ./go-modules/RedisLib
        ./go-modules/db/DBUsers
        ./go-modules/db/DBWrapper
        ./go-modules/db/backends/mongo/MongoDBWrapper
        ./go-modules/db/backends/mongo/UsersMongoDB
        ./go-modules/db/common
)
```

Take into consideration that the final `Huntdown` folder contents will look something like:

```
$ ls -la
total 36
drwxr-xr-x.   7 user user 4096 May    1 09:09 .
drwx--x---+ 129 user user 4096 May    3 01:11 ..
drwxr-xr-x.   5 user user 4096 May    1 02:44 go-modules
-rw-rw-r--.   1 user user  398 Apr   19 22:30 go.work
drwxr-xr-x.  10 user user 4096 May    3 00:23 HuntDown
```

After this you should be able to run the setup:

```
cd HuntDown
./setup.sh
```

And follow the on-screen instructions.

> [!WARNING]
> If you encounter any problem related to permissions when installing it in your local machine, please remove the if statement `if [ $UID -ne 0 ] ; then` and declare `sudo="sudo"`. Then you will be able to run the script as `sudo ./setup.sh`.

##### Generic GO setup

If you want to setup GO globally, you can add it to the PATH environment as:

```
sudo nano ~/.bashrc
```

At the end of the file add:

```
export PATH="$PATH:/usr/lib/go/bin"
```

Finally, update the `~/.bashrc` file:

```
source ~/.bashrc
```

#### Workaround's

If these scripts do not succeed due to permissions issues, you can run them manually as:

```
cd Huntdown
sudo -u huntdown env "PATH=$PATH" go run ./src/database/PresetsInit/presetsInit.go -presets ./src/database/presets.json -config /etc/huntdown/environment.json
sudo -u huntdown env "PATH=$PATH" go run ./src/database/MachinesInit/machineInit.go -machines ./src/database/machines.json -config /etc/huntdown/environment.json
sudo -u huntdown env "PATH=$PATH" go run ./src/database/addUsers/addUsers.go -users ./src/database/users.json -env /etc/huntdown/environment.json
```

By default the application will be installed into the `/usr/local/huntdown` directory, and the configuration files will be placed in `/etc/huntdown`.

#### User creation

The only missing step is to create an user for the whole system, we can accomplish this by running:

```
sudo -u huntdown /usr/local/huntdown/bin/hd-add-users -u email@address.com -env /etc/huntdown/environment.json
```

Now the setup should be finished, go the next section to start everything

<p align="right">(<a href="#top">back to top</a>)</p>

### Running the whole application

Once deployed, the system should start all the services, but in some situations this may fail, follow the next steps to validate and start the different services:
The process consists of four activities:

1. Check if the backend is correctly running:
   - This should give you an idea of the missing services in your system.
   - In case some services are not properly running the below script will provide you with the missing instructions to do the initialization of the services

```
sudo /usr/local/huntdown/bin/check-all-services.sh
```

2. Start the UI: By default, it should be started automatically with the pm2 service under the user `huntdown`. You can test it running:

```
sudo -u huntdown pm2 l
```

3. Start the UI for debug:

```
$ sudo -u huntdown pm2 stop all
$ sudo apt install screen [optional]
$ cd Huntdown
$ cd src/Interface/backend && npm install && cd .. && cd node-pulsar && npm install && cd .. && npm install cd ../..
$ cd src/Interface/backend
$ [screen] npm run debug
  <Press CTRL+a + d to suspend the screen>
$ cd ..
$ screen npm run-script start-frontend
  <Press CTRL+a + d to suspend the screen>
```

You can recover the screens anytime by running:

```
screen -r <session number>
```

You can list all the screens with:

```
screen -L
```

## How to

### Add a new tool to the arsenal

> Note: Tools are contained under the `HuntDown/src/attacks` directory.

> Note 2: A new JSON format has be developed to improve options parsing and add backend security to HuntDown. Refer to the "**Update a tool to the new JSON format**" section for more information.

First start by duplicating the folder of a tool already present like `gobuster` for example.

It contains all these files:

```bash
.
├── gobuster-attack.json
├── gobuster-dashboard.json
├── gobuster-options.json
├── gobusterTable
│   ├── gobusterTable.go
│   ├── rp_gobuster_test.go
│   └── tests
│       └── crash_test1.json
├── gobusterWorkflow
│   ├── gobusterWorkflow.go
│   ├── rp_gobusterWorkflow_test.go
│   └── tests
│       └── crash_test1.json
├── Makefile
├── playbook-gobuster.template
├── rp_gobuster.go
└── rp_gobuster.so

4 directories, 13 files
```

#### Important Files

The most important files are the following:

- `tool-attack.json`
- `tool-options.json`

The other files can (for now) just be copy/pasted and renamed accordingly to the new tool you want to add.

##### `tool-attack.json`

This file defines how the tool needs to be ran:

- **How to trigger the tool**: Usually the binary name.
- **Which runner to use**: Web, Docker or a VM.
- **The commands to execute before running the tool**: install packages, create directories, etc.

##### `tool-options.json`

This file contains all the possible flags of the tool. It can handle multiple tool configuration (for multiple versions for example) but for now we only use one.

The definition of a tool configuration is like the following:

```json
[
  {
    "name": "tool",
    "optionsid": "tool01",
    "optionsname": "",
    "options": [] # contains the config flags
  },
]
```

The fields definition inside the `"options": []` array follow this structure:

- `title`: the name of option
- `maingroup`: the group to affect the option on the UI
- `subgroup`: the subgroup of the group (if any)
- `flag`: the flag to trigger the option (if any)
- `value`: default value, will be overwritten by user input (if any)
- `needcheckbox`: true if the option needs to be activated/deactivated by user (mandatory options must not be deactivated by user)
- `checkbox`: true to activate by default an option
- `textinput`: true to let user type text
- `information`: text displayed in the tooltip
- `require`: array containing the option(s) this one is depending on
- `conflict`: array containing the options(s) this one can't be used with
- `regex`: base64 encoded regular expression controlling user input (if any)

It is important to keep the following "rules" in mind while populating the options:

- The mandatory option should be first using: `"maingroup": "Required",`
- The most used options should follow the required ones
- If an option allows to export the output using another format like `.xml` or `.json` they have to go last using `"maingroup": "Output",` and one of them must be activated by default
- You can organize the option group as you want but usually following the tool's manual is sufficient
- All the options that require user input `must` have a regex encoded in base64 to check that the input matches what is expected

### Update a tool to the new JSON format

This quite simple update takes 3 steps:

1. Define the tool's delimiter
2. Set all the regex
3. Fill in the `require` and `conflict` arrays

#### Define tool's delimiter

The tool delimiter is the character that every tool uses to separate the flags and the values.

Here are some examples:

```bash
tool -option value    # here the delimiter is a space
tool -option:value    # here the delimiter is “:”
tool --option=value   # here the delimiter is “=”
```

Currently the supported delimiters are the following:

1. `"space"` representing the space char
2. `":"` representing the semicolon char
3. `"="` representing the equal sign char

> Note: other delimiter values can easily be added, keep reading.

Once the delimiter is identified for the tool you need to update you need to update its `OptionsFile` like the following:

```json
[
  {
    "name": "gobuster",
    "optionsid": "gobuster01",
    "optionsname": "",
    "delimiter": "space",         # add this field with the right value for you
    "options": [
      {
        "title": "Mode",
        "maingroup": "Required",
        ...
      }
    ]
  }
]
```

#### Set all the regex

In the `OptionsFile` of the tool to be updated all the options having the `textinput` field set to `true` must have a valid `regex` associated.

> Don't forget to specify what is expected by the regex in the `information` field!

Examples:

```json
{
  "title": "Option 1",
  "textinput": true,
  "information": "A number between 1 and 100. Example: 10",
  "regex": "XihkaXJ8ZG5zfHZob3N0fGZ1enxzM3xnY3N8dGZ0cCkk",
},
{
  "title": "Option 2",
  "textinput": true,
  "information": "A value between: dir, vhost and fuzz. Example: dir",
  "regex": "XihkaXIudHh0fHN1YmRvbWFpbnMudHh0fGJ1Y2tldHMuYXR8ZnV6ei50eHR8ZmlsZXMudHh0KSQ=",
},
{
  "title": "Option 3",
  "textinput": false,
  "information": "Check this to enable ...",
  "regex": "",
},
```

- `Option 1` and `Option 2` **MUST** have a `regex` associated because the `textinput` field is set to `true`. The value expected is precised in the `information` field with a given example.
- `Option 3` does not need any `regex` because its `textinput` field is set to `false`. the `information` field should explain the option.

#### Fill in the `require` and `conflict` arrays

This step can be very quick or very long depending on the tool's number of options and behavior. You have to study the tool behavior and test all the options to understand them.

You need to update the `OptionsFile` of the tool and add the following fields:

- `"require": []`
- `"conflict": []`

The require array is used to link options that rely on other options to be used. If an option requires another option to be present in order to be used then it must be added to the require array (explained below).

Example: using nmap the port option (`-p`) can only be used after the target ip address has been set.

the conclict array is used to define options that cannot be used together. If two options are not compatible together then it must be added to the conflict array.

Example: using nmap we cannot run two TCP scans at same time. Using `-sT` with `-sS` will result in a failing attack.

The updated `OptionsFile` look like this (some fields have been omited in purpose):

```json
{
  "title": "Option 1",
  "flag": "-A",
  ...
  "require": [],
  "conflict": []
},
{
  "title": "Option 2",
  "flag": "-B",
  ...
  "require": ["Option 2"],
  "conflict": ["Option 3"]
},
{
  "title": "Option 3",
  "flag": "-C",
  ...
  "require": ["Option 1"],
  "conflict": ["Option 2"]
},
```

When testing the tool execution:

```bash
$ tool -A           # OK
$ tool -B           # NOK missing -A
$ tool -C           # NOK missing -A
$ tool -A -B        # OK
$ tool -A -C        # OK
$ tool -B -C        # NOK conflicts
$ tool -A -B -C     # NOK conflicts
```

> Note: As many options can be inserted into the arrays. Be sure to only specify the `title` field.

#### Add more delimiters

If the tool you want to update/add is using a delimiter character not present in the list above you can add your own by updating the `AssembleCommandLineString` function located in `/HuntDown/src/helpers/OptionParser/option_parser.go`.

Update the second case or add your own if the tool's logic is different.

---

### Create new attacks

If you want to add a new attack to the project, the first step is to create a new file with the AttackSchema structure. First of all you should give it a name . The next step is to know the commands that will be executed in your attack. If it is only the execution of a tool like nmap, it may be just one command, otherwise you may need more comands and dependencies. Once all the commands are identified, you must add the packages required to execute them along with their aproximate size.
After adding the corresponding <b>attackid</b> and the default values of the other fields, you can start adding the commands inside the <b>clicommands</b> vector. At the <b>action</b> field you must add the value that will trigger the action. If it has options you will add them later using the <b>OptionsFile</b> struct.

It's also important to add the correct value at the <b>optionsid</b> field: it should be your attack name with "01" at the end. If you are using a command that has alreade been used and has its OptionFile developed you can add the corresponding id.

Once you have finished your AttackSchema it's time to create an <b>OptionsFile</b> in the case its needed. Lets assume that you are implementing an attack that uses a tool that require some options. Since you will only have one command that requires options, you will only have to develop one <b>OptionsFile</b>. Before creating the <b>OptionsFile</b> you should study the manual of that tool/command and identify the flags that are necessary and the ones that are not. Then, you can create a new file and copy the basic structure of an <b>OptionsFile</b>. The <b>optionsid</b> should be the same that you added in the corresponding comand at the <b>AttackSchema</b> structure. Once you start adding values into the options struct, is important to have this things in mind:

<ul>
<li> The options that are required for the correct execution of the attack should go first with the string <b>Required</b> at the <b>maingroup</b> field.
<li> The most used options should go after the <b>Required</b> options
<li> If there is a option that allows to export the result using another format like XML or JSON to another file, it has to be the last group, the <b>maingroup</b> name must be <b>Output</b> and one of them (if multiple) has to be activated by default.
<li> You are responsible of creating the other options group
<li> If two options can't be active at the same time because the command won't work, you have to use the <b>subgroup</b> field and put the same value in both of them
<li> All the options that require some input of the user must have a <b>regex</b> expression encoded in base64 that validates that the input is valid.
<ul>
Remember to add the flag that the option needs and put <b>true</b> at the corresponding field if you want to have user input and checkboxes.

The <b>AttackSchema</b> should be stored inside the database microservice, the path is: /src/database/storage-persistance/general/attack-schemas and the <b>OptionsFile</b> go to /src/database/storage-persistance/general/attack-options.

You also have to add this inside the /src/database/attacks.json file.

```
{
  "id":"1",
  "name": #your-attack-name
}
```

This would be all in the case none of the commands of the new attack has an <b>Output</b> group in its <b>OptionsFile</b>, however, lets assume that your command can extract the results in a JSON file.

If the results can be represented in JSON, the <b>ResultsFile</b> will have all the data inside the <b>resultsjson</b> output. Since we want to represent a the data in a nice table to make it more readable, you will need to add a <b>Processor</b> function inside the attack-manager microservice package (src/attack-manager/AttackAbstraction/results_processor/). You must create a new file (pleas following the name convention) and in this file you only have to create the processor. The purpose of this processor function is to fill the <b>table</b> field in a way that is representative and easy to read. You will have to deal with golang interfaces and maps. I suggest to take a look at the other processors and understand how they work. You will need to identify the important fields in the JSON output that you want to include and reach them using a map, so it will be necessary to "hardcode" the field names.

Once the processor function has been programmed, you must modify the formatresults.go file (inside src/attack-manager/AttackAbstraction/) and add an <b>if</b> condition that triggers your new processor. This <b>if</b> condition should check one of the first unique field names inside the <b>resultsjson</b> field of the <b>ResultsFile</b> that han help to identify what type of results we are dealing with and call the propper <b>processor</b>.

<p align="right">(<a href="#top">back to top</a>)</p>

[product-screenshot]: images/screenshot.png
