#!/bin/bash

DIRECTORIES=("database-manager" "attack-manager" "instantiation-manager" "workflow-manager")
HD_DIRS=("database" "attack-manager" "instantiation" "workflow")
length=${#DIRECTORIES[@]}

for (( i = 0; i < length; i++ )); do
	echo Creating "${DIRECTORIES[$i]}" directory and files
	DIRECTORY_SERVICE=tmp/"${DIRECTORIES[$i]}"

    mkdir -p $DIRECTORY_SERVICE/var/
    mkdir -p $DIRECTORY_SERVICE/etc/
    mkdir -p $DIRECTORY_SERVICE/etc/huntdown
    mkdir -p $DIRECTORY_SERVICE/usr/local/

    cp -ra /var/huntdown/ $DIRECTORY_SERVICE/var/
    if [ $? -ne 0 ]; then
        echo "Error while copying /var/huntdown"
        exit 1
    fi

    cp -ra /etc/huntdown/ $DIRECTORY_SERVICE/etc/
    cp -ra config/environment.json.in $DIRECTORY_SERVICE/etc/huntdown/
    if [ $? -ne 0 ]; then
        echo "Error while copying /etc/huntdown"
        exit 1
    fi

    cp -ra /usr/local/huntdown/ $DIRECTORY_SERVICE/usr/local/
    if [ $? -ne 0 ]; then
        echo "Error while copying /usr/local/huntdown"
        exit 1
    fi

    DOCKERFILE=src/"${HD_DIRS[$i]}"/Dockerfile
    if [ -f "$DOCKERFILE" ]; then
        cp $DOCKERFILE $DIRECTORY_SERVICE/
        if [ $? -ne 0 ]; then
            echo "Error while copying Dockerfile to $DOCKERFILE"
            exit 1
        fi
    else
        echo "$DOCKERFILE does not exist."
        pwd
        ls -lstr
        exit 1
    fi
done