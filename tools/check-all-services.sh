#!/bin/bash

#OLD VERSION might not work anymore (November 2023)

PULSAR_SERVICE="pulsar-standalone"
HUNTDOWN_SERVICES="huntdown-database huntdown-instantiation huntdown-attack-manager"
PM2_BINARY="/usr/local/bin/pm2"
UI_SERVICES="Frontend Backend"

ENVIRONMENT_FILE="/etc/huntdown/environment.json"

function get_huntdown_user {
    if [ ! -z $HUNTDOWN_USER ] ; then
        return
    fi
    if [ -f "$ENVIRONMENT_FILE" ] ; then
        HUNTDOWN_USER=$(sudo cat "$ENVIRONMENT_FILE" | jq .huntdown_user | sed s/\"//g)
    else
        HUNTDOWN_USER="huntdown"
    fi
}

function print_code {
    echo -n "$1"
    shift
    echo -e " \033[93m$*\033[0m"
}

function print_ok {
    echo -n "$1"
    shift
    echo -e " \033[32m$*\033[0m"
}

function print_nok {
    echo -n "$1"
    shift
    echo -e " \033[31m$*\033[0m"
}

function reinstall_message {
    echo If this doesn\'t work you can reinstall Huntdown
}

function check_enabled_service {
    service="$1"
    status=$(systemctl is-enabled "$service" 2> /dev/null)
    if [ "$status" = "enabled" ] ; then
        print_ok "$service" Enabled
        return 0
    else
        print_nok "$service" Not enabled
        echo You can fix this running:
        print_code sudo systemctl enable "$service"
        return 1
    fi
}

#Database is in the same machine (for now)
function check_database_service {
    service="$1"
    healthcheck=$(sudo docker exec $service bash -c "mongosh --eval 'printjson(db.serverStatus())' 2>&1")

    if echo "$healthcheck" | grep -q 'ok: 1'; then
        print_ok "$service" is running
        return 0
    else
        print_nok "$service" Not enabled
        echo You can fix this running:
        print_code docker start "$service"
        return 1
    fi
}

function check_active_service {
    service="$1"
    status=$(systemctl is-active "$service")
    if [ "$status" = "active" ] ; then
        print_ok "$service" Active
        return 0
    else
        print_nok "$service" Failed
        echo You can fix this running:
        print_code sudo systemctl start "$service"
        return 1
    fi
}

as_huntdown() {
    get_huntdown_user
    sudo -u $HUNTDOWN_USER "$@"
}

function check_ui {
    get_huntdown_user

    if [ -f $PM2_BINARY ] ; then
        print_ok UI Manager Installed
        for service in $UI_SERVICES ; do
            PID=$(as_huntdown $PM2_BINARY pid $service)
            if [ $? -eq 0 ] ; then
                print_ok $service Running
            else
                print_nok $service Not Running
                echo You can fix this running:
                print_code sudo -u $HUNTDOWN_USER $PM2_BINARY start "$service"
                echo If this doesn\'t work you can reinstall Huntdown
            fi
        done
        return 0
    else
        print_nok UI Manager Not installed, not validating
        return 1
    fi
}

if [[ ! -z "$1" && $1 == "-d" ]] ; then
    if [ "$1" = "docker" ] ; then
        DOCKER_INSTALL=1
    fi
fi

check_enabled_service $PULSAR_SERVICE
if [ $? -eq 1 ] ; then
    print_nok "$PULSAR_SERVICE" may not be running
fi
check_active_service $PULSAR_SERVICE

if [ ! -z $DOCKER_INSTALL] ; then
    check_database_service huntdown-db
    if [ $? -eq 1 ] ; then
        echo If this does not work you need to reinstall your database
    fi
fi

for service in $HUNTDOWN_SERVICES ; do
    check_enabled_service "$service"
    if [ $? -eq 1 ] ; then
        reinstall_message
    else
        check_active_service "$service"
    fi
done

check_ui