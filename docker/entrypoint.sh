#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source $DIR/environment.sh

if [ -z $1 ]; then
    echo "Usage: $0 module"
    exit 1
fi

MODULE=$1

ENV_JSON="/etc/huntdown/environment.json.in"

generate_environment_file

/usr/local/huntdown/bin/$MODULE