echo "Configuring Pulsar"

sed "s~%APACHE_PULSAR_PREFIX%~${APACHE_PULSAR_PREFIX}~" < "systemd/$PULSAR_UNIT_NAME.in" | \
    tee "/lib/systemd/system/$PULSAR_UNIT_NAME" > /dev/null

sed "s~%APACHE_PULSAR_PREFIX%~${APACHE_PULSAR_PREFIX}~" < "docker/pulsar-standalone" | \
    tee "/etc/init.d/pulsar-standalone" > /dev/null

chmod 755 "/etc/init.d/pulsar-standalone"

mkdir "/var/run/pulsar"

if ! getent passwd "$PULSAR_USER" > /dev/null 2>&1 ; then
    useradd -r -s /bin/false "$PULSAR_USER"
fi

if [ ! -d "$PULSAR_LOG_DIR" ]; then
    mkdir "$PULSAR_LOG_DIR"
    chown $PULSAR_USER:$PULSAR_GROUP "$PULSAR_LOG_DIR"
fi

sed "s~%PULSAR_LOG_DIR%~${PULSAR_LOG_DIR}~" < config/pulsar/log4j2.yaml.in | \
    tee "$APACHE_PULSAR_PREFIX/conf/log4j2.yaml" > /dev/null

#echo Starting Pulsar...
#service pulsar-standalone start

#until [[ $($APACHE_PULSAR_PREFIX/bin/pulsar-admin broker-stats destinations 2> /dev/null) ]]; do
#    sleep 1
#done

echo "Pulsar running successfully!"
