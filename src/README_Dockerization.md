//Makefile p.ej. src/database/Makefile

### Prerequisites

To create docker images of the three main services of HuntDown it is necessary to follow these steps:

1. Check the following systemd files:
    - src/attack-manager/systemd/huntdown-attack-manager.service.in
    - src/database/systemd/huntdown-database.service.in
    - src/instantiation/systemd/huntdown-instantiation.service.in
    - src/systemd/pulsar-standalone.service.in

    Make sure to have uncommented the ExecStart for executing each service through its <b> binary </b>. And check that the pulsar-service is using the correct HD repo path.

2. Install huntdown through the `setup.sh` script. Please follow the instructions you can find in the HuntDown/README.md file.

3. Once you have HD running in your local machine, you will be ready to create its Docker images. Now, create a folder for each service (database_manager, instantiation_manager and attack_manager) outside the HD Git repository.

    ```
    mkdir -p $PATH_TO_DIRECTORY_SERVICE/var
    mkdir -p $PATH_TO_DIRECTORY_SERVICE/etc
    mkdir -p $PATH_TO_DIRECTORY_SERVICE/usr/local
    ```

4. Then, inside of each folder execute the following:


    ```
    sudo cp -r /var/huntdown/ $PATH_TO_DIRECTORY_SERVICE/var/

    sudo cp -r /etc/huntdown/ $PATH_TO_DIRECTORY_SERVICE/etc/

    sudo cp -r /usr/local/huntdown/ $PATH_TO_DIRECTORY_SERVICE/usr/local/

    cd $PATH_TO_DIRECTORY_SERVICE

    sudo chown -R $USER:$USER var/

    sudo chown -R $USER:$USER etc/

    sudo chown -R $USER:$USER usr/

    #Look for the Dockerfiles inside each service folder of HuntDown repository (src/database, src/instantiation and src/attack-manager)
    cp $PATH_TO_HD_REPO/src/.../Dockerfile $PATH_TO_DIRECTORY_SERVICE/
    ```

## Configure AWS CLI
    ```
    sudo apt install awscli
    aws configure

    # add aws_session_key
    nano .aws/credentials

    # test if aws is correctly configured
    aws ecr describe-repositories
    ```


## Build and Push Docker images

Then, execute the commands below for each service (hd-database-manager, hd-instantiation-manager and hd-attack-manager).

    ```
    aws ecr get-login-password --region us-east-1 | sudo docker login --username AWS --password-stdin <AWS_ECR_URL>

    cd $PATH_TO_DIRECTORY_SERVICE/

    sudo docker build -t hd-database-manager .

    sudo docker tag hd-database-manager:latest <AWS_ECR_URL>/hd-database-manager:latest

    sudo docker push <AWS_ECR_URL>/hd-database-manager:latest
    ```

Once done, your images will be stored in <b> AWS ECR </b>.
