package Rpc

import (
	"bytes"
	"context"
	"crypto/tls"
	"errors"
	"fmt"
	"net/http"
	"reflect"
	"time"

	"github.com/vmihailenco/msgpack/v5"
)

type RPC struct {
	http  *http.Client
	url   string
	token string
}
type ClientOpt struct {
	Timeout         time.Duration
	TLSClientConfig *tls.Config
	Token           string
	SSL             bool
	APIVersion      string
}
type Client struct {
	username   string
	password   string
	apiVersion string
	rpc        *RPC
}

func newHTTPClient(options ClientOpt) (*http.Client, error) {
	transport := http.DefaultTransport.(*http.Transport).Clone()
	transport.TLSClientConfig = options.TLSClientConfig

	return &http.Client{
		Timeout:   options.Timeout,
		Transport: transport,
	}, nil
}

func NewRPC(address string, optFns ...func(o *ClientOpt)) *Client {

	options := ClientOpt{
		Token:           "",
		SSL:             true,
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true}, //nolint:gosec //unknown ca
		APIVersion:      "1.0",
	}
	for _, fn := range optFns {
		fn(&options)
	}
	http, err := newHTTPClient(options)
	if err != nil {
		return nil
	}
	protocol := "https"
	url := fmt.Sprintf("%s://%s/api/%s", protocol, address, options.APIVersion)

	rpc := &RPC{
		http: http,
		url:  url,
	}
	c := &Client{
		apiVersion: options.APIVersion,
		rpc:        rpc,
	}
	return c
}

func (r *RPC) Call(req, res interface{}) error {

	var method string
	stype := reflect.ValueOf(req).Elem()
	field := stype.FieldByName("Method")

	if field.IsValid() {
		method = field.String()
	} else {
		method = ""
	}
	if method != "auth.login" {
		if r.token == "" {
			return errors.New("client not authenticated")
		}
	}

	buf := new(bytes.Buffer)
	enc := msgpack.NewEncoder(buf)
	enc.UseArrayEncodedStructs(true)

	if err := enc.Encode(req); err != nil {
		return err
	}

	request, err := http.BuildMessageWithContext(context.Background(), "POST", r.url, buf)
	if err != nil {
		return err
	}

	request.Header.Add("Content-Type", "binary/message-pack")

	response, err := r.http.Do(request)
	if err != nil {
		return err
	}
	/*b, err := httputil.DumpResponse(response, true) //testing
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println(string(b))*/
	defer response.Body.Close()

	if res != nil {
		if err := msgpack.NewDecoder(response.Body).Decode(res); err != nil {
			return err
		}
	}

	return nil
}

type AuthLoginReq struct {
	Method   string
	Username string // The username
	Password string // The password
}

type AuthLoginRes struct {
	Result string `msgpack:"result"`
	Token  string `msgpack:"token"`
}

func Login(user, pass string, c *Client) error {

	req := &AuthLoginReq{
		Method:   "auth.login",
		Username: user,
		Password: pass,
	}

	var res *AuthLoginRes
	if err := c.rpc.Call(req, &res); err != nil {
		return err
	}
	if res.Result == "failure" || res.Token == "" {
		return errors.New("Authentication failed")
	}

	c.username = user
	c.password = pass

	c.rpc.token = res.Token
	fmt.Println("Client logged in")

	return nil
}

type AuthLogoutReq struct {
	Method      string
	Token       string
	LogoutToken string
}

type AuthLogoutRes struct {
	Result string `msgpack:"result"`
}

func Logout(c *Client) error {

	req := &AuthLogoutReq{
		Method:      "auth.logout",
		Token:       c.rpc.token,
		LogoutToken: c.rpc.token,
	}

	var res *AuthLogoutRes
	if err := c.rpc.Call(req, &res); err != nil {
		return err
	}

	c.username = ""
	c.password = ""
	c.rpc.token = ""

	fmt.Println("Client logged out")
	return nil
}

type CoreVersionReq struct {
	Method string
	Token  string
}

type CoreVersionRes struct {
	Version string `msgpack:"version"` // Framework version
	Ruby    string `msgpack:"ruby"`    // Ruby version
	API     string `msgpack:"api"`     // API version
}

func APIVersion(c *Client) (*CoreVersionRes, error) {
	req := &CoreVersionReq{
		Method: "core.version",
		Token:  c.rpc.token,
	}

	var res *CoreVersionRes
	if err := c.rpc.Call(req, &res); err != nil {
		return nil, err
	}
	return res, nil

}

type ModuleInfoReq struct {
	Method     string
	Token      string
	ModuleType string
	ModuleName string
}

type ModuleInfoRes struct {
	Name        string     `msgpack:"name"`
	Description string     `msgpack:"description"`
	License     string     `msgpack:"license"`
	FilePath    string     `msgpack:"filepath"`
	Version     string     `msgpack:"version"`
	Rank        string     `msgpack:"rank"`
	References  [][]string `msgpack:"references"`
	Authors     []string   `msgpack:"authors"`
}

func ModuleInfo(c *Client, mType string, mName string) (*ModuleInfoRes, error) {

	req := &ModuleInfoReq{
		Method:     "module.info",
		Token:      c.rpc.token,
		ModuleType: mType,
		ModuleName: mName,
	}

	var res *ModuleInfoRes
	if err := c.rpc.Call(req, &res); err != nil {
		return nil, err
	}

	return res, nil
}

type ModuleSearchReq struct {
	Method     string
	Token      string
	ModuleName string
}

type ModuleSearchRes []struct {
	Type           string `msgpack:"type"`
	Name           string `msgpack:"name"`
	Fullname       string `msgpack:"fullname"`
	Rank           string `msgpack:"rank"`
	Disclosuredate string `msgpack:"disclosuredate"`
}

func ModuleSearch(c *Client, keyword string) (*ModuleSearchRes, error) {

	req := &ModuleSearchReq{
		Method:     "module.search",
		Token:      c.rpc.token,
		ModuleName: keyword,
	}

	var res *ModuleSearchRes
	if err := c.rpc.Call(req, &res); err != nil {
		return nil, err

	}
	return res, nil
}

type ModuleCompatiblePayloadsReq struct {
	Method     string
	Token      string
	ModuleName string
}

type ModuleCompatiblePayloadsRes struct {
	Payloads []string `msgpack:"payloads"`
}

// CompatiblePayloads returns the compatible payloads for a specific exploit
func CompatiblePayloads(c *Client, moduleName string) ([]string, error) {
	req := &ModuleCompatiblePayloadsReq{
		Method:     "module.compatible_payloads",
		Token:      c.rpc.token,
		ModuleName: moduleName,
	}

	var res *ModuleCompatiblePayloadsRes
	if err := c.rpc.Call(req, &res); err != nil {
		return nil, err
	}

	return res.Payloads, nil
}

type SessionListReq struct {
	Method string
	Token  string
}

type SessionListRes map[uint32]struct {
	Type        string `msgpack:"type"`
	TunnelLocal string `msgpack:"tunnel_local"`
	TunnelPeer  string `msgpack:"tunnel_peer"`
	ViaExploit  string `msgpack:"via_exploit"`
	ViaPayload  string `msgpack:"via_payload"`
	Description string `msgpack:"desc"`
	Info        string `msgpack:"info"`
	Workspace   string `msgpack:"workspace"`
	SessionHost string `msgpack:"session_host"`
	SessionPort int    `msgpack:"session_port"`
	Username    string `msgpack:"username"`
	UUID        string `msgpack:"uuid"`
	ExploitUUID string `msgpack:"exploit_uuid"`
}

func ListSessions(c *Client) (SessionListRes, error) {
	req := &SessionListReq{
		Method: "session.list",
		Token:  c.rpc.token,
	}

	var res SessionListRes
	if err := c.rpc.Call(req, &res); err != nil {
		return nil, err
	}

	return res, nil
}

type ModuleExecuteReq struct {
	Method     string
	Token      string
	ModuleType string
	ModuleName string
	Options    map[string]string
}

type ModuleExecuteRes struct {
	JobID int    `msgpack:"job_id"`
	UUID  string `msgpack:"uuid"`
}

func Exploit(c *Client /*moduleType, */, moduleName string, LHOST string, RHOST string, PAYLOAD string /*,  options map[string]string*/) (int, string, error) {

	options := map[string]string{
		"LHOST":   LHOST,
		"RHOST":   RHOST,
		"PAYLOAD": PAYLOAD,
	}
	req := &ModuleExecuteReq{
		Method:     "module.execute",
		Token:      c.rpc.token,
		ModuleType: "exploit",
		ModuleName: moduleName,
		Options:    options,
	}

	var res *ModuleExecuteRes
	if err := c.rpc.Call(req, &res); err != nil {
		return 0, "", err
	}

	return res.JobID, res.UUID, nil
}

type SessionShellWriteReq struct {
	Method    string
	Token     string
	SessionID string
	Command   string
}

type SessionShellWriteRes struct {
	WriteCount string `msgpack:"write_count"`
}
type SessionShellReadReq struct {
	Method    string
	Token     string
	SessionID string
	//ReadPointer uint32
}
type SessionShellReadRes struct {
	Seq  uint32 `msgpack:"seq"`
	Data string `msgpack:"data"`
}

func ShellWriteANDRead(c *Client, session string, command string) (string, error) {

	//write
	reqWrite := &SessionShellWriteReq{
		Method:    "session.shell_write",
		Token:     c.rpc.token,
		SessionID: session,
		Command:   command,
	}
	var resWrite SessionShellWriteRes
	if err := c.rpc.Call(reqWrite, &resWrite); err != nil {
		return "", err
	}
	//read
	reqRead := &SessionShellReadReq{
		Method:    "session.shell_read",
		Token:     c.rpc.token,
		SessionID: session,
		//	ReadPointer: readPointer,
	}

	var resRead SessionShellReadRes
	if err := c.rpc.Call(reqRead, &resRead); err != nil {
		return "", err
	}

	return resRead.Data, nil
}

type JobInfoReq struct {
	Method string
	Token  string
	JobID  string
}

type JobInfoRes struct {
	JobID     int                    `msgpack:"jid"`
	Name      string                 `msgpack:"name"`
	StartTime int                    `msgpack:"start_time"`
	URIPath   interface{}            `msgpack:"uripath,omitempty"`
	Datastore map[string]interface{} `msgpack:"datastore,omitempty"`
}

func CheckJob(c *Client, jobID string) (*JobInfoRes, error) {
	req := &JobInfoReq{
		Method: "job.info",
		Token:  c.rpc.token,
		JobID:  jobID,
	}

	var res *JobInfoRes
	if err := c.rpc.Call(req, &res); err != nil {
		return nil, err
	}
	return res, nil
}

type JobListReq struct {
	Method string
	Token  string
}

type JobListRes map[string]string

// List returns a list of jobs
func JobList(c *Client) (*JobListRes, error) {
	req := &JobListReq{
		Method: "job.list",
		Token:  c.rpc.token,
	}

	var res *JobListRes
	if err := c.rpc.Call(req, &res); err != nil {
		return nil, err
	}

	return res, nil
}
