package main

import (
	"bufio"
	"confManager"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	querymanager "hd-database/QueryManager"
	"helpers/GeneralHelper"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
	"syscall"

	dbw "gitlab.com/HuntDownUPC/go-modules/db/DBWrapper/v2"
	dbd "gitlab.com/HuntDownUPC/go-modules/db/common/v2"
	"golang.org/x/crypto/ssh/terminal"
)

func FillDBConnection(db_conn *dbd.DB_Connection) {
	db_conn.Host = GeneralHelper.ConfigEnvironment.DatabaseHost
	db_conn.Username = GeneralHelper.ConfigEnvironment.DatabaseUsername
	db_conn.Password = GeneralHelper.ConfigEnvironment.DatabasePassword
	db_conn.Port = GeneralHelper.ConfigEnvironment.DatabasePort
}

func json_get_key_array(json_byte []byte, key string) (any, error) {
	var json_map []map[string]interface{}
	var value any
	var exists bool
	err := json.Unmarshal(json_byte, &json_map)
	if err != nil {
		log.Println("Invalid JSON File: ", err.Error())
		return nil, err
	}
	for i := range json_map {
		log.Println(i)
		value, exists = json_map[i][key]
		break
	}
	if !exists {
		log.Println("Not existing key: ", key)
		return nil, errors.New("key not found")
	}
	return value, nil
}

func json_get_key(json_byte []byte, key string) (any, error) {
	json_map := make(map[string]any)
	err := json.Unmarshal(json_byte, &json_map)
	if err != nil {
		log.Println("Invalid JSON File: ", err.Error())
		return nil, err
	}
	value, exists := json_map[key]
	if !exists {
		log.Println("Not existing key: ", key)
		return nil, errors.New("key not found")
	}
	return value, nil
}

func exists_json_key_array(json_byte []byte, key string) (bool, error) {
	var json_map []map[string]interface{}
	err := json.Unmarshal(json_byte, &json_map)
	if err != nil {
		log.Println("Invalid JSON File: ", err.Error())
		return false, err
	}
	if len(json_map) == 0 {
		return false, errors.New("options must be an array")
	}
	if _, exists := json_map[0][key]; !exists {
		log.Println("Not existing key: ", key)
		return false, errors.New("key not found")
	}
	return true, nil
}

func exists_json_key(json_byte []byte, key string) (bool, error) {
	json_map := make(map[string]any)
	err := json.Unmarshal(json_byte, &json_map)
	if err != nil {
		log.Println("Invalid JSON File: ", err.Error())
		return false, err
	}
	if _, exists := json_map[key]; !exists {
		log.Println("Not existing key: ", key)
		return false, errors.New("key not found")
	}
	return true, nil
}

func FindClassification(file_name string, attack_name string) map[string]interface{} {
	file, err := os.ReadFile(file_name)
	if err != nil {
		log.Println("Error reading JSON file:", err)
	}

	var jsonData []map[string]interface{}

	if err := json.Unmarshal(file, &jsonData); err != nil {
		log.Println("Error unmarshalling JSON:", err)
		return nil
	}

	jsonClassification := make(map[string]interface{})

	for _, data := range jsonData {
		for key, value := range data {
			if strings.Contains(key, "classification") {
				switch v := value.(type) {
				case []interface{}:
					classificationjson := make(map[string]interface{})

					for _, elem := range v {
						attackClassification := make(map[string]interface{})
						if reconMap, ok := elem.(map[string]interface{}); ok {
							for innerKey, innerValue := range reconMap {
								if reconnaissanceMaps, ok := innerValue.([]interface{}); ok {
									for _, elem := range reconnaissanceMaps {
										if reconMap, ok := elem.(map[string]interface{}); ok {
											for innerKey2, innerValue2 := range reconMap {
												if innerKey2 == "name" {
													attackClassification["category"] = innerKey
													attackClassification["name"] = attack_name
													classificationjson[innerValue2.(string)] = attackClassification

												}
											}
										}
									}
								} else {
									log.Println("Value is not a slice")
								}
								if innerKey == "name" {
									attackClassification["name"] = attack_name
									classificationjson[innerValue.(string)] = attackClassification
								}
							}
						}
					}
					jsonClassification["classId"] = attack_name
					jsonClassification["classification"] = classificationjson

				case map[string]interface{}:
					for innerKey, innerValue := range v {
						log.Println("Inner Key:", innerKey, "Inner Value:", innerValue)
					}
				default:
					log.Println("Unsupported type for reconnaissance value")
				}
			}
		}
	}
	log.Println(jsonClassification)
	return jsonClassification
}

func updateDB(mapData map[string]interface{}, client dbd.Db_Context) {
	client.Database = "general"
	client.Collection = "Classification"

	jsonData, err := json.Marshal(mapData)

	if err != nil {
		log.Println("Error converting to byte")
	}

	if insertResult, err := dbw.Insert_document(&client, jsonData); err == nil {
		log.Println("attack description inserted to general db")
		log.Println("Inserted document ID:", insertResult)
	} else {
		log.Println("Failed to insert attack description to general db")
		log.Println(err)
	}
}

func main() {
	var step_num int = 13
	var config_file *string
	var mongo_host *string
	var mongo_port *int
	var attack_schema *string
	var options_schema *string
	var dashboard_schema *string
	var mongo_user *string
	var mongo_pass *string

	config_file = flag.String("config", "", "Select configuration file")
	mongo_host = flag.String("mongo-host", "", "MongoDB host: e.g. 192.168.122.168")
	mongo_user = flag.String("mongo-user", "", "User for MongoDB")
	mongo_pass = flag.String("mongo-pass", "", "Password for MongoDB")
	mongo_port = flag.Int("mongo-port", 27017, "Mongo port: e.g. 27017")
	attack_schema = flag.String("attack-schema", "", "JSON File with the attack Schema")
	options_schema = flag.String("options-schema", "", "JSON File with the options Schema")
	dashboard_schema = flag.String("dashboard-schema", "", "JSON File with the dashboard Schema")
	flag.Parse()

	if *config_file == "" {
		GeneralHelper.LoadConfig()
	} else if _, err := os.Stat(*config_file); err != nil {
		log.Println("Error: file ", *config_file, " not found")
		confManager.LoadConfigFromFile(*config_file)
	}

	if *mongo_user == "" && GeneralHelper.ConfigEnvironment.DatabaseUsername == "" {
		reader := bufio.NewReader(os.Stdin)
		fmt.Printf("Enter Mongo Username: ")
		*mongo_user, _ = reader.ReadString('\n')
	}
	if *mongo_user != "" {
		GeneralHelper.ConfigEnvironment.DatabaseUsername = *mongo_user
	}

	if *mongo_pass == "" && GeneralHelper.ConfigEnvironment.DatabasePassword == "" {
		log.Printf("Enter MongoDB password: ")
		bytePassword, err := terminal.ReadPassword(int(syscall.Stdin))
		if err != nil {
			fmt.Println("Error reading password:", err)
			return
		}
		*mongo_pass = string(bytePassword)
	}
	if *mongo_pass != "" {
		GeneralHelper.ConfigEnvironment.DatabasePassword = *mongo_pass
	}

	if *attack_schema == "" {
		log.Println("Please specify -attack-schema file")
		os.Exit(-10)
	}

	if *options_schema == "" {
		log.Println("Please specify -options-schema file")
		os.Exit(-10)
	}

	if *dashboard_schema == "" {
		log.Println("Please specify -dashboard-schema file")
		os.Exit(-10)
	}

	if *mongo_host == "" && GeneralHelper.ConfigEnvironment.DatabaseHost == "" {
		GeneralHelper.ConfigEnvironment.DatabaseHost = "localhost"
	} else if *mongo_host != "" {
		GeneralHelper.ConfigEnvironment.DatabaseHost = *mongo_host
	}

	if GeneralHelper.ConfigEnvironment.DatabasePort == "" {
		GeneralHelper.ConfigEnvironment.DatabasePort = strconv.Itoa(*mongo_port)
	}

	var db_conn dbd.DB_Connection
	FillDBConnection(&db_conn)

	log.Println("Connecting to Mongo database in ", GeneralHelper.ConfigEnvironment.DatabaseHost)
	session_id, err := dbw.Connect(db_conn)
	if err != nil {
		log.Println("Error connecting to database: ", err.Error())
		os.Exit(-10)
	}

	client, err := dbw.GetDBContext(session_id)
	if err != nil {
		log.Println("Error getting database context: ", err.Error())
		os.Exit(-10)
	}

	if _, err := os.Stat(*options_schema); err != nil {
		log.Println("Error: file ", options_schema, " not found")
		os.Exit(-1)
	}

	client.Database = "general"
	client.Collection = "OptionsFile"
	dbw.Change_index(&client, "optionsid")

	byteValue, err := ioutil.ReadFile(*options_schema)
	if err != nil {
		log.Println("Error: file ", options_schema, " not found")
		os.Exit(-1)
	}

	_, err = exists_json_key_array(byteValue, "optionsid")
	if err != nil {
		os.Exit(-2)
	}

	var key_exists bool
	var any_value any
	any_value, err = json_get_key_array(byteValue, "optionsid")
	if err != nil {
		log.Println("Error in JSON optionsid not found")
		os.Exit(-2)
	}
	value := any_value.(string)
	key_exists, _ = querymanager.RecordExists(client, "optionsid", value)
	if key_exists {
		log.Println("Deleting previous options record", value)
		var filters []dbd.Filters
		querymanager.ConstructBasicFilter("optionsid", value, "$eq", &filters)
		dbw.Delete_document(&client, &filters)
	}

	if _, err := dbw.Insert_documents(&client, byteValue); err == nil {
		log.Println("Options ", value, " correctly inserted to general db")
	} else {
		log.Println("Failed to insert options ", value, " to general db")
		log.Println(err)
		os.Exit(-3)
	}

	client.Collection = "AttackSchema"
	dbw.Change_index(&client, "attackid")

	byteValue, err = ioutil.ReadFile(*attack_schema)
	if err != nil {
		log.Println("Error: file ", attack_schema, " not found")
		os.Exit(-1)
	}
	_, err = exists_json_key_array(byteValue, "attackid")
	if err != nil {
		os.Exit(-2)
	}
	any_value, err = json_get_key_array(byteValue, "attackid")
	if err != nil {
		os.Exit(-4)
	}
	value = any_value.(string)
	key_exists, _ = querymanager.RecordExists(client, "attackid", value)
	if key_exists {
		log.Println("Deleting previous attack record")
		var filters []dbd.Filters
		querymanager.ConstructBasicFilter("attackid", value, "$eq", &filters)
		dbw.Delete_document(&client, &filters)
	}
	if _, err := dbw.Insert_documents(&client, byteValue); err == nil {
		log.Println("Attack ", value, " correctly inserted to general db")
	} else {
		log.Println("Failed to insert attack ", value, " to general db")
		log.Println(err)
		os.Exit(-3)
	}

	client.Collection = "DashboardSchema"
	dbw.Change_index(&client, "attacknamedashboard")
	byteValue, err = ioutil.ReadFile(*dashboard_schema)
	if err != nil {
		log.Println("Error: file ", dashboard_schema, " not found")
		os.Exit(-1)
	}
	_, err = exists_json_key_array(byteValue, "attacknamedashboard")
	if err != nil {
		os.Exit(-2)
	}
	any_value, err = json_get_key_array(byteValue, "attacknamedashboard")
	if err != nil {
		log.Println("Error in JSON attacknamedashboard not found")
		os.Exit(-4)
	}
	attack_name := any_value.(string)
	key_exists, _ = querymanager.RecordExists(client, "attacknamedashboard", attack_name)
	if key_exists {
		log.Println("Deleting previous attack record")
		var filters []dbd.Filters
		querymanager.ConstructBasicFilter("attacknamedashboard", attack_name, "$eq", &filters)
		dbw.Delete_document(&client, &filters)
	}
	if _, err := dbw.Insert_documents(&client, byteValue); err == nil {
		log.Println("Attack ", attack_name, " correctly inserted to DashboardSchema")
	} else {
		log.Println("Failed to insert attack ", attack_name, " to DashboardSchema")
		log.Println(err)
		os.Exit(-3)
	}

	client.Collection = "attacks"
	key_exists, _ = querymanager.RecordExists(client, "name", attack_name)
	if key_exists {
		log.Println("Deleting previous attack description")
		var filters []dbd.Filters
		querymanager.ConstructBasicFilter("name", attack_name, "$eq", &filters)
		dbw.Delete_document(&client, &filters)
	}
	attack_desc := map[string]any{"id": "1", "name": attack_name, "step_num": step_num}
	byteValue, err = json.Marshal(attack_desc)
	if err != nil {
		log.Println("Error parsing JSON ", attack_desc)
		os.Exit(-1)
	}

	if _, err := dbw.Insert_document(&client, byteValue); err == nil {
		log.Println("attack description inserted to general db")
	} else {
		log.Println("Failed to insert attack description to general db")
		log.Println(err)
	}

	mapClassification := FindClassification("../../attacks/"+attack_name+"/"+attack_name+"-classification.json", attack_name)
	updateDB(mapClassification, client)

	dbw.Disconnect(session_id)
}
