package main

import (
	"encoding/json"
	querymanager "hd-database/QueryManager"
	GeneralHelper "helpers/GeneralHelper"
	"strconv"

	log "github.com/sirupsen/logrus"
	dbw "gitlab.com/HuntDownUPC/go-modules/db/DBWrapper/v2"
	dbd "gitlab.com/HuntDownUPC/go-modules/db/common/v2"
)

func monitor(value []byte) {
	received := make(map[string]any)
	json.Unmarshal(value, &received)
	var filter_list []dbd.Filters
	var session_id dbd.Db_id
	var tmp_id int
	var err error
	log.Println("Monitoring Updating step")

	tmp_id, err = GeneralHelper.GetSession_id(received)
	if err != nil {
		log.Println("Monitoring update error " + err.Error())
		return
	}
	session_id = dbd.Db_id(tmp_id)

	var step string
	err = GeneralHelper.GetMapElement(received, "step", &step)
	if err != nil {
		log.Println("Monitoring update error " + err.Error())
		return
	}
	var step_num float64
	err = GeneralHelper.GetMapElement(received, "step_num", &step_num)
	if err != nil {
		log.Println("Monitoring update error " + err.Error())
		return
	}

	var task_id string
	err = GeneralHelper.GetMapElement(received, "task_id", &task_id)
	if err != nil {
		log.Println("Monitoring update error " + err.Error())
		return
	}

	var info dbd.Db_Context
	info, err = dbw.GetDBContext(session_id)
	if err != nil {
		log.Println("Step Update error " + err.Error() + " on " + task_id)
		return
	}
	querymanager.ConstructBasicFilter("_id", task_id, "$eq", &filter_list)
	err = dbw.Update_documents(&info, &filter_list, "step", step)
	if err != nil {
		log.Println("Error updating monitoring step " + step + " " + info.Database + " " + task_id)
	}
	err = dbw.Update_documents(&info, &filter_list, "step_num", strconv.Itoa(int(step_num)))
	if err != nil {
		log.Println("Step Update error " + err.Error() + " on " + task_id)
	}
}
