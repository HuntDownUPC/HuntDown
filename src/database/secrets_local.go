package main

import (
	"encoding/json"
	DatabaseHelper "helpers/DatabaseHelper"
	GeneralHelper "helpers/GeneralHelper"

	log "github.com/sirupsen/logrus"
	dbw "gitlab.com/HuntDownUPC/go-modules/db/DBWrapper/v2"
	dbd "gitlab.com/HuntDownUPC/go-modules/db/common/v2"
)

func addLocalSecret(session_id int, secret Secret) ([]string, GeneralHelper.ErrMessage) {
	ctx, err := dbw.GetDBContext(dbd.Db_id(session_id))
	if err != nil {
		var err_msg GeneralHelper.ErrMessage
		err_msg.Success = false
		err_msg.Error = err.Error()
		err_msg.ErrorCode = GeneralHelper.NoDBConnection
		log.Error("Error in Database Context")
		return []string{}, err_msg
	}
	var to_insert []map[string]interface{}
	to_insert = append(to_insert, GeneralHelper.StructToMap(secret))
	ids, err_msg := insertLocalElements(ctx, to_insert)
	if !err_msg.Success {
		log.Error("Error in Insertion")
		return []string{}, err_msg
	}
	return ids, err_msg
}

func getLocalSecret(session_id int, secret_key string) ([]map[string]interface{}, GeneralHelper.ErrMessage) {
	ctx, err := dbw.GetDBContext(dbd.Db_id(session_id))
	if err != nil {
		log.Error("Error in Database Context")
		var err_msg GeneralHelper.ErrMessage
		err_msg.Success = false
		err_msg.Error = err.Error()
		err_msg.ErrorCode = GeneralHelper.NoDBConnection
		return []map[string]interface{}{}, err_msg
	}
	var filters []DatabaseHelper.Filter
	filter := DatabaseHelper.Filter{Field: "secret_key", Op: "$eq", Value: secret_key}
	filters = append(filters, filter)

	list, err_msg := localQuery(ctx, filters)
	if !err_msg.Success {
		log.Error("Error in Query")
		return []map[string]interface{}{}, err_msg
	}
	var list_map []map[string]interface{}
	err = json.Unmarshal(list, &list_map)
	if err != nil {
		log.Error("Error in response format")
		var err_msg GeneralHelper.ErrMessage
		err_msg.Success = false
		err_msg.Error = err.Error()
		err_msg.ErrorCode = GeneralHelper.InvalidFormat
		return []map[string]interface{}{}, err_msg
	}

	if len(list_map) == 0 {
		var err_msg GeneralHelper.ErrMessage
		err_msg.Success = false
		err_msg.Error = "Secret not found"
		err_msg.ErrorCode = GeneralHelper.EmptyAnswer
		return []map[string]interface{}{}, err_msg
	}
	return list_map, err_msg
}

func deleteLocalSecret(session_id int, secret_key string) (bool, GeneralHelper.ErrMessage) {
	ctx, err := dbw.GetDBContext(dbd.Db_id(session_id))
	if err != nil {
		log.Error("Error in Database Context")
		var err_msg GeneralHelper.ErrMessage
		err_msg.Success = false
		err_msg.Error = err.Error()
		err_msg.ErrorCode = GeneralHelper.NoDBConnection
		return false, err_msg
	}
	var filters []DatabaseHelper.Filter
	filter := DatabaseHelper.Filter{Field: "secret_key", Op: "$eq", Value: secret_key}
	filters = append(filters, filter)

	success, err_msg := localDeleteOne(ctx, filters)
	if !success {
		log.Error(err_msg.Error)
		return false, err_msg
	}
	return true, err_msg
}
