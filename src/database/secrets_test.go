package main

import (
	"confManager"
	"encoding/hex"
	"encoding/json"
	"flag"
	"hd-database/libGenKey"
	"helpers/GeneralHelper"
	"os"
	"strconv"
	"testing"

	log "github.com/sirupsen/logrus"
	PulsarLib "gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
	"gitlab.com/HuntDownUPC/go-modules/db/common/v2"
)

func TestLocalSecretsHandling(t *testing.T) {
	var creds *string
	var mongo_host *string
	var mongo_port *string

	mongo_host = flag.String("mongo", "cybersecurity.pc.ac.upc.edu", "MongoDB host: e.g. 192.168.122.168")
	mongo_port = flag.String("mongo-port", "27017", "MongoDB port: e.g. 27017")

	creds = flag.String("credentials", "../../config/credentials.json", "JSON file with the credentials")

	flag.Parse()

	log.Print("Logging in")
	cred_raw, err := os.ReadFile(*creds)
	if err != nil {
		log.Error("Error reading credentials file")
		t.Error("Error reading credentials file")
		return
	}
	var cred GeneralHelper.Credentials
	json.Unmarshal(cred_raw, &cred)
	h_pass := libGenKey.GenKey(cred.Password)
	var db_conn common.DB_Connection
	db_conn.Host = *mongo_host
	db_conn.Port = *mongo_port
	db_conn.Username = cred.Username
	db_conn.Password = hex.EncodeToString(h_pass)

	client_context, err_msg := LoginLocal(db_conn)
	if !err_msg.Success {
		log.Println("Error logging in: ", err)
		t.Error("Error logging in")
		return
	}
	session_id := int(client_context.Db_id)

	dummy_secret := Secret{SecretKey: "TestSecret", Description: "TestDescription", Value: "TestValue"}

	ids, err_msg := addLocalSecret(session_id, dummy_secret)

	if !err_msg.Success {
		log.Println("Error parsing addSecret result: ", err)
		LogoutLocal(session_id)
		t.Error("Error parsing addSecret result")
		return
	}
	if len(ids) == 0 {
		log.Println("Error parsing addSecret result: empty")
		LogoutLocal(session_id)
		t.Error("Error parsing addSecret result: empty")
		return
	}

	// Now let's get the element
	res, err_msg := getLocalSecret(session_id, dummy_secret.SecretKey)
	if !err_msg.Success {
		log.Println("Error parsing getSecret result: ", err)
		LogoutLocal(session_id)
		t.Error("Error parsing getSecret result")
		return
	}
	var fetched_secret []Secret
	err = GeneralHelper.ArrayMapToStruct(res, &fetched_secret)
	if fetched_secret[0].SecretKey != dummy_secret.SecretKey {
		log.Println("Error parsing getSecret result: wrong secret key")
		LogoutLocal(session_id)
		t.Error("Error parsing getSecret result: wrong secret key")
		return
	}
	if fetched_secret[0].Description != dummy_secret.Description {
		log.Println("Error parsing getSecret result: wrong description")
		LogoutLocal(session_id)
		t.Error("Error parsing getSecret result: wrong description")
		return
	}
	if fetched_secret[0].Value != dummy_secret.Value {
		log.Println("Error parsing getSecret result: wrong value")
		LogoutLocal(session_id)
		t.Error("Error parsing getSecret result: wrong value")
		return
	}

	success, err_msg := deleteLocalSecret(session_id, dummy_secret.SecretKey)
	if !success {
		log.Println("Error parsing deleteSecret result: ", err)
		LogoutLocal(session_id)
		t.Error("Error parsing deleteSecret result")
		return
	}
	LogoutLocal(session_id)
}

func TestSecretsHandling(t *testing.T) {
	var pulsar_host *string
	var pulsar_port *int
	var creds *string

	creds = flag.String("credentials", "../../config/credentials.json", "JSON file with the credentials")
	pulsar_host = flag.String("pulsar", "", "Pulsar host: e.g. 192.168.122.168")
	pulsar_port = flag.Int("pulsar-port", 0, "Pulsar port: e.g. 6650")

	flag.Parse()

	confManager.LoadDefaultConfig()
	if *pulsar_host == "" {
		*pulsar_host = confManager.GetValue("pulsar_host")
	}
	if *pulsar_host == "" {
		*pulsar_host = "localhost"
	}

	port, err := strconv.Atoi(confManager.GetValue("pulsar_port"))
	if port != 0 && *pulsar_port == 0 {
		if err != nil {
			t.Fatalf("failed to convert pulsar port: %v", err)
		}
		*pulsar_port = port
	}
	if *pulsar_port == 0 {
		*pulsar_port = 6650
	}
	pulsar_port_str := strconv.Itoa(*pulsar_port)
	client := PulsarLib.InitClient("pulsar://" + *pulsar_host + ":" + pulsar_port_str)
	comm_client = &client
	defer (*comm_client.Client).Close()

	log.Print("Logging in")
	session_id, err := GeneralHelper.LoginWithCredentialFile(client, *creds)
	if err != nil {
		log.Println("Error logging in: ", err)
		t.Error("Error logging in")
		return
	}
	

	dummy_secret := Secret{SecretKey: "TestSecret", Description: "TestDescription", Value: "TestValue"}

	parsed := make(map[string]interface{})
	parsed["session_id"] = session_id
	parsed["secret"] = dummy_secret
	msg_to_instantiation := PulsarLib.BuildMessage(parsed)
	result := PulsarLib.SendRequestSync(*comm_client, "ui-db.addSecret", msg_to_instantiation)

	log.Println("Result: ", string(result))
	var parsed_result []string
	err = json.Unmarshal(result, &parsed_result)
	if err != nil {
		log.Println("Error parsing addSecret result: ", err)
		GeneralHelper.Logout(*comm_client, session_id)
		t.Error("Error parsing addSecret result")
		return
	}
	if len(parsed_result) == 0 {
		log.Println("Error parsing addSecret result: empty")
		GeneralHelper.Logout(*comm_client, session_id)
		t.Error("Error parsing addSecret result: empty")
		return
	}

	// Now let's get the element
	to_search := make(map[string]interface{})
	to_search["session_id"] = session_id
	to_search["secret_key"] = dummy_secret.SecretKey
	msg_to_instantiation = PulsarLib.BuildMessage(to_search)
	result = PulsarLib.SendRequestSync(*comm_client, "ui-db.getSecret", msg_to_instantiation)
	log.Info("Secret: ", string(result))

	result = PulsarLib.SendRequestSync(*comm_client, "ui-db.deleteSecret", msg_to_instantiation)
	log.Info("Deletion: ", string(result))
	GeneralHelper.Logout(*comm_client, session_id)
}
