package querymanager

import (
	log "github.com/sirupsen/logrus"

	dbw "gitlab.com/HuntDownUPC/go-modules/db/DBWrapper/v2"
	dbd "gitlab.com/HuntDownUPC/go-modules/db/common/v2"
)

var valid_ops []string = []string{"$eq", "$geq", "$lte", "$ne", "$gt", "$gte", "$lt", "$exists"}

func isValidFilterOperation(op string) bool {
	for _, v := range valid_ops {
		if v == op {
			return true
		}
	}
	return false
}

func ConstructBasicFilter(field string, value any, operation string, current_filter *[]dbd.Filters) bool {
	var filter dbd.Filters

	if !isValidFilterOperation(operation) {
		log.Error("Invalid filter operation ", operation)
		return false
	}
	filter = dbd.Filters{Filter: field, Value: value, Op: operation}
	*current_filter = append(*current_filter, filter)
	return true
}

func RunMultipleQuery(info dbd.Db_Context, filter *[]dbd.Filters) ([]byte, error) {
	return dbw.Get_documents(&info, filter)
}

func RunQuery(info dbd.Db_Context, filter *[]dbd.Filters) ([]byte, error) {
	return dbw.Get_document(&info, filter)
}

func RecordExists(client dbd.Db_Context, key string, value any) (bool, error) {
	var filters []dbd.Filters
	ConstructBasicFilter(key, value, "$eq", &filters)
	_, err := RunQuery(client, &filters)
	if err != nil {
		return false, err
	}
	return true, nil
}
