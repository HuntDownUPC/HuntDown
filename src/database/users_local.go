package main

import (
	GeneralHelper "helpers/GeneralHelper"

	log "github.com/sirupsen/logrus"
	dbu "gitlab.com/HuntDownUPC/go-modules/db/DBUsers/v2"
	dbw "gitlab.com/HuntDownUPC/go-modules/db/DBWrapper/v2"
	dbd "gitlab.com/HuntDownUPC/go-modules/db/common/v2"
)

func LogoutLocal(session_id int) {
	dbw.Disconnect(dbd.Db_id(session_id))
}

func LoginLocal(db_conn dbd.DB_Connection) (ClientContext, GeneralHelper.ErrMessage) {
	var clientContext ClientContext
	var err_msg GeneralHelper.ErrMessage
	err_msg.Success = true
	db_user, err := dbw.Connect(db_conn)
	if err != nil {
		log.Println("Connection to db failed")
		err_msg.Success = false
		err_msg.Error = err.Error()
		err_msg.ErrorCode = GeneralHelper.FieldNotFound
		return clientContext, err_msg
	}
	clientUser := dbu.GetUserDatabase(db_user, db_conn.Username)
	if clientUser == "" {
		err_msg.Success = false
		err_msg.Error = "Error with Username or Password"
		err_msg.ErrorCode = GeneralHelper.NoDBConnection
		return clientContext, err_msg
	}
	dbw.UseDatabase(db_user, clientUser)
	dbw.UseCollection(db_user, db_conn.Username)
	isAdmin := dbu.IsAdmin(db_user, db_conn.Username)
	log.Info("Connection to db succeded using Database ", clientUser)

	clientContext.LoggedIn = true
	clientContext.Client = clientUser
	clientContext.IsAdmin = isAdmin
	clientContext.Db_id = db_user

	return clientContext, err_msg
}
