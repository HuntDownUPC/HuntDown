package main

import (
	"encoding/json"
	"flag"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"

	"confManager"

	dbw "gitlab.com/HuntDownUPC/go-modules/db/DBWrapper/v2"
	dbd "gitlab.com/HuntDownUPC/go-modules/db/common/v2"
)

type Classification struct {
	Name string `json:"name"`
}

func LoadConfig(config_file string) error {
	var err error
	if config_file == "" {
		err = confManager.LoadDefaultConfig()
	} else {
		err = confManager.LoadConfigFromFile(config_file)
	}
	if err != nil {
		return err
	}
	mongo_host := confManager.GetValue("mongo_host")
	pulsar_host := confManager.GetValue("pulsar_host")
	pulsar_port := confManager.GetValue("pulsar_port")
	mongo_port := confManager.GetValue("mongo_port")
	mongo_user := confManager.GetValue("mongo_user")
	mongo_password := confManager.GetValue("mongo_password")

	ConfigEnvironment.DatabaseUsername = mongo_user
	ConfigEnvironment.DatabasePassword = mongo_password
	ConfigEnvironment.DatabaseHost = mongo_host
	ConfigEnvironment.DatabasePort = mongo_port
	ConfigEnvironment.PulsarHost = pulsar_host
	ConfigEnvironment.PulsarPort = pulsar_port
	return nil
}

var ConfigEnvironment confManager.ConfigContext

func FillDBConnection(db_conn *dbd.DB_Connection) {
	db_conn.Host = ConfigEnvironment.DatabaseHost
	db_conn.Username = ConfigEnvironment.DatabaseUsername
	db_conn.Password = ConfigEnvironment.DatabasePassword
	db_conn.Port = ConfigEnvironment.DatabasePort
}

func main() {
	var env_json *string
	//var class_json *string
	var mongo_host *string
	var mongo_port *int

	env_json = flag.String("env", "", "Environment json file: e.g. /etc/huntdown/environment.json")
	mongo_host = flag.String("mongo", "", "MongoDB host: e.g. 192.168.122.168")
	mongo_port = flag.Int("mongo-port", 27017, "Mongo port: e.g. 27017")
	//class_json = flag.String("users", "", "Users json file: e.g. ./classification.json")

	flag.Parse()

	var err error
	err = LoadConfig(*env_json)
	if err != nil {
		log.Println("Error loading config file:", err)
		log.Println("Specify a config file with -env <file>")
		os.Exit(1)
	}

	/*if _, err := os.Stat(*class_json); os.IsNotExist(err) {
		println("Error: classification.json does not exist")
		os.Exit(1)
	}*/

	if *mongo_host == "" && ConfigEnvironment.DatabaseHost == "" {
		ConfigEnvironment.DatabaseHost = "localhost"
	} else if *mongo_host != "" {
		ConfigEnvironment.DatabaseHost = *mongo_host
	}

	if ConfigEnvironment.DatabasePort == "" {
		ConfigEnvironment.DatabasePort = strconv.Itoa(*mongo_port)
	}

	var db_conn dbd.DB_Connection
	FillDBConnection(&db_conn)

	session_id, _ := dbw.Connect(db_conn)
	client, err := dbw.GetDBContext(session_id)

	//process_with_json(client, *class_json)

	//mapClassification := find_classification("../../attacks/hping3/hping3-classification.json", "hping3")

	addPreset(client, int(session_id), "goldeneyeddos.json")

	dbw.Disconnect(session_id)
}

func addPreset(client dbd.Db_Context, session_id int, file_name string) {

	client.Database = "general"
	client.Collection = "attacksPresets"

	jsonFile, err := os.Open("../../../../../Presets/" + file_name)

	if err != nil {
		log.Println("Error with the document")
	}
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)

	var document map[string]interface{}

	err = json.Unmarshal(byteValue, &document)

	if err != nil {
		log.Println("Error al decodificar el JSON: %v", err)
	}

	if insertResult, err := dbw.Insert_document(&client, document); err == nil {
		log.Println("attack description inserted to general db")
		log.Println("Inserted document ID:", insertResult)
	} else {
		log.Println("Failed to insert attack description to general db")
		log.Println(err)
	}
}

func find_classification(file_name string, attack_name string) map[string]interface{} {
	file, err := os.ReadFile(file_name)
	if err != nil {
		log.Println("Error reading JSON file:", err)
	}

	var jsonData []map[string]interface{}

	if err := json.Unmarshal(file, &jsonData); err != nil {
		log.Println("Error unmarshalling JSON:", err)
		return nil
	}

	jsonClassification := make(map[string]interface{})

	for _, data := range jsonData {
		for key, value := range data {
			if strings.Contains(key, "classification") {
				switch v := value.(type) {
				case []interface{}:
					classificationjson := make(map[string]interface{})

					for _, elem := range v {
						attackClassification := make(map[string]interface{})
						if reconMap, ok := elem.(map[string]interface{}); ok {
							for innerKey, innerValue := range reconMap {
								if reconnaissanceMaps, ok := innerValue.([]interface{}); ok {
									for _, elem := range reconnaissanceMaps {
										if reconMap, ok := elem.(map[string]interface{}); ok {
											for innerKey2, innerValue2 := range reconMap {
												if innerKey2 == "name" {
													attackClassification["category"] = innerKey
													attackClassification["name"] = attack_name
													classificationjson[innerValue2.(string)] = attackClassification

												}
											}
										}
									}
								} else {
									log.Println("Value is not a slice")
								}
								if innerKey == "name" {
									attackClassification["name"] = attack_name
									classificationjson[innerValue.(string)] = attackClassification
								}
							}
						}
					}
					jsonClassification["classId"] = attack_name
					jsonClassification["classification"] = classificationjson

				case map[string]interface{}:
					for innerKey, innerValue := range v {
						log.Println("Inner Key:", innerKey, "Inner Value:", innerValue)
					}
				default:
					log.Println("Unsupported type for reconnaissance value")
				}
			}
		}
	}
	log.Println(jsonClassification)
	return jsonClassification
}

func update_DB(mapData map[string]interface{}, client dbd.Db_Context, session_id int, attack_name string) {
	client.Database = "general"
	client.Collection = "Classification"

	//all_constructs, err := DatabaseHelper.GetGeneralEntryByName(session_id, comm_client, "Classification", "classId", attack_name)

	//log.Println(all_constructs)
	dbw.Change_index(&client, attack_name)

	//dbw.Update_documents(&client, ,"classification", mapData)

	jsonData, err := json.Marshal(mapData)

	if err != nil {
		log.Println("Error converting to byte")
	}

	if insertResult, err := dbw.Insert_document(&client, jsonData); err == nil {
		log.Println("attack description inserted to general db")
		log.Println("Inserted document ID:", insertResult)
	} else {
		log.Println("Failed to insert attack description to general db")
		log.Println(err)
	}
}

func update_DBPresets(mapData map[string]interface{}, client dbd.Db_Context, session_id int, attack_name string) {
	client.Database = "general"
	client.Collection = "attacksPresetsMarc"

	//log.Println(all_constructs)
	dbw.Change_index(&client, attack_name)

	jsonData, err := json.Marshal(mapData)

	if err != nil {
		log.Println("Error converting to byte")
	}

	if insertResult, err := dbw.Insert_document(&client, jsonData); err == nil {
		log.Println("attack description inserted to general db")
		log.Println("Inserted document ID:", insertResult)
	} else {
		log.Println("Failed to insert attack description to general db")
		log.Println(err)
	}
}
