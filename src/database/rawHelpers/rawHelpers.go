package rawHelpers

import (
	"bytes"
	"encoding/json"
	querymanager "hd-database/QueryManager"
	"os"

	"confManager"

	log "github.com/sirupsen/logrus"

	dbw "gitlab.com/HuntDownUPC/go-modules/db/DBWrapper/v2"
	dbd "gitlab.com/HuntDownUPC/go-modules/db/common/v2"
)

const GeneralDB = "general"

type OverwritePolicy int

const (
	Delete OverwritePolicy = iota
	Skip   OverwritePolicy = iota
)

var overwrite = Delete

func DataIsArray(data []byte) bool {
	x := bytes.TrimLeft(data, " \t\r\n")

	return len(x) > 0 && x[0] == '['
}

func SetOverwritePolicy(overwrite_policy OverwritePolicy) {
	overwrite = overwrite_policy
}

func CreateDBConnection(confEnvironment confManager.ConfigContext) dbd.DB_Connection {
	var db_conn dbd.DB_Connection

	db_conn.Host = confEnvironment.DatabaseHost
	db_conn.Username = confEnvironment.DatabaseUsername
	db_conn.Password = confEnvironment.DatabasePassword
	db_conn.Port = confEnvironment.DatabasePort
	return db_conn
}

func InsertJSONFileToDB(context dbd.Db_Context, filename string, key_name string) error {
	byteValue, err := os.ReadFile(filename)
	if err != nil {
		log.Fatalf("Could not find the file: %v", err)
	}
	return InsertDataToDB(context, byteValue, key_name)
}

func deleteElement(context dbd.Db_Context, key_name string, key string) error {
	filter := []dbd.Filters{}
	querymanager.ConstructBasicFilter(key_name, key, "$eq", &filter)
	_, err := dbw.Get_document(&context, &filter)
	if err == nil {
		log.Info("Deleting element:", key)
		dbw.Delete_document(&context, &filter)
	}
	return err
}

func insertSingleElementToDB(context dbd.Db_Context, data []byte, key_name string) error {
	var json_data = map[string]interface{}{}
	err := json.Unmarshal(data, &json_data)
	if err == nil {
		exists, _ := entryExists(context, key_name, json_data[key_name].(string))
		if exists {
			if overwrite == Delete {
				deleteElement(context, key_name, json_data[key_name].(string))
			} else {
				log.Info("Skipping duplicate")
				return nil
			}
		}
		if _, err := dbw.Insert_document(&context, data); err != nil {
			log.Error("Error inserting data:", err)
			return err
		}
	}
	return err
}

func InsertDataToDB(context dbd.Db_Context, data []byte, key_name string) error {
	if !DataIsArray(data) {
		return insertSingleElementToDB(context, data, key_name)
	}

	var purged_data []byte
	if key_name != "" {
		if overwrite == Delete {
			overwriteDuplicates(context, data, key_name)
			purged_data = data
		} else {
			purged_data = unselectDuplicates(context, data, key_name)
		}
	} else {
		purged_data = data
	}
	if len(purged_data) == 0 {
		log.Info("No data to insert")
		return nil
	}
	if _, err := dbw.Insert_documents(&context, purged_data); err != nil {
		log.Error("Error inserting data:", err)
		return err
	}
	return nil
}

func overwriteDuplicates(context dbd.Db_Context, data []byte, key_name string) []byte {
	if key_name == "" {
		return data
	}

	var json_data = []map[string]interface{}{}
	var deleted_data = []map[string]interface{}{}
	if err := json.Unmarshal(data, &json_data); err != nil {
		log.Error("Error unmarshalling data")
		return data
	}

	for _, item := range json_data {
		deleteElement(context, key_name, item[key_name].(string))
		deleted_data = append(deleted_data, item)
	}
	clean_data, _ := json.Marshal(deleted_data)
	return clean_data
}

func entryExists(context dbd.Db_Context, key_name string, key string) (bool, error) {
	filter := []dbd.Filters{}
	querymanager.ConstructBasicFilter(key_name, key, "$eq", &filter)
	_, err := dbw.Get_document(&context, &filter)
	if err == nil {
		return true, nil
	}
	return false, err
}

func unselectDuplicates(context dbd.Db_Context, data []byte, key_name string) []byte {
	if key_name == "" {
		return data
	}

	var json_data = []map[string]interface{}{}
	var dedup_data = []map[string]interface{}{}
	if err := json.Unmarshal(data, &json_data); err != nil {
		log.Error("Error unmarshalling data")
		return data
	}

	for _, item := range json_data {
		key := item[key_name]
		success, _ := entryExists(context, key_name, key.(string))
		if success {
			log.Info("Skipping duplicate:", key)
		} else {
			dedup_data = append(dedup_data, item)
		}
	}
	if len(dedup_data) == 0 {
		return nil
	}
	clean_data, _ := json.Marshal(dedup_data)
	return clean_data
}
