package main

import (
	"flag"
	querymanager "hd-database/QueryManager"
	"os"

	dbw "gitlab.com/HuntDownUPC/go-modules/db/DBWrapper/v2"
	dbd "gitlab.com/HuntDownUPC/go-modules/db/common/v2"

	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"sync"

	log "github.com/sirupsen/logrus"

	DatabaseHelper "helpers/DatabaseHelper"
	GeneralHelper "helpers/GeneralHelper"

	PulsarLib "gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
)

// Global Configuration
var db_admin dbd.Db_id

func isSecureDatabase(dbname string) bool {
	if dbname == "general" || !strings.HasPrefix(dbname, "client") || dbname != "admin" {
		return true
	}
	return false
}

func insertLocalElements(ctx dbd.Db_Context, data []map[string]interface{}) ([]string, GeneralHelper.ErrMessage) {
	var response []string
	var err error
	var err_msg GeneralHelper.ErrMessage

	response, err = dbw.Insert_documents(&ctx, data)
	if err != nil {
		return response, GeneralHelper.BuildErrorMessage(err, GeneralHelper.EmptyAnswer)
	}
	err_msg.Success = true
	return response, err_msg
}

func insertElements(msg []byte) {
	var info dbd.Db_Context
	var message PulsarLib.MessageResponse
	var err error
	var insert_data DatabaseHelper.DatabaseInsert

	json.Unmarshal(msg, &message)
	err = GeneralHelper.MapToStruct(message.Msg, &insert_data)

	if err != nil {
		log.Error("Error malformed query ", err)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.InvalidFormat))
		return
	}

	info, err = dbw.GetDBContext(dbd.Db_id(insert_data.Session_id))
	if err != nil {
		log.Error("Error getting database context ", err)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoDBConnection))
		return
	}
	if insert_data.Database != "" && isSecureDatabase(insert_data.Database) {
		info.Database = insert_data.Database
	}
	if insert_data.Collection != "" {
		info.Collection = insert_data.Collection
	}

	response, err_msg := insertLocalElements(info, insert_data.ToInsert)
	if err_msg.Success == false {
		log.Error("Error", err_msg.Error)
		GeneralHelper.SendErrorMessage(*comm_client, message.Id, err_msg)
		return
	}
	result := make(map[string]interface{})
	result["success"] = true
	result["ids"] = response
	jsonArr, err := json.Marshal(result)
	if err != nil {
		log.Error("Error:", err)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.InvalidFormat))
		return
	}
	PulsarLib.SendMessage(*comm_client, message.Id, jsonArr)
}

func deleteElements(msg []byte) {
	var info dbd.Db_Context
	var message PulsarLib.MessageResponse
	var err error
	var delete_data DatabaseHelper.DatabaseDelete

	json.Unmarshal(msg, &message)
	log.Println(message.Msg)
	err = GeneralHelper.MapToStruct(message.Msg, &delete_data)
	log.Println(delete_data)
	if err != nil {
		log.Error("Error malformed query ", err)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.InvalidFormat))
		return
	}

	info, err = dbw.GetDBContext(dbd.Db_id(delete_data.Session_id))
	if err != nil {
		log.Error("Error getting database context ", err)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoDBConnection))
		return
	}
	if delete_data.Database != "" && isSecureDatabase(delete_data.Database) {
		info.Database = delete_data.Database
	}
	if delete_data.Collection != "" {
		info.Collection = delete_data.Collection
	}

	success, err_msg := localDelete(info, delete_data.Filters)
	if !success {
		GeneralHelper.SendErrorMessage(*comm_client, message.Id, err_msg)
	} else {
		var response []byte = []byte("{\"success\": true}")
		PulsarLib.SendMessage(*comm_client, message.Id, response)
	}
}

func localDeleteOne(info dbd.Db_Context, raw_filters []DatabaseHelper.Filter) (bool, GeneralHelper.ErrMessage) {
	var err error
	var filters []dbd.Filters

	for _, filter := range raw_filters {
		querymanager.ConstructBasicFilter(filter.Field, filter.Value, filter.Op, &filters)
	}

	var success bool
	var err_msg GeneralHelper.ErrMessage
	err_msg.Success = true
	success, err = dbw.Delete_document(&info, &filters)
	if err != nil || !success {
		err_msg = GeneralHelper.BuildErrorMessage(err, GeneralHelper.EmptyAnswer)
		return success, err_msg
	}
	return success, err_msg
}

func localDelete(info dbd.Db_Context, raw_filters []DatabaseHelper.Filter) (bool, GeneralHelper.ErrMessage) {
	var err error
	var filters []dbd.Filters

	for _, filter := range raw_filters {
		querymanager.ConstructBasicFilter(filter.Field, filter.Value, filter.Op, &filters)
	}

	var success bool
	var err_msg GeneralHelper.ErrMessage
	err_msg.Success = true
	success, err = dbw.Delete_documents(&info, &filters)
	if err != nil || !success {
		err_msg = GeneralHelper.BuildErrorMessage(err, GeneralHelper.EmptyAnswer)
		return success, err_msg
	}
	return success, err_msg
}

func updateElement(msg []byte) {
	var filters []dbd.Filters
	var info dbd.Db_Context
	var message PulsarLib.MessageResponse
	var err error
	var update_data DatabaseHelper.DatabaseUpdate

	json.Unmarshal(msg, &message)
	log.Println(message.Msg)
	err = GeneralHelper.MapToStruct(message.Msg, &update_data)
	log.Println(update_data)
	if err != nil {
		log.Error("Error malformed query ", err)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.InvalidFormat))
		return
	}

	info, err = dbw.GetDBContext(dbd.Db_id(update_data.Session_id))
	if err != nil {
		log.Error("Error getting database context ", err)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoDBConnection))
		return
	}
	if update_data.Database != "" && isSecureDatabase(update_data.Database) {
		info.Database = update_data.Database
	}
	if update_data.Collection != "" {
		info.Collection = update_data.Collection
	}

	log.Info("Database internal info: ", info)
	for _, filter := range update_data.Filters {
		querymanager.ConstructBasicFilter(filter.Field, filter.Value, filter.Op, &filters)
	}
	err = dbw.Update_documents(&info, &filters, update_data.Field, update_data.Value)
	if err != nil {
		log.Error("Error", err)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.EmptyAnswer))
	} else {
		var response []byte = []byte("{\"success\": true}")
		PulsarLib.SendMessage(*comm_client, message.Id, response)
	}
}

func attendSearchRecord(msg []byte) {
	var info dbd.Db_Context
	var message PulsarLib.MessageResponse
	var err error

	json.Unmarshal(msg, &message)
	var query_data DatabaseHelper.DatabaseQuery
	err = GeneralHelper.MapToStruct(message.Msg, &query_data)
	log.Println(query_data)
	if err != nil {
		log.Error("Error malformed query ", err)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.InvalidFormat))
		return
	}

	info, err = dbw.GetDBContext(dbd.Db_id(query_data.Session_id))
	if err != nil {
		log.Error("Error getting database context ", err)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoDBConnection))
		return
	}

	if query_data.Database != "" && isSecureDatabase(query_data.Database) {
		info.Database = query_data.Database
	}
	if query_data.Collection != "" {
		info.Collection = query_data.Collection
	}

	var response []byte
	response, err_msg := localQuery(info, query_data.Filters)
	if err_msg.Success == false {
		log.Error("Error", err)
		GeneralHelper.SendErrorMessage(*comm_client, message.Id, err_msg)
	} else {
		PulsarLib.SendMessage(*comm_client, message.Id, response)
	}
}

func localQuery(info dbd.Db_Context, raw_filters []DatabaseHelper.Filter) ([]byte, GeneralHelper.ErrMessage) {
	var response []byte
	var err error
	var filters []dbd.Filters
	for _, filter := range raw_filters {
		querymanager.ConstructBasicFilter(filter.Field, filter.Value, filter.Op, &filters)
	}
	response, err = querymanager.RunMultipleQuery(info, &filters)
	if err != nil {
		var err_msg GeneralHelper.ErrMessage
		err_msg = GeneralHelper.BuildErrorMessage(err, GeneralHelper.UnknownError)
		return nil, err_msg
	}
	return response, GeneralHelper.ErrMessage{Success: true}
}

func FillDBConnection(db_conn *dbd.DB_Connection) {
	db_conn.Host = GeneralHelper.ConfigEnvironment.DatabaseHost
	db_conn.Username = GeneralHelper.ConfigEnvironment.DatabaseUsername
	db_conn.Password = GeneralHelper.ConfigEnvironment.DatabasePassword
	db_conn.Port = GeneralHelper.ConfigEnvironment.DatabasePort
}

func main() {
	var mongo_host *string
	var mongo_user *string
	var mongo_pass *string
	var pulsar_host *string
	var pulsar_port *int
	var mongo_port *int

	GeneralHelper.LoadConfig()
	mongo_host = flag.String("mongo", "", "MongoDB host: e.g. 192.168.122.168")
	mongo_user = flag.String("mongo-user", "", "MongoDB user, overrides config file")
	mongo_pass = flag.String("mongo-pass", "", "MongoDB password, overrides config file")
	pulsar_host = flag.String("pulsar", "", "Pulsar host: e.g. 192.168.122.168")
	pulsar_port = flag.Int("pulsar-port", 6650, "Pulsar port: e.g. 6650")
	mongo_port = flag.Int("mongo-port", 27017, "Mongo port: e.g. 27017")
	flag.Parse()

	var pulsarHostURI string
	if *pulsar_host == "" && GeneralHelper.ConfigEnvironment.PulsarHost == "" {
		GeneralHelper.ConfigEnvironment.PulsarHost = "localhost"
	} else if *pulsar_host != "" {
		GeneralHelper.ConfigEnvironment.PulsarHost = *pulsar_host
	}

	if GeneralHelper.ConfigEnvironment.PulsarPort == "" {
		GeneralHelper.ConfigEnvironment.PulsarPort = strconv.Itoa(*pulsar_port)
	}
	pulsarHostURI = fmt.Sprintf("pulsar://%s:%s", GeneralHelper.ConfigEnvironment.PulsarHost, GeneralHelper.ConfigEnvironment.PulsarPort)

	if *mongo_host == "" && GeneralHelper.ConfigEnvironment.DatabaseHost == "" {
		GeneralHelper.ConfigEnvironment.DatabaseHost = "localhost"
	} else if *mongo_host != "" {
		GeneralHelper.ConfigEnvironment.DatabaseHost = *mongo_host
	}

	if GeneralHelper.ConfigEnvironment.DatabasePort == "" {
		GeneralHelper.ConfigEnvironment.DatabasePort = strconv.Itoa(*mongo_port)
	}

	if GeneralHelper.ConfigEnvironment.DatabaseUsername == "" || *mongo_user != "" {
		GeneralHelper.ConfigEnvironment.DatabaseUsername = *mongo_user
	}
	if GeneralHelper.ConfigEnvironment.DatabasePassword == "" || *mongo_pass != "" {
		GeneralHelper.ConfigEnvironment.DatabasePassword = *mongo_pass
	}

	var db_conn dbd.DB_Connection
	FillDBConnection(&db_conn)
	var err error
	db_admin, err = dbw.Connect(db_conn)
	if err != nil {
		log.Println("Error connecting to database: ", err.Error())
		os.Exit(-1)
	}
	//Create the Pulsar Client
	client := PulsarLib.InitClient(pulsarHostURI)
	defer (*client.Client).Close()

	InitConfig(db_admin, &client)

	var mainwg sync.WaitGroup
	mainwg.Add(15)
	PulsarLib.WaitEvent(client, "ui-db.login", Login)
	PulsarLib.WaitEvent(client, "ui-db.logout", Logout)
	PulsarLib.WaitEvent(client, "ui-db.createUser", CreateUser)
	PulsarLib.WaitEvent(client, "ui-db.users", Users)
	PulsarLib.WaitEvent(client, "ui-db.UserExists", UserExistsPulsar)
	PulsarLib.WaitEvent(client, "ui-db.deleteUser", DeleteUser)

	PulsarLib.WaitEvent(client, "ui-db.addSecret", addSecret)
	PulsarLib.WaitEvent(client, "ui-db.getSecret", getSecretByName)
	PulsarLib.WaitEvent(client, "ui-db.listSecrets", listSecrets)
	PulsarLib.WaitEvent(client, "ui-db.deleteSecret", deleteSecret)

	PulsarLib.WaitEvent(client, "db.newQuery", attendSearchRecord)
	PulsarLib.WaitEvent(client, "db.updateField", updateElement)
	PulsarLib.WaitEvent(client, "db.insertElements", insertElements)
	PulsarLib.WaitEvent(client, "db.deleteElements", deleteElements)

	PulsarLib.WaitEvent(client, "monitor", monitor)
	mainwg.Wait()

	dbw.Disconnect(db_admin)
}
