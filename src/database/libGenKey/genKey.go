package libGenKey

import (
	"crypto/sha512"

	"golang.org/x/crypto/pbkdf2"
	//"math/rand"
)

func GenKey(key string) []byte {
	// salt := make([]byte, 4)
	salt := []byte{49}
	// rand.Read(salt)
	dk := pbkdf2.Key([]byte(key), salt, 1000, 64, sha512.New)
	return dk
}
