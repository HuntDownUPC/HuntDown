package main

import (
	"encoding/json"
	"fmt"
	"helpers/GeneralHelper"

	log "github.com/sirupsen/logrus"

	PulsarLib "gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
	dbu "gitlab.com/HuntDownUPC/go-modules/db/DBUsers/v2"
	dbw "gitlab.com/HuntDownUPC/go-modules/db/DBWrapper/v2"
	dbd "gitlab.com/HuntDownUPC/go-modules/db/common/v2"
)

var comm_client *PulsarLib.PL_Client
var Db_admin_for_users dbd.Db_id

type user_map struct {
	Name string `json:"name"`
}
type ClientContext struct {
	Db_id    dbd.Db_id `json:"session_id"`
	IsAdmin  bool      `json:"admin"`
	Client   string    `json:"client"`
	LoggedIn bool      `json:"success"`
}

// OK this does not support multitenancy and is crappy, we need ways of authenticating Pulsar and allowing multiple users at the same time
func InitConfig(my_dbadmin dbd.Db_id, my_client *PulsarLib.PL_Client) {
	Db_admin_for_users = my_dbadmin
	comm_client = my_client
}

func Logout(msg []byte) {
	var message PulsarLib.MessageResponse
	json.Unmarshal(msg, &message)
	logout_info := message.Msg

	my_id, err := GeneralHelper.GetSession_id(logout_info)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoSession))
		return
	}

	dbw.Disconnect(dbd.Db_id(my_id))
	var resp = make(map[string]string)
	resp["success"] = "true"
	jsonStr, _ := json.Marshal(resp)
	PulsarLib.SendMessage(*comm_client, message.Id, jsonStr)

	log.Println("Listing connections")
	for _, i := range dbw.ListConnections() {
		log.Println(i)
	}
}

func Login(msg []byte) {
	var db_conn dbd.DB_Connection
	var message PulsarLib.MessageResponse
	json.Unmarshal(msg, &message)
	login_info := message.Msg

	log.Println(login_info)
	log.Println("Connecting to db...")
	_, found_user := login_info["username"]
	_, found_hash := login_info["hash"]
	if !found_user || !found_hash {
		var resp = make(map[string]string)
		resp["success"] = "false"
		jsonStr, _ := json.Marshal(resp)
		PulsarLib.SendMessage(*comm_client, message.Id, jsonStr)
		log.Println("Connection to db failed")
		return
	}

	username := login_info["username"].(string)
	password := login_info["hash"].(string)

	db_conn.Host = GeneralHelper.ConfigEnvironment.DatabaseHost
	db_conn.Username = username
	db_conn.Password = password
	db_conn.Port = GeneralHelper.ConfigEnvironment.DatabasePort

	ctx, err_msg := LoginLocal(db_conn)
	if !err_msg.Success {
		GeneralHelper.SendErrorMessage(*comm_client, message.Id, err_msg)
		log.Error("Connection to db failed")
		return
	}

	jsonStr, _ := json.Marshal(ctx)
	PulsarLib.SendMessage(*comm_client, message.Id, jsonStr)

	log.Info("Listing connections")
	for _, i := range dbw.ListConnections() {
		log.Info(i)
	}
}

func CreateUser(msg []byte) {
	user_info := make(map[string]any)
	var jsonStr []byte
	json.Unmarshal(msg, &user_info)

	if user_info == nil {
		jsonStr = []byte("{}")
	} else {
		username := user_info["username"].(string)
		user_exists := dbu.UserExistsComplete(Db_admin_for_users, username)
		if user_exists["success"] == "true" {
			jsonStr, _ = json.Marshal(user_exists)
		} else {
			hash := user_info["hash"].(string)

			session_id := user_info["session_id"].(string)
			result := dbu.Add_user_UI(Db_admin_for_users, username, hash, session_id)
			jsonStr, _ = json.Marshal(result)
		}
	}
	PulsarLib.SendMessage(*comm_client, user_info["Id"].(string), jsonStr)
}

func Users(value []byte) {
	var jsonStr []byte
	fmt.Println("Getting users for UI search")

	var message PulsarLib.MessageResponse
	json.Unmarshal(value, &message)
	user_to_search := message.Msg
	// check if user_to_search has session_id
	var session_id any
	if _, ok := user_to_search["session_id"]; ok {
		session_id = user_to_search["session_id"]
	} else {
		jsonStr = []byte("{\"success\": false}")
		PulsarLib.SendMessage(*comm_client, message.Id, jsonStr)
		return
	}
	var s_id int
	if _, ok := session_id.(float64); ok {
		s_id = int(session_id.(float64))
	} else {
		s_id = session_id.(int)
	}
	user := dbw.GetCollection(dbd.Db_id(s_id))

	if user == "" {
		jsonStr = []byte("{\"success\": false}")
		PulsarLib.SendMessage(*comm_client, message.Id, jsonStr)
		return
	}

	response_aux := dbu.GetUsers(dbd.Db_id(s_id))
	var userss []dbd.Users
	json.Unmarshal(response_aux, &userss)
	var response []user_map
	for _, name := range userss {
		if name.User != user {
			new_user := user_map{Name: name.User}
			response = append(response, new_user)
		}
	}
	if response == nil {
		jsonStr = []byte("{\"success\": false}")
	} else {
		jsonStr, _ = json.Marshal(response)
	}
	PulsarLib.SendMessage(*comm_client, message.Id, jsonStr)
}

func UserExists(value []byte) bool {
	log.Println("Seeing if uses exists")

	var message PulsarLib.MessageResponse
	json.Unmarshal(value, &message)
	result := message.Msg

	exists, _ := dbu.UserExists(Db_admin_for_users, result["username"].(string))
	return exists
}

func UserExistsPulsar(value []byte) {
	log.Println("Seeing if uses exists")

	var message PulsarLib.MessageResponse
	json.Unmarshal(value, &message)
	result := message.Msg

	response := dbu.UserExistsComplete(Db_admin_for_users, result["username"].(string))
	jsonStr, _ := json.Marshal(response)
	PulsarLib.SendMessage(*comm_client, message.Id, jsonStr)
}

func DeleteUser(msg []byte) {
	log.Println("Deleting user")
	success := dbu.DeleteRole(Db_admin_for_users, string(msg))
	if !success {
		log.Println("Error deleting role")
	}
	success = dbu.DeleteUser(Db_admin_for_users, string(msg))
	if success {
		log.Println("User deleted")
	}
}
