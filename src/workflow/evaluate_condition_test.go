package main

import (
	"testing"
)

func TestEvaluateCondition(t *testing.T) {
	check_equal(t)
	check_noequal(t)
	check_gt(t)
	check_ge(t)
	check_lt(t)
	check_le(t)
	t.Log("Success")
}

func check_lt(t *testing.T) {
	var i_value int
	var i_op int
	condition := "<"
	i_value = 5
	i_op = 5
	if test_evaluateCondition(condition, i_value, i_op, false) {
		t.Log("Correct", condition, i_value, i_op, false)
	} else {
		t.Fatalf("Test failed on < operation")
	}

	i_op = 6
	if test_evaluateCondition(condition, i_value, i_op, true) {
		t.Log("Correct", condition, i_value, i_op, true)
	} else {
		t.Fatalf("Test failed on < operation")
	}

	s_value := "5.0"
	s_op := "5.0"
	if test_evaluateCondition(condition, s_value, s_op, false) {
		t.Log("Correct", condition, s_value, s_op, false)
	} else {
		t.Fatalf("Test failed on < operation")
	}

	s_value = "5.1"
	if test_evaluateCondition(condition, s_value, s_op, false) {
		t.Log("Correct", condition, s_value, s_op, false)
	} else {
		t.Fatalf("Test failed on < operation")
	}

	var f_value float64
	var f_op float64
	f_value = 5.0
	f_op = 5.0
	if test_evaluateCondition(condition, f_value, f_op, false) {
		t.Log("Correct", condition, f_value, f_op, false)
	} else {
		t.Fatalf("Test failed on < operation")
	}

	f_op = 6.0
	if test_evaluateCondition(condition, f_value, f_op, true) {
		t.Log("Correct", condition, f_value, f_op, true)
	} else {
		t.Fatalf("Test failed on < operation")
	}
}

func check_le(t *testing.T) {
	var i_value int
	var i_op int
	condition := "<="
	i_value = 5
	i_op = 5
	if test_evaluateCondition(condition, i_value, i_op, true) {
		t.Log("Correct", condition, i_value, i_op, true)
	} else {
		t.Fatalf("Test failed on <= operation")
	}

	i_op = 6
	if test_evaluateCondition(condition, i_value, i_op, true) {
		t.Log("Correct", condition, i_value, i_op, true)
	} else {
		t.Fatalf("Test failed on <= operation")
	}

	s_value := "5.0"
	s_op := "5.0"
	if test_evaluateCondition(condition, s_value, s_op, true) {
		t.Log("Correct", condition, s_value, s_op, true)
	} else {
		t.Fatalf("Test failed on <= operation")
	}

	s_value = "5.1"
	if test_evaluateCondition(condition, s_value, s_op, false) {
		t.Log("Correct", condition, s_value, s_op, false)
	} else {
		t.Fatalf("Test failed on <= operation")
	}

	var f_value float64
	var f_op float64
	f_value = 5.0
	f_op = 5.0
	if test_evaluateCondition(condition, f_value, f_op, true) {
		t.Log("Correct", condition, f_value, f_op, true)
	} else {
		t.Fatalf("Test failed on <= operation")
	}

	f_op = 6.0
	if test_evaluateCondition(condition, f_value, f_op, true) {
		t.Log("Correct", condition, f_value, f_op, true)
	} else {
		t.Fatalf("Test failed on <= operation")
	}
}

func check_ge(t *testing.T) {
	var i_value int
	var i_op int
	condition := ">="
	i_value = 5
	i_op = 5
	if test_evaluateCondition(condition, i_value, i_op, true) {
		t.Log("Correct", condition, i_value, i_op, true)
	} else {
		t.Fatalf("Test failed on >= operation")
	}

	i_op = 6
	if test_evaluateCondition(condition, i_value, i_op, false) {
		t.Log("Correct", condition, i_value, i_op, false)
	} else {
		t.Fatalf("Test failed on >= operation")
	}

	s_value := "5.0"
	s_op := "5.0"
	if test_evaluateCondition(condition, s_value, s_op, true) {
		t.Log("Correct", condition, s_value, s_op, true)
	} else {
		t.Fatalf("Test failed on >= operation")
	}

	s_value = "5.1"
	if test_evaluateCondition(condition, s_value, s_op, true) {
		t.Log("Correct", condition, s_value, s_op, true)
	} else {
		t.Fatalf("Test failed on >= operation")
	}

	var f_value float64
	var f_op float64
	f_value = 5.0
	f_op = 5.0
	if test_evaluateCondition(condition, f_value, f_op, true) {
		t.Log("Correct", condition, f_value, f_op, true)
	} else {
		t.Fatalf("Test failed on >= operation")
	}

	f_op = 6.0
	if test_evaluateCondition(condition, f_value, f_op, false) {
		t.Log("Correct", condition, f_value, f_op, false)
	} else {
		t.Fatalf("Test failed on >= operation")
	}
}

func check_gt(t *testing.T) {
	var i_value int
	var i_op int
	condition := ">"
	i_value = 5
	i_op = 5
	if test_evaluateCondition(condition, i_value, i_op, false) {
		t.Log("Correct", condition, i_value, i_op, false)
	} else {
		t.Fatalf("Test failed on > operation")
	}

	i_op = 6
	if test_evaluateCondition(condition, i_value, i_op, false) {
		t.Log("Correct", condition, i_value, i_op, false)
	} else {
		t.Fatalf("Test failed on > operation")
	}

	s_value := "5.0"
	s_op := "5.0"
	if test_evaluateCondition(condition, s_value, s_op, false) {
		t.Log("Correct", condition, s_value, s_op, false)
	} else {
		t.Fatalf("Test failed on > operation")
	}

	s_value = "5.1"
	if test_evaluateCondition(condition, s_value, s_op, true) {
		t.Log("Correct", condition, s_value, s_op, true)
	} else {
		t.Fatalf("Test failed on > operation")
	}

	var f_value float64
	var f_op float64
	f_value = 5.0
	f_op = 5.0
	if test_evaluateCondition(condition, f_value, f_op, false) {
		t.Log("Correct", condition, f_value, f_op, false)
	} else {
		t.Fatalf("Test failed on > operation")
	}

	f_op = 6.0
	if test_evaluateCondition(condition, f_value, f_op, false) {
		t.Log("Correct", condition, f_value, f_op, false)
	} else {
		t.Fatalf("Test failed on > operation")
	}
}

func check_noequal(t *testing.T) {
	var i_value int
	var i_op int
	condition := "!="
	i_value = 5
	i_op = 5
	if test_evaluateCondition(condition, i_value, i_op, false) {
		t.Log("Correct", condition, i_value, i_op, false)
	} else {
		t.Fatalf("Test failed on != operation")
	}

	i_op = 6
	if test_evaluateCondition(condition, i_value, i_op, true) {
		t.Log("Correct", condition, i_value, i_op, true)
	} else {
		t.Fatalf("Test failed on != operation")
	}

	s_value := "hello"
	s_op := "hello"
	if test_evaluateCondition(condition, s_value, s_op, false) {
		t.Log("Correct", condition, s_value, s_op, false)
	} else {
		t.Fatalf("Test failed on != operation")
	}

	s_value = "bye"
	if test_evaluateCondition(condition, s_value, s_op, true) {
		t.Log("Correct", condition, s_value, s_op, true)
	} else {
		t.Fatalf("Test failed on != operation")
	}

	var f_value float64
	var f_op float64
	f_value = 5.0
	f_op = 5.0
	if test_evaluateCondition(condition, f_value, f_op, false) {
		t.Log("Correct", condition, f_value, f_op, false)
	} else {
		t.Fatalf("Test failed on != operation")
	}

	f_op = 6.0
	if test_evaluateCondition(condition, f_value, f_op, true) {
		t.Log("Correct", condition, f_value, f_op, true)
	} else {
		t.Fatalf("Test failed on != operation")
	}
}

func check_equal(t *testing.T) {
	var i_value int
	var i_op int
	condition := "=="
	i_value = 5
	i_op = 5
	if test_evaluateCondition(condition, i_value, i_op, true) {
		t.Log("Correct", condition, i_value, i_op)
	} else {
		t.Fatalf("Test failed on == operation")
	}

	i_op = 6
	if test_evaluateCondition(condition, i_value, i_op, false) {
		t.Log("Correct", condition, i_value, i_op)
	} else {
		t.Fatalf("Test failed on == operation")
	}

	s_value := "hello"
	s_op := "hello"
	if test_evaluateCondition(condition, s_value, s_op, true) {
		t.Log("Correct", condition, s_value, s_op)
	} else {
		t.Fatalf("Test failed on == operation")
	}

	s_value = "bye"
	if test_evaluateCondition(condition, s_value, s_op, false) {
		t.Log("Correct", condition, s_value, s_op)
	} else {
		t.Fatalf("Test failed on == operation")
	}

	var f_value float64
	var f_op float64
	f_value = 5.0
	f_op = 5.0
	if test_evaluateCondition(condition, f_value, f_op, true) {
		t.Log("Correct", condition, f_value, f_op)
	} else {
		t.Fatalf("Test failed on == operation")
	}

	f_op = 6.0
	if test_evaluateCondition(condition, f_value, f_op, false) {
		t.Log("Correct", condition, f_value, f_op)
	} else {
		t.Fatalf("Test failed on == operation")
	}
}

func test_evaluateCondition(condition string, value interface{}, op interface{}, expected bool) bool {
	return evaluateCondition(condition, value, op) == expected
}
