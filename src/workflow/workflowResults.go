package main

import (
	"encoding/json"
	"helpers/DatabaseHelper"
	"helpers/GeneralHelper"

	log "github.com/sirupsen/logrus"
	PulsarLib "gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
)

func getGeneralWorkflowResultsSchema(value []byte) {
	var session_id int
	var err error
	var message PulsarLib.MessageResponse
	json.Unmarshal(value, &message)

	session_id, err = GeneralHelper.GetSession_id(message.Msg)
	if err != nil {
		log.Println("Error malformed query on attack options", err.Error())
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoSession))
		return
	}

	var workflow_name string
	err = GeneralHelper.GetMapElement(message.Msg, "workflow_name", &workflow_name)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, []byte("{}"))
		return
	}

	workflow_results, success := getLocalGeneralWorkflowResults(session_id, workflow_name)
	var response []byte
	if !success {
		response, _ = json.Marshal("{}")
	} else {
		response, _ = json.Marshal(workflow_results)
	}
	PulsarLib.SendMessage(*comm_client, message.Id, response)
}

func getLocalGeneralWorkflowResults(session_id int, workflow_name string) (result map[string]interface{}, success bool) {
	var filter DatabaseHelper.Filter
	var err error

	success = false
	if string(workflow_name) == "" {
		return
	}
	filter = DatabaseHelper.Filter{Field: "workflow", Value: workflow_name, Op: "$eq"}
	query := DatabaseHelper.DatabaseQuery{Session_id: session_id,
		Database:   "general",
		Collection: "WorkflowResultsSchema",
		Filters:    []DatabaseHelper.Filter{filter}}

	to_send := PulsarLib.BuildMessage(GeneralHelper.StructToMap(query))
	response := PulsarLib.SendRequestSync(*comm_client, "db.newQuery", to_send)
	var formatted []map[string]interface{}
	err = json.Unmarshal(response, &formatted)
	if err != nil || formatted == nil || len(formatted) == 0 {
		log.Println("Error getting general result schema:", err)
		return
	}
	result = formatted[0]
	success = true
	return
}
