package main

import (
	"encoding/json"
	"errors"
	"helpers/DatabaseHelper"
	"helpers/GeneralHelper"

	log "github.com/sirupsen/logrus"
	PulsarLib "gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
)

// Preset structure
type Options struct {
	Type        string `json:"type"`
	Description string `json:"description"`
	Value       string `json:"value"`
}

type Preset struct {
	MandatoryUserOptions []string           `json:"mandatoryUserOptions"`
	UserOptions          []string           `json:"UserOptions"`
	Preset               string             `json:"preset"`
	Description          string             `json:"description"`
	Options              map[string]Options `json:"options"`
	AttackName           string             `json:"attackName"`
}

type PresetInfo struct {
	Preset      string      `json:"preset"`
	Description string      `json:"description"`
	UserOptions interface{} `json:"mandatoryUserOptions"`
}

func getAttackPresets(session []byte) {
	var message PulsarLib.MessageResponse
	//Given the name of an attack, the name of all presets related to the attack are obtained.
	log.Println("Getting attack presets for a given attack")

	json.Unmarshal(session, &message)
	result := message.Msg

	//Validating session
	session_id, err := GeneralHelper.GetSession_id(result)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoSession))
		return
	}

	//Obtaining Attack
	var attack string
	err = GeneralHelper.GetMapElement(result, "attack_name", &attack)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.FieldNotFound))
		return
	}
	// Create a new slice to store the preset information
	var presetsInfo []PresetInfo

	getGeneralAttackPresets(session_id, attack, &presetsInfo)

	// Serialize the new slice in a JSON object
	presetsInfoBytes, err := json.Marshal(presetsInfo)
	if err != nil {
		log.Error("Error marshalling preset info:", err)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.InvalidFormat))
		return
	}

	// Send preset information
	PulsarLib.SendMessage(*comm_client, message.Id, presetsInfoBytes)
}

func getAttackPreset(preset []byte) {
	//Given the name of a preset, its structure is obtained from the database
	var message PulsarLib.MessageResponse

	json.Unmarshal(preset, &message)
	result := message.Msg

	//Validating session
	session_id, err := GeneralHelper.GetSession_id(result)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoSession))
		return
	}

	//Obtaining the desired preset value
	var attackPreset string
	err = GeneralHelper.GetMapElement(result, "preset", &attackPreset)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.FieldNotFound))
		return
	}

	found_preset, status := getGeneralAttackPreset(session_id, attackPreset)
	if !status {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(errors.New("preset not found"), GeneralHelper.FieldNotFound))
		return
	}

	byte_preset, err := json.Marshal(found_preset)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.InvalidFormat))
		return
	}
	PulsarLib.SendMessage(*comm_client, message.Id, byte_preset)
}

func getGeneralAttackPreset(session_id int, preset_name string) (PresetInfo, bool) {
	var err error
	var preset PresetInfo

	all_results, err := DatabaseHelper.GetGeneralEntryByName(session_id, comm_client, "attacksPresets", "preset", preset_name)
	log.Println("Presets: ", all_results)
	if err != nil {
		log.Println("Error getting general presets", err)
		return preset, false
	}
	err = GeneralHelper.MapToStruct(all_results, &preset)
	if err != nil {
		log.Error("Error getting presets", err)
		return preset, false
	}
	return preset, true
}

func getGeneralAttackPresets(session_id int, attack_name string, presets *[]PresetInfo) bool {
	var err error

	all_results, err := DatabaseHelper.GetAllGeneralEntriesByName(session_id, comm_client, "attacksPresets", "attackName", attack_name)
	log.Println("Presets: ", all_results)
	if err != nil {
		log.Println("Error getting general presets", err)
		return false
	}
	err = GeneralHelper.ArrayMapToStruct(all_results, &presets)
	if err != nil {
		log.Error("Error getting presets", err)
		return false
	}
	if presets == nil || len(*presets) == 0 {
		log.Debug("No presets found", attack_name)
		return false
	}

	return true
}
