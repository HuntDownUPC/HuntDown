package main

import (
	"bytes"
	"confManager"
	"encoding/base64"
	"encoding/json"
	"errors"
	as "helpers/AttackSchema"
	"helpers/GeneralHelper"
	me "helpers/MonitorEvents"
	"os"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"time"
	"vagrant-mod/Logs"

	log "github.com/sirupsen/logrus"

	"github.com/noirbizarre/gonja"
	"github.com/sbabiv/xml2map"
	PulsarLib "gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
	"golang.org/x/crypto/ssh"
)

var results_dir string
var var_dir string
var vagrant_base_dir string
var etc_dir string = "/etc/huntdown"

var running_attacks map[string]bool
var registry_username string
var registry_password string
var attacks_machine_ip string

func getFile(client *ssh.Client, from, to string) error {
	f, err := os.Create(to)
	if err != nil {
		return err
	}
	defer f.Close()

	session, err := client.NewSession()
	if err != nil {
		return err
	}
	defer session.Close()

	session.Stdout = f
	var stderr bytes.Buffer
	session.Stderr = &stderr
	err = session.Run("cat '" + from + "'")
	if err != nil && stderr.Len() > 0 {
		err = errors.New(err.Error() + ": " + stderr.String())
	}
	return err
}

func isJSON(s string) bool {
	var js interface{}
	return json.Unmarshal([]byte(s), &js) == nil
}

func CreateResultSchema(attack *as.AttackSchema, start string, end string) ([]byte, error) {
	var schema as.AttackResult

	for i := 0; i < len(attack.CliCommands); i++ {
		var num_results = 0
		var auxCommand as.ResultSchema
		if attack.CliCommands[i].NeedResults {
			rawbyte, err := os.ReadFile(results_dir + "/attack_" + attack.Id + "/RAW/" + attack.CliCommands[i].OptionsId + "_output_" + strconv.Itoa(i+1))
			if err != nil {
				log.Println("Could not read RAW file: ", err)
				return []byte{}, err
			}

			sEnc := base64.StdEncoding.EncodeToString(rawbyte)

			/* NEW Version */
			start = strings.TrimSuffix(start, "\n")
			end = strings.TrimSuffix(end, "\n")
			schema.Start, _ = time.Parse("2006/01/02 15-04-05.000000", start)
			schema.End, _ = time.Parse("2006/01/02 15-04-05.000000", end)
			auxCommand.Resultsoutput = sEnc
			auxCommand.CommandInfo = attack.CliCommands[i]

			schema.CommandsResult = append(schema.CommandsResult, auxCommand)
			if attack.CliCommands[i].OutputType == "XML" {
				xmlbyte, err := os.ReadFile(results_dir + "/attack_" + attack.Id + "/XML/" + attack.CliCommands[i].OptionsId + "_xml")
				if err != nil {
					log.Error("Could not read XML file.")
					return []byte{}, err
				}

				xmlstring := string(xmlbyte)
				decoder := xml2map.NewDecoder(strings.NewReader(xmlstring))
				result, _ := decoder.Decode()
				schema.CommandsResult[num_results].Resultsjson = result

			} else if attack.CliCommands[i].OutputType == "JSON" {
				byteValue, _ := os.ReadFile(results_dir + "/attack_" + attack.Id + "/JSON/" + attack.CliCommands[i].OptionsId + "_json")
				var result map[string]interface{}
				json.Unmarshal(byteValue, &result)
				if isJSON(string(byteValue)) {
					schema.CommandsResult[num_results].Resultsjson = result
				}
			}
			num_results++
		}
	}

	finaldata := make(map[string]any)
	finaldata["result"] = schema
	finaldata["success"] = true

	raw_data, _ := json.Marshal(finaldata)
	return raw_data, nil
}

func structToMap(i interface{}, result *gonja.Context) {
	val := reflect.ValueOf(i)
	t := reflect.TypeOf(i)

	for i := 0; i < t.NumField(); i++ {
		(*result)[t.Field(i).Name] = val.Field(i).Interface()
	}
}

func OptionsParser(input string) (map[string]string, error) {
	// Split the string into parts
	parts := strings.Fields(input)

	// Initialize an empty map to store the key-value pairs
	data := make(map[string]string)
	for i := 0; i < len(parts); i += 2 {
		// Remove the "-" and store the key-value pair in the map
		if i+1 < len(parts) {
			key := "{" + strings.TrimPrefix(parts[i], "-") + "}"
			data[key] = parts[i+1]
		}
	}

	return data, nil
}

func performAttack(value []byte) {
	var message PulsarLib.MessageResponse
	var attack as.AttackSchema
	var result []byte
	var err error
	var session_id int

	json.Unmarshal(value, &message)
	log.Println(string(value))

	session_id, err = GeneralHelper.GetSession_id(message.Msg)
	if err != nil {
		log.Println("Error malformed session ID", err.Error())
		PulsarLib.SendMessage(global_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoSession))
		return
	}

	raw_attack, _ := json.Marshal(message.Msg["attack"])
	json.Unmarshal(raw_attack, &attack)
	if _, ok := running_attacks[attack.Id]; ok {
		err_msg := "Attack " + attack.AttackID + " already running"
		log.Printf(err_msg)
		PulsarLib.SendMessage(global_client, message.Id, generateErrorFromString(err_msg))
		return
	}
	attack_id := attack.Id

	var monitor me.MonitorContext
	monitor.Id = attack_id
	monitor.PulsarClient = global_client
	monitor.Session_id = strconv.Itoa(session_id)

	// look for error messages in attack schema
	if attack.ErrorMessage != "" {
		me.SendMonitorError(&monitor, errors.New(attack.ErrorMessage))
		delete(running_attacks, attack.Id)
		return
	}

	running_attacks[attack.Id] = true

	if attack.RunnerType == "webrunner" {
		me.SendAndIncreaseMonitorEvent(&monitor, "Creating final results")
		result, err = performAttackWebRunner(&attack, &monitor)
		if err != nil {
			me.SendMonitorError(&monitor, err)
		}
	} else if attack.RunnerType == "docker" || (attack.DockerImage != "" && attacks_machine_ip != "") {
		result, err = performAttackDocker(&attack, &monitor)
		if err != nil {
			me.SendMonitorError(&monitor, err)
		}
	} else { // Fallback to vagrant instantiation
		me.SendAndIncreaseMonitorEvent(&monitor, "Generating Ansible Playbook")
		Logs.AddLogEntryToAttack(&attack, "Generating Ansible Playbook")

		success, err := generate_ansible_playbook(attack)
		if !success {
			me.SendMonitorError(&monitor, err)
			PulsarLib.SendMessage(global_client, message.Id, generateError(err))
			delete(running_attacks, attack.Id)
			Logs.CommitLogMessages(global_client, session_id, attack_id, attack.Logs)
			return
		}

		/* Retrieve SIZE data */
		me.SendAndIncreaseMonitorEvent(&monitor, "Asking for a machine")
		attackMachine := searchMachines(session_id, &global_client, attack.OsType)

		me.SendAndIncreaseMonitorEvent(&monitor, "Generating Vagrantfile")
		/* Setting up the Vagrantfile from a jinja2 template */

		success, err = generate_vagrantfile(attackMachine)
		if !success {
			monitor.Error = err
			me.SendMonitorError(&monitor, err)
			PulsarLib.SendMessage(global_client, message.Id, generateError(err))
			delete(running_attacks, attack.Id)
			Logs.CommitLogMessages(global_client, session_id, attack_id, attack.Logs)
			return
		}
		me.SendAndIncreaseMonitorEvent(&monitor, "Executing Attack")
		// vagrant_response, vagrantClient, persistent := runVagrant(&monitor, bdJsonAttack, jsonMachine)
		vagrant_response, _, persistent := runVagrant(&monitor, &attack, attackMachine)
		result = vagrant_response

		if !persistent {
			log.Println("Machine is not persistent, DESTROYING MACHINE...")
			log.Println("Skipped to avoid crashes for now!!!!")
			// cmd3 := vagrantClient.Destroy()
			// cmd3.Run()
		}
	}
	var tmp map[string]interface{}
	json.Unmarshal(result, &tmp)
	if tmp["success"] == false {
		me.SendMonitorError(&monitor, errors.New("attack failed"))
		PulsarLib.SendMessage(global_client, message.Id, result)
		delete(running_attacks, attack.Id)
		Logs.CommitLogMessages(global_client, session_id, attack_id, attack.Logs)
		return
	}
	me.SendAndIncreaseMonitorEvent(&monitor, "Attack Finished Successfully")
	Logs.AddLogEntryToAttack(&attack, "Attack Finished Successfully")
	Logs.CommitLogMessages(global_client, session_id, attack_id, attack.Logs)
	PulsarLib.SendMessage(global_client, message.Id, result)

	delete(running_attacks, attack.Id)
}

func main() {
	confManager.LoadDefaultConfig()
	pulsar_host := confManager.GetValue("pulsar_host")
	pulsar_port := confManager.GetValue("pulsar_port")
	registry_username = confManager.GetValue("docker_registry_username")
	registry_password = confManager.GetValue("docker_registry_password")
	attacks_machine_ip = confManager.GetValue("attacks_machine_ip")
	results_dir = confManager.GetValue("results_dir")
	vagrant_base_dir = confManager.GetValue("vagrant_base_dir")
	var_dir = confManager.GetValue("var_base_dir")
	if results_dir == "" {
		results_dir = "./results"
	}
	client := PulsarLib.InitClient("pulsar://" + pulsar_host + ":" + pulsar_port)
	global_client = client
	defer (*client.Client).Close()

	var mainwg sync.WaitGroup
	running_attacks = make(map[string]bool)

	mainwg.Add(2) //Num of waitevents
	PulsarLib.WaitEvent(client, "instantiation.whichMachine", getMachine)
	PulsarLib.WaitEvent(client, "instantiation.runAttack", performAttack)
	mainwg.Wait()
}
