package Logs

import (
	as "helpers/AttackSchema"

	"gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
)

func AddLogEntryToAttack(result *as.AttackSchema, message string) {
	result.Logs = append(result.Logs, message)
}

func AddLogEntry(result map[string]interface{}, message string) {
	if _, ok := result["logs"]; !ok {
		result["logs"] = make([]string, 0)
	}
	result["logs"] = append(result["logs"].([]string), message)
}

func CommitLogMessages(client PulsarLib.PL_Client, session_id int, attack_id string, logs []string) (string, error) {
	message := make(map[string]interface{})
	message["logs"] = logs
	message["session_id"] = session_id
	message["result_id"] = attack_id

	message_raw := PulsarLib.BuildMessage(message)
	response := PulsarLib.SendRequestSync(client, "ui-db.updateAttackLogs", message_raw)
	return string(response), nil
}
