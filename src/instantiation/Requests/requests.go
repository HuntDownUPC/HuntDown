package Requests

import (
	as "helpers/AttackSchema"
	"io"
	"net/http"
	"net/url"
	"strings"
	"vagrant-mod/Logs"
)

func MakeGenericHTTPRequest(config *as.AttackSchema) ([]byte, error) {
	var body []byte

	if config.Webrunner[0].Type == "get" {
		req, err := http.NewRequest("GET", config.Webrunner[0].Url, nil)
		if err != nil {
			Logs.AddLogEntryToAttack(config, "Error with query: "+err.Error())
			return nil, err
		}

		for key, value := range config.Webrunner[0].Headers {
			req.Header.Set(key, value)
		}

		q := req.URL.Query()
		for key, value := range config.Webrunner[0].QueryParams {
			q.Add(key, value)
		}
		req.URL.RawQuery = q.Encode()
		Logs.AddLogEntryToAttack(config, "Running query: "+req.URL.String())

		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			Logs.AddLogEntryToAttack(config, "Error with query: "+err.Error())
			return nil, err
		}
		defer resp.Body.Close()

		body, err = io.ReadAll(resp.Body)
		if err != nil {
			Logs.AddLogEntryToAttack(config, "Error with response: "+err.Error())
			return nil, err
		}
		Logs.AddLogEntryToAttack(config, "Raw Response: "+string(body))
	}

	if config.Webrunner[0].Type == "post" {
		QueryParams := url.Values{}
		for key, value := range config.Webrunner[0].QueryParams {
			QueryParams.Set(key, value)
		}

		client := &http.Client{}
		req, err := http.NewRequest("POST", config.Webrunner[0].Url, strings.NewReader(QueryParams.Encode()))
		if err != nil {
			Logs.AddLogEntryToAttack(config, "Error with query: "+err.Error())
			return nil, err
		}

		for key, value := range config.Webrunner[0].Headers {
			req.Header.Set(key, value)
		}

		res, err := client.Do(req)
		if err != nil {
			Logs.AddLogEntryToAttack(config, "Error with query: "+err.Error())
			return nil, err
		}
		defer res.Body.Close()

		body, err = io.ReadAll(res.Body)
		if err != nil {
			Logs.AddLogEntryToAttack(config, "Error with response: "+err.Error())
			return nil, err
		}
		Logs.AddLogEntryToAttack(config, "Raw Response: "+string(body))
	}
	return body, nil
}
