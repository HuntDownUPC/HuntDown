package main

import (
	"confManager"
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
	"vagrant-mod/Logs"

	me "helpers/MonitorEvents"
	op "helpers/OptionParser"

	log "github.com/sirupsen/logrus"

	"github.com/docker/cli/cli/connhelper"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/image"
	"github.com/docker/docker/api/types/registry"
	dockerClient "github.com/docker/docker/client"
	"github.com/docker/docker/pkg/stdcopy"

	as "helpers/AttackSchema"
)

func performAttackDocker(attack *as.AttackSchema, monitor *me.MonitorContext) ([]byte, error) {
	var auths string
	var cli *dockerClient.Client
	var err error

	attack_id := attack.Id

	docker_host := confManager.GetValue("docker_host")
	if docker_host == "remote" {
		cli, err = connect_docker_remote()
	} else {
		cli, err = connect_to_docker()
	}
	if err != nil {

		return generateError(err), err
	}

	if strings.Contains(attack.DockerImage, "registry.gitlab.com/huntdownupc/attacks/") {
		Logs.AddLogEntryToAttack(attack, "Docker Attack "+attack.AttackName+" Logging into the Repository")
		auths = loginRegistry(cli, registry_username, registry_password)
	}

	Logs.AddLogEntryToAttack(attack, "Removing previous container")
	removeAllContainersFromImage(cli, attack.DockerImage)

	container_id, err := runContainer(cli, auths, monitor, attack_id, attack)
	if err != nil {
		return generateError(err), err
	}
	running_attacks[attack_id] = true

	return generate_and_retrieve_file_docker(cli, monitor, container_id, attack)
}

func connect_docker_local() (*dockerClient.Client, error) {
	log.Println("connect_docker_local")
	return dockerClient.NewClientWithOpts(dockerClient.FromEnv, dockerClient.WithAPIVersionNegotiation())
}

/* FIXME we have to recover remote Docker connections, for now we use local */
func connect_docker_remote() (*dockerClient.Client, error) {
	var sshOpts []string

	docker_host := confManager.GetValue("docker_host")
	docker_sshkey := confManager.GetValue("docker_sshkey")
	docker_user := confManager.GetValue("docker_user")

	sshOpts = append(sshOpts, "-i", docker_sshkey, "-o", "StrictHostKeyChecking=no")

	var sshAccess string
	if docker_user == "" {
		sshAccess = "ssh://" + docker_host
	} else {
		sshAccess = "ssh://" + docker_user + "@" + docker_host
	}
	helper, err := connhelper.GetConnectionHelperWithSSHOpts(sshAccess, sshOpts)
	if err != nil {
		log.Error("Unable connect to Docker daemon with error:", err)
		return nil, err
	}

	httpClient := &http.Client{
		Transport: &http.Transport{
			DialContext: helper.Dialer,
		},
	}

	var clientOpts []dockerClient.Opt
	clientOpts = append(clientOpts,
		dockerClient.WithHTTPClient(httpClient),
		dockerClient.WithHost(helper.Host),
		dockerClient.WithDialContext(helper.Dialer),
		dockerClient.WithAPIVersionNegotiation(),
	)
	return dockerClient.NewClientWithOpts(clientOpts...)
}

func connect_to_docker() (*dockerClient.Client, error) {
	var cli *dockerClient.Client
	var err error

	cli, err = connect_docker_local()

	if err != nil {
		log.Error("Unable to create docker client, please make sure that docker is locally installed", err)
		return nil, err
	}
	defer cli.Close()

	return cli, nil
}

func generate_and_retrieve_file_docker(cli *dockerClient.Client, monitor *me.MonitorContext,
	containerId string, attack *as.AttackSchema) ([]byte, error) {

	ctx := context.Background()

	me.SendAndIncreaseMonitorEvent(monitor, "Retrieving results")

	//....... Creating RAW output

	optionsLogs := container.LogsOptions{ShowStdout: true, ShowStderr: true}

	out, err := cli.ContainerLogs(ctx, containerId, optionsLogs)
	if err != nil {
		log.Println("Error reading container logs, reason: ", err)
		return []byte{}, err
	}

	defer out.Close()

	err = os.Mkdir(results_dir+"/attack_"+attack.Id+"/RAW", 0755)
	if err != nil {
		log.Println("Could not create RAW folder: ", err)
		return generateError(err), err
	}

	command := attack.CliCommands[len(attack.CliCommands)-1]
	rawFile, err := os.OpenFile(results_dir+"/attack_"+attack.Id+"/RAW/"+command.OptionsId+"_output_"+strconv.Itoa(len(attack.CliCommands)),
		os.O_RDWR|os.O_CREATE, 0777)
	defer rawFile.Close()
	if err != nil {
		log.Println("Could not open the output file: ", err)
		return generateError(err), err
	}

	// Debug print to ensure logs are being read correctly
	log.Println("Logs output: ", out)

	_, err = io.Copy(rawFile, out)
	if err != nil {
		log.Println("Error writing logs to file: ", err)
		return generateError(err), err
	}

	//....... Creating TIME data

	inspect, err := cli.ContainerInspect(ctx, containerId)
	if err != nil {
		log.Println("Error inspecting container, reason: ", err)
		return generateError(err), err
	}

	err = os.Mkdir(results_dir+"/attack_"+attack.Id+"/TIME", 0755)
	if err != nil {
		log.Println("Could not create TIME folder")
		return generateError(err), err
	}

	started_time := parseTimeToUnixFormat(inspect.State.StartedAt)
	start_time_bytes := []byte(started_time)

	err = os.WriteFile(results_dir+"/attack_"+attack.Id+"/TIME/start_time", start_time_bytes, 0777)
	if err != nil {
		log.Println("Could not write into the start_time file")
		return generateError(err), err
	}

	finished_time := parseTimeToUnixFormat(inspect.State.FinishedAt)
	finished_time_bytes := []byte(finished_time)

	err = os.WriteFile(results_dir+"/attack_"+attack.Id+"/TIME/end_time", finished_time_bytes, 0777)
	if err != nil {
		log.Println("Could not write into the end_time file")
		return generateError(err), err
	}

	me.SendAndIncreaseMonitorEvent(monitor, "Creating final results")
	return CreateResultSchema(attack, started_time, finished_time)
}

func parseTimeToUnixFormat(dockerTime string) string {
	parsedTime, _ := time.Parse(time.RFC3339Nano, dockerTime)
	formattedTime := parsedTime.Format("2006/01/02 15-04-05.000000")

	return formattedTime
}

func runContainer(cli *dockerClient.Client, auths string, monitor *me.MonitorContext,
	containerName string, attack *as.AttackSchema) (string, error) {

	ctx := context.Background()

	imageName := attack.DockerImage

	me.SendAndIncreaseMonitorEvent(monitor, "Pulling Docker Image")
	Logs.AddLogEntryToAttack(attack, "Pulling Docker Image")

	var events io.ReadCloser
	var err error
	if auths != "" {
		events, err = cli.ImagePull(ctx, imageName, image.PullOptions{RegistryAuth: auths})
		if err != nil {
			Logs.AddLogEntryToAttack(attack, "Error pulling image")
			return "", err
		}
	} else {
		events, err = cli.ImagePull(ctx, imageName, image.PullOptions{})
		if err != nil {
			Logs.AddLogEntryToAttack(attack, "Error pulling image")
			return "", err
		}
	}
	Logs.AddLogEntryToAttack(attack, "Image pulled successfully")

	defer events.Close()

	err = managingPullEvents(events)
	if err != nil {
		return "", err
	}

	command := attack.CliCommands[len(attack.CliCommands)-1]

	var options []string

	// Determine if we're using the old string format or the new JSON format
	if command.Flags.Delimiter != "" {
		// New JSON format
		options, err = op.AssembleCommandLineStrSlice(command.Flags)
		if err != nil {
			log.Println("Error assembling command line options: ", err)
		}
	} else {
		// Old string format
		re := regexp.MustCompile(`[^\s"]+|"[^"]*"`)
		options = re.FindAllString(command.Options, -1)
		for i := range options {
			options[i] = strings.ReplaceAll(options[i], "\"", "")
		}
	}

	options, _ = updateWordlistPathToMountPoint(options)
	Logs.AddLogEntryToAttack(attack, "Preparing the environment")
	attack_folder := "/attack_" + attack.Id
	err = os.MkdirAll(results_dir+attack_folder, 0755)

	if err != nil {
		log.Error("Could not create Attack folder: ", err)
		Logs.AddLogEntryToAttack(attack, "Could not create Attack folder "+err.Error())
		return "Could not create Attack folder", err
	}

	var hostConfig *container.HostConfig
	me.SendAndIncreaseMonitorEvent(monitor, "Mounting wordlist directory")
	hostConfig = &container.HostConfig{
		// mount wordlist dir in /data
		Binds: []string{"/etc/huntdown/wordlists:/data"},
		// give container access to vm network
		NetworkMode: "host",
	}

	if command.Fileoutput {
		var output_file string
		if command.OutputType == "XML" {
			attack_folder = attack_folder + "/XML/"
			output_file = attack_folder + command.OptionsId + "_xml"
		} else if command.OutputType == "JSON" {
			attack_folder = attack_folder + "/JSON/"
			output_file = attack_folder + command.OptionsId + "_json"
		}

		if output_file != "" {
			options = append(options, output_file)

			err = os.MkdirAll(results_dir+attack_folder, 0755)
			if err != nil {
				err_msg := "Could not create Attack folder with output file"
				log.Error(err_msg, err)
				Logs.AddLogEntryToAttack(attack, err_msg+err.Error())
				return err_msg, err
			}

			hostConfig = &container.HostConfig{
				Binds: []string{
					results_dir + attack_folder + ":" + attack_folder,
				},
			}
		}
	}

	create_msg := "Creating Docker container for Attack"
	me.SendAndIncreaseMonitorEvent(monitor, create_msg)
	Logs.AddLogEntryToAttack(attack, create_msg)
	log.Debug("     options...", options)

	me.SendAndIncreaseMonitorEvent(monitor, "Creating Docker container for Attack")
	log.Println("     options...", options)

	resp, err := cli.ContainerCreate(
		ctx,
		&container.Config{
			Image: imageName,
			Cmd:   options,
		},
		hostConfig,
		nil,
		nil,
		containerName,
	)

	if err != nil {
		me.SendAndIncreaseMonitorEvent(monitor, "Error when creating Attack")
		Logs.AddLogEntryToAttack(attack, "Error when creating Attack "+err.Error())
		return "", err
	}
	my_msg := "Starting Attack Container"
	Logs.AddLogEntryToAttack(attack, my_msg)
	me.SendAndIncreaseMonitorEvent(monitor, my_msg)

	if err := cli.ContainerStart(ctx, resp.ID, container.StartOptions{}); err != nil {
		my_msg := "Error when starting Attack"
		me.SendAndIncreaseMonitorEvent(monitor, my_msg)
		Logs.AddLogEntryToAttack(attack, my_msg)
		return "", err
	}

	// (future) what if container remains running?
	statusCh, errCh := cli.ContainerWait(ctx, resp.ID, container.WaitConditionNotRunning)
	select {
	case err := <-errCh:
		if err != nil {
			me.SendAndIncreaseMonitorEvent(monitor, "Error waiting for Attack to finish")
			return "", err
		}
	case status := <-statusCh:
		if status.StatusCode != 0 && status.Error != nil {
			me.SendAndIncreaseMonitorEvent(monitor, "Error when executing the Attack")
			return "", errors.New(status.Error.Message)
		}
	}
	out, err := cli.ContainerLogs(ctx, resp.ID, container.LogsOptions{ShowStdout: true, ShowStderr: true})
	if err != nil {
		return "", err
	}
	defer out.Close()

	var stdout, stderr strings.Builder
	_, err = stdcopy.StdCopy(&stdout, &stderr, out)
	if err != nil {
		return "", err
	}
	Logs.AddLogEntryToAttack(attack, stdout.String())
	Logs.AddLogEntryToAttack(attack, stderr.String())

	return resp.ID, nil
}

func managingPullEvents(events io.ReadCloser) error {
	// https://stackoverflow.com/questions/44452679/golang-docker-api-parse-result-of-imagepull

	d := json.NewDecoder(events)

	type Event struct {
		Status         string `json:"status"`
		Error          string `json:"error"`
		Progress       string `json:"progress"`
		ProgressDetail struct {
			Current int `json:"current"`
			Total   int `json:"total"`
		} `json:"progressDetail"`
	}

	var event *Event
	for {
		if err := d.Decode(&event); err != nil {
			if err == io.EOF {
				break
			}

			if event.Error != "" {
				return errors.New(event.Error)
			}
		}
		fmt.Printf("EVENT: %+v\n", event)
	}
	return nil
}

func stopAndRemoveContainer(client *dockerClient.Client, containerId string) error {
	ctx := context.Background()

	removeOptions := container.RemoveOptions{
		Force: true,
	}

	if err := client.ContainerRemove(ctx, containerId, removeOptions); err != nil {
		log.Printf("Unable to remove container: %s", err)
		return err
	}

	return nil
}

func loginRegistry(client *dockerClient.Client, registry_username string, registry_password string) string {
	ctx := context.Background()

	auth := registry.AuthConfig{
		Username:      registry_username,
		Password:      registry_password,
		ServerAddress: "registry.gitlab.com/huntdownupc/attacks",
	}

	_, err := client.RegistryLogin(ctx, auth)
	if err != nil {
		log.Printf("Unable to login to the docker registry: %s", err)
	}

	authData, err := json.Marshal(auth)
	if err != nil {
		log.Printf("Failed to marshal authData: %s", err)
	}

	return base64.URLEncoding.EncodeToString(authData)
}

func removeAllContainersFromImage(client *dockerClient.Client, imageName string) {
	ctx := context.Background()

	containers, err := client.ContainerList(
		ctx,
		container.ListOptions{
			All: true,
		},
	)
	if err != nil {
		log.Printf("Unable to list all containers from image: %s", err)
	}

	for _, container := range containers {
		if container.Image == imageName {
			err = stopAndRemoveContainer(client, container.ID)
			if err != nil {
				log.Printf("Unable to remove container: %s", err)
			}
		}
	}
}

func updateWordlistPathToMountPoint(strings []string) ([]string, error) {

	wordlistDir := "/etc/huntdown/wordlists"
	var modifiedStrings []string

	// Get list of files in the wordlist directory
	files, err := os.ReadDir(wordlistDir)
	if err != nil {
		return nil, fmt.Errorf("failed to read directory: %v", err)
	}

	// Create a map for quick lookup
	fileMap := make(map[string]bool)
	for _, file := range files {
		fileMap[file.Name()] = true
	}

	// Check each string in the input list
	for _, str := range strings {
		if fileMap[str] {
			// If the file exists in the directory, prepend "/data/"
			modifiedStrings = append(modifiedStrings, "/data/"+str)
		} else {
			// Otherwise, append the original string
			modifiedStrings = append(modifiedStrings, str)
		}
	}

	return modifiedStrings, nil

}
