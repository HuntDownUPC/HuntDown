package helpers

import (
	"io"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/jung-kurt/gofpdf"
)

// create a new PDF document
func NewReport(domain string) *gofpdf.Fpdf {

	pdf := gofpdf.New("P", "mm", "A4", "")

	pdf.AddPage()

	pdf.SetFont("Times", "B", 24)
	pdf.CellFormat(0, 15, "Information Gathering Report", "", 1, "C", false, 0, "")

	pdf.SetFont("Times", "B", 18)
	pdf.CellFormat(0, 10, "Domain: "+domain, "", 1, "C", false, 0, "")
	pdf.Ln(5)

	return pdf
}

// write on the reprt the subsection
func AddSubsectionToReport(pdf *gofpdf.Fpdf, ip string) *gofpdf.Fpdf {

	pdf.SetFont("Times", "B", 16)
	pdf.CellFormat(0, 10, "IP: "+ip, "", 1, "C", false, 0, "")

	return pdf
}

func AddContentToReport(pdf *gofpdf.Fpdf, content string, isList bool, listSymbol string) *gofpdf.Fpdf {

	if isList {
		pdf.SetFont("Courier", "", 11)

		// Determine cell width based on page settings
		pageSizeW, _ := pdf.GetPageSize()
		marginLeft, _, marginRight, _ := pdf.GetMargins()
		cellWidth := pageSizeW - marginLeft - marginRight

		// Set line height and padding
		lineHeight := pdf.PointConvert(18)
		padding := pdf.PointConvert(4)

		pdf.MultiCell(cellWidth, lineHeight, listSymbol+content, "", "", false)
		pdf.Ln(padding)

	} else {
		pdf.SetFont("Times", "", 13)

		// Determine cell width based on page settings
		pageSizeW, _ := pdf.GetPageSize()
		marginLeft, _, marginRight, _ := pdf.GetMargins()
		cellWidth := pageSizeW - marginLeft - marginRight

		// Set line height and padding
		lineHeight := pdf.PointConvert(18)
		padding := pdf.PointConvert(4)

		// Add content with text wrapping
		pdf.MultiCell(cellWidth, lineHeight, content, "", "", false)

		// Add padding after the content
		pdf.Ln(padding)
	}

	return pdf
}

func DownloadReport(pdf *gofpdf.Fpdf) {

	// save the PDF to a file
	err := pdf.OutputFileAndClose("report.pdf")
	if err != nil {
		log.Println("Error occurred:", err)
	}

	// it is possible to follow the link to download
	downloadURL := "http://localhost:8080/download"
	log.Printf("Download the PDF from: %s\n", downloadURL)

	http.HandleFunc("/download", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Disposition", "attachment; filename=report.pdf")
		w.Header().Set("Content-Type", "application/pdf")
		http.ServeFile(w, r, "report.pdf")
	})

	// start the server in one other goroutine
	go func() {
		log.Println(http.ListenAndServe(":8080", nil))
	}()

	time.Sleep(1 * time.Second)

	// this automatic download the pdf
	response, err := http.Get(downloadURL)
	if err != nil {
		log.Println("Error occurred while downloading the PDF:", err)
	}
	defer response.Body.Close()

	// create the output file
	outputFile, err := os.Create("report.pdf")
	if err != nil {
		log.Println("Error creating the output file:", err)
	}
	defer outputFile.Close()

	// copy the response body into the output file
	_, err = io.Copy(outputFile, response.Body)
	if err != nil {
		log.Println("Error copying the response body:", err)
	}

	log.Println("PDF downloaded successfully.")

	// close the server after 10 seconds
	time.AfterFunc(30*time.Second, func() {
		log.Println("Shutting down server")
		os.Exit(0)
	})

	// Give the possibility to download waiting for the goroutine to finish (the server shuts down in 30s)
	select {}
}
