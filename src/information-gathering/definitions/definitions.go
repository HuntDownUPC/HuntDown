package definitions

import (
	as "helpers/AttackSchema"
	of "helpers/OptionsFile"
)

type LoginResponse struct {
	Admin     string `json:"admin"`
	Client    string `json:"client"`
	SessionID string `json:"session_id"`
	Success   string `json:"success"`
}

// AttackList is an array that contains the configured attacks because they can be more than one
type AttackList []as.AttackSchema

type OptionsList []of.OptionsFile

type FierceResponse struct {
	Found []struct {
		Ip      string `json:"ip"`
		DnsName string `json:"dnsname"`
	} `json:"found"`
}

type NmapResponse struct {
	NmapRun struct {
		Args         string `json:"@args"`
		Scanner      string `json:"@scanner"`
		Start        string `json:"@start"`
		StartStr     string `json:"@startstr"`
		Version      string `json:"@version"`
		XmlOutputVer string `json:"@xmloutputversion"`
		Debugging    struct {
			Text  string `json:"#text"`
			Level string `json:"@level"`
		} `json:"debugging"`
		Host struct {
			EndTime   string `json:"@endtime"`
			StartTime string `json:"@starttime"`
			Address   struct {
				Text     string `json:"#text"`
				Addr     string `json:"@addr"`
				AddrType string `json:"@addrtype"`
			} `json:"address"`
			//Hostnames string `json:"hostnames"`
			HostNames struct {
				HostName struct {
					Text string `json:"#text"`
					Name string `json:"@name"`
					Type string `json:"@type"`
				} `json:"hostname"`
			} `json:"hostnames"`
			Ports struct {
				ExtraPorts struct {
					Count        string `json:"@count"`
					State        string `json:"@state"`
					ExtraReasons struct {
						Text   string `json:"#text"`
						Count  string `json:"@count"`
						Reason string `json:"@reason"`
					} `json:"extrareasons"`
				} `json:"extraports"`
				Port []struct {
					PortID   string `json:"@portid"`
					Protocol string `json:"@protocol"`
					Script   []struct {
						ID     string `json:"@id"`
						Output string `json:"@output"`
						Elem   string `json:"elem"`
					} `json:"script"`
					Service struct {
						Text      string   `json:"#text"`
						Conf      string   `json:"@conf"`
						Extrainfo string   `json:"@extrainfo"`
						Method    string   `json:"@method"`
						Name      string   `json:"@name"`
						OsType    string   `json:"@ostype"`
						Product   string   `json:"@product"`
						Version   string   `json:"@version"`
						Cpe       []string `json:"cpe"`
					} `json:"service"`
					State struct {
						Text      string `json:"#text"`
						Reason    string `json:"@reason"`
						ReasonTTL string `json:"@reason_ttl"`
						State     string `json:"@state"`
					} `json:"state"`
				} `json:"port"`
			} `json:"ports"`
			Status struct {
				Text      string `json:"#text"`
				Reason    string `json:"@reason"`
				ReasonTTL string `json:"@reason_ttl"`
				State     string `json:"@state"`
			} `json:"status"`
			Times struct {
				Text   string `json:"#text"`
				RttVar string `json:"@rttvar"`
				Srtt   string `json:"@srtt"`
				To     string `json:"@to"`
			} `json:"times"`
		} `json:"host"`
		RunStats struct {
			Finished struct {
				Text    string `json:"#text"`
				Elapsed string `json:"@elapsed"`
				Exit    string `json:"@exit"`
				Summary string `json:"@summary"`
				Time    string `json:"@time"`
				TimeStr string `json:"@timestr"`
			} `json:"finished"`
			Hosts struct {
				Text  string `json:"#text"`
				Down  string `json:"@down"`
				Total string `json:"@total"`
				Up    string `json:"@up"`
			} `json:"hosts"`
		} `json:"runstats"`
		ScanInfo struct {
			Text     string `json:"#text"`
			NumPorts string `json:"@numservices"`
			Protocol string `json:"@protocol"`
			Services string `json:"@services"`
			ScanType string `json:"@type"`
		} `json:"scaninfo"`
		Verbose struct {
			Text  string `json:"#text"`
			Level string `json:"@level"`
		} `json:"verbose"`
	} `json:"nmaprun"`
}
