package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"

	as "helpers/AttackSchema"
	of "helpers/OptionsFile"
	utils "information-gathering/attackUtils"
	def "information-gathering/definitions"
	helpers "information-gathering/helpers"
	itr "information-gathering/iterator"

	"github.com/jung-kurt/gofpdf"
	"gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
)

type LoginResponse = def.LoginResponse
type AttackSchema = as.AttackSchema
type AttackList = def.AttackList
type OptionsFile = of.OptionsFile
type NmapResponse = def.NmapResponse
type FierceResponse = def.FierceResponse

type AttackState string

const (
	NotExecuted AttackState = "notExecuted"
	Deleted     AttackState = "deleted"
	Executed    AttackState = "executed"
	Executing   AttackState = "executing"
	Error       AttackState = "error"
)

var sessionID string
var client PulsarLib.PL_Client

var Report gofpdf.Fpdf

func main() {

	var pulsar_host *string
	var pulsar_port *int

	// Init pulsar connection
	pulsar_host = flag.String("pulsar", "147.83.42.239", "Pulsar host: 147.83.42.239")
	pulsar_port = flag.Int("pulsar-port", 6650, "Pulsar port: 6650")
	flag.Parse()

	pulsar_port_str := strconv.Itoa(*pulsar_port)
	client = PulsarLib.InitClient("pulsar://" + *pulsar_host + ":" + pulsar_port_str)
	defer (*client.Client).Close()

	// Retrieve credentials
	credentials, err := os.ReadFile("data/login.json")

	if err != nil {
		log.Println(err)
	}

	var loginData map[string]string
	json.Unmarshal(credentials, &loginData)

	// create LoginJson and send the request
	login_json := helpers.CreateLoginJson(loginData)
	jsonMachine := helpers.LoginSendRequestSync(client, login_json)

	// At this point we are logged
	log.Println("Logged in succesfully")
	log.Println(string(jsonMachine)) // {"admin":"true","client":"client3","session_id":"288233","success":"true"}

	// Save the response of the login into a struct
	var loginResponse LoginResponse
	err = json.Unmarshal([]byte(jsonMachine), &loginResponse)

	if err != nil {
		log.Println(err)
	}

	// Access the session_id field and save it in sessionID
	sessionID = loginResponse.SessionID

	// I read the domain name from the terminal
	fmt.Println("Please insert the domain name you want to gather information about:")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	err = scanner.Err()

	if err != nil {
		log.Println(err)
	}

	var domainName string

	domainNameRead := scanner.Text()
	domainName = strings.TrimSpace(domainNameRead)

	// Create the message to getAttacks and send the request
	getAttacks_json := helpers.CreateGetAttacksJson(sessionID)
	configuredAttacks := helpers.GetAttacksSendRequestSync(client, getAttacks_json)

	var attackList AttackList
	err = json.Unmarshal(configuredAttacks, &attackList)

	if err != nil {
		log.Println(err)
	}

	// check if there are the config attack needed
	if len(attackList) > 4 {

		// fierceManager does all the operations and give the result of the fierce attack
		result := utils.FierceManager(client, domainName, sessionID)

		log.Println("RESULT")
		log.Println(result)

		// the result is processed and ipList is the list of all the ips we neen for the next step
		var domainName string

		ipList, domainName := utils.FierceFindIpList(result)

		log.Println("ipList after Fierce Attack")
		log.Println(ipList)
		log.Println("Domain Name")
		log.Println(domainName)

		// inizialize the report
		report := helpers.NewReport(domainName)
		report = helpers.AddContentToReport(report, "After conducting an investigation of the company's domain, we have successfully identified all associated IP addresses. Please find below the list of discovered IP addresses for your reference.", false, "")

		// writing on the report
		for _, item := range ipList {
			report = helpers.AddContentToReport(report, "ip: "+item["ip"]+", dns: "+item["dns"], true, " - ")
		}
		report = helpers.AddContentToReport(report, "The ip addresses are now analyzed one by one through the report.", false, "")

		// starts with the iterator to find information for every ip discovered in the first phase
		report = itr.Iterator(client, sessionID, ipList, report)

		helpers.DownloadReport(report)

	} else {
		log.Println("There are no configured attack to execute")
		log.Println("To get the result of the attack")
	}

	// logout
	logout_json := make(map[string]interface{})
	json.Unmarshal(jsonMachine, &logout_json)
	req := PulsarLib.BuildMessage(logout_json)
	PulsarLib.SendRequestSync(client, "ui-db.logout", req)

	log.Println("Now we logged out")
}
