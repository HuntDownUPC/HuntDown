package iterator

import (
	"log"
	"strconv"

	"github.com/jung-kurt/gofpdf"
	"gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"

	utils "information-gathering/attackUtils"
	def "information-gathering/definitions"
	helpers "information-gathering/helpers"
)

type NmapResponse = def.NmapResponse

func IteratorNew(result []map[string]string, value string) {

}

// Iterates between all the ips found and submits, starts and retrieve the results of the attack for every of them
func Iterator(client PulsarLib.PL_Client, sessionID string, list []map[string]string, report *gofpdf.Fpdf) *gofpdf.Fpdf {

	listIpPorts := make(map[string][]string)

	for _, item := range list {

		ip := item["ip"]
		report = helpers.AddSubsectionToReport(report, item["ip"]+"  DNS: "+item["dns"])
		report = helpers.AddContentToReport(report, "The following are the traceroute results, showing the network path and the intermediate routers encountered during the journey to the destination host.", false, "")
		report = helpers.AddContentToReport(report, "----------------------------------------------------------------------------------------------", false, "")
		result := utils.TracerouteManager(client, sessionID, item["ip"])

		var i = 1
		for _, item := range result {
			report = helpers.AddContentToReport(report, item, true, strconv.Itoa(i)+" ")
			i++
		}
		i = 0
		report = helpers.AddContentToReport(report, "----------------------------------------------------------------------------------------------", false, "")

		var response NmapResponse
		var table interface{}

		response, table = utils.NmapManager(client, sessionID, ip)

		log.Println("SCAN OF ")
		log.Println("ip: " + ip)
		log.Println("hostname: " + response.NmapRun.Host.HostNames.HostName.Name)

		var performHping3 = false
		if response.NmapRun.Host.Ports.Port != nil {
			report = helpers.AddContentToReport(report, "Presented below are the port scanning results, providing details about protocols, ports, and other pertinent information discovered on the target host or network.", false, "")
			performHping3 = true
		}

		var performNikto = false
		var performNiktoSSL = false
		for _, port := range response.NmapRun.Host.Ports.Port {

			if port.State.State == "open" {

				listIpPorts[ip] = append(listIpPorts[ip], port.PortID, port.Protocol)

				if port.PortID == "80" {
					performNikto = true
				}

				if port.PortID == "443" {
					performNiktoSSL = true
				}
			}
		}

		utils.NmapWriteOnReport(table, report)

		if performNikto {
			report = utils.NiktoManager(client, sessionID, ip, false, report)
		}

		if performNiktoSSL {
			report = utils.NiktoManager(client, sessionID, ip, true, report)
		}

		if performHping3 {
			report = utils.HpingManager(client, sessionID, ip, report)
		}

	}

	return report
}
