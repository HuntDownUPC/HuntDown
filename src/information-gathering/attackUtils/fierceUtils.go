package attackUtils

import (
	"encoding/json"
	"log"
	"strings"

	PulsarLib "gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"

	as "helpers/AttackSchema"
	of "helpers/OptionsFile"
	def "information-gathering/definitions"
	helpers "information-gathering/helpers"
)

type AttackSchema = as.AttackSchema
type AttackList = def.AttackList
type OptionsFile = of.OptionsFile
type FierceResponse = def.FierceResponse

func FierceManager(client PulsarLib.PL_Client, domainName string, sessionID string) map[string]interface{} {

	// to take it from the configured one
	attackNameFierce := "fierce"

	// retrieve the configured attacks to get the _id of the attack i want to start
	getAttacks_json := helpers.CreateGetAttacksJson(sessionID)
	configuredAttacks := helpers.GetAttacksSendRequestSync(client, getAttacks_json)

	var attackList AttackList
	json.Unmarshal(configuredAttacks, &attackList)

	attackFierce, err := helpers.FindAttackByAttackName(attackList, attackNameFierce)

	if err != nil {
		log.Println(err)
	}

	getOptions_json := helpers.CreateGetOptionsJson("fierce01", sessionID)
	fierceOptionsByte := helpers.GetOptionsSendRequestSync(client, getOptions_json)

	var fierceOptionsFile OptionsFile
	err = json.Unmarshal(fierceOptionsByte, &fierceOptionsFile)

	if err != nil {
		log.Println(err)
	}

	var fierceOptionsList []OptionsFile
	fierceOptionsList = append(fierceOptionsList, fierceOptionsFile)

	// edit attackSchema and OptionsFile
	name := "fierce " + domainName
	attackFierce.AttackName = name
	options := "--domain " + domainName
	attackFierce.CliCommands[1].Options = options
	fierceOptionsList[0].Options[0].Value = domainName

	// create submitAttack_json and submit the attack to have it configured
	submitAttack_json := helpers.CreateSubmitAttackJson(sessionID, attackFierce.AttackName, fierceOptionsList, "fierce")
	helpers.SubmitAttackSendRequestSync(client, submitAttack_json)

	// retrieve the configured attacks to get the _id of the attack i want to start
	getAttacks_json = helpers.CreateGetAttacksJson(sessionID)
	configuredAttacks = helpers.GetAttacksSendRequestSync(client, getAttacks_json)

	var attList AttackList
	json.Unmarshal(configuredAttacks, &attList)

	fierceAttack, err := helpers.FindAttackByAttackName(attList, name)

	if err != nil {
		log.Println(err)
	}

	// create startAttack_json and start the attack
	startAttack_json := helpers.CreateStartAttackJson(fierceAttack.AttackName, fierceAttack.Id, sessionID)
	helpers.StartAttackSendAsyncRequest(client, startAttack_json)

	// create getResult_json and get the result
	getResult_json := helpers.CreateGetResultJson1(fierceAttack.AttackName, fierceAttack.Id, sessionID)
	result := helpers.GetResultSendRequestSync(client, getResult_json)

	return result
}

func FierceFindIpList(result map[string]interface{}) ([]map[string]string, string) {

	var response FierceResponse
	var domainName string

	// init ipList
	ipList := []map[string]string{}

	// analyze the result of the attack to extract the ips
	for _, indiv_attack := range result["commandsresult"].([]interface{}) {

		if indiv_attack != nil && indiv_attack.(map[string]interface{})["commandinfo"] != nil {
			domainName = findDomainName(indiv_attack.(map[string]interface{})["commandinfo"].(map[string]interface{}))
		}

		if indiv_attack != nil && indiv_attack.(map[string]interface{})["resultsjson"] != nil {
			resultsjson := indiv_attack.(map[string]interface{})["resultsjson"].(map[string]interface{})

			data, err := json.Marshal(resultsjson)
			if err != nil {
				log.Println(err)
			}

			err = json.Unmarshal(data, &response)
			if err != nil {
				log.Println(err)
			}
		}
	}

	// save the ip addresses in ipList avoiding duplicate
	seenIPs := make(map[string]bool)

	for _, item := range response.Found {
		log.Println("RESPONSE.FOUND")
		log.Println(response.Found)
		if !seenIPs[item.Ip] { // check if the IP address has already been added
			ipMap := make(map[string]string)
			ipMap["ip"] = item.Ip
			ipMap["dns"] = item.DnsName
			ipList = append(ipList, ipMap)
			seenIPs[item.Ip] = true // mark the IP address as seen
			log.Println(item.Ip)
		}
	}

	return ipList, domainName
}

func findDomainName(commandInfo map[string]interface{}) string {

	options := commandInfo["options"].(string)
	domainName := strings.TrimPrefix(options, "--domain ")

	return domainName
}
