package main

import (
	PulsarLib "gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
)

func main() {
	client := PulsarLib.InitClient("pulsar://localhost:6650")
	defer (*client).Close()
	msgtest := []byte("2")
	PulsarLib.SendMessage(client, "attack-implement-options", msgtest)
}
