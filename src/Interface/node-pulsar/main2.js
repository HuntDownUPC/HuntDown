const PulsarLib = require("./PulsarLib/PulsarLib.js");
const fs = require("fs");

(async () => {
    const client = PulsarLib.InitClient("pulsar://localhost:6650")
    msgRequest= {
        Id:"",
        Msg:"hola"
    };
    const res = await PulsarLib.SendRequestSync(client,"test",msgRequest)
    console.log(res)
})();
  