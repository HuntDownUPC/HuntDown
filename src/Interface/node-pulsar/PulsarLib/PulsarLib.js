const Pulsar = require('pulsar-client');

function InitClient(URL){
  const client = new Pulsar.Client({
    serviceUrl: URL,
  });
  return client;
}

async function WaitEvent(client, topic, callback){
  const consumer = await client.subscribe({
    topic: topic,
    subscription: topic,
    subscriptionType: 'Exclusive',
  });
  while (true) {
    const msg = await consumer.receive();
    consumer.acknowledge(msg);
    const res = JSON.parse(msg.getData().toString())
    callback(res)
  }
  await consumer.close();
}

async function SendRequestSync(client, topic, message){
  const id = Math.floor(Math.random() * 10000)
  const topicId = topic + "." + id
  message.Id = topicId
  //Creacio consumer al topic generat
  const consumer = await client.subscribe({
    topic: topicId,
    subscription: topicId,
    subscriptionType: 'Exclusive',
  });
  //Creacio producer
  const producer = await client.createProducer({
    topic: topic,
  });
  //Send message
  const msg=JSON.stringify(message)
  producer.send({
    data: Buffer.from(msg),
  });
  console.log(`Sent message: ${msg}`);
  await producer.flush();
  await producer.close();
  //Recive message
  const res = await consumer.receive();
  consumer.acknowledge(res);
  await consumer.close();
  return res.getData().toString();
}

async function SendRequestAsync(client, topic, message, callback){
  const id = Math.floor(Math.random() * 10000)
  const topicId = topic+"."+id
  message.Id = topicId
  //Creacio consumer al topic generat
  const consumer = await client.subscribe({
    topic: topicId,
    subscription: topicId,
    subscriptionType: 'Exclusive',
  });
  //Creacio producer
  const producer = await client.createProducer({
    topic: topic,
  });
  //Send message
  const msg=JSON.stringify(message)
  producer.send({
    data: Buffer.from(msg),
  });
  console.log(`Sent ASYNC message: ${msg}`);
  await producer.flush();
  await producer.close();
  //Recive message
  const res = await consumer.receive();
  consumer.acknowledge(res);
  await consumer.close();
  callback(res.getData().toString())
}

async function SendMessage(client, topic, message){
  //Creacio producer
  const producer = await client.createProducer({
    topic: topic,
  });
  //Send message
  producer.send({
    data: Buffer.from(message),
  });
  console.log(`Sent message: ${message}`);
  await producer.flush();
  await producer.close();
  console.log("Finished sending message");
}

module.exports = { InitClient, SendRequestAsync, SendRequestSync, SendMessage, WaitEvent};