/*
Control Panel View
------------------

This view is the main view of the application, it is the first view that the user sees when they log in.
It charges the View and the different carousels and other data that might be added.

*/

import React from 'react';
import { observer } from 'mobx-react';
import { Navigate } from "react-router-dom";
import { toast } from "react-toastify";
import Select from 'react-select';

import './App.css';
import SideMenu from './components/SideMenu';
import UserStore from './stores/userStore';
import Carousel from './components/Carousel';
import SearchControlPanel from './components/controlPanelFilters/SearchControlPanel';
import LastAttackCard from './components/controlPanelCards/LastAttackCard';
import PopularAttackCard from './components/controlPanelCards/PopularAttackCard';
import { getAttacksByState, getPopularAttacks, getRemoteSecrets } from './utils/commonUtils';
import SecretStore from './stores/secretStore';


class ControlPanel extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            //original data
            lastAttacksData: [],
            executingAttacksData: [],
            popularAttacksData: [],
            
            //filtered data
            filteredLastAttacksData: ["loading"], //to check if data is being loaded
            searchLastAttacksData: ["loading"],
            searchPopularAttacksData: ["loading"],
            searchExecutingAttacksData: ["loading"],

            //filters default states
            selectedFilters: ["executed", "notExecuted", "error"],
            selectedOrderLastAttacks: "state",
            selectedOrderExecutingAttacks: "newest",
            selectedOrderPopularAttacks: "a-z",
        };

        //filter by state options for last attacks
        this.stateOptions = [
            { label: "Executed", value: "executed" },
            { label: "Not executed", value: "notExecuted" },
            { label: "Error", value: "error" },
        ];

        //sorting options for last attacks cards
        this.sortingOptionsLastAttacks = [
            { label: "From A to Z", value: "a-z"},
            { label: "From Z to A", value: "z-a"},
            { label: "Latest first", value: "newest"},
            { label: "Oldest first", value: "oldest"},
            { label: "By state", value: "state"},
        ];

        //sorting options for popular attacks cards
        this.sortingOptionsPopularAttacks = [
            { label: "From A to Z", value: "a-z"},
            { label: "From Z to A", value: "z-a"},
            { label: "Most executed", value: "mostExecuted"},
            { label: "Most errors", value: "mostError"},
            { label: "Most in execution", value: "mostExecuting"},
        ];

        //styles for the dropdown menus
        this.dropdownStyles = {
            control: (base, state) => ({
                ...base,
                borderRadius: '18px',
                borderColor: state.isFocused ? '#007BFF' : '#ccc',
                boxShadow: state.isFocused ? '0 0 4px rgba(0, 123, 255, 0.5)' : 'none',
                transition: 'border-color 0.3s ease',
                    '&:hover': {
                    borderColor: state.isFocused ? '#007BFF' : '#ccc',
                },
            }),
            option: (base) => ({
                ...base,
                borderRadius: '13px',
            }),
            menu: (base) => ({
                ...base,
                borderRadius: '18px',
            }),
            menuList: (base) => ({
                ...base,
                borderRadius: '18px',
                paddingTop: 0,
                paddingBottom: 0,
            }),
            multiValue: (base) => ({
                ...base,
                borderRadius: '12px',
            }),
            multiValueLabel: (base) => ({
                ...base,
                borderRadius: '12px',
            }),
            multiValueRemove: (base) => ({
                ...base,
                borderRadius: '12px',
            }),
        };          
    }
    
    componentDidMount() {
        //RECOVER SAVED FILTERS
        const savedSelectedFilters = sessionStorage.getItem('selectedFilters');
        const savedSelectedOrderLastAttacks = sessionStorage.getItem('selectedOrderLastAttacks');
        const savedSelectedOrderExecutingAttacks = sessionStorage.getItem('selectedOrderExecutingAttacks');
        const savedSelectedOrderPopularAttacks = sessionStorage.getItem('selectedOrderPopularAttacks');

        if (savedSelectedFilters) {
            this.setState({ selectedFilters: JSON.parse(savedSelectedFilters) });
        }
        if (savedSelectedOrderLastAttacks) {
            this.setState({ selectedOrderLastAttacks: savedSelectedOrderLastAttacks });
        }
        if (savedSelectedOrderExecutingAttacks) {
            this.setState({ selectedOrderExecutingAttacks: savedSelectedOrderExecutingAttacks });
        }
        if (savedSelectedOrderPopularAttacks) {
            this.setState({ selectedOrderPopularAttacks: savedSelectedOrderPopularAttacks });
        }

        //FETCH DATA
        this.fetchLastAttacksData();
        this.fetchPopularAttacksData();
        this.fetchExecutingAttacksData();
    
        //every 5 seconds, the attacks in execution will be updated
        this.executingInterval = setInterval(() => {
            this.fetchExecutingAttacksData();
        }, 5000);
        
        //every 60 seconds, the last and popular attacks will be updated
        this.lastPopularInterval = setInterval(() => {
            this.fetchLastAttacksData();
            this.fetchPopularAttacksData();
        }, 60000);
    }
    
    componentWillUnmount() {
        //clear intervals
        clearInterval(this.executingInterval);
        clearInterval(this.lastPopularInterval);
    }

    //saves the selected filters and sorting options on sessionStorage
    componentDidUpdate(prevProps, prevState) {
        if (prevState.selectedFilters !== this.state.selectedFilters) {
            sessionStorage.setItem('selectedFilters', JSON.stringify(this.state.selectedFilters));
        }
        if (prevState.selectedOrderLastAttacks !== this.state.selectedOrderLastAttacks) {
            sessionStorage.setItem('selectedOrderLastAttacks', this.state.selectedOrderLastAttacks);
        }
        if (prevState.selectedOrderExecutingAttacks !== this.state.selectedOrderExecutingAttacks) {
            sessionStorage.setItem('selectedOrderExecutingAttacks', this.state.selectedOrderExecutingAttacks);
        }
        if (prevState.selectedOrderPopularAttacks !== this.state.selectedOrderPopularAttacks) {
            sessionStorage.setItem('selectedOrderPopularAttacks', this.state.selectedOrderPopularAttacks);
        }
    }
    
    //get data for all executed, not executed and error attacks
    fetchLastAttacksData = async () => {
        const statusArray = ["executed", "notExecuted", "error"];

        const data = await getAttacksByState(statusArray);
        if (data.length === 1 && Object.keys(data[0]).length === 0) {
            toast.error("Failed to fetch Last Attacks data");
        } else {
            const sortedData = this.sortLastAttacks(data, this.state.selectedOrderLastAttacks);
            this.setState({ lastAttacksData: sortedData }, this.updateFilteredData);
        }
    };

    //get data for attacks in state "executing"
    fetchExecutingAttacksData = async () => {
        const statusArray = ["executing"];

        const data = await getAttacksByState(statusArray);
        if (data.length === 1 && Object.keys(data[0]).length === 0) {
            toast.error("Failed to fetch Attacks in Execution data");
        } else {
            const sortedData = this.sortLastAttacks(data, this.state.selectedOrderExecutingAttacks);
            this.setState({ executingAttacksData: sortedData });
        }
    }

    //get data for the popular attacks
    fetchPopularAttacksData = async () => {
        const data = await getPopularAttacks();

        if (data.length === 1 && Object.keys(data[0]).length === 0) {
            toast.error("Failed to fetch Popular Attacks data");
        } else {
            const sortedData = this.sortPopularAttacks(data, this.state.selectedOrderPopularAttacks);
            this.setState({ popularAttacksData: sortedData });
        }
    };

    fetchSecrets = async () => {
        const secrets = await getRemoteSecrets();
        for (const secret of secrets) {
            SecretStore.addSecret(secret.secret_key, secret)
        }
    };

    //returns sorted last attack cards by the options available on sortingOptionsLastAttacks
    sortLastAttacks = (data, sortOption) => {
        let sortedData = [...data];

        switch (sortOption) {
            case "a-z":
                sortedData.sort((a, b) => a.name.localeCompare(b.name));
                break;
            case "z-a":
                sortedData.sort((a, b) => b.name.localeCompare(a.name));
                break;
            case "newest":
                sortedData.sort((a, b) => new Date(b.results.start) - new Date(a.results.start));
                break;
            case "oldest":
                sortedData.sort((a, b) => new Date(a.results.start) - new Date(b.results.start));
                break;
            case "state": { //sorts by state: executed first, then not executed, then error (by priority)
                const statePriority = {
                    executed: 1,
                    notExecuted: 2,
                    error: 3,
                };
    
                sortedData.sort((a, b) => {
                    const priorityA = statePriority[a.state] || 99; //if the state is not recognizes, least priority
                    const priorityB = statePriority[b.state] || 99;
                    return priorityA - priorityB;
                });

                break;
            }
            default:
                toast.info("Invalid sorting option for Last Attack card");
                break;
        }

        return sortedData;
    };

    //returns sorted popular attack cards by the options available on sortingOptionsPopularAttacks
    sortPopularAttacks = (data, sortOption) => {
        let sortedData = [...data];
        
        switch (sortOption) {
            case "a-z":
                sortedData.sort((a, b) => a.name.localeCompare(b.name));
                break;
            case "z-a":
                sortedData.sort((a, b) => b.name.localeCompare(a.name));
                break;
            case "mostExecuted":
                sortedData.sort((a, b) => b.executed - a.executed);
                break;
            case "mostError":
                sortedData.sort((a, b) => b.error - a.error);
                break;
            case "mostExecuting":
                sortedData.sort((a, b) => b.executing - a.executing);
                break;
            default:
                toast.info("Invalid sorting option for Popular Attack card");
                break;
        }

        return sortedData;
    };

    //given an ID, we remove the attack from the lastAttacksData or executingAttacksData to update the view quickly
    removeAttackById = (id) => {
        this.setState((prevState) => {
            let updates = {};
            //if the attack is in lastAttacksData we remove it
            const updatedLastAttacks = prevState.lastAttacksData.filter(attack => attack.id !== id);
            if (updatedLastAttacks.length !== prevState.lastAttacksData.length) {
                updates.lastAttacksData = updatedLastAttacks;

                setTimeout(this.updateFilteredData, 0);
            }
    
            //else if the attack is in executingAttacksData we remove it
            const updatedExecutingAttacks = prevState.executingAttacksData.filter(attack => attack.id !== id);
            if (updatedExecutingAttacks.length !== prevState.executingAttacksData.length) {
                updates.executingAttacksData = updatedExecutingAttacks;

                //remove it also from searchExecutingAttacksData
                /*updates.searchExecutingAttacksData = updatedExecutingAttacks.filter((attack) =>
                    Object.values(attack).some((value) =>
                        String(value).toLowerCase().includes(prevState.searchQuery || "")
                    )
                );*/
            }
    
            return updates;
        });
    };

    //updates filteredLastAttacksData with the selected state filters on selectedFilters
    updateFilteredData = () => {
        const { lastAttacksData, selectedFilters } = this.state;

        const filteredData = lastAttacksData.filter((attack) =>
            selectedFilters.includes(attack.state)
        );

        this.setState({ filteredLastAttacksData: filteredData });
    };
    
    render() {
        if (UserStore.isLoggedIn === false && UserStore.loading === false) { //TODO FIX
            // alert("Your session has expired or you have logged out, please log in again");
            return <Navigate to='/' />;
        }

        this.fetchSecrets();

        return (
            <div className='menu-container'>
                <div className="grid">
                    <SideMenu />
                    <div className="control-panel-view">
                        <div className="carousel-section">
                            <div className="carousel-header">
                                <div>
                                    <h1>Last Attacks</h1>
                                </div>
                                <div className="control-panel-filters">
                                    <SearchControlPanel
                                        data={this.state.filteredLastAttacksData}
                                        onChange={(filteredData) =>
                                            this.setState({ searchLastAttacksData: filteredData })
                                        }
                                    />
                                    
                                    <Select
                                        styles={this.dropdownStyles}
                                        options={this.stateOptions}
                                        value={this.stateOptions.filter(option => this.state.selectedFilters.includes(option.value))}
                                        onChange={(selectedOptions) => {
                                            const selectedFilters = selectedOptions ? selectedOptions.map(option => option.value) : [];
                                            this.setState({ selectedFilters }, this.updateFilteredData);
                                        }}
                                        isMulti={true}
                                        closeMenuOnSelect={false}
                                        placeholder="Select States"
                                    />

                                    <Select
                                        styles={this.dropdownStyles}
                                        options={this.sortingOptionsLastAttacks}
                                        value={this.sortingOptionsLastAttacks.find(option => option.value === this.state.selectedOrderLastAttacks)}
                                        onChange={(selectedOption) => {
                                            this.setState({ selectedOrderLastAttacks: selectedOption ? selectedOption.value : '' }, () => {
                                                const sortedData = this.sortLastAttacks(this.state.lastAttacksData, this.state.selectedOrderLastAttacks);
                                                this.setState({ lastAttacksData: sortedData }, this.updateFilteredData);
                                            });
                                        }}
                                        placeholder="Order By"
                                    />
                                </div>
                            </div>
                            <Carousel
                                data={this.state.searchLastAttacksData}
                                CardComponent={LastAttackCard}
                                removeAttack={this.removeAttackById}
                            />
                        </div>

                        <div className="carousel-section">
                            <div className="carousel-header">
                                <div>
                                    <h1>Attacks in Execution</h1>
                                </div>
                                <div className="control-panel-filters">
                                    <SearchControlPanel
                                        data={this.state.executingAttacksData}
                                        onChange={(filteredData) =>
                                            this.setState({ searchExecutingAttacksData: filteredData })
                                        }
                                    />
                                    
                                    <Select
                                        styles={this.dropdownStyles}
                                        options={this.sortingOptionsLastAttacks}
                                        value={this.sortingOptionsLastAttacks.find(option => option.value === this.state.selectedOrderExecutingAttacks)}
                                        onChange={(selectedOption) => {
                                            this.setState({ selectedOrderExecutingAttacks: selectedOption ? selectedOption.value : '' }, () => {
                                                const sortedData = this.sortLastAttacks(this.state.executingAttacksData, this.state.selectedOrderExecutingAttacks);
                                                this.setState({ executingAttacksData: sortedData });
                                            });
                                        }}
                                        placeholder="Order By"
                                    />
                                </div>
                            </div>
                            <Carousel
                                data={this.state.searchExecutingAttacksData}
                                CardComponent={LastAttackCard}
                                removeAttack={this.removeAttackById}
                            />
                        </div>

                        <div className="carousel-section">
                            <div className="carousel-header">
                                <div>
                                    <h1>Popular Attacks</h1>
                                </div>
                                <div className="control-panel-filters">
                                    <SearchControlPanel
                                        data={this.state.popularAttacksData}
                                        onChange={(filteredData) =>
                                            this.setState({ searchPopularAttacksData: filteredData })
                                        }
                                    />

                                    <Select
                                        styles={this.dropdownStyles}
                                        options={this.sortingOptionsPopularAttacks}
                                        value={this.sortingOptionsPopularAttacks.find(option => option.value === this.state.selectedOrderPopularAttacks)}
                                        onChange={(selectedOption) => {
                                            this.setState({ selectedOrderPopularAttacks: selectedOption ? selectedOption.value : '' }, () => {
                                                const sortedData = this.sortPopularAttacks(this.state.popularAttacksData, this.state.selectedOrderPopularAttacks);
                                                this.setState({ popularAttacksData: sortedData});
                                            });
                                        }}
                                        placeholder="Order By"
                                    />
                                </div>
                            </div>
                            <Carousel
                                data={this.state.searchPopularAttacksData}
                                CardComponent={PopularAttackCard}
                            />
                        </div>
                        <div
                            style={{height: '50px'}}>  
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default observer(ControlPanel);
