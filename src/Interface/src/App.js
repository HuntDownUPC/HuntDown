import React from 'react';
import { runInAction, makeAutoObservable } from "mobx";
import { observer } from 'mobx-react';
import UserStore from './stores/userStore';
import LoginForm from './components/login/LoginForm';
import './App.css';
import { Navigate } from "react-router-dom";


class App extends React.Component {

  async componentDidMount() {

    try {
      let res = await fetch('/isLoggedIn', {
        method: 'post',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      });
      let result = await res.json();

      if (result && result.success) {
        runInAction(() => {
          UserStore.loading = false;
          UserStore.isLoggedIn = true;
          UserStore.username = result.username;
          UserStore.admin = result.admin;
        });
      }
      else {
        runInAction(() => {
          UserStore.loading = false;
          UserStore.isLoggedIn = false;
        });
      }
    }
    catch (e) {
      runInAction(() => {
        UserStore.loading = false;
        UserStore.isLoggedIn = false;
      });
    }

  }
  render() {
    if (UserStore.loading === false && UserStore.isLoggedIn) {
      return <Navigate to='/controlPanel' />;
    }
    return (
      <div className="app">
        <LoginForm />
      </div>
    );
  }
}

export default observer(App);
