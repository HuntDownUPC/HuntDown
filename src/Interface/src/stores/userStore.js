import { extendObservable } from "mobx";

class UserStore {

    constructor(){

        extendObservable( this, {
            loading: true,
            isLoggedIn: false,
            username: '',
            session_id: '',
            admin: "false"
        })
    }
}

export default new UserStore();