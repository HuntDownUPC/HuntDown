//import result from "./results.json";
import React from "react";
import base64 from "react-native-base64";
import Accordion from 'react-bootstrap/Accordion'
import Button from 'react-bootstrap/Button'

function ResultsView(props) {
  var result = props.posts
  var outputtext = "";
  var splitted_results = [result.commandsresult.length];

  result.commandsresult.map((result, index) => {
    var simpletext = "";
    if (index !== 0) {
      outputtext += "\n";
    }
    outputtext +=
      "<--------------- RESULTS FOR " +
      result.commandinfo.action +
      " " +
      result.commandinfo.options +
      " --------------->\n\n";
    simpletext = base64.decode(result.resultsoutput);
    splitted_results[index] = simpletext;
    outputtext += simpletext;
    outputtext += "\n\n";
  });
  const downloadTxtFile = () => {
    const element = document.createElement("a");
    const file = new Blob([outputtext], {
      type: "text/plain",
    });
    element.href = URL.createObjectURL(file);
    element.download = "myFile.txt";
    document.body.appendChild(element);
    element.click();
  };
  const [finaltext, setfinaltext] = React.useState();
  return (
    <div id="results-view">
      <div id="results-title">
        <h2 id="results-title-text">
          {" "}
          <b>Attack Results</b>
        </h2>
      </div>
      {result.commandsresult.map((result, index) => {
       if(result.table) {
          var table = result.table;
          return (
            <div key={"tables" + index + result.commandinfo.optionsid}>
              <div className="table-title-container" key={"title-container" + index + result.commandinfo.optionsid}>
                <p
                  className="table-title-text"
                  key={"title-text" + index + result.commandinfo.optionsid}
                >
                  {"Results Table of: " +
                    result.commandinfo.action +
                    " " +
                    result.commandinfo.options}{" "}
                </p></div>
              <div className="table-container" key={"table-container" + index + result.commandinfo.optionsid}>
                <div className="table" key={index + result.commandinfo.optionsid}>
                  {table.map((extitem, index) => {
                    return extitem.item.row.map((column, index3) => {
                      return (
                        <div
                          className="customrow"
                          key={"row" + index3 + result.commandinfo.optionsid}
                        >
                          {column.columns.map((atom, index4) => {
                            var boolStyle = atom.color === "true" ? true : false;
                            if (boolStyle) {
                              return (
                                <div
                                  className="column"
                                  key={
                                    "col" + index4 + result.commandinfo.optionsid
                                  }
                                  style={{
                                    backgroundColor: "#CC544F",
                                    textAlign: "center",
                                    textWeight: "bold",
                                    color: "white",
                                  }}
                                >
                                  {atom.value}
                                </div>
                              );
                            } else {
                              return (
                                <div
                                  className="column"
                                  key={
                                    "col" + index4 + result.commandinfo.optionsid
                                  }
                                >
                                  {atom.value}
                                </div>
                              );
                            }
                          })}
                        </div>
                      );
                    });
                  })}
                </div>
              </div>
            </div>
          );
        }
      })}

      {result.commandsresult.map((result, index) => {
        return (
          <Accordion
            defaultActiveKey="0"
            key={index}
            className="results-accordion"
          >
            <Accordion.Item eventKey={0} bsPrefix="results_custom_accordion">
              <Accordion.Header className="results-header">
                {"Output of: " +
                  result.commandinfo.action +
                  " " +
                  result.commandinfo.options}
              </Accordion.Header>
              <Accordion.Body className="results_custom_accordion_body">
                <div id="resultsText">{splitted_results[index]}</div>
              </Accordion.Body>
            </Accordion.Item>
          </Accordion>
        );
      })}
      <div>
        <Button
          className="download-results"
          onClick={downloadTxtFile}
          id="downloadResults"
        >
          Download Results
        </Button>
      </div>
    </div>
  );
}

export default ResultsView;
