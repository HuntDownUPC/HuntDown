import React, { useEffect, useState } from 'react';
import ResultsView from './ResultsView.js';
import { useNavigate } from "react-router-dom";
import { Progress } from 'react-sweet-progress';
import { toast } from "react-toastify";
import { getAttackInfo, getAttacksByState } from '../utils/commonUtils';
import "react-sweet-progress/lib/style.css";

export default function Results({ posts }) {
    const [click, setClick] = useState(false)
    const [data, setData] = useState();
    const [resultsList, setResultsList] = useState([])
    const WAIT_TIME = 5000;
    const navigate = useNavigate();

    useEffect(() => {
        setResultsList(posts);
    }, [posts]);
    
    useEffect(() => {
        const id = setInterval(async () => {
            const statusArray = ["executed", "executing", "error"];
            const data = await getAttacksByState(statusArray);
            if (data) {
                setResultsList(data);
            } else {
                toast.error("Failed to fetch attack results information");
            }
        }, WAIT_TIME);
        return () => clearInterval(id);
    }, [resultsList]);

    const getAttacks = async (id) => {
        try {
            const attackInfo = await getAttackInfo(id);
            console.log(attackInfo);
            if (attackInfo && attackInfo.results) {
                if (!attackInfo.results.commandsresult) {
                    toast.error("The selected attack does not have any results");
                } else {
                    sessionStorage.setItem('attackResults', JSON.stringify(attackInfo.results));
                    navigate('/information');
                }
            } else {
                toast.error("Failed to fetch attack information or results are missing");
            }
        } catch (error) {
            toast.error("An error occurred while getting attack information:", error);
        }
    };

    return (
        <div style={{ overflowY: 'auto' }}>
            <nav>
                <ul id="results">
                    {resultsList.map((resultElement) => (
                        <div 
                            key={resultElement.id}
                            id="attackBoxContainer"
                            className={resultElement.state}
                            onClick={() => getAttacks(resultElement.id)}
                        >    
                            <div id="attackBox">
                                <div id="nameBox">
                                    {resultElement.name}
                                </div>
                                <Progress
                                    theme={{
                                        default: {
                                            symbol: resultElement.percent+"%",
                                            trailColor: 'rgb(141, 153, 174,0.5)',
                                            color: '#2B2D42'
                                        }
                                    }}
                                    percent={resultElement.percent}
                                    status={resultElement.status}
                                />
                                <div id="stepBox">
                                    {resultElement.step}
                                </div>
                            </div>
                        </div>
                    ))}
                </ul>
            </nav>
        </div >
    );
}