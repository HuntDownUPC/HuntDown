import React from 'react';
import { Navbar, Nav, Container, NavDropdown } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import '../../App.css';
import UserStore from '../../stores/userStore'
import { runInAction, makeAutoObservable } from "mobx";

class Navbarr extends React.Component {

  async doLogout() {
    
    try {
      let res = await fetch('/logout', {
        method: 'post',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'

        }
      });
      let result = await res.json();

      if (result && result.success) {
        runInAction(() => {
          UserStore.isLoggedIn = false;
          UserStore.username = '';
          UserStore.admin = false;
        });
      }

    }
    catch (e) {
      console.log(e);
    }

  }

  render(){
    return (
        <>
            <Navbar collapseOnSelect fixed='top' expand='lg' bg='light' variant='light' >
                <Container fluid >
                    <Navbar.Brand href="/dashboard">CPAnalyzer</Navbar.Brand>
                    <Navbar.Toggle aria-controls="navbar-dark-example" />
                    <Navbar.Collapse  id='navbar-dark-example'>
                    <Nav >
                     <NavDropdown  id="nav-dropdown-dark"  title="MENU" menuVariant="light" >
                        <NavDropdown.Item href="/instmaquina">Instantiate Machine</NavDropdown.Item>
                        <NavDropdown.Item href="/attack">New Attack</NavDropdown.Item>
                        <NavDropdown.Item href="/result">Result History</NavDropdown.Item>
                        <NavDropdown.Item href="/" onClick={() => this.doLogout() }>Logout</NavDropdown.Item>
                    </NavDropdown>
                    </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </>
)};
}

export default Navbarr;