import { runInAction, makeAutoObservable } from "mobx"
import React from "react";
import UserStore from "../stores/userStore";
import "../App.css";
import { Button } from "react-bootstrap";
import house from "../images/house.png";
import copy from "../images/copy.png";
import engranaje from "../images/engranaje.png";
import logout from "../images/logout.png";
import sword from "../images/sword.png";
import chain from "../images/chain.png";
import userStore from "../stores/userStore";
import admin from "../images/admin.png"
import back from "../images/back.png"
import key from "../images/llave-inglesa.png"

function refreshPage() {
  window.location.reload(false);
}

class SideMenu extends React.Component {
  async doLogout() {
    try {
      let res = await fetch("/logout", {
        method: "post",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      });
      let result = await res.json();

      if (result && result.success) {
        runInAction(() => {
          UserStore.isLoggedIn = false;
          UserStore.loading = false;
          UserStore.username = "";
          UserStore.session_id = "";
          UserStore.admin = "false";
        });
      }
    } catch (e) {
      console.log(e);
    }
  }

  render() {
    if (userStore.admin === "false"){
      return (
        <div className="menu">
          <p className="brand">HuntDown</p>
          <hr id="menuhr" />
          <Button className="btnside" href="/#/controlPanel">
            <img className="house_logo" src={house}></img>
            <p className="pbtnside">Control Panel</p>
          </Button>
          <Button className="btnside" /*onClick={refreshPage} */ href="/#/attack">
            <img className="sword_logo" src={sword}></img>
            <p className="pbtnside">New Attack</p>
          </Button>
          <Button className="btnside" /*onClick={refreshPage} */ href="/#/chainedAttack">
            <img className="chain_logo" src={chain}></img>
            <p className="pbtnside">Chained Attack</p>
          </Button>
          <Button className="btnside" /*onClick={refreshPage} */ href="/#/configuredAttacks" >
            <img className="engranaje_logo" src={engranaje}></img>
            <p className="pbtnside">Configured Attacks</p>
          </Button>
          <Button className="btnside" /*onClick={refreshPage} */ href="/#/result">
            <img className="copy_logo" src={copy}></img>
            <p className="pbtnside">Result History</p>
          </Button>

          <Button
            className="btnside"
            /*onClick={refreshPage} */ href="/#/configuredAttacks"
          >
            <img className="engranaje_logo" src={engranaje}></img>
            <p className="pbtnside">Configured Attacks</p>
          </Button>
          <Button
            className="btnside"
            /*onClick={refreshPage} */ href="/#/configuredAttacks"
          >
            <img className="engranaje_logo" src={key}></img>
            <p className="pbtnside">Settings</p>
          </Button>
          <Button className="btnside" /*onClick={refreshPage} */ href="/#/">
            <img className="admin_logo" src={back}></img>
            <p className="pbtnside">To Main Screen</p>
          </Button>
          
           <a className="btn3" href="/" onClick={() => this.doLogout()}>
            {" "}
            <img className="logout_logo" src={logout}></img>
            Logout
          </a>
        
        </div>
      );
    }else{
      return (
        <div className="menu">
          <p className="brand">HuntDown</p>
          <hr id="menuhr" />
          <Button className="btnside" href="/#/controlPanel">
            <img className="house_logo" src={house}></img>
            <p className="pbtnside">Control Panel</p>
          </Button>
          <Button className="btnside" /*onClick={refreshPage} */ href="/#/attack">
            <img className="sword_logo" src={sword}></img>
            <p className="pbtnside">New Attack</p>
          </Button>
          <Button className="btnside" /*onClick={refreshPage} */ href="/#/chainedAttack">
            <img className="chain_logo" src={chain}></img>
            <p className="pbtnside">Chained Attack</p>
          </Button>
          <Button className="btnside" /*onClick={refreshPage} */ href="/#/configuredAttacks" >
            <img className="engranaje_logo" src={engranaje}></img>
            <p className="pbtnside">Configured Attacks</p>
          </Button>
          <Button className="btnside" /*onClick={refreshPage} */ href="/#/result">
            <img className="copy_logo" src={copy}></img>
            <p className="pbtnside">Result History</p>
          </Button>
          <Button className="btnside" /*onClick={refreshPage} */ href="/#/admin">
            <img className="admin_logo" src={admin}></img>
            <p className="pbtnside">Admin</p>
          </Button>
          <Button
            className="btnside"
            /*onClick={refreshPage} */ href="/#/setting"
          >
            <img className="engranaje_logo" src={key}></img>
            <p className="pbtnside">Settings</p>
          </Button>
          <Button className="btnside" /*onClick={refreshPage} */ href="/#/">
            <img className="admin_logo" src={back}></img>
            <p className="pbtnside">To Main Screen</p>
          </Button>

          <a className="btn3" href="/" onClick={() => this.doLogout()}>
            {" "}
            <img className="logout_logo" src={logout}></img>
            Logout
          </a>
        
        </div>
      );
    
    }
  }
}

export default SideMenu;
