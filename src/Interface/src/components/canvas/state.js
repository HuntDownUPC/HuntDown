import { createStore } from "@halka/state";
import produce from "immer";
import { toast } from "react-toastify";
import base64 from "react-native-base64";
const { customAlphabet } = require("nanoid");
const nanoid = customAlphabet("1234567890", 12);

const baseState = {
  selected: null,
  selectedConection: null,
  shapes: {},
  connections: {},
};

const setState = (fn) => useShapes.set(produce(fn));

export const useShapes = createStore(() => {
  return { ...baseState, shapes: {}, connections: {} };
});

//** Workflow Manager */

export function loadNewConstruct(jsonworkflow) {
  console.log(jsonworkflow);
  if (jsonworkflow !== null) {
    const initialShapes = {};
    const initialconnections = {};
    const {
      entry_point_data,
      attacks_data,
      conditional_data,
      connections_data,
    } = jsonworkflow;

    /* Load attacks */
    attacks_data.forEach((attack) => {
      const id = attack.attack_id;
      initialShapes[id] = {
        type: "attackRectangle",
        width: 150,
        height: 100,
        fill: "#ffffff",
        stroke: "#000000",
        attack: attack.attack,
        attackNumber: attack.attackNumber,
        x: attack.position.x,
        y: attack.position.y,
        preset: attack.preset,
        inputs: attack.input,
        output: attack.attack + "_" + attack.attackNumber + "_output",
      };
    });

    /* Load entryPointData */
    entry_point_data.forEach((entryPoint) => {
      console.log(entryPoint.entry_point_id);
      console.log(entryPoint.entry_point_inputs);
      const id = entryPoint.entry_point_id;
      const inputs = entryPoint.entry_point_inputs;
      console.log(inputs.map((input) => input.dataKey));
      const position = entryPoint.position_entry_point;
      initialShapes[id] = {
        type: "entryPointCircle",
        radius: 50,
        fill: "#ffffff",
        stroke: "#000000",
        text: "Entry Point",
        x: position.x,
        y: position.y,
        keys: inputs.map((input) => input.dataKey),
        values: inputs.map((input) => input.value),
      };
    });

    /* Load conditionals */
    conditional_data.forEach((conditional) => {
      const id = conditional.conditional_id;
      initialShapes[id] = {
        type: "conditionRhombus",
        width: 150,
        height: 100,
        fill: "#ffffff",
        stroke: "#000000",
        text: "Cond",
        x: conditional.position.x,
        y: conditional.position.y,
        outputName: conditional.output_name,
        dataKey: conditional.data_key,
        valueToCompare: conditional.value_to_compare,
        valueToCompareManual: conditional.value_to_compare_manual,
        operator: conditional.operator,
        next_if_true: conditional.next_if_true,
      };
    });

    /* Load connections */
    connections_data.forEach((connection) => {
      const id = connection.conditional_id;
      initialconnections[id] = {
        key: id,
        from: connection.from_id,
        to: connection.to_id,
        fromPos: connection.from_pos,
        toPos: connection.to_pos,
        fromType: connection.from_type,
        toType: connection.to_type,
      };
    });

    setState((state) => {
      state.connections = initialconnections;
      state.shapes = initialShapes;
    });
  }
}

export async function saveConstruct(textFieldValue) {
  if (textFieldValue === "") {
    toast.error("Enter a valid name", {
      position: "top-right",
      autoClose: 2000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false,
      draggable: true,
      progress: undefined,
      theme: "light",
    });
  } else {
    var connections = useShapes.get().connections;
    var shapes = useShapes.get().shapes;
    var construct = {
      construct_id: null,
      construct_name: null,
      entry_point_data: [],
      attacks_data: [],
      conditional_data: [],
      connections_data: [],
    };
    construct.construct_name = textFieldValue;
    construct.construct_id = nanoid();

    for (var key in shapes) {
      if (shapes[key].type === "entryPointCircle") {
        var entrypointInput = [];
        for (var i = 0; i < shapes[key].keys.length; i++) {
          var endpointInput = { dataKey: null, value: null };
          endpointInput.dataKey = shapes[key].keys[i];
          endpointInput.value = shapes[key].values[i];
          entrypointInput.push(endpointInput);
        }
        var entryPoint = {};
        entryPoint["entry_point_id"] = key;
        entryPoint["entry_point_inputs"] = entrypointInput;
        entryPoint["position_entry_point"] = {
          x: shapes[key].x,
          y: shapes[key].y,
        };
        construct.entry_point_data.push(entryPoint);
      } else if (shapes[key].type === "attackRectangle") {
        var attack = {};
        attack["attack_id"] = key;
        attack["attack"] = shapes[key].attack;
        attack["attackNumber"] = shapes[key].attackNumber;
        attack["position"] = { x: shapes[key].x, y: shapes[key].y };
        attack["preset"] = shapes[key].preset;
        attack["input"] = shapes[key].inputs;
        construct.attacks_data.push(attack);
      } else if (shapes[key].type === "conditionRhombus") {
        var conditional = {};
        if (shapes[key].text === "Cond") {
          console.log(shapes[key]);
          console.log(shapes[key].dataKey);
          conditional["conditional_id"] = key;
          conditional["position"] = { x: shapes[key].x, y: shapes[key].y };
          conditional["output_name"] = shapes[key].outputName;
          conditional["data_key"] = shapes[key].dataKey;
          conditional["value_to_compare"] = shapes[key].valueToCompare;
          conditional["value_to_compare_manual"] =
            shapes[key].valueToCompareManual;
          conditional["operator"] = shapes[key].operator;
          conditional["next_if_true"] = shapes[key].next_if_true;
          console.log(conditional);
          construct.conditional_data.push(conditional);
        } else if (shapes[key].text === "Iterate") {
          console.log(shapes[key]);
          console.log(shapes[key].dataKey);
          conditional["conditional_id"] = key;
          conditional["position"] = { x: shapes[key].x, y: shapes[key].y };
          conditional["output_name"] = shapes[key].outputName;
          conditional["data_key"] = shapes[key].dataKey;
          conditional["value_to_compare"] = shapes[key].valueToCompare;
          conditional["value_to_compare_manual"] =
            shapes[key].valueToCompareManual;
          conditional["operator"] = shapes[key].operator;
          conditional["next_if_true"] = shapes[key].next_if_true;
          console.log(conditional);
          construct.conditional_data.push(conditional);
        }
        
      } 
    }
      console.log(connections)
      console.log("connections")
    for (var connectionKey in connections) {
      console.log(connectionKey)
      console.log("connectionKey")
      var connection = {};
      connection["conditional_id"] = connectionKey;
      connection["from_id"] = connections[connectionKey].from;
      connection["to_id"] = connections[connectionKey].to;
      connection["from_pos"] = connections[connectionKey].fromPos;
      connection["to_pos"] = connections[connectionKey].toPos;
      connection["from_type"] = connections[connectionKey].fromType;
      connection["to_type"] = connections[connectionKey].toType;
      construct.connections_data.push(connection);
    }
    console.log(construct);
    await saveConstructBackend(construct);
    toast.success("Construct saved", {
      position: "top-right",
      autoClose: 2000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false,
      draggable: true,
      progress: undefined,
      theme: "light",
    });
  }
}

async function saveConstructBackend(construct) {
  try {
    const encodedConstruct = base64.encode(JSON.stringify(construct)); //Encode construct to base64 for backend request
    await fetch("/saveConstruct", {
      method: "post",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        construct: encodedConstruct,
      }),
    });
  } catch (e) {
    console.log(e);
  }
}

export function resetWorkflow() {
  toast.success("Workflow reseted", {
    position: "top-right",
    autoClose: 4000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: false,
    draggable: true,
    progress: undefined,
    theme: "light",
  });
  useShapes.set(baseState);
}

export async function executeWorkflow(textFieldValue) {
  if (textFieldValue === "") {
    toast.error("Enter a valid name", {
      position: "top-right",
      autoClose: 2000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false,
      draggable: true,
      progress: undefined,
      theme: "light",
    });
  } else {
    if (!validateWorkflowForExecution()) {
      return;
    }
    var connections = useShapes.get().connections;
    var shapes = useShapes.get().shapes;
    var workflow = {
      workflow_name: null,
      workflow_description: "",
      attack_entry_point: null,
      attack_workflow: [],
    };
    workflow.workflow_name = textFieldValue;
    var entryPoint = null;
    for (let key in shapes) {
      if (shapes[key].type === "entryPointCircle") {
        entryPoint = shapes[key];
        workflow.attack_entry_point = parseInt(
          Object.values(connections).find(
            (connection) => connection.from === key
          ).to
        );
        break;
      }
    }
    for (var key in shapes) {
      if (shapes[key].type === "attackRectangle") {
        var attack = {};
        attack["block_id"] = parseInt(key);
        attack["attack_name"] = shapes[key].attack;
        attack["preset"] = shapes[key].preset;
        attack["output"] = shapes[key].output;

        const childConnection = Object.values(connections).find(
          (connection) => connection.from === key
        );

        if (
          childConnection !== undefined &&
          shapes[childConnection.to].type === "conditionRhombus"
        ) {
          attack["next_block"] = parseInt(
            shapes[childConnection.to].next_if_true
          ); //If the next block is a conditional, the next block is the true block
        } else if (childConnection !== undefined) {
          attack["next_block"] = parseInt(childConnection.to); //If the next block is not a conditional, the next block is the next block
        } else {
          attack["next_block"] = -1; //If there is no next block, the next block is the end of the workflow
        }

        const relatedConditionalConexion = Object.values(connections).find(
          (connection) =>
            connection.to === key && connection.fromType === "conditionRhombus" //Find the conditional that is parent to the current block
        );
        console.log(relatedConditionalConexion)
        console.log("relatedConditionalConexion")

        if (
          relatedConditionalConexion !== undefined &&
          shapes[relatedConditionalConexion.from].next_if_true === key //If the current block is the true block of the conditional
        ) {
          const elseConditionalConexion = Object.values(connections).find(
            (connection) =>
              connection.to !== key &&
              connection.from === relatedConditionalConexion.from
          );

          console.log("elseConditionalConexion")
          console.log(elseConditionalConexion)

          attack["next_block"] =
            elseConditionalConexion === undefined
              ? -1
              : parseInt(elseConditionalConexion.to); //The next block is the false block of the conditional
          
          console.log("attack (next block)")
          console.log(attack["next_block"])
          var conditional = {};
          console.log("shapes[relatedConditionalConexion.from]")
          console.log(shapes[relatedConditionalConexion.from])

          if (shapes[relatedConditionalConexion.from].text === "Cond"){
            conditional["OutputName"] =
            shapes[relatedConditionalConexion.from].outputName;
            conditional["Datakey"] =
            shapes[relatedConditionalConexion.from].dataKey;
            conditional["condition"] =
            shapes[relatedConditionalConexion.from].operator;
            conditional["if_true"] =
            childConnection === undefined ? -1 : undefined ? -1 : parseInt(childConnection.to); //The true block of the conditional is the next block
            
          if (
            shapes[relatedConditionalConexion.from].valueToCompare ===
            "Manual Input"
          ) {
            conditional["op"] =
              shapes[relatedConditionalConexion.from].valueToCompareManual;
          } else {
            for (let i = 0; i < entryPoint.keys.length; i++) {
              //Find the value of the entry point that is the value to compare of the conditional
              if (
                shapes[relatedConditionalConexion.from].valueToCompare ===
                entryPoint.keys[i]
              ) {
                conditional["op"] = entryPoint.values[i];
                break;
              }
            }
          }
            attack["execute_if"] = conditional;
          } else if (shapes[relatedConditionalConexion.from].text === "Iterate") {
            conditional["OutputName"] =
            shapes[relatedConditionalConexion.from].outputName;
            console.log("childConnection")
            console.log(childConnection)
            /*
            conditional["if_true"] =
            childConnection === undefined ? -1 : parseInt(childConnection.to); //The true block of the conditional is the next block
            */
            if (childConnection === undefined)             
              conditional["if_true"] = -1 
              else {
                const childConnectionTo = Object.values(connections).find(
                  (connection) => connection.from === childConnection.to
                );
                conditional["if_true"] = parseInt(childConnectionTo.to)
              }
            attack["iterate_over"] = conditional;
          }
        }

        var inputList = {};
        for (let i = 0; i < shapes[key].inputs.length; i++) {
          var input = {};
          if (shapes[key].inputs[i].value === "Manual Input") {
            input["value"] = shapes[key].inputs[i].manualInput;
          } else {
            var isEntryPointInput = false;
            for (let j = 0; j < entryPoint.keys.length; j++) {
              if (shapes[key].inputs[i].value === entryPoint.keys[j]) {
                input["value"] = entryPoint.values[j];
                isEntryPointInput = true;
                break;
              }
            }
            if (!isEntryPointInput) {
              input[shapes[key].inputs[i].value] = shapes[key].inputs[i].value;
            }
          }
          inputList[shapes[key].inputs[i].name] = input;
        }
        attack["input"] = inputList;
        workflow.attack_workflow.push(attack);
      }
    }
    toast.success("Workflow started execution", {
      position: "top-right",
      autoClose: 4000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false,
      draggable: true,
      progress: undefined,
      theme: "light",
    });
    await executeWorkflowBackend(workflow);
    toast.success("Workflow finished", {
      position: "top-right",
      autoClose: 4000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false,
      draggable: true,
      progress: undefined,
      theme: "light",
    });
  }
}

async function executeWorkflowBackend(workflow) {
  const encodedWorkflow = base64.encode(JSON.stringify(workflow));
  try {
    let res = await fetch("/saveWorkflow", {
      method: "post",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        workflow: encodedWorkflow,
      }),
    });

    let result = await res.json();
    if (result && !result.success && "error" in result) {
      toast.error("Error saving workflow for execution", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
      return;
    }

    let resex = await fetch("/startWorkflow", {
      method: "post",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        workflowID: result.msg,
      }),
    });
    let resultex = await resex.json();
    if (resultex && !resultex.success && "error" in resultex) {
      toast.error("Error executing workflow", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
      return;
    }
  } catch (e) {
    throw e;
  }
}

function validateWorkflowForExecution() {
  var connections = useShapes.get().connections;
  var shapes = useShapes.get().shapes;
  if (
    !everyShapConnected(connections, shapes) ||
    !minimumEntryPointAndAttack(shapes) ||
    !noShapeWithoutRequiredValues(shapes)
  ) {
    return false;
  }
  return true;
}

function everyShapConnected(connections, shapes) {
  // Check that every shape is connected
  const shapeIds = Object.keys(shapes);
  const connectedIds = new Set();

  // Add the starting shape
  connectedIds.add(shapeIds[0]);

  // Traverse the connections and add all connected shapes
  Object.values(connections).forEach(({ from, to }) => {
    if (connectedIds.has(from)) {
      connectedIds.add(to);
    } else if (connectedIds.has(to)) {
      connectedIds.add(from);
    }
  });
  console.log(shapeIds.every((id) => connectedIds.has(id)));
  // Check that all shapes are connected
  if (!shapeIds.every((id) => connectedIds.has(id))) {
    toast.error("All elements should be connected", {
      position: "top-right",
      autoClose: 2000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false,
      draggable: true,
      progress: undefined,
      theme: "light",
    });
    return false;
  }
  return true;
}

function minimumEntryPointAndAttack(shapes) {
  if (
    !Object.values(shapes).some((shape) => shape.type === "entryPointCircle") &&
    Object.values(shapes).some((shape) => shape.type === "attackRectangle")
  ) {
    toast.error("Not enough elements", {
      position: "top-right",
      autoClose: 4000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false,
      draggable: true,
      progress: undefined,
      theme: "light",
    });
    return false;
  }
  return true;
}

function noShapeWithoutRequiredValues(shapes) {
  if (
    !Object.values(shapes).every(
      (shape) =>
        (shape.type === "attackRectangle" && shape.preset !== null) ||
        (shape.type === "conditionRhombus" && shape.outputName !== null) ||
        shape.type === "entryPointCircle"
    )
  ) {
    toast.error("Elements can't have null values", {
      position: "top-right",
      autoClose: 2000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false,
      draggable: true,
      progress: undefined,
      theme: "light",
    });
    return false;
  }
  return true;
}

//** Create Shapes */

export function createAttackRectangle({ x, y, attack }) {
  setState((state) => {
    const values = Object.values(state.shapes).filter(
      (shape) => shape.attack === attack
    );
    for (
      var attackNumber = 1;
      attackNumber < values.length + 1;
      attackNumber++
    ) {
      //Assign the first available attackNumber
      const hasAttackNumber = values.some(
        (shape) => shape.attackNumber === attackNumber
      );
      if (!hasAttackNumber) {
        break;
      }
    }
    state.shapes[nanoid()] = {
      type: "attackRectangle",
      width: 150,
      height: 100,
      fill: "#ffffff",
      stroke: "#000000",
      isMoving: false,
      attack: attack,
      attackNumber: attackNumber,
      x: x,
      y: y,
      preset: null,
      inputs: [],
      output: attack + "_" + attackNumber + "_output",
    };
  });
}

export function createEntryPointCircle({ x, y, text }) {
  setState((state) => {
    for (var key in state.shapes) {
      //Check if there is already a circle
      if (state.shapes[key].type === "entryPointCircle") {
        return;
      }
    }
    state.shapes[nanoid()] = {
      type: "entryPointCircle",
      radius: 50,
      fill: "#ffffff",
      stroke: "#000000",
      text: text,
      x: x,
      y: y,
      keys: [],
      values: [],
    };
  });
}

export function createConditionRhombus({ x, y, text }) {
  setState((state) => {
    state.shapes[nanoid()] = {
      type: "conditionRhombus",
      width: 150,
      height: 100,
      fill: "#ffffff",
      stroke: "#000000",
      text: text,
      x: x,
      y: y,
      outputName: null,
      dataKey: null,
      valueToCompare: null,
      valueToCompareManual: null,
      operator: null,
      next_if_true: null,
    };
  });
}

export function createConnection(id1, id2, fromPos, toPos, fromType, toType) {
  setState((state) => {
    const key = nanoid();
    state.connections[key] = {
      key: key,
      from: id1,
      to: id2,
      fromPos: fromPos,
      toPos: toPos,
      fromType: fromType,
      toType: toType,
    };
  });
}

//** Selection Managment */

export function selectShape(id) {
  setState((state) => {
    state.selected = id;
    state.selectedConection = null;
  });
}

export function selectConnection(id) {
  setState((state) => {
    state.selected = null;
    state.selectedConection = id;
  });
}

export function clearSelection() {
  setState((state) => {
    state.selected = null;
  });
}

//** Get Shapes Information */

export function getselectedShapeId() {
  return useShapes.get().selected;
}

export function getTypeAttackById(id1) {
  return useShapes.get().shapes[id1].type;
}

export function geShapeConnectionsById(id1) {
  var connectionsFromId = {};
  var concop = useShapes.get().connections;
  for (var key in concop) {
    if (concop[key].from === id1) {
      connectionsFromId[key] = concop[key];
    }
  }
  return Object.values(connectionsFromId);
}

export function getIdShapeOnPosition(mouseX, mouseY, position) {
  var id = null;
  const createdShapes = useShapes.get().shapes;
  for (var key in createdShapes) {
    if (createdShapes[key].type === "attackRectangle") {
      if (
        mouseX > createdShapes[key].x - position.x &&
        mouseX < createdShapes[key].x - position.x + createdShapes[key].width &&
        mouseY > createdShapes[key].y - position.y &&
        mouseY < createdShapes[key].y - position.y + createdShapes[key].height
      ) {
        //Check if the mouse is inside the rectangle
        id = key;
      }
    } else if (createdShapes[key].type === "conditionRhombus") {
      const dx = Math.abs(mouseX - createdShapes[key].x + position.x);
      const dy = Math.abs(mouseY - createdShapes[key].y + position.y);
      const distance = Math.sqrt(dx * dx + dy * dy);
      if (distance < 50) {
        //Check if the mouse is inside the rhombus
        id = key;
      }
    }
  }
  return id;
}

export function getPositionShapeFromId(id) {
  var position = null;
  const createdShapes = useShapes.get().shapes;
  if (createdShapes[id] !== undefined) {
    position = { x: createdShapes[id].x, y: createdShapes[id].y };
  }
  return position;
}

export function getNameAndKeyOfConditionalOptions(id) {
  const connections = useShapes.get().connections;
  var conditionalOptions = {
    firstOption: null,
    firstOptionKey: null,
    secondOption: null,
    secondOptionKey: null,
  };
  for (var key in connections) {
    if (connections[key].from === id) {
      if (conditionalOptions.firstOption === null) {
        conditionalOptions.firstOption = getNameAndNumberAttackWithId(
          connections[key].to
        );
        conditionalOptions.firstOptionKey = connections[key].to;
      } else {
        conditionalOptions.secondOption = getNameAndNumberAttackWithId(
          connections[key].to
        );
        conditionalOptions.secondOptionKey = connections[key].to;
      }
    }
  }
  return conditionalOptions;
}

export function getNameAndNumberAttackWithId(id) {
  const attacks = useShapes.get().shapes;
  return attacks[id].attack + " " + attacks[id].attackNumber;
}

export function getAllUsableOutputs(id) {
  var usableOutputs = [];
  usableOutputs = usableOutputs.concat(getAttacksOutputs(id));
  usableOutputs = usableOutputs.concat(getEntryPointOutputs());
  return usableOutputs;
}

export function getAttacksOutputs(id) {
  const connections = useShapes.get().connections;
  const shapes = useShapes.get().shapes;

  var usableOutputs = [];
  var reachedTop = false;
  var currentId = id;
  var idOfEntrypoint = null;

  for (let key in shapes) {
    if (shapes[key].type === "entryPointCircle") {
      idOfEntrypoint = key;
      break;
    }
  }

  while (!reachedTop) {
    var currentConnection = Object.values(connections).find(
      (connection) => connection.to === currentId
    );
    if (
      currentConnection === undefined ||
      currentConnection.from === idOfEntrypoint
    ) {
      reachedTop = true;
    } else {
      const nextAttack = shapes[currentConnection.from];
      if (nextAttack.type === "attackRectangle") {
        usableOutputs.push({
          value: nextAttack.output,
          label: nextAttack.output,
        });
      }
      currentId = currentConnection.from;
    }
  }
  return usableOutputs;
}

export function getEntryPointOutputs() {
  var usableOutputs = [];
  const entryPoint = Object.values(useShapes.get().shapes).find(
    (shape) => shape.type === "entryPointCircle"
  );
  if (entryPoint !== undefined) {
    const entryKeys = entryPoint.keys;
    for (let i = 0; i < entryKeys.length; i++) {
      usableOutputs.push({ value: entryKeys[i], label: entryKeys[i] });
    }
  }
  usableOutputs.push({ value: "Manual Input", label: "Manual Input" });
  return usableOutputs;
}

export async function getShapePresetsFromName(attackName) {
  var presetsOfAttacks;
  try {
    let res = await fetch("/getPreset", {
      method: "post",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        attack: attackName,
      }),
    });
    let result = await res.json();
    if (result && !result.success && "error" in result) {
      toast.error("Couldn't get presets for this attack", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
      return;
    }
    return result;
  } catch (e) {
    console.log(e);
  }
  return presetsOfAttacks;
}

//** Update Shapes */

export function moveShape(id, event) {
  setState((state) => {
    const shape = state.shapes[id];
    if (shape) {
      shape.x = event.target.x();
      shape.y = event.target.y();
    }
  });
}

export function moveConection(id1) {
  setState((state) => {
    for (var key in state.connections) {
      if (state.connections[key].from === id1) {
        state.connections[key].fromPos = getPositionShapeFromId(id1);
      } else if (state.connections[key].to === id1) {
        state.connections[key].toPos = getPositionShapeFromId(id1);
      }
    }
  });
}

export function deleteShape() {
  useShapes.set(
    produce((state) => {
      for (var key in state.connections) {
        if (
          state.connections[key].from === state.selected ||
          state.connections[key].to === state.selected
        ) {
          delete state.connections[key];
        }
      }
      delete state.shapes[state.selected];
    })
  );
}

export function deleteConnection() {
  useShapes.set(
    produce((state) => {
      delete state.connections[state.selectedConection];
    })
  );
}

export function updateAttack(preset, input) {
  setState((state) => {
    const shape = state.shapes[state.selected];
    if (shape) {
      shape.preset = preset;
      shape.inputs = input;
    }
  });
}

export function rectangleIsMoving(id) {
  setState((state) => {
    state.shapes[id].isMoving = true;
  });
}

export function rectangleIsNotMoving(id) {
  setState((state) => {
    state.shapes[id].isMoving = false;
  });
}

export function updateEntrypoint(keys, values) {
  setState((state) => {
    const shape = state.shapes[state.selected];
    if (shape) {
      shape.keys = keys;
      shape.values = values;
    }
  });
}

export function updateConditional(conditional) {
  setState((state) => {
    const shape = state.shapes[state.selected];
    if (shape) {
      shape.outputName = conditional.outputName;
      shape.dataKey = conditional.dataKey;
      shape.valueToCompare = conditional.valueToCompare;
      shape.valueToCompareManual = conditional.valueToCompareManual;
      shape.operator = conditional.operator;
      shape.next_if_true = conditional.next_if_true;
    }
  });
}

//** Utilities */

export function conectionAllowed(idSource, idDestination) {
  var listShapes = useShapes.get().shapes;
  if (
    listShapes[idDestination].type === "conditionRhombus" &&
    (listShapes[idSource].type === "entryPointCircle" ||
      listShapes[idSource].type === "conditionRhombus")
  ) {
    return false; // A conditionRhombus can't be connected to a entryPointCircle or another conditionRhombus
  }

  var connectionLimitNotReached = true;
  var conditionalConnections = 0;

  setState((state) => {
    for (var key in state.connections) {
      if (
        state.connections[key].fromType !== "conditionRhombus" &&
        (state.connections[key].from === idSource ||
          state.connections[key].to === idDestination)
      ) {
        connectionLimitNotReached = false; //If the shape is not a conditionRhombus and is already connected to the source or destination, it can't be connected again
      } else if (
        state.connections[key].fromType === "conditionRhombus" &&
        state.connections[key].from === idSource
      ) {
        ++conditionalConnections;
      }
    }
  });
  if (conditionalConnections >= 2) {
    connectionLimitNotReached = false; //A conditionRhombus can't have more than 2 connections
  }

  var listConnections = Object.assign({}, useShapes.get().connections);
  var newConnection = {
    key: nanoid(),
    from: idSource,
    to: idDestination,
    fromPos: null,
    toPos: null,
    fromType: null,
    toType: null,
  };
  listConnections[newConnection.key] = newConnection;

  if (connectionLimitNotReached && checkForCyclesDFS(listConnections)) {
    return false; //If the connection creates a cycle, it can't be created
  }

  return connectionLimitNotReached;
}

function checkForCyclesDFS(connections) {
  var visited = {};
  var stack = {};
  // Perform DFS from each unvisited shape
  for (var key in connections) {
    if (!visited[connections[key].from]) {
      if (dfs(connections[key].from, visited, stack, connections)) {
        return true;
      }
    }
  }

  return false;

  // Perform DFS from the given shape
  function dfs(id, visited, stack, connections) {
    visited[id] = true;
    stack[id] = true;

    // Traverse the connections
    var neighbors = getNeighbors(id, connections);
    for (var i = 0; i < neighbors.length; i++) {
      var neighbor = neighbors[i];
      if (!visited[neighbor]) {
        if (dfs(neighbor, visited, stack, connections)) {
          return true;
        }
      } else if (stack[neighbor]) {
        // A cycle is found
        return true;
      }
    }

    // Remove the current shape from the path
    delete stack[id];
    return false;
  }

  // Get the neighbors of the given shape
  function getNeighbors(id, connections) {
    var neighbors = [];
    for (var key in connections) {
      if (connections[key].from === id) {
        neighbors.push(connections[key].to);
      }
    }
    return neighbors;
  }
}
