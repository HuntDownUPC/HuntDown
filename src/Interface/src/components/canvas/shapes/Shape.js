import React, { useCallback } from "react";

import { useShapes } from "../state";
import { Rectangle } from "./Rectangle";
import { Circle } from "./Circle";
import { Rhombus } from "./Rhombus";

export function Shape({ shape }) {
  const isSelectedSelector = useCallback(
    (state) => state.selected === shape.id,
    [shape]
  );

  const isSelected = useShapes(isSelectedSelector);

  if (shape.type === "attackRectangle") {
    return <Rectangle {...shape} isSelected={isSelected} />;
  } else if (shape.type === "entryPointCircle") {
    return <Circle {...shape} isSelected={isSelected} />;
  } else if (shape.type === "conditionRhombus") {
    return <Rhombus {...shape} isSelected={isSelected} />;
  }

  return null;
}
