import React, { useState, useEffect } from "react";
import {
  getAttacksOutputs,
  getEntryPointOutputs,
  useShapes,
  deleteShape,
  deleteConnection,
  getShapePresetsFromName,
  updateAttack,
  updateEntrypoint,
  updateConditional,
  getNameAndKeyOfConditionalOptions,
} from "./state";
import Select from "react-select";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";
import Form from "react-bootstrap/Form";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const shapeSelector = (state) => state.shapes[state.selected];
const connectionSelector = (state) => state.connections[state.selectedConection];
const selectedShapeSelector = (state) => state.selected;

var auxSelected = null; //This variable is used to store the selected shape before its updated
var renderInputs = false; //This variable is used to render the inputs of the selected attack if the preset is selected

export function PropertiesPanel() {
  const selectedShape = useShapes(shapeSelector);
  const selectedConnection = useShapes(connectionSelector);
  const selectedShapeId = useShapes(selectedShapeSelector);
  const [attackPresets, setAttackPresets] = useState(null);
  const [selectedPreset, setSelectedPreset] = useState(null);
  const [entryPointInput, setEntryPointInput] = useState(null);
  const [conditionalInput, setConditionalInput] = useState(null);
  const [attackInput, setAttackInput] = useState([]);
  const [isDeletePopupOpen, setIsDeletePopupOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {


    const fetchAttackPreset = async () => {
      try {
        setIsLoading(true);
        const response = await getShapePresetsFromName(selectedShape.attack);
        setAttackPresets(response);
      } catch (error) {
        console.error("Error fetching data:", error);
      } finally {
        setIsLoading(false);
      }
    };
    if (
      selectedShape !== undefined &&
      selectedShape.type === "attackRectangle" &&
      !selectedShape.isMoving //This condition is used to avoid the constant render of the inputs when the shape is moving
    ) {
      fetchAttackPreset();
    }
  }, [selectedShape, selectedShapeId]);

  /**Attack Properties */
  function handleAttackInputChange(index, selectedOption) {
    const newInputs = [...attackInput];
    newInputs[index] = { ...newInputs[index], value: selectedOption.value };
    setAttackInput(newInputs);
  }

  function handleManualInputChange(index, event) {
    const newInputs = [...attackInput];
    newInputs[index] = { ...newInputs[index], manualInput: event.target.value };
    setAttackInput(newInputs);
  }

  function handleSelectedPresetChange(selectedOption) {
    setAttackInput([]);
    setSelectedPreset(selectedOption.value);
  }

  const handleSaveAttackClick = () => {
    if (selectedPreset !== null && selectedPreset !== undefined) {
      updateAttack(selectedPreset, attackInput);
      toast.success("Attack Saved", {
        position: "top-right",
        autoClose: 4000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    } else {
      toast.error("Attack could not be saved", {
        position: "top-right",
        autoClose: 4000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };

  const checkGeneralAttack = () => {
    if (
      selectedShape == null ||
      selectedShape.type === "entryPointCircle" ||
      selectedShape.type === "conditionRhombus"
    )
      return null;

    var attackInformation = [];
    attackInformation.push(
      <h2
      className="propertiesTitle"

      >
        {selectedShape.attack + " " + selectedShape.attackNumber}
      </h2>
    );
    if (!isLoading && attackPresets !== null) {
      attackInformation.push(attackProperties());
    }
    return attackInformation;
  };

  const attackProperties = () => {
    const presetsOptions = [];
    //**Charge presets options and if selected show it */
    for (let key in attackPresets) {
      presetsOptions.push({
        value: attackPresets[key].preset,
        label: attackPresets[key].preset,
      });
    }
    var preset = null;
    if (selectedShapeId !== auxSelected || selectedPreset === null) {
      preset = selectedShape.preset; //The value saved or null
      renderInputs = true; //It can render the inputs because the shape preset has finished loading
    } else {
      preset = selectedPreset; //This way we ensure the new selected preset is not lost when the shape is modified
    }

    var propertiesElements = [];
    propertiesElements.push(
    <>
      <Select
        options={presetsOptions}
        onChange={handleSelectedPresetChange}
        key={selectedShapeId + preset + "Selector"}
        className="dropdown"
        placeholder="Select a preset"
        defaultValue={
          preset === null
            ? null
            : presetsOptions.find((element) => element.value === preset)
        }
      />
      {(preset === undefined || preset === null) && <br />}
    </>
    );

    //**Charge inputs if the preset is selected*/
    const usedPreset = attackPresets?.find(
      (element) => element.preset === preset
    );
    if (usedPreset !== undefined) {
      var inputs = null;
      if (renderInputs) {
        //If the shape is changed, the inputs are reseted
        inputs = [];
      } else {
        inputs = [...attackInput]; //If the shape is not changed, the inputs are the same
      }
      console.log("inputs", inputs);
      propertiesElements.push(
        <>
          <br />
          <h4>Preset description:</h4>
          <div>{usedPreset.description}</div>
          <h4 className="propertiesSpace">Preset options:</h4>
        </>
      );

      //const usabeOutputs = getAllUsableOutputs(selectedShapeId); //This should be uncomented when the backend is ready to use outputs on attacks
      const usableOutputs = getEntryPointOutputs(selectedShapeId);

      for (let index = 0; index < usedPreset.mandatoryUserOptions.length; index++) {
        //For each input of the preset

        const attackInput = usedPreset.mandatoryUserOptions[index];
        if (renderInputs) {
          //If the shape has changed, the inputs need to be defined again
          inputs[index] = {
            name:
              selectedShape.inputs[index] === undefined
                ? attackInput
                : selectedShape.inputs[index].name,
            value:
              selectedShape.inputs[index] === undefined
                ? null
                : selectedShape.inputs[index].value,
            manualInput:
              selectedShape.inputs[index] === undefined ||
              selectedShape.inputs[index].value !== "Manual Input"
                ? null
                : selectedShape.inputs[index].manualInput,
          };
        }
        propertiesElements.push(
          <>
            <h6>{attackInput}:</h6>
            <Select
              key={selectedShapeId + preset + index}
              options={usableOutputs}
              defaultValue={
                inputs[index]?.value === null
                  ? null
                  : usableOutputs.find(
                      (element) => element.value === inputs[index]?.value
                    )
              }
              placeholder="Select an input"
              onChange={(selectedOption) =>
                handleAttackInputChange(index, selectedOption)
              }
              className="dropdown"
            />
            {inputs[index]?.value === "Manual Input" && (
              <>
                <br />
                <InputGroup className="presetInput">
                  <FormControl
                    key={selectedShapeId + preset + index + "manualInput"}
                    defaultValue={inputs[index]?.manualInput}
                    onChange={(event) => handleManualInputChange(index, event)}
                    placeholder="Enter input value"
                  />
                </InputGroup>
              </>
            )}
            <br />
          </>
        );
      }
      renderInputs = false; //The inputs are not rendered again until the shape is changed
    }

    propertiesElements.push(
      <>
        <InputGroup className="propertiesButton">
          <button className="button" onClick={handleSaveAttackClick}>
            Save
          </button>
          <button className="button" onClick={handleDeleteClick}>
            Delete
          </button>
        </InputGroup>
      </>
    );
    auxSelected = selectedShapeId; //Save the selected shape to compare it later
    if (JSON.stringify(attackInput) !== JSON.stringify(inputs)) {
      setAttackInput(inputs);
    }
    if (JSON.stringify(selectedPreset) !== JSON.stringify(preset)) {
      setSelectedPreset(preset);
    }
    return propertiesElements;
  };

  /**Entry Point Properties */

  function handleSaveEntryPointClick() {
    const keysSet = new Set(entryPointInput.keys); //No duplicates
    if (keysSet.size !== entryPointInput.keys.length) {
      toast.error("Keys should be unique", {
        position: "top-right", //Refactor this shit
        autoClose: 4000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    } else if (entryPointInput.values.includes(null)) {
      toast.error("Values cant be null", {
        position: "top-right",
        autoClose: 4000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    } else {
      toast.success("Entry Point Saved", {
        position: "top-right",
        autoClose: 4000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
      updateEntrypoint(entryPointInput.keys, entryPointInput.values);
    }
  }

  function handleAddEntryPointInput() {
    const newKeys = [...entryPointInput.keys, null];
    const newValues = [...entryPointInput.values, null];
    setEntryPointInput({ keys: newKeys, values: newValues });
  }

  function handleDeleteEntryPointInput() {
    const newKeys = [...entryPointInput.keys];
    const newValues = [...entryPointInput.values];
    newKeys.pop();
    newValues.pop();
    setEntryPointInput({ keys: newKeys, values: newValues });
  }

  function handleEntryPointKeyChange(index, event) {
    const newKeys = [...entryPointInput.keys];
    newKeys[index] = event.target.value;
    setEntryPointInput({ ...entryPointInput, keys: newKeys });
  }

  function handleEntryPointValueChange(index, event) {
    const newValues = [...entryPointInput.values];
    newValues[index] = event.target.value;
    setEntryPointInput({ ...entryPointInput, values: newValues });
  }

  const checkEntryPoint = () => {
    if (selectedShape == null || selectedShape.type !== "entryPointCircle")
      return null;

    const entryPointElements = [];
    entryPointElements.push(
      <>
        <h2
          className="propertiesTitle"
        >
          EntryPoint
        </h2>
      </>
    );
    if (entryPointInput === null) {
      setEntryPointInput({
        keys: selectedShape.keys === null ? [] : selectedShape.keys,
        values: selectedShape.values === null ? [] : selectedShape.values,
      });
    } else {
      for (let index = 0; index < entryPointInput.keys.length; index++) {
        if (index === 0) {
          entryPointElements.push(
            <>
              <h6>Enter inputs for the attacks:</h6>
            </>
          );
        }
        entryPointElements.push(
          <>
            <InputGroup className="presetInput">
              <FormControl
                key={auxSelected + "Key" + index}
                defaultValue={
                  entryPointInput.keys[index]
                    ? entryPointInput.keys[index]
                    : null
                }
                placeholder="Key"
                onChange={(event) => handleEntryPointKeyChange(index, event)}
              />
              <FormControl
                key={auxSelected + "Value" + index}
                defaultValue={
                  entryPointInput.values[index]
                    ? entryPointInput.values[index]
                    : null
                }
                placeholder="Value"
                onChange={(event) => handleEntryPointValueChange(index, event)}
              />
            </InputGroup>
            <br />
          </>
        );
      }
      auxSelected = selectedShapeId;
      entryPointElements.push(
        <>
          <InputGroup className="propertiesButton">
            <button className="button" onClick={handleAddEntryPointInput}>
              Add
            </button>
            {entryPointInput.keys.length > 0 && (
              <button className="button" onClick={handleDeleteEntryPointInput}>
                Remove
              </button>
            )}
          </InputGroup>
          <br />
          <InputGroup className="propertiesButton">
            <button className="button" onClick={handleSaveEntryPointClick}>
              Save
            </button>
            <button className="button" onClick={handleDeleteClick}>
              Delete
            </button>
          </InputGroup>
        </>
      );
      return entryPointElements;
    }
  };

  /**Conditional Properties */

  function handleConditionalOutputName(selectedOption) {
    const newConditionalState = {
      ...conditionalInput,
      outputName: selectedOption.value,
    };
    setConditionalInput(newConditionalState);
  }

  function handleConditionalDatakeyChange(event) {
    const newConditionalState = {
      ...conditionalInput,
      dataKey: event.target.value,
    };
    setConditionalInput(newConditionalState);
  }

  function handleOperatorChange(selectedOption) {
    const newConditionalState = {
      ...conditionalInput,
      operator: selectedOption.value,
    };
    setConditionalInput(newConditionalState);
  }

  function handleValueToCompareChange(selectedOption) {
    const newConditionalState = {
      ...conditionalInput,
      valueToCompare: selectedOption.value,
    };
    setConditionalInput(newConditionalState);
  }

  function handleValueToCompareManualChange(event) {
    const newConditionalState = {
      ...conditionalInput,
      valueToCompareManual: event.target.value,
    };
    setConditionalInput(newConditionalState);
  }

  function handleChangeConditionalOption(attackKey, event) {
    var newConditionalState = null;
    newConditionalState = { ...conditionalInput, next_if_true: attackKey };
    setConditionalInput(newConditionalState);
  }

  function loadOperators() {
    let operatorsUsed = [];
    operatorsUsed.push({ value: "==", label: "==" });
    operatorsUsed.push({ value: "!=", label: "!=" });
    operatorsUsed.push({ value: ">=", label: ">=" });
    operatorsUsed.push({ value: "<=", label: "<=" });
    operatorsUsed.push({ value: ">", label: ">" });
    operatorsUsed.push({ value: "<", label: "<" });
    return operatorsUsed;
  }

  function handleSaveConditionClick() {

      if (
        conditionalInput.outputName === null || ((
        conditionalInput.operator === null ||
        conditionalInput.next_if_true === null ||
        conditionalInput.dataKey === null ||
        conditionalInput.valueToCompare === null)
        && (selectedShape.text !== "Iterate"))
      ) {
        toast.error("Values can't be null", {
          position: "top-right",
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
          theme: "light",
        });
      } else if ((!Number.isInteger(parseInt(conditionalInput.dataKey))) && (selectedShape.text !== "Iterate")) {
        toast.error("Value to compare needs to be a number", {
          position: "top-right",
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
          theme: "light",
        });
      } else if (
        conditionalInput.valueToCompare === "Manual Input" &&
        (conditionalInput.valueToCompareManual === null ||
          conditionalInput.valueToCompareManual === "")
      ) {
        toast.error("Manual input needs to be defined", {
          position: "top-right",
          autoClose: 4000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
          theme: "light",
        });
      } else {
        toast.success("Conditional Saved", {
          position: "top-right",
          autoClose: 4000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
          theme: "light",
        });
        updateConditional(conditionalInput);
      }
  }

  const checkConditional = () => {

  console.log("selectedShape")
  console.log(selectedShape)

    if (selectedShape == null || selectedShape.type !== "conditionRhombus")
      return null;
  
    if (selectedShape.text === "Cond") {
      return checkIf()
    } else if (selectedShape.text === "Iterate") {
      return checkIterate()
    } else return null
  };

  function checkIterate() {
    const conditionalElements = [];

    conditionalElements.push(
      <>
        <h2
          className="propertiesTitle"
        >
          Iterate Over
        </h2>
      </>
    );

    const conditionalOptions =
      getNameAndKeyOfConditionalOptions(selectedShapeId);
    const attackOutputs = getAttacksOutputs(selectedShapeId);
    var condicionalInputs = null;

    console.log("selectedShape", selectedShapeId);
    console.log("aux", auxSelected);
    if (auxSelected !== selectedShapeId || conditionalInput === null) {
      condicionalInputs = {
        outputName:
          selectedShape.outputName === null ? null : selectedShape.outputName,
        dataKey: selectedShape.dataKey === null ? null : selectedShape.dataKey,
        valueToCompare:
          selectedShape.valueToCompare === null
            ? null
            : selectedShape.valueToCompare,
        valueToCompareManual:
          selectedShape.valueToCompareManual === null
            ? null
            : selectedShape.valueToCompareManual,
        operator:
          selectedShape.operator === null ? null : selectedShape.operator,
        next_if_true:
          selectedShape.next_if_true === null
            ? null
            : selectedShape.next_if_true,
      };
    } else {
      condicionalInputs = { ...conditionalInput };
    }

    conditionalElements.push(
      <>
        <h6>Output to check:</h6>
        <Select
          key={selectedShapeId + auxSelected + "outputName"}
          placeholder={"Select an input"}
          options={attackOutputs}
          defaultValue={
            condicionalInputs.outputName === null
              ? null
              : attackOutputs.find(
                  (element) => element.value === condicionalInputs.outputName
                )
          }
          onChange={(selectedOption) =>
            handleConditionalOutputName(selectedOption)
          }
          className="dropdown"
        />
        <br />
        <h6>Next block:</h6>
        <div key={`inline-radio`} className="mb-3">
          <Form.Check
            label={
              conditionalOptions.firstOption === null
                ? "Undefined"
                : conditionalOptions.firstOption
            }
            name="group1"
            type="radio"
            key={selectedShapeId + auxSelected + "firstOption"}
            onChange={(event) =>
              handleChangeConditionalOption(
                conditionalOptions.firstOptionKey,
                event
              )
            }
            id={`inline-radio-1`}
            defaultChecked={
              conditionalOptions.firstOptionKey === null
                ? false
                : conditionalOptions.firstOptionKey ===
                  condicionalInputs.next_if_true
            }
          />
          <Form.Check
            label={
              conditionalOptions.secondOption === null
                ? "Undefined"
                : conditionalOptions.secondOption
            }
            name="group1"
            key={selectedShapeId + auxSelected + "secondOption"}
            type="radio"
            onChange={(event) =>
              handleChangeConditionalOption(
                conditionalOptions.secondOptionKey,
                event
              )
            }
            id={`inline-radio-2`}
            defaultChecked={
              conditionalOptions.secondOptionKey === null
                ? false
                : conditionalOptions.secondOptionKey ===
                  condicionalInputs.next_if_true
            }
          />
        </div>
        <InputGroup className="propertiesButton">
          <button className="button" onClick={handleSaveConditionClick}>
            Save
          </button>
          <button className="button" onClick={handleDeleteClick}>
            Delete
          </button>
        </InputGroup>
      </>
    );
    auxSelected = selectedShapeId;
    

    if (
      JSON.stringify(conditionalInput) !== JSON.stringify(condicionalInputs)
    ) {
      setConditionalInput(condicionalInputs);
    }
    return conditionalElements;
  }

  function checkIterate2() {
    const conditionalElements = [];

    conditionalElements.push(
      <>
        <h2
          className="propertiesTitle"
        >
          Iterate Over
        </h2>
      </>
    );

    const attackOutputs = getAttacksOutputs(selectedShapeId);
    var condicionalInputs = null;

    console.log("selectedShape", selectedShapeId);
    console.log("aux", auxSelected);
    if (auxSelected !== selectedShapeId || conditionalInput === null) {
      condicionalInputs = {
        outputName:
          selectedShape.outputName === null ? null : selectedShape.outputName,
      };
    } else {
      condicionalInputs = { ...conditionalInput };
    }

    conditionalElements.push(
      <>
        <h6>Output to iterate over:</h6>
        <Select
          key={selectedShapeId + auxSelected + "outputName"}
          placeholder={"Select an input"}
          options={attackOutputs}
          defaultValue={
            condicionalInputs.outputName === null
              ? null
              : attackOutputs.find(
                  (element) => element.value === condicionalInputs.outputName
                )
          }
          onChange={(selectedOption) =>
            handleConditionalOutputName(selectedOption)
          }
          className="dropdown"
        />
        <br />
        <InputGroup className="propertiesButton">
          <button className="button" onClick={handleSaveConditionClick}>
            Save
          </button>
          <button className="button" onClick={handleDeleteClick}>
            Delete
          </button>
        </InputGroup>
      </>
    );
    auxSelected = selectedShapeId;

    if (
      JSON.stringify(conditionalInput) !== JSON.stringify(condicionalInputs)
    ) {
      setConditionalInput(condicionalInputs);
    }
    return conditionalElements;
  }

  function checkIf() {
    const conditionalElements = [];

    conditionalElements.push(
      <>
        <h2
          className="propertiesTitle"
        >
          Conditional
        </h2>
      </>
    );

    const conditionalOptions =
      getNameAndKeyOfConditionalOptions(selectedShapeId);
    const entryPointOutputs = getEntryPointOutputs(selectedShapeId);
    const attackOutputs = getAttacksOutputs(selectedShapeId);
    var operatorsUsed = loadOperators();
    var condicionalInputs = null;

    console.log("selectedShape", selectedShapeId);
    console.log("aux", auxSelected);
    if (auxSelected !== selectedShapeId || conditionalInput === null) {
      condicionalInputs = {
        outputName:
          selectedShape.outputName === null ? null : selectedShape.outputName,
        dataKey: selectedShape.dataKey === null ? null : selectedShape.dataKey,
        valueToCompare:
          selectedShape.valueToCompare === null
            ? null
            : selectedShape.valueToCompare,
        valueToCompareManual:
          selectedShape.valueToCompareManual === null
            ? null
            : selectedShape.valueToCompareManual,
        operator:
          selectedShape.operator === null ? null : selectedShape.operator,
        next_if_true:
          selectedShape.next_if_true === null
            ? null
            : selectedShape.next_if_true,
      };
    } else {
      condicionalInputs = { ...conditionalInput };
    }

    conditionalElements.push(
      <>
        <h6>Output to check:</h6>
        <Select
          key={selectedShapeId + auxSelected + "outputName"}
          placeholder={"Select an input"}
          options={attackOutputs}
          defaultValue={
            condicionalInputs.outputName === null
              ? null
              : attackOutputs.find(
                  (element) => element.value === condicionalInputs.outputName
                )
          }
          onChange={(selectedOption) =>
            handleConditionalOutputName(selectedOption)
          }
          className="dropdown"
        />
        <br />
        <h6>Selected output value:</h6>
        <FormControl
          key={selectedShapeId + auxSelected + "datakey"}
          defaultValue={condicionalInputs.dataKey}
          onChange={(event) => handleConditionalDatakeyChange(event)}
          placeholder="Enter number of output"
        />
        <br />
        <h6>Operator:</h6>
        <Select
          key={selectedShapeId + auxSelected + "Operator"}
          placeholder={"Select an operation"}
          options={operatorsUsed}
          defaultValue={
            condicionalInputs.operator === null
              ? null
              : operatorsUsed.find(
                  (element) => element.value === condicionalInputs.operator
                )
          }
          onChange={handleOperatorChange}
          className="dropdown"
        />
        <br />
        <h6>Value to compare:</h6>
        <Select
          key={selectedShapeId + auxSelected + "valueToCompare"}
          placeholder={"Select input value"}
          options={entryPointOutputs}
          defaultValue={
            condicionalInputs.valueToCompare === null
              ? null
              : entryPointOutputs.find(
                  (element) =>
                    element.value === condicionalInputs.valueToCompare
                )
          }
          onChange={(selectedOption) =>
            handleValueToCompareChange(selectedOption)
          }
          className="dropdown"
        />
        {condicionalInputs.valueToCompare === "Manual Input" && (
          <>
            <br />
            <InputGroup className="presetInput">
              <FormControl
                key={selectedShapeId + auxSelected + "valueToCompareManual"}
                defaultValue={condicionalInputs.valueToCompareManual}
                onChange={(event) => handleValueToCompareManualChange(event)}
                placeholder="Enter input value"
              />
            </InputGroup>
          </>
        )}
        <br />
        <h6>Next block if true:</h6>
        <div key={`inline-radio`} className="mb-3">
          <Form.Check
            label={
              conditionalOptions.firstOption === null
                ? "Undefined"
                : conditionalOptions.firstOption
            }
            name="group1"
            type="radio"
            key={selectedShapeId + auxSelected + "firstOption"}
            onChange={(event) =>
              handleChangeConditionalOption(
                conditionalOptions.firstOptionKey,
                event
              )
            }
            id={`inline-radio-1`}
            defaultChecked={
              conditionalOptions.firstOptionKey === null
                ? false
                : conditionalOptions.firstOptionKey ===
                  condicionalInputs.next_if_true
            }
          />
          <Form.Check
            label={
              conditionalOptions.secondOption === null
                ? "Undefined"
                : conditionalOptions.secondOption
            }
            name="group1"
            key={selectedShapeId + auxSelected + "secondOption"}
            type="radio"
            onChange={(event) =>
              handleChangeConditionalOption(
                conditionalOptions.secondOptionKey,
                event
              )
            }
            id={`inline-radio-2`}
            defaultChecked={
              conditionalOptions.secondOptionKey === null
                ? false
                : conditionalOptions.secondOptionKey ===
                  condicionalInputs.next_if_true
            }
          />
        </div>
        <InputGroup className="propertiesButton">
          <button className="button" onClick={handleSaveConditionClick}>
            Save
          </button>
          <button className="button" onClick={handleDeleteClick}>
            Delete
          </button>
        </InputGroup>
      </>
    );
    auxSelected = selectedShapeId;

    if (
      JSON.stringify(conditionalInput) !== JSON.stringify(condicionalInputs)
    ) {
      setConditionalInput(condicionalInputs);
    }
    return conditionalElements;
  };

  /**Connection Properties */

  function checkConnection() {
    if (selectedConnection == null) return null;
    return (
      <>
        <div className="key">
          Type <span className="value">Arrow</span>
        </div>
        <div className="deleteButton">
          <button className="button" onClick={deleteConnection}>
            Delete
          </button>
        </div>
      </>
    );
  }

  function handleDeleteClick() {
    setIsDeletePopupOpen(true);
  }

  function handlePopupDeleteClose() {
    setIsDeletePopupOpen(false);
  }

  function handlePopupDelete() {
    setIsDeletePopupOpen(false);
    deleteShape();
  }

  return (
    <div
      className="propertiesDiv"
    >
      {isDeletePopupOpen && (
        <div className="popup">
          <h2>Do you want to delete it?</h2>
          <div className="popupbuttons">
            <button onClick={handlePopupDelete}>Delete</button>
            <button onClick={handlePopupDeleteClose}>Close</button>
          </div>
        </div>
      )}
      <aside className="panel">
        <div className="properties">
          {checkGeneralAttack()}
          {checkConnection()}
          {checkEntryPoint()}
          {checkConditional()}
        </div>
      </aside>
    </div>
  );
}
