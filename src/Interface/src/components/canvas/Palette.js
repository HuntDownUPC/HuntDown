import React, { useEffect, useState } from "react";
import { loadNewConstruct } from "./state";
import { useCollapse } from "react-collapsed";
import { toast } from "react-toastify";

var selectedConstruct = null;

export function Palette(props) {
  const [savedConstructList, setSavedConstructList] = useState([]);
  const [isLoadPopupOpen, setIsLoadPopupOpen] = useState(false);
  const [isDeletePopupOpen, setIsDeletePopupOpen] = useState(false);

  useEffect(() => {
    setSavedConstructList(props.constructList);
  }, [props.constructList]);

  function handleDragStart(event) {
    const type = event.target.dataset.shape;
    const attack = event.target.innerText;

    if (type) {
      const offsetX = event.nativeEvent.offsetX;
      const offsetY = event.nativeEvent.offsetY;

      const clientWidth = event.target.clientWidth;
      const clientHeight = event.target.clientHeight;

      const dragPayload = JSON.stringify({
        type,
        offsetX,
        offsetY,
        clientWidth,
        clientHeight,
        attack,
      });

      event.nativeEvent.dataTransfer.setData(
        "__drag_data_payload__",
        dragPayload
      );
    }
  }

  async function handleConstructLoad() {
    try {
      console.log(savedConstructList)
      console.log(selectedConstruct)
      const newConstruct = Object.values(savedConstructList).find(
        (construct) => construct.construct_id === selectedConstruct
      );
      loadNewConstruct(newConstruct);
    } catch (error) {
      console.error(error);
      toast.error("Error loading construct", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
    toast.success("Construct loaded correctly", {
      position: "top-right",
      autoClose: 2000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false,
      draggable: true,
      progress: undefined,
      theme: "light",
    });
  }

  async function handleConstructDelete() {
    try {
      await deleteConstruct(selectedConstruct);
      props.updateConstructList();
    } catch (error) {
      console.error(error);
      toast.error("Error deleting construct", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
    toast.success("Construct deleted correctly", {
      position: "top-right",
      autoClose: 2000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false,
      draggable: true,
      progress: undefined,
      theme: "light",
    });
  }

  async function deleteConstruct(key) {
    try {
      await fetch("/deleteConstruct", {
        method: "post",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          constructID: key,
        }),
      });
    } catch (e) {
      throw e;
    }
  }

  function listSavedConstruct() {
    if (savedConstructList !== null && savedConstructList.length !== 0) {
      return savedConstructList.map((construct) => {
        return (
          <div key={construct.construct_id} className="constructContainer">
            <div className="constructName">{construct.construct_name}</div>
            <div className="constructButtons">
              <button
                className="workflowButton"
                onClick={(event) =>
                  handleLoadClick(construct.construct_id, event)
                }
              >
                <i className="fa-regular fa-share-from-square"></i>
              </button>
              <button
                className="workflowButton"
                onClick={(event) =>
                  handleDeleteClick(construct.construct_id, event)
                }
              >
                <i className="fas fa-trash-alt"></i>
              </button>
            </div>
          </div>
        );
      });
    } else {
      return;
    }
  }

  function attackList() {
    return (
      <>
        {props.attackList.map((attackType, id) =>
          attackType.name !== "sqlmap" &&
          attackType.name !== "metasploit" ? ( //Filter out sqlmap and metasploit  because doesn't have presets
            <div
              className="shape rectangle"
              data-shape={"attackRectangle"}
              draggable
              onDragStart={handleDragStart}
              key={id}
            >
              {attackType.name}
            </div>
          ) : null
        )}
      </>
    );
  }

  function otherList() {
    return (
      <>
        <div
          className="shape circle"
          data-shape={"entryPointCircle"}
          draggable
          key="EntryPoint"
          onDragStart={handleDragStart}
        >
          Entry Point
        </div>
        <div
          className="shape rhombus"
          data-shape={"conditionRhombus"}
          draggable
          key="Conditional"
          onDragStart={handleDragStart}
        >
          <div className="text">Cond</div>
        </div>
        <div
          className="shape rhombus"
          data-shape={"conditionRhombus"}
          draggable
          key="ConditionalIterate"
          onDragStart={handleDragStart}
          style={{ marginTop: '20px' }}
        >
           <div className="text" style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>Iterate</div>
        </div>
      </>
    );
  }

  function handleLoadClick(constructId) {
    selectedConstruct = constructId;
    setIsLoadPopupOpen(true);
  }

  function handlePopupLoadClose() {
    setIsLoadPopupOpen(false);
  }

  function handlePopupLoad() {
    handleConstructLoad();
    setIsLoadPopupOpen(false);
  }

  function handleDeleteClick(constructId) {
    selectedConstruct = constructId;
    setIsDeletePopupOpen(true);
  }

  function handlePopupDeleteClose() {
    setIsDeletePopupOpen(false);
  }

  function handlePopupDelete() {
    handleConstructDelete();
    setIsDeletePopupOpen(false);
  }

  return (
    <div className="paletteDiv">
      {isLoadPopupOpen && (
        <div className="popup">
          <h2>Do you want to load the construct?</h2>
          <br />
          <div className="popupbuttons">
            <button onClick={handlePopupLoad}>Load</button>
            <button onClick={handlePopupLoadClose}>Close</button>
          </div>
        </div>
      )}

      {isDeletePopupOpen && (
        <div className="popup">
          <h2>Do you want to delete the construct?</h2>
          <br />
          <div className="popupbuttons">
            <button onClick={handlePopupDelete}>Delete</button>
            <button onClick={handlePopupDeleteClose}>Close</button>
          </div>
        </div>
      )}
      <aside className="palette">
        <h2>Tools and Shapes</h2>
        <Collapsible title="Constructs">{listSavedConstruct()}</Collapsible>
        <Collapsible title="Attacks">{attackList()}</Collapsible>
        <Collapsible title="Other elements">
          <div className="otherList">{otherList()}</div>
        </Collapsible>
      </aside>
    </div>
  );
}

function Collapsible(props) {
  //Defines the collapsible component, doesn't has its own file becasue it's only used here
  const config = {
    collapsedHeight: props.collapsedHeight || 0,
  };
  const { getCollapseProps, getToggleProps, isExpanded } = useCollapse(config);
  return (
    <>
      <div className="collapsible">
        <div className="header" {...getToggleProps()}>
          <div className="title">{props.title}</div>
          <div className="icon">
            <i
              className={
                "fas fa-chevron-circle-" + (isExpanded ? "up" : "down")
              }
            ></i>
          </div>
        </div>
        <div {...getCollapseProps()}>
          <div className="content">{props.children}</div>
        </div>
      </div>
    </>
  );
}
export default Palette;
