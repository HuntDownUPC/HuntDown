import React, { useEffect, useRef, useCallback, useState } from "react";
import GridLines from "react-gridlines";
import { Layer, Stage } from "react-konva";
import {
  useShapes,
  clearSelection,
  createAttackRectangle,
  saveConstruct,
  resetWorkflow,
  createEntryPointCircle,
  createConditionRhombus,
  executeWorkflow,
} from "./state";
import { Shape } from "./shapes/Shape";
import "react-toastify/dist/ReactToastify.css";

export var stageRef; //Can't be passed as a prop to the shape, so it's defined as a global variable

export function Canvas(props) {
  const [isSavePopupOpen, setIsSavePopupOpen] = useState(false);
  const [isExecutePopupOpen, setIsExecutePopupOpen] = useState(false);
  const [isResetPopupOpen, setIsResetPopupOpen] = useState(false);
  const [textFieldValue, setTextFieldValue] = useState("");

  const [dimensions, setDimensions] = useState({
    width: 0,
    height: 0,
  });
  const shapes = useShapes((state) => Object.entries(state.shapes));

  stageRef = useRef(null);
  var gridRef = useRef(null);
  var divRef = useRef(null);
  var lastCenter = null;
  var lastDist = 0;

  useEffect(() => {
    function handleResize() {
      if (divRef.current?.offsetHeight && divRef.current?.offsetWidth) {
        setDimensions({
          width: divRef.current.offsetWidth,
          height: divRef.current.offsetHeight,
        });
      }
    }

    handleResize();
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [divRef]);

  const handleDragOver = (event) => event.preventDefault();

  function zoomStage(event) {
    event.evt.preventDefault();

    if (stageRef.current !== null) {
      const stage = stageRef.current;
      const oldScale = stage.scaleX();
      const { x: pointerX, y: pointerY } = stage.getPointerPosition();
      const newScale = event.evt.deltaY > 0 ? oldScale / 1.05 : oldScale * 1.05; // Calculating the new scale based on the deltaY value
      const mousePointTo = {
        // Calculating the mouse point position relative to the stage
        x: (pointerX - stage.x()) / oldScale,
        y: (pointerY - stage.y()) / oldScale,
      };

      stage.scale({ x: newScale, y: newScale });

      const newPos = {
        x: pointerX - mousePointTo.x * newScale,
        y: pointerY - mousePointTo.y * newScale,
      };

      stage.position(newPos);
      stage.batchDraw();
      stageRef.current = stage;
    }
  }

  function handleTouch(e) {
    e.evt.preventDefault();
    var touch1 = e.evt.touches[0];
    var touch2 = e.evt.touches[1];
    const stage = stageRef.current;

    if (stage !== null) {
      if (touch1 && touch2) {
        if (stage.isDragging()) {
          stage.stopDrag();
        }

        var p1 = {
          x: touch1.clientX,
          y: touch1.clientY,
        };
        var p2 = {
          x: touch2.clientX,
          y: touch2.clientY,
        };

        if (!lastCenter) {
          lastCenter = getCenter(p1, p2);
          return;
        }
        var newCenter = getCenter(p1, p2);

        var dist = getDistance(p1, p2);

        if (!lastDist) {
          lastDist = dist;
        }

        // local coordinates of center point
        var pointTo = {
          x: (newCenter.x - stage.x()) / stage.scaleX(),
          y: (newCenter.y - stage.y()) / stage.scaleX(),
        };

        var scale = stage.scaleX() * (dist / lastDist);

        stage.scaleX(scale);
        stage.scaleY(scale);

        // calculate new position of the stage
        var dx = newCenter.x - lastCenter.x;
        var dy = newCenter.y - lastCenter.y;

        var newPos = {
          x: newCenter.x - pointTo.x * scale + dx,
          y: newCenter.y - pointTo.y * scale + dy,
        };

        stage.position(newPos);
        stage.batchDraw();

        lastDist = dist;
        lastCenter = newCenter;
        stageRef.current = stage;
        gridRef.current = stage;
      }
    }
  }

  function handleTouchEnd() {
    lastCenter = null;
    lastDist = 0;
  }

  function isTouchEnabled() {
    return (
      "ontouchstart" in window ||
      navigator.maxTouchPoints > 0 ||
      navigator.msMaxTouchPoints > 0
    );
  }

  const handleDrop = useCallback((event) => {
    const draggedData = event.nativeEvent.dataTransfer.getData(
      "__drag_data_payload__"
    );

    if (draggedData) {
      const { offsetX, offsetY, type, attack, clientWidth, clientHeight } =
        JSON.parse(draggedData);

      stageRef.current.setPointersPositions(event);

      const coords = stageRef.current.getPointerPosition(); //get the position of the mouse on the canvas

      if (type === "attackRectangle") {
        // rectangle x, y is at the top,left corner
        createAttackRectangle({
          x:
            (coords.x - stageRef.current.x()) / stageRef.current.scaleX() -
            offsetX,
          y:
            (coords.y - stageRef.current.y()) / stageRef.current.scaleY() -
            offsetY,
          attack: attack,
        });
      } else if (type === "entryPointCircle") {
        createEntryPointCircle({
          x:
            (coords.x - stageRef.current.x()) / stageRef.current.scaleX() -
            (offsetX - clientWidth / 2),
          y:
            (coords.y - stageRef.current.y()) / stageRef.current.scaleY() -
            (offsetY - clientHeight / 2),
          text: attack,
        });
      } else if (type === "conditionRhombus") {
        createConditionRhombus({
          x:
            (coords.x - stageRef.current.x()) / stageRef.current.scaleX() -
            offsetX,
          y:
            (coords.y - stageRef.current.y()) / stageRef.current.scaleY() -
            offsetY,
          text: attack,
        });
      }
    }
  }, []);

  function handleSaveClick() {
    setIsSavePopupOpen(true);
  }

  function handleExecuteClick() {
    setIsExecutePopupOpen(true);
  }

  function handleResetClick() {
    setIsResetPopupOpen(true);
  }

  function handlePopupSaveClose() {
    setIsSavePopupOpen(false);
  }

  function handlePopupExecuteClose() {
    setIsExecutePopupOpen(false);
  }

  function handlePopupResetClose() {
    setIsResetPopupOpen(false);
  }

  function handlePopupSave() {
    saveConstruct(textFieldValue);
    props.updateConstructList();
    setIsSavePopupOpen(false);
  }

  function handlePopupExecute() {
    executeWorkflow(textFieldValue);
    setIsExecutePopupOpen(false);
  }

  function handlePopupReset() {
    resetWorkflow();
    setIsResetPopupOpen(false);
  }

  function handleTextFieldChange(event) {
    setTextFieldValue(event.target.value);
  }

  function getDistance(p1, p2) {
    return Math.sqrt(Math.pow(p2.x - p1.x, 2) + Math.pow(p2.y - p1.y, 2));
  }

  function getCenter(p1, p2) {
    return {
      x: (p1.x + p2.x) / 2,
      y: (p1.y + p2.y) / 2,
    };
  }

  return (
    <div
      ref={divRef}
      className="canvasDiv"
      onDrop={handleDrop}
      onDragOver={handleDragOver}
    >
      <div className="buttons">
        <button onClick={handleSaveClick}>Save</button>
        <button onClick={handleResetClick}>Reset</button>
        <button onClick={handleExecuteClick}>Execute</button>
      </div>

      {isSavePopupOpen && (
        <div className="popup">
          <h2>Save as</h2>
          <input
            type="text"
            onChange={handleTextFieldChange}
            placeholder="Enter the construct name"
          />
          <div className="popupbuttons">
            <button onClick={handlePopupSave}>Save</button>
            <button onClick={handlePopupSaveClose}>Close</button>
          </div>
        </div>
      )}

      {isExecutePopupOpen && (
        <div className="popup">
          <h2>Execute as</h2>
          <input
            type="text"
            value={textFieldValue}
            onChange={handleTextFieldChange}
            placeholder="Enter the workflow name"
          />
          <div className="popupbuttons">
            <button onClick={handlePopupExecute}>Execute</button>
            <button onClick={handlePopupExecuteClose}>Close</button>
          </div>
        </div>
      )}

      {isResetPopupOpen && (
        <div className="popup">
          <h2>Do you want to reset the canvas?</h2>
          <br />
          <div className="popupbuttons">
            <button onClick={handlePopupReset}>Reset</button>
            <button onClick={handlePopupResetClose}>Close</button>
          </div>
        </div>
      )}

      <GridLines
        className="grid-area"
        ref={gridRef}
        width={dimensions.width}
        height={dimensions.height}
        cellWidth={60}
        strokeWidth={2}
        cellWidth2={15}
      >
        <Stage
          ref={stageRef}
          width={dimensions.width}
          height={dimensions.height}
          onClick={clearSelection}
          draggable={!isTouchEnabled()}
          onWheel={zoomStage}
          onTouchMove={handleTouch}
          onTouchEnd={handleTouchEnd}
        >
          <Layer>
            {shapes.map(([key, shape]) => (
              <Shape key={key} shape={{ ...shape, id: key }} />
            ))}
          </Layer>
        </Stage>
      </GridLines>
    </div>
  );
}
