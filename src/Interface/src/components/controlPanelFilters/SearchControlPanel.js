import React, { useState, useEffect, useMemo } from "react";

export default function SearchControlPanel({ data, onChange }) {
    const [query, setQuery] = useState("");

    const filteredData = useMemo(() => {
        if (!query) return data;

        return data.filter(item =>
            Object.values(item).some(value =>
                String(value).toLowerCase().includes(query.toLocaleLowerCase())
            )
        );
    }, [data, query]);

    useEffect(() => {
        onChange(filteredData);
    }, [filteredData]);

    const handleSearch = (event) => {
        setQuery(event.target.value);
    };

    return (
        <div className="search-container-control-panel">
            <input
                type="text"
                placeholder="Search..."
                value={query}
                onChange={handleSearch}
                className="search-input"
            />
        </div>
    );
}
