import React from 'react';
import './App.css';
import { observer } from 'mobx-react';
import { Button } from 'react-bootstrap';
import 'react-pro-sidebar/dist/css/styles.css';
import SideMenu from './components/SideMenu';
import UserStore from './stores/userStore';
import { Navigate } from "react-router-dom";
import { runInAction, makeAutoObservable } from "mobx";

function refreshPage() {
  window.location.reload(false);
}

class Home extends React.Component {

  async componentDidMount() {

    try {
      let res = await fetch('/isLoggedIn', {
        method: 'post',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'

        }
      });
      let result = await res.json();

      if (result && result.success) {
        runInAction(() => {
          UserStore.loading = false;
          UserStore.isLoggedIn = true;
          UserStore.username = result.username;
          UserStore.admin = result.admin;
        });
      }
      else {
        runInAction(() => {
          UserStore.loading = false;
          UserStore.isLoggedIn = false;
        });
      }
    }
    catch (e) {
      runInAction(() => {
        UserStore.loading = false;
        UserStore.isLoggedIn = false;
      });
    }

  }

  render() {

    /*
    if (UserStore.isLoggedIn == false && UserStore.loading == false ) { //TODO FIX
      // alert(UserStore.isLoggedIn)
      //alert("Your session has expired or you have logged out, please log in again");
      return <Navigate to='/' />;
    }
    */

    return (
      <div className='Home'>
        <div className='menu-container'>
          <div class="grid">

            <SideMenu></SideMenu>
            <div class="cards">

              <div class="card">

                <Button className='menubtn'/*onClick={refreshPage}*/ variant="light" href="/#/attack">NEW ATTACK</Button>

              </div>
              <div class="card">

                <Button className='menubtn'/*onClick={refreshPage}*/ variant="light" href="/#/result">RESULT HISTORY</Button>
              </div>
              <div class="card">

                <Button className='menubtn'/*onClick={refreshPage}*/ variant="light" href="/#/configuredAttacks">CONFIGURED ATTACKS</Button>
              </div>
            </div>
          </div>
        </div>
      </div>

    );
  }




}

export default observer(Home);
