const crypto = require('crypto');
const PulsarLib = require('../node-pulsar/PulsarLib/PulsarLib.js');
const fs = require("fs");
const { format } = require('path');

const pulsar_host = process.env.PULSAR_HOST || "localhost";
const pulsar_port = process.env.PULSAR_PORT || "6650";
const general_database = process.env.GENERAL_DB_NAME || "general";

const error_codes = {
  NoSession: 0,
  NoDBConnection: 1,
  FieldNotFound: 2,
  InvalidFormat: 3,
  EmptyAnswer: 4,
  AlreadyExists: 5,
  NoResults: 6,
  UnknownError: 7,
};

function ValidateEmail(mail) {
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
    return true;
  }
  return false;
}
class Router {
  constructor(app) {
    this.login(app);
    this.logout(app);
    this.isLoggedIn(app);
    this.searchAttacks(app);
    this.getAttackOptions(app);
    this.submitAttack(app);
    this.createUser(app);
    this.getAttacks(app);
    this.startAttack(app);
    this.deleteAttack(app);
    this.deleteUser(app);
    this.getNotExecutedAttacks(app);
    this.getPreset(app);
    this.saveWorkflow(app);
    this.startWorkflow(app);
    this.saveConstruct(app);
    this.deleteConstruct(app);
    this.getResult(app);
    this.dashboard(app);
    this.users(app);
    this.getAllConstructs(app);
    this.addSecret(app);
    this.deleteSecret(app);
    this.listSecrets(app);
    this.getSecret(app);
    this.getAttackOptionsById(app);
    this.getAttack(app);
  }
  login(app) {
    app.post("/login", async (req, res) => {
      console.log("Login");
      let username = req.body.username;
      let password = req.body.password;

      //check email
      if (!ValidateEmail(username)) {
        res.json({
          success: false,
          msg: "Enter a valid email please",
        });
        console.log("Bad username");
        return false;
      }

      //hash password
      this.salt = "1";
      var hash = crypto
        .pbkdf2Sync(password, this.salt, 1000, 64, `sha512`)
        .toString(`hex`);

      //Try to connect to DB
      const connect = { username: username, hash: hash };
      const msgRequest = {
        Id: "",
        Msg: connect,
      };

      const client = PulsarLib.InitClient(
        "pulsar://" + pulsar_host + ":" + pulsar_port
      );
      const pulsar_response = await PulsarLib.SendRequestSync(
        client,
        "ui-db.login",
        msgRequest
      );

      try {
        var j_response = JSON.parse(pulsar_response);

        if (j_response.success !== true) {
          res.json({
            success: false,
            msg: "Your email or password is incorrect",
          });
          console.error("Bad email/password");
          return false;
        }
        //success connect
        req.session.userID = username;
        req.session.pass = hash;
        req.session.admin = j_response.admin;
        req.session.session_id = j_response.session_id;
        res.json({
          success: true,
          username: username,
          admin: j_response.admin,
        });
        return true;
      } catch (e) {
        res.json({
          success: false,
          msg: "Invalid Response",
        });
        console.error("Syntax Error in response: " + e.message);
        console.error(j_response);
        return false;
      }
    });
  }
  createUser(app) {
    app.post("/createUser", async (req, res) => {
      console.log("Create User");
      let username = req.body.username;
      let password = req.body.password;

      //check email
      if (!ValidateEmail(username)) {
        res.json({
          success: false,
          msg: "Enter a valid email please",
        });
        console.log("Bad username");
        return false;
      }

      //hash password
      this.salt = "1";
      var hash = crypto
        .pbkdf2Sync(password, this.salt, 1000, 64, `sha512`)
        .toString(`hex`);

      //see if username exists
      const client = PulsarLib.InitClient(
        "pulsar://" + pulsar_host + ":" + pulsar_port
      );
      var my_user = { username: username };
      const msgRequest = {
        Id: "",
        Msg: my_user,
      };
      const server_response = await PulsarLib.SendRequestSync(
        client,
        "ui-db.UserExists",
        msgRequest
      );
      try {
        var j_response = JSON.parse(server_response);
        console.log(j_response);
        if (j_response.success === "true") {
          res.json({
            success: false,
            msg: "Username Exists",
          });
          console.log("User exists");
          return false;
        }
      } catch (e) {
        res.json({
          success: false,
          msg: "Invalid Response",
        });
        console.error("Syntax Error in response: " + e.message);
        console.error(j_response);
        return false;
      }

      my_user = {
        username: username,
        hash: hash,
        session_id: req.session.session_id,
      };

      const pulsar_response = await PulsarLib.SendRequestSync(
        client,
        "ui-db.createUser",
        my_user
      );
      try {
        var j_response = JSON.parse(pulsar_response);
        res.json({
          success: true,
          user: username,
        });
        return true;
      } catch (e) {
        res.json({
          success: false,
          msg: "Invalid Response",
        });
        console.error("Syntax Error in response: " + e.message);
        console.error(j_response);
        return false;
      }
    });
  }
  logout(app) {
    app.post("/logout", async (req, res) => {
      console.log("Logout");
      if (req.session.userID) {
        const disconnect = { session_id: req.session.session_id };
        const msgRequest = {
          Id: "",
          Msg: disconnect,
        };

        const client = PulsarLib.InitClient(
          "pulsar://" + pulsar_host + ":" + pulsar_port
        );
        const pulsar_response = await PulsarLib.SendRequestSync(
          client,
          "ui-db.logout",
          msgRequest
        );

        try {
          var j_response = JSON.parse(pulsar_response);

          if (j_response && "error_code" in obj) {
              if (j_response.error_code === error_codes.NoSession) {
                  req.session.isLoggedIn = false;
                  req.session.destroy();
              }
              res.json({
                  error: error_codes.NoSession,
                  success: false
              })
              return false
          }
          if (j_response.success !== "true") {
            res.json({
              success: false,
              msg: "Logout process failed",
            });
            return false;
          }
          req.session.isLoggedIn = false;
          console.log("log out");
          req.session.destroy();
          res.json({
            success: true,
          });
          return true;
        } catch {
          console.log("Logout failed");
        }
      } else {
        res.json(JSON.parse("{}"));
        return false;
      }
    });
  }
  isLoggedIn(app) {
    app.post("/isLoggedIn", (req, res) => {
      console.log("Is LoggedIn");
      if (req.session.userID) {
        console.log("Logged in");
        res.json({
          success: true,
          username: req.session.userID,
          session_id: req.session.session_id,
          admin: req.session.admin,
        });
        return true;
      } else {
        console.log("Not logged in");
        res.json({
          success: false,
        });
        return false;
      }
    });
  }
  searchAttacks(app) {
    app.post("/searchAttacks", async (req, res) => {
      console.log("Search Attacks");
      const msgRequest = {
        Id: "",
        Msg: {
          session_id: req.session.session_id,
        },
      };

      const client = PulsarLib.InitClient(
        "pulsar://" + pulsar_host + ":" + pulsar_port
      );
      const p_response = await PulsarLib.SendRequestSync(
        client,
        "ui-db.search",
        msgRequest
      );
      try {
        const obj = JSON.parse(p_response);
        console.log(obj);
        if ("error_code" in obj) {
          if (obj.error_code === error_codes.NoSession) {
            req.session.isLoggedIn = false;
            req.session.destroy();
            res.json({
              error: error_codes.NoSession,
              success: false,
            });
          } else if (obj.error_code === error_codes.UnknownError) {
            res.json({});
          }
          return false;
        }
        console.log("getting attacks2");
        res.json(obj);
        return true;
      } catch (e) {
        res.json({
          success: false,
          msg: "Search Attacks Invalid Response",
        });
        console.error("Syntax Error in response: " + e.message);
        return false;
      }
    });
  }
  users(app) {
    app.post("/users", async (req, res) => {
      console.log("Users");
      const msg = { session_id: req.session.session_id };
      const msgRequest = {
        Id: "",
        Msg: msg,
      };

      const client = PulsarLib.InitClient(
        "pulsar://" + pulsar_host + ":" + pulsar_port
      );
      console.log("Getting users");
      const response = await PulsarLib.SendRequestSync(
        client,
        "ui-db.users",
        msgRequest
      );
      try {
        var obj;
        obj = JSON.parse(response);
        if ("error_code" in obj) {
          if (obj.error_code === error_codes.NoSession) {
            req.session.isLoggedIn = false;
            req.session.destroy();
          }
          res.json({
            error: error_codes.NoSession,
            success: false,
          });
          return false;
        }
        if (obj === "" || obj?.success === false) {
          res.json(JSON.parse("[]"));
          return false;
        } else {
          obj = JSON.parse(response);
        }
        res.json(obj);
        return true;
      } catch (e) {
        res.json({
          success: false,
          msg: "Search Attacks Invalid Response",
        });
        console.error("Syntax Error in response: " + e.message);
        return false;
      }
    });
  }
  getAttacks(app) {
    app.post("/results", async (req, res) => {
      console.log("Getting resultsUI");

      let msg = {
        session_id: req.session.session_id,
        state: req.body.state,
        filters: req.body.filters,
      };

      const msgRequest = {
        Id: "",
        Msg: msg,
      };
      const client = PulsarLib.InitClient(
        "pulsar://" + pulsar_host + ":" + pulsar_port
      );
      var attacks = await PulsarLib.SendRequestSync(
        client,
        "ui-db.getAttacks",
        msgRequest
      );
      if (attacks == "null") {
        res.json([]);
        return false;
      }
      try {
        const obj = JSON.parse(attacks);
        if ("error_code" in obj) {
          if (obj.error_code === error_codes.NoSession) {
            req.session.isLoggedIn = false;
            req.session.destroy();
          }
          res.json({
            error: error_codes.NoSession,
            success: false,
          });
          return false;
        }
        res.json(obj);
        return true;
      } catch (e) {
        res.json({
          success: false,
          msg: "Search Attacks Invalid Response",
        });
        console.error("Syntax Error in response: " + e.message);
        return false;
      }
    });
  }
  getPreset(app) {
    app.post("/getPreset", async (req, res) => {
      console.log("Getting preset");
      const attack = req.body.attack;

      if (attack === "undefined") {
        res.json(JSON.parse("{}"));
        return false;
      }

      let msg = { attack_name: attack, session_id: req.session.session_id };
      const msgRequest = {
        Id: "",
        Msg: msg,
      };

      const client = PulsarLib.InitClient(
        "pulsar://" + pulsar_host + ":" + pulsar_port
      );
      const response = await PulsarLib.SendRequestSync(
        client,
        "ui-wm.getAttackPresets",
        msgRequest
      );

      const obj = JSON.parse(response);
      if (obj !== null && "error_code" in obj) {
        if (obj.error_code === error_codes.NoSession) {
          req.session.isLoggedIn = false;
          req.session.destroy();
        }
        res.json({
          error: error_codes.NoSession,
          success: false,
        });
        return false;
      }
      res.json(obj);
      return true;
    });
  }
  saveWorkflow(app) {
    app.post("/saveWorkflow", async (req, res) => {
      console.log("Saving workflow");
      const workflow = req.body.workflow;

      if (workflow === "undefined") {
        res.json(JSON.parse("{}"));
        return false;
      }

      const msgRequest = {
        Id: "",
        Msg: {
          workflow: workflow,
          session_id: req.session.session_id,
          _id: req.body._id,
        },
      };

      const client = PulsarLib.InitClient(
        "pulsar://" + pulsar_host + ":" + pulsar_port
      );
      const p_response = await PulsarLib.SendRequestSync(
        client,
        "ui-wm.saveWorkflow",
        msgRequest
      );

      try {
        let j_response = JSON.parse(p_response);
        if (j_response.success !== "true" && j_response.error_code === error_codes.AlreadyExists) {
          res.json({
            success: false,
            msg: msgRequest.Msg.workflow,
          });
          console.error("Save Workflow Failed");
          return false;
        }
        res.json({
          success: true,
          msg: p_response,
        });
        return true;
      } catch (e) {
        res.json({
          success: false,
          msg: "Save Workflow Invalid Response",
        });
        console.error("Syntax Error in response: " + e.message);
        return false;
      }
    });
  }
  startWorkflow(app) {
    app.post("/startWorkflow", async (req, res) => {
      console.log("Start workflow");
      const workflowID = req.body.workflowID;

      if (workflowID === "undefined") {
        res.json(JSON.parse("{}"));
        return false;
      }

      const msgRequest = {
        Id: "",
        Msg: {
          workflow_id: workflowID,
          session_id: req.session.session_id,
          _id: req.body._id,
        },
      };
      const client = PulsarLib.InitClient(
        "pulsar://" + pulsar_host + ":" + pulsar_port
      );
      const p_response = await PulsarLib.SendRequestSync(
        client,
        "ui-wm.startWorkflow",
        msgRequest
      );

      res.json(JSON.parse(p_response));
      return true;
    });
  }
  saveConstruct(app) {
    app.post("/saveConstruct", async (req, res) => {
      console.log("Saving construct");
      const construct = req.body.construct;

      if (construct === "undefined") {
        res.json(JSON.parse("{}"));
        return false;
      }

      const msgRequest = {
        Id: "",
        Msg: {
          construct: construct,
          session_id: req.session.session_id,
          _id: req.body._id,
        },
      };
      const client = PulsarLib.InitClient(
        "pulsar://" + pulsar_host + ":" + pulsar_port
      );
      PulsarLib.SendMessage(
        client,
        "ui-wm.saveConstruct",
        JSON.stringify(msgRequest)
      );

      res.json(JSON.parse("{}"));
      return true;
    });
  }
  getAllConstructs(app) {
    app.post("/getAllConstructs", async (req, res) => {
      console.log("Getting all constructs");
      const msgRequest = {
        Id: "",
        Msg: {
          session_id: req.session.session_id,
          _id: req.body._id,
        },
      };
      const client = PulsarLib.InitClient(
        "pulsar://" + pulsar_host + ":" + pulsar_port
      );
      const response = await PulsarLib.SendRequestSync(
        client,
        "ui-wm.getAllConstructs",
        msgRequest
      );
      const obj = JSON.parse(response);
      if (obj !== null && "error_code" in obj) {
        if (obj.error_code === error_codes.NoSession) {
          req.session.isLoggedIn = false;
          req.session.destroy();
        }
        res.json({
          error: error_codes.NoSession,
          success: false,
        });
        return false;
      }
      res.json(obj);
      return true;
    });
  }
  deleteConstruct(app) {
    app.post("/deleteConstruct", async (req, res) => {
      console.log("Delete construct");
      const constructID = req.body.constructID;

      if (constructID === "undefined") {
        res.json(JSON.parse("{}"));
        return false;
      }

      const msgRequest = {
        Id: "",
        Msg: {
          construct_id: constructID,
          session_id: req.session.session_id,
          _id: req.body._id,
        },
      };
      const client = PulsarLib.InitClient(
        "pulsar://" + pulsar_host + ":" + pulsar_port
      );
      PulsarLib.SendMessage(
        client,
        "ui-wm.deleteConstruct",
        JSON.stringify(msgRequest)
      );

      res.json(JSON.parse("{}"));
      return true;
    });
  }
  submitAttack(app) {
    app.post("/submitAttack", async (req, res) => {
      console.log("Submitting Attack");
      const attack = req.body.attack;
      if (typeof attack === "undefined") {
        console.log(attack);
        res.json(JSON.parse("{}"));
        return false;
      }
      
      const title = req.body.attackName;
      
      const msgRequest = {
        Id: "",
        Msg: {
          attack_name: attack,
          attack_title: title,
          session_id: req.session.session_id,
          options: req.body.options,
        },
      };
      
      const client = PulsarLib.InitClient(
        "pulsar://" + pulsar_host + ":" + pulsar_port
      );
      const response = await PulsarLib.SendRequestSync(
        client,
        "ui-db.AttackSubmission",
        msgRequest
      );
      
      const obj = JSON.parse(response);

      if (obj !== null && "error_code" in obj) {
        if (obj.error_code === error_codes.NoSession) {
          req.session.isLoggedIn = false;
          req.session.destroy();
        }
        res.json({
          error: error_codes.NoSession,
          success: false,
        });
        return false;
      }
      res.json(obj);
      return true;
    });
  }
  startAttack(app) {
    app.post("/startAttack", async (req, res) => {
      console.log("Staring attack");
      const attack = req.body.nameAttack;

      if (typeof attack === "undefined") {
        res.json(JSON.parse("{}"));
        return false;
      }

      const title = req.body.attackName;
      const msgRequest = {
        Id: "",
        Msg: {
          attack_name: attack,
          session_id: req.session.session_id,
          _id: req.body._id,
        },
      };
      const client = PulsarLib.InitClient(
        "pulsar://" + pulsar_host + ":" + pulsar_port
      );
      PulsarLib.SendMessage(
        client,
        "ui-db.startAttack",
        JSON.stringify(msgRequest)
      );

      res.status(200).json(JSON.parse("{}"));
      return true;
    });
  }
  deleteAttack(app) {
    app.post("/deleteAttack", async (req, res) => {
      console.log("Deleting Attack");
      const attack = req.body.nameAttack;

      if (typeof attack === "undefined") {
        res.json(JSON.parse("{}"));
        return false;
      }
      const msgRequest = {
        Id: "",
        Msg: {
          attack_name: attack,
          session_id: req.session.session_id,
          _id: req.body._id,
        },
      };
      const client = PulsarLib.InitClient(
        "pulsar://" + pulsar_host + ":" + pulsar_port
      );
      const response = await PulsarLib.SendRequestSync(
        client,
        "ui-db.deleteAttack",
        msgRequest
      );

      const obj = JSON.parse(response);
      if ("error_code" in obj) {
        if (obj.error_code === error_codes.NoSession) {
          req.session.isLoggedIn = false;
          req.session.destroy();
        }
        res.json({
          error: error_codes.NoSession,
          success: false,
        });
        return false;
      }
      res.json(obj);
      return true;
    });
  }
  deleteUser(app) {
    app.post("/deleteUser", async (req, res) => {
      console.log("Deleting user");
      const user = req.body.user;
      const msgRequest = {
        Id: "",
        Msg: {
          username: user,
          session_id: req.session.session_id,
        },
      };

      const client = PulsarLib.InitClient(
        "pulsar://" + pulsar_host + ":" + pulsar_port
      );
      const response = await PulsarLib.SendRequestSync(
        client,
        "ui-db.deleteUser",
        msgRequest
      );
      const obj = JSON.parse(response);

      if ("error_code" in obj) {
        if (obj.error_code === error_codes.NoSession) {
          req.session.isLoggedIn = false;
          req.session.destroy();
        }
        res.json({
          error: error_codes.NoSession,
          success: false,
        });
        return false;
      }

      obj = JSON.parse(response);
      res.json(obj);
      return true;
    });
  }
  getNotExecutedAttacks(app) {
    app.post("/getNotExecutedAttacks", async (req, res) => {
      console.log("getting Attack");

      const msgRequest = {
        Id: "",
        Msg: {
          session_id: req.session.session_id,
        },
      };

      const client = PulsarLib.InitClient(
        "pulsar://" + pulsar_host + ":" + pulsar_port
      );
      const p_response = await PulsarLib.SendRequestSync(
        client,
        "ui-db.getNotExecutedAttacks",
        msgRequest
      );

      try {
        let obj = JSON.parse(p_response);
        if (obj !== "[]" && "error_code" in obj) {
          if (obj.error_code === error_codes.NoSession) {
            req.session.isLoggedIn = false;
            req.session.destroy();
          }
          res.json({
            error: error_codes.NoSession,
            success: false,
          });
          return false;
        }
        console.log(p_response);
        var attacks = [];
        if (obj.length === 0) {
          res.json({
            success: true,
            error: "Get Attacks Empty Response",
          });
          console.log("Empty Response: no attacks");
          return true;
        }
        obj.forEach((element) => {
          var str = element.attackname
            .replace(/[']/g, "")
            .replace(/\u0000/g, "\\0");
          attacks.push({
            name: str,
            _id: element._id,
          });
        });
        console.log(res);
        res.json(attacks);
        return true;
      } catch (e) {
        res.json({
          success: false,
          error: "Get Attacks Invalid Response",
        });
        console.error("Syntax Error in response: " + e.message);
        return false;
      }
    });
  }
  getAttackOptions(app) {
    app.post("/getAttackOptions", async (req, res) => {
      console.log("Here we are with options:");
      const msgRequest = {
        Id: "",
        Msg: { name: req.body.nameAttack, session_id: req.session.session_id },
      };
      req.session.attack = req.body.nameAttack;

      const client = PulsarLib.InitClient(
        "pulsar://" + pulsar_host + ":" + pulsar_port
      );
      const p_response = await PulsarLib.SendRequestSync(
        client,
        "ui-db.getAttackOptions",
        msgRequest
      );
      
      const obj = JSON.parse(p_response);
      if (obj && "error_code" in obj) {
        if (obj.error_code === error_codes.NoSession) {
          req.session.isLoggedIn = false;
          req.session.destroy();
        }
        res.json({
          error: error_codes.NoSession,
          success: false,
        });
        return false;
      } else if (!obj) {
        res.json({
          error: error_codes.InvalidFormat,
          success: false,
        });
        return false;
      }

      console.log("getting options");

      res.json(obj);

      return true;
    });
  }
  getResult(app) {
    app.post("/getResult", async (req, res) => {
      console.log("Get single Result");
      const attackname = req.body.nameAttack;

      const msgRequest = {
        Id: "",
        Msg: {
          attack_name: attackname,
          session_id: req.session.session_id,
          attack_id: req.body._id,
        },
      };

      req.session.attack = req.body.nameAttack;

      const client = PulsarLib.InitClient(
        "pulsar://" + pulsar_host + ":" + pulsar_port
      );
      const pulsar_response = await PulsarLib.SendRequestSync(
        client,
        "ui-db.getResult",
        msgRequest
      );
      try {
        const obj = JSON.parse(pulsar_response);
        if ("error_code" in obj) {
          if (obj.error_code === error_codes.NoSession) {
            req.session.isLoggedIn = false;
            req.session.destroy();
          }
          res.json({
            error: error_codes.NoSession,
            success: false,
          });
          return false;
        }
        console.log(JSON.stringify(obj));
        res.json(obj);
      } catch (json_error) {
        console.log("Pulsar response " + pulsar_response);
        console.log(json_error);
        res.json(JSON.parse("{}"));
        return false;
      }
      return true;
    });
  }

  dashboard(app) {
    app.post("/dashboard", async (req, res) => {
      console.log("Dashboard");
      const typeAttack = req.body.typeAttack;

      if (typeof typeAttack === "undefined") {
        res.json(JSON.parse("{}"));
        return false;
      }

      const msgRequest = {
        Id: "",
        Msg: {
          type_of_attack: typeAttack,
          session_id: req.session.session_id,
        },
      };

      const client = PulsarLib.InitClient(
        "pulsar://" + pulsar_host + ":" + pulsar_port
      );
      const pulsar_response = await PulsarLib.SendRequestSync(
        client,
        "ui-db.getDashboard",
        msgRequest
      );
      
      var output;
      try {
        const obj = JSON.parse(pulsar_response);
        if ("error_code" in obj) {
          if (obj.error_code === error_codes.NoSession) {
            req.session.isLoggedIn = false;
            req.session.destroy();
          }
          res.json({
            error: error_codes.NoSession,
            success: false,
          });
          return false;
        } else if (
          !"name" in obj ||
          !"executed" in obj ||
          !"executing" in obj ||
          !"error" in obj ||
          !"attackmonth" in obj ||
          !"avgtime" in obj
        ) {
          output = [{}];
        } else {
          output = [
            {
              name: obj.attacknamedashboard,
              executed: obj.options.executed,
              executing: obj.options.executing,
              error: obj.options.error,
              attackmonth: obj.options.attackmonth,
              avgtime: obj.options.avgtime,
            },
          ];
        }
        console.log(JSON.stringify(output));
        res.json(output);
        return true;
      } catch (json_error) {
        if (pulsar_response !== "") {
          console.log("Pulsar response " + pulsar_response);
          console.log(json_error);
        }
        res.json(JSON.parse("{}"));
        return false;
      }
      
      /*    var times = nameArr[4].split(',');
                var sum = 0;
                for (let i = 0; i < times.length; i++) {
                    sum += parseInt(times[i])
                }
                sum = sum / times.length;
                nameArr[4]=sum.toString();
            }
            if (nameArr[5] == "")
                nameArr[5]="0"
            else {
                var month = nameArr[5].split(',');
                nameArr[5]=month[1];
            }
            attacks += '"name":"' + nameArr[0]+'",'
            attacks += '"executing":"' + nameArr[1]+'",'
            attacks += '"executed":"' + nameArr[2]+'",'
            attacks += '"error":"' + nameArr[3]+'",'
            attacks += '"avgtime":"' + nameArr[4]+'",'
            attacks += '"attackmonth":"' + nameArr[5]+'"'
    
            attacks += '}]'
            console.log(attacks )
            const obj = JSON.parse(attacks);
         
*/
    });

    
  }
  addSecret(app) {
    app.post("/addSecret", async (req, res) => {
      console.log("addSecret - Router");
      console.log("Datos: " + req.session.session_id + " - " + req.body.key + " - " + req.body.description + " - " + req.body.secret);
      
      const msgRequest = {
        Id: "",
        Msg: {
            session_id: req.session.session_id,
            secret: {
                secret_key: req.body.key,
                description: req.body.description,
                value: req.body.secret
            }
        }
      };

      try {
        const client = PulsarLib.InitClient(
            "pulsar://" + pulsar_host + ":" + pulsar_port
        );
        
        const response = await PulsarLib.SendRequestSync(
            client,
            "ui-db.addSecret",
            msgRequest
        );

        res.status(200).json({
          success: true,
          message: "Secret added successfully"
        });

      } catch (error) {
          console.error("Error adding secret:", error);
          res.status(500).json({
              success: false,
              error: "Failed to add secret: " + error.message
          });
      }
    });
    
  }
  getSecret(app) {
    app.post("/getSecret", async (req, res) => {
      console.log("getSecret - Router");
      
      
      const msgRequest = {
        Msg: {
          session_id: req.session.session_id,
          secret_key: req.body.secret
        },
      };

      
      const client = PulsarLib.InitClient(
          "pulsar://" + pulsar_host + ":" + pulsar_port
      );
      
      try {
        const pulsar_response = await PulsarLib.SendRequestSync(
            client,
            "ui-db.getSecret",
            msgRequest
        );

        if (!pulsar_response) {
            console.error("No response from Pulsar");
            return res.status(500).json({ error: "No response from Pulsar" });
        }

        const obj = JSON.parse(pulsar_response);
        console.log("Response from Pulsar:", obj);

        res.status(200).json(obj); 
      } catch (error) {
          console.error("Error in getSecret:", error);
          res.status(500).json({ error: "Internal server error" });
      }
    });
    
  }

  deleteSecret(app) {
    app.post("/deleteSecret", async (req, res) => {
      console.log("deleteSecret - Router");

      const msgRequest = {
        Msg: {
            session_id: req.session.session_id,
            secret_key: req.body.secret
        },
      };

      const client = PulsarLib.InitClient(
        "pulsar://" + pulsar_host + ":" + pulsar_port
      );
      const pulsar_response = await PulsarLib.SendRequestSync(
        client,
        "ui-db.deleteSecret",
        msgRequest
      );
      
      try {
        const obj = JSON.parse(pulsar_response);
        if(res.status(200)){
          console.log("Success deleting key");
          res.json(obj);
        }

      } catch (json_error) {
          console.error("Error parsing Pulsar response:", json_error);
          console.log("Pulsar response:", pulsar_response);

          res.status(500).json({
              success: false,
              error: "Error processing delete secret",
          });
      }
    });
  }
  listSecrets(app) {
    app.post("/listSecrets", async (req, res) => {
      console.log("listSecrets - Router");

      const msgRequest = {
        Msg: {
            session_id: req.session.session_id,
        },
      };

      const client = PulsarLib.InitClient(
        "pulsar://" + pulsar_host + ":" + pulsar_port
      );
      const pulsar_response = await PulsarLib.SendRequestSync(
        client,
        "ui-db.listSecrets",
        msgRequest
      );
      
      try {
        
        const obj = JSON.parse(pulsar_response);
        console.log(obj);

        const output = obj.map((secret, index) => ({
            id: index + 1,
            secret_key: secret.secret_key || "Unknown Key",
            description: secret.description || "No description available",
            secret: secret.value || "error pass",
        }));

        console.log("Formatted Secrets:", output);
        res.json(output);
      } catch (json_error) {
          console.error("Error parsing Pulsar response:", json_error);
          console.log("Pulsar response:", pulsar_response);

          res.status(500).json({
              success: false,
              error: "Error processing secrets",
          });
      }
    });
  }

  /**
   * Get the options of an existing attack by its options id
   * 
   * @param {int} req.body.optionsId id of the attack's options
   * @param {iny} req.body.session_id id of the session
   */
  getAttackOptionsById(app) {
    app.post("/getAttackOptionsById", async (req, res) => {
      console.log("Trying to retrieve the options file:");
      const msgRequest = {
        Id: "",
        Msg: { optionsId: req.body.optionsId,
               session_id: req.session.session_id },
      };
      req.session.attack = req.body.nameAttack;

      const client = PulsarLib.InitClient(
        "pulsar://" + pulsar_host + ":" + pulsar_port
      );
      const p_response = await PulsarLib.SendRequestSync(
        client,
        "ui-db.getOptions",
        msgRequest
      );
      
      const obj = JSON.parse(p_response);
      if (obj && "error_code" in obj) {
        if (obj.error_code === error_codes.NoSession) {
          req.session.isLoggedIn = false;
          req.session.destroy();
        }
        res.json({
          error: error_codes.NoSession,
          success: false,
        });
        return false;
      } else if (!obj) {
        res.json({
          error: error_codes.InvalidFormat,
          success: false,
        });
        return false;
      }

      console.log("getting options from specific id:");
      console.log(obj);

      res.json(obj);

      return true;
    });
  }

  /**
   * Gets informatio about the attack with id
   * 
   * @param {int} req.body.attack_id id of the attack
   * @param {iny} req.body.session_id id of the session
   */
  getAttack(app) {
    app.post("/getAttack", async (req, res) => {
      console.log("Getting attack info with id: " + req.body.attack_id);
      const msgRequest = {
        Id: "",
        Msg: { 
          attack_id: req.body.attack_id,
          session_id: req.session.session_id,
        },
      };

      const client = PulsarLib.InitClient(
        "pulsar://" + pulsar_host + ":" + pulsar_port
      );

      const p_response = await PulsarLib.SendRequestSync(
        client,
        "ui-db.getAttack",
        msgRequest
      );
      const obj = JSON.parse(p_response);
      console.log(obj);
      if (obj && "error_code" in obj) {
        if (obj.error_code === error_codes.NoSession) {
          req.session.isLoggedIn = false;
          req.session.destroy();
        }
         
        return res.status(400).json({ //Error there is no session
          error: error_codes.NoSession,
          success: false,
        });
      } else if (!obj) { //Error format invalid
        return res.status(400).json({
          error: error_codes.InvalidFormat,
          success: false,
        });
      }

      return res.status(200).json(obj); //putting the obtained in the response
    });
  }
}

module.exports = Router;
