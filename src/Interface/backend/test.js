import React from "react";
import InputField from "./InputField";
import SubmitButton from "./SubmitButton";
import userStore from "../../stores/userStore";
import { observer } from "mobx-react";
import login_image from "../../images/login_logo.png";
import person from "../../images/person.png";
import lock from "../../images/padlock.png";

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      session_id: "",
      buttonDisabled: false,
    };
  }
  setInputValue(property, val) {
    //max length username,pass
    val = val.trim();
    this.setState({
      [property]: val,
    });
  }
  resetForm() {
    this.setState({
      username: "",
      password: "",
      buttonDisabled: false,
    });
  }
  async doLogin() {
    if (!this.state.username) {
      return;
    }
    if (!this.state.password) {
      return;
    }
    this.setState({ buttonDisabled: true });
    try {
      let res = await fetch("/login", {
        method: "post",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          //send username and password
          username: this.state.username,
          password: this.state.password,
        }),
      });
      let result = await res.json();
      if (result && result.success) {
        userStore.isLoggedIn = true;
        userStore.username = result.username;
        userStore.admin = result.admin;
        userStore.session_id = result.session_id;

      } else if (result && result.success === false) {
        this.resetForm();
        alert(result.msg);
      }
    } catch (e) {
      console.log(e);
      this.resetForm();
    }
  }

  render() {
    return (
      <div className="loginForm">
        <div className="left"></div>
        <div className="right">
          <div className="login_title">
            <img className="login_logo" src={login_image} alt=""></img>
            <h1 id="login">HuntDown</h1>
          </div>
          <div className="email_logo">
            <img className="person_logo" src={person} alt=""></img>
            <p id="login_email">Email Address</p>
          </div>
          <InputField
            type="text"
            placeholder="Enter email"
            value={this.state.username ? this.state.username : ""}
            onChange={(val) => this.setInputValue("username", val)}
          />
          <div className="pass_logo">
            <img className="lock_logo" src={lock} alt=""></img>
            <p id="login_pass">Password</p>
          </div>
          <InputField
            type="password"
            placeholder="Enter password"
            value={this.state.password ? this.state.password : ""}
            onChange={(val) => this.setInputValue("password", val)}
          />
          <SubmitButton
            text="SUBMIT"
            disabled={this.state.buttonDisabled}
            onClick={() => this.doLogin()}
          />
        </div>
      </div>
    );
  }
}

export default LoginForm;