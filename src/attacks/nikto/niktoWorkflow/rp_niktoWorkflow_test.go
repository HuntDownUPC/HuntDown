package niktoWorkflow

import (
	"encoding/json"
	"io/ioutil"
	"testing"
)

func TestNiktoWorkflow(t *testing.T) {
	// create mock input data
	data, err := ioutil.ReadFile("tests/crash_test1.json")
	if err != nil {
		t.Fatalf("failed to read input file: %v", err)
	}

	// unmarshal the JSON data into a map[string]interface{}
	var mockResults map[string]interface{}
	if err := json.Unmarshal(data, &mockResults); err != nil {
		t.Fatalf("failed to unmarshal input JSON: %v", err)
	}

	// call the function
	resultBytes := ExtractVulnerabilities(mockResults)

	var result map[string]interface{}
	if err := json.Unmarshal(resultBytes, &result); err != nil {
		t.Errorf("unexpected error: %v", err)
	}
	if _, ok := result["processedOutput"]; !ok {
		t.Errorf("processedOutput not found in result")
	}
	processedOutput := result["processedOutput"].([]interface{})

	// Verify the length and content of processedOutput
	expectedVulnerabilities := []map[string]string{
		{"OSVDB-29786": "/admin.php?en_log_id=0\u0026action=config"},
		{"OSVDB-3092": "/admin.php"},
	}
	if len(processedOutput) != len(expectedVulnerabilities) {
		t.Errorf("unexpected length of processedOutput: %d", len(processedOutput))
	}
	for i, vulnInfo := range processedOutput {
		for key, value := range vulnInfo.(map[string]interface{}) {
			if expectedVulnerabilities[i][key] != value.(string) {
				t.Errorf("unexpected vulnerability info at index %d, expected %s:%s, got %s:%s", i, key, expectedVulnerabilities[i][key], key, value)
			}
		}
	}
}
