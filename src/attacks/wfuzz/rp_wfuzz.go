package main

import (
	"encoding/json"
)

var Name = "wfuzz"

func TableGenerator(results map[string]interface{}) []byte {
	out, _ := json.Marshal(results)
	return out
}

func ResultProcessor(results map[string]interface{}) []byte {
	out, _ := json.Marshal(results)
	return out
}

func WorkflowProcessor(results map[string]interface{}) []byte {
	out, _ := json.Marshal(results)
	return out
}

func WfuzzTableGenerator(results map[string]interface{}) []byte {
	bytes_final, _ := json.Marshal(results)
	return bytes_final
}
