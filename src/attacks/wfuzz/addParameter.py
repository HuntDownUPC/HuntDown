import json

# Ruta del archivo JSON
json_path = "c:/Users/bruno/huntdown/HuntDown/src/attacks/wpscan/wpscan-options.json"

# Cargar el archivo JSON
with open(json_path, "r", encoding="utf-8") as file:
    data = json.load(file)

# Nuevo parámetro a añadir
new_parameter = {
    "formattype": "select"
}

# Iterar sobre cada elemento en la lista de datos
for item in data:
    # Verificar si el elemento tiene la clave "options" y es una lista
    if "options" in item and isinstance(item["options"], list):
        for option in item["options"]:
            # Añadir el parámetro `type` si no existe en el objeto `option`
            if "mode" not in option:
                option.update(new_parameter)

# Guardar los cambios en el archivo JSON
with open(json_path, "w", encoding="utf-8") as file:
    json.dump(data, file, indent=4, ensure_ascii=False)

print("Parámetro 'type' añadido correctamente en cada elemento de 'options'.")
