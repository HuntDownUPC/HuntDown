package wfuzzworkflow

import (
	"encoding/json"
	"io/ioutil"
	"testing"
)

func TestWfuzzWorkflow(t *testing.T) {
	// create mock input data
	data, err := ioutil.ReadFile("tests/crash_test1.json")
	if err != nil {
		t.Fatalf("failed to read input file: %v", err)
	}

	// unmarshal the JSON data into a map[string]interface{}
	var mockResults map[string]interface{}
	if err := json.Unmarshal(data, &mockResults); err != nil {
		t.Fatalf("failed to unmarshal input JSON: %v", err)
	}

	// call the function
	resultBytes := ExtractPortsInfo(mockResults)
	// check the result
	var result map[string]interface{}
	if err := json.Unmarshal(resultBytes, &result); err != nil {
		t.Errorf("unexpected error: %v", err)
	}
	if _, ok := result["processedOutput"]; !ok {
		t.Errorf("processedOutput not found in result")
	}
	processedOutput := result["processedOutput"].([]interface{})

	// Verify the length and content of processedOutput
	expectedPortsInfo := []map[string]string{
		{"22": "open"},
	}
	if len(processedOutput) != len(expectedPortsInfo) {
		t.Errorf("unexpected length of processedOutput: %d", len(processedOutput))
	}
	for i, portInfo := range processedOutput {
		for key, value := range portInfo.(map[string]interface{}) {
			if expectedPortsInfo[i][key] != value.(string) {
				t.Errorf("unexpected port info at index %d, expected %s:%s, got %s:%s", i, key, expectedPortsInfo[i][key], key, value)
			}
		}
	}
}
