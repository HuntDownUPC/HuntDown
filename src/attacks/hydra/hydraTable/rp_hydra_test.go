package hydratable

import (
	"encoding/json"
	"io/ioutil"
	"testing"
)

func TestNmapTableGenerator(t *testing.T) {
	// create mock input data
	data, err := ioutil.ReadFile("tests/crash_test1.json")
	if err != nil {
		t.Fatalf("failed to read input file: %v", err)
	}

	// unmarshal the JSON data into a map[string]interface{}
	var mockResults map[string]interface{}
	if err := json.Unmarshal(data, &mockResults); err != nil {
		t.Fatalf("failed to unmarshal input JSON: %v", err)
	}

	// call the function
	resultBytes := HydraTableGenerator(mockResults)

	// check the result
	var result map[string]interface{}
	if err := json.Unmarshal(resultBytes, &result); err != nil {
		t.Errorf("unexpected error: %v", err)
	}
	if _, ok := result["commandsresult"]; !ok {
		t.Errorf("commandsresult not found in result")
	}
	commandsResult := result["commandsresult"].([]interface{})
	if len(commandsResult) != 1 {
		t.Errorf("unexpected length of commandsresult: %d", len(commandsResult))
	}
	commandResult := commandsResult[0].(map[string]interface{})
	if _, ok := commandResult["table"]; !ok {
		t.Errorf("table not found in command result")
	}
	table := commandResult["table"].([]interface{})
	if len(table) != 1 {
		t.Errorf("unexpected length of table: %d", len(table))
	}
}