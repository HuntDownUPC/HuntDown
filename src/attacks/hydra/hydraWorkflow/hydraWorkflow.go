package hydraworkflow

import "encoding/json"

func ExtractUserPassword(results map[string]interface{}) []byte {
	userPassMap := make([]map[string]string, 0)

	commandsResult := results["commandsresult"].([]interface{})
	for _, indivAttack := range commandsResult {
		if indivAttack != nil && indivAttack.(map[string]interface{})["resultsjson"] != nil {
			resultsjson := indivAttack.(map[string]interface{})["resultsjson"].(map[string]interface{})
			resultsArr := resultsjson["results"].([]interface{})
			for _, indivResult := range resultsArr {
				login := indivResult.(map[string]interface{})["login"].(string)
				password := indivResult.(map[string]interface{})["password"].(string)
				userPassMap = append(userPassMap, map[string]string{login: password})
			}
		}
	}

	// Store the processed user-password information in the object "results"
	results["processedOutput"] = userPassMap

	schema_bytes, _ := json.Marshal(results)
	return schema_bytes
}
