package main

import (
	ht "attacks/hydra/hydraTable"
	hw "attacks/hydra/hydraWorkflow"
	"encoding/base64"
	"encoding/json"
	"log"
	"regexp"
	"strconv"
	"strings"
)

var Name = "hydra"

func cleanText(text string) string {
	// Expresión regular para eliminar los códigos de escape ANSI
	re := regexp.MustCompile(`\x1b\[[0-9;]*m`)
	// Eliminar los códigos de escape de la cadena de texto
	cleanedText := re.ReplaceAllString(text, "")
	return cleanedText
}

type HydraData struct {
	Solutions []Solution `json:"solutions"`
}

type Solution struct {
	Port     int    `json:"port"`
	Service  string `json:"service"`
	Host     string `json:"host"`
	Login    string `json:"login"`
	Password string `json:"password"`
}

func TableGenerator(results map[string]interface{}) []byte {
	return ht.HydraTableGenerator(results)
}

func ResultProcessor(results map[string]interface{}) []byte {
	data := HydraData{
		Solutions: []Solution{},
	}

	if base64Output, ok := results["commandsresult"].([]interface{})[0].(map[string]interface{})["resultsoutput"].(string); ok {
		outputBytes, err := base64.StdEncoding.DecodeString(base64Output)
		if err != nil {
			log.Println("Error decoding base64:", err)
			return nil
		}
		output := string(outputBytes)
		lines := strings.Split(output, "\n")

		for _, line := range lines {
			if strings.Contains(line, "[") && strings.Contains(line, "]") {
				// Extracting port, service, host, login, and password
				re := regexp.MustCompile(`\[([^\[\]]+)\]`)
				matches := re.FindStringSubmatch(line)
				if len(matches) >= 6 {
					port, _ := strconv.Atoi(matches[1])
					service := matches[2]
					host := matches[3]
					login := matches[4]
					password := matches[5]

					solution := Solution{
						Port:     port,
						Service:  service,
						Host:     host,
						Login:    login,
						Password: password,
					}
					data.Solutions = append(data.Solutions, solution)
				}
			}
		}

	}
	results["processedOutput"] = data
	out, _ := json.Marshal(results)
	return out
}

func WorkflowProcessor(results map[string]interface{}) []byte {
	return hw.ExtractUserPassword(results)
}
