package main

import (
	ge "attacks/goldeneye/goldeneyeWorkflow"
	"encoding/json"
)

var Name = "goldeneye"

func TableGenerator(results map[string]interface{}) []byte {
	return goldeneyeTableGenerator(results)
}

func ResultProcessor(results map[string]interface{}) []byte {
	// Hydra supports JSON output so we don't handle that
	out, _ := json.Marshal(results)
	return out
}

func WorkflowProcessor(results map[string]interface{}) []byte {
	return ge.ExtractInfo(results)
}

func goldeneyeTableGenerator(results map[string]interface{}) []byte {
	bytes_final, _ := json.Marshal(results)
	return bytes_final
}
