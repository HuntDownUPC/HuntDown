package goldeneyeWorkflow

import (
	"encoding/base64"
	"encoding/json"
	"strings"
)

/*
"processedOutput":[

	{
		"142.250.201.68":"DoS attack failed"
	}

]
*/
func ExtractInfo(results map[string]interface{}) []byte {
	var attackResultM []map[string]string
	if base64Output, ok := results["commandsresult"].([]interface{})[0].(map[string]interface{})["resultsoutput"].(string); ok {
		outputBytes, err := base64.StdEncoding.DecodeString(base64Output)
		if err != nil {
			return nil
		}
		output := string(outputBytes)
		lines := strings.Split(output, "\n")

		attackResult := make(map[string]string)

		for _, line := range lines {
			if strings.Contains(line, "open") || strings.Contains(line, "(UNKNOWN)") {
				fields := strings.Fields(line)
				if len(fields) > 1 {
					ip := strings.TrimSpace(fields[1])
					// Trim brackets
					ip = strings.Trim(ip, "[]")
					if strings.Contains(line, "open") {
						attackResult[ip] = "DoS attack failed"
					} else {
						attackResult[ip] = "DoS attack might have been successful"
					}
				}
			}
		}

		// Convert the attackResult map to a slice of maps
		for ip, status := range attackResult {
			attackResultM = append(attackResultM, map[string]string{ip: status})
		}
	}

	// Store the processed attack information in the object "results".
	results["processedOutput"] = attackResultM
	schema_bytes, _ := json.Marshal(results)
	return schema_bytes
}
