package main

import (
	"encoding/json"
	"log"
	"os"
	"testing"
)

func TestTableUnique(t *testing.T) {
	// create mock input data
	data, err := os.ReadFile("tests/test1.json")
	if err != nil {
		t.Fatalf("failed to read input file: %v", err)
	}

	// unmarshal the JSON data into a map[string]interface{}
	var mockResults map[string]interface{}
	if err := json.Unmarshal(data, &mockResults); err != nil {
		t.Fatalf("failed to unmarshal input JSON: %v", err)
	}

	// call the function
	resultBytes := ResultProcessor(mockResults)
	if err := json.Unmarshal(resultBytes, &mockResults); err != nil {
		t.Fatalf("failed to unmarshal input JSON: %v", err)
	}
	log.Println(string(resultBytes))

	resultBytes = TableGenerator(mockResults)

	log.Println(string(resultBytes))
}
