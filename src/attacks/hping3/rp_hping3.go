package main

import (
	aa "attack-manager/AttackAbstraction"
	hw "attacks/hping3/hping3Workflow"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"strings"
)

var Name = "hping3"

func TableGenerator(results map[string]interface{}) []byte {
	return hping3TableGenerator(results)
}

func ResultProcessor(results map[string]interface{}) []byte {
	data := make(map[string]interface{})
	if base64Output, ok := results["commandsresult"].([]interface{})[0].(map[string]interface{})["resultsoutput"].(string); ok {
		outputBytes, err := base64.StdEncoding.DecodeString(base64Output)
		if err != nil {
			log.Println("Error decoding base64:", err)
			return nil
		}
		output := string(outputBytes)
		lines := strings.Split(output, "\n")
		packetCount := 0
		var hops []map[string]interface{}

		for _, line := range lines {
			if strings.Contains(line, "(") {
				stringIp := strings.Split(line, "(")
				ipNotYet := strings.Split(stringIp[1], " ")[1]
				if strings.Contains(ipNotYet, ")") {
					ip := strings.Split(ipNotYet, ")")[0]
					data["ip"] = ip
				}
			}
			hopInfo := make(map[string]interface{})

			if strings.Contains(line, "seq=") {
				stringSequenceNumber := strings.Split(line, "seq=")[1]
				sequenceNumber := strings.Split(stringSequenceNumber, " ")[0]
				hopInfo["sequence"], _ = strconv.Atoi(sequenceNumber)
			}

			if strings.Contains(line, "dport=") {
				stringDport := strings.Split(line, "dport=")[1]
				dport := strings.Split(stringDport, " ")[0]
				hopInfo["dport"], _ = strconv.Atoi(dport)
			}

			if strings.Contains(line, "sport=") {
				stringSport := strings.Split(line, "sport=")[1]
				sport := strings.Split(stringSport, " ")[0]
				hopInfo["sport"], _ = strconv.Atoi(sport)
			}

			if strings.Contains(line, "rtt=") {
				rtt := strings.Split(line, "rtt=")[1]
				hopInfo["rtt"] = rtt
			}

			if strings.Contains(line, "0 packets received") {
				fmt.Println("Failed to receive packets.")
				break
			}
			if len(hopInfo) != 0 {
				hops = append(hops, hopInfo)
			}
			packetCount++
		}
		data["hops"] = hops
		data["packets"] = packetCount
	}
	log.Println("Processed output", data)
	results["processedOutput"] = data
	results["commandsresult"].([]interface{})[0].(map[string]interface{})["resultsjson"] = data
	out, _ := json.Marshal(results)
	return out
}

func WorkflowProcessor(results map[string]interface{}) []byte {
	return hw.ExtractHostStatus(results)
}

func internalTableGeneration(results map[string]interface{}) []byte {
	auxTable := aa.NewTable()
	var row aa.Row

	row.AddColumnFields("true", "IP")
	row.AddColumnFields("true", aa.GetMapInterfaceField(results, "ip").(string))
	auxTable.AddRow(row)
	row.Columns = nil

	row.AddColumnFields("true", "Hop")
	row.AddColumnFields("true", "RTT")
	auxTable.AddRow(row)
	row.Columns = nil

	int_hop := 0
	hops := aa.GetInterfaceField(results, "hops").([]interface{})
	for _, hop := range hops {
		var row aa.Row

		row.AddColumnFields("false", strconv.Itoa(int_hop))
		row.AddColumnFields("false", hop.(map[string]interface{})["rtt"].(string))

		auxTable.AddRow(row)
	}

	auxTableBytes, _ := json.Marshal(auxTable)
	return auxTableBytes
}

func hping3TableGenerator(results map[string]interface{}) []byte {
	log.Println("Generating HPing Results table")
	tmp := results["commandsresult"].([]interface{})[0].(map[string]interface{})["resultsjson"].(map[string]interface{})
	output := internalTableGeneration(tmp)
	var output_schema []interface{}
	json.Unmarshal(output, &output_schema)

	results["commandsresult"].([]interface{})[0].(map[string]interface{})["table"] = output_schema
	schema_bytes, _ := json.Marshal(results)
	return schema_bytes
}
