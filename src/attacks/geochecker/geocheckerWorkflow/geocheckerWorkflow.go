package geocheckerworkflow

import (
	"encoding/json"
)

func ExtractCountryInfo(results map[string]interface{}) []byte {
	var geoInfo []map[string]string

	// Asumiendo que 'commandsresult' siempre está presente y es un slice.
	for _, indivAttack := range results["commandsresult"].([]interface{}) {
		resultsjson := indivAttack.(map[string]interface{})["resultsjson"].(map[string]interface{})

		// Obtenemos 'country_name' y 'region_name', que serán cadenas vacías si no existen.
		countryName, _ := resultsjson["country_name"].(string)
		regionName, _ := resultsjson["region_name"].(string)

		// Solo agregamos información al slice si tenemos al menos uno de los valores no vacíos.
		if countryName != "" || regionName != "" {
			geoInfo = append(geoInfo, map[string]string{
				"country_name": countryName,
				"region_name":  regionName,
			})
		}
	}

	// Agregamos la información procesada a 'results'.
	results["processedOutput"] = geoInfo

	// Convertimos 'results' a JSON.
	schemaBytes, _ := json.Marshal(results) // En producción, manejar este error.
	return schemaBytes
}
