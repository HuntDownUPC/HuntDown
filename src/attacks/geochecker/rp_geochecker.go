package main

import (
	nm "attacks/geochecker/geocheckerTable"
	nmw "attacks/geochecker/geocheckerWorkflow"
	"encoding/json"
)

var Name = "geochecker"

func TableGenerator(results map[string]interface{}) []byte {
	return nm.GeocheckerTableGenerator(results)
}

func ResultProcessor(results map[string]interface{}) []byte {
	// Hydra supports JSON output so we don't handle that
	out, _ := json.Marshal(results)
	return out
}

func WorkflowProcessor(results map[string]interface{}) []byte {
	return nmw.ExtractCountryInfo(results)
}
