package netcatWorkflow

import (
	"encoding/base64"
	"encoding/json"
	"log"
	"strings"
)

/*
"processedOutput": [

	  {
	    "3181": "open"
	  },
	  {
	    "4181": "open"
	  }
	]
*/
func ExtractInfo(results map[string]interface{}) []byte {
	var portListM []map[string]string

	if base64Output, ok := results["commandsresult"].([]interface{})[0].(map[string]interface{})["resultsoutput"].(string); ok {
		outputBytes, err := base64.StdEncoding.DecodeString(base64Output)
		if err != nil {
			log.Println("Error decoding base64:", err)
			return nil
		}
		output := string(outputBytes)
		lines := strings.Split(output, "\n")

		portList := make(map[string]string)

		for _, line := range lines {
			println("line:")
			println(line)
			// Open port
			if strings.Contains(line, "succeeded!") {
				fields := strings.Fields(line)
				log.Println(fields)
				port := strings.TrimSpace(fields[3])
				portList[port] = "open"
			}
		}

		// Convert the portList map to a slice of maps
		for port, status := range portList {
			portListM = append(portListM, map[string]string{port: status})
		}
	}

	// Store the processed vulnerability information in the object "results".
	results["processedOutput"] = portListM
	schema_bytes, _ := json.Marshal(results)
	return schema_bytes
}
