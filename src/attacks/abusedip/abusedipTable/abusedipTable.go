package abusediptable

import (
	aa "attack-manager/AttackAbstraction"
	"encoding/json"
	"fmt"
	"log"
)

// processes the results assuming that will have hydra syntax and returns the processed results marshaled
func AbusedipTableGenerator(results map[string]interface{}) []byte {
	for _, indiv_attack := range results["commandsresult"].([]interface{}) {

		if indiv_attack != nil && indiv_attack.(map[string]interface{})["resultsjson"] != nil {
			resultsjson := indiv_attack.(map[string]interface{})["resultsjson"].(map[string]interface{})
			output := internalTableGeneration(resultsjson)
			var output_schema []interface{}
			json.Unmarshal(output, &output_schema)

			indiv_attack.(map[string]interface{})["table"] = output_schema
		}
	}
	schema_bytes, _ := json.Marshal(results)
	return schema_bytes
}

func internalTableGeneration(results map[string]interface{}) []byte {
	var finalResult aa.ResultTable
	var col aa.Element
	var row aa.Row

	if errors, ok := results["errors"].(map[string]interface{}); ok {
		col.Value = "Error"
		col.Color = "True"
		row.AddColumn(col)

		col.Value = "Status"
		col.Color = "True"
		row.AddColumn(col)

		col.Value = errors["details"].(string)
		row.AddColumn(col)

		col.Value = fmt.Sprint(errors["status"].(float64))
		row.AddColumn(col)

	} else {
		data, ok := results["data"].(map[string]interface{})
		if !ok {
			//log.Fatalf("Error: 'data' no está presente o no es del tipo esperado")
			log.Println("Warning: 'data' no está presente o no es del tipo esperado")
		}

		// Añade el campo 'IP'.
		if ipAddress, ok := data["ipAddress"].(string); ok {
			col.Value = "IP"
			col.Color = "true" // Asumiendo que Color es un string.
			row.AddColumn(col)
			col.Color = ""
			col.Value = ipAddress
			row.AddColumn(col)
			finalResult.AddRow(row)
			row.Columns = nil
		}

		if abuseConfidenceScore, ok := data["abuseConfidenceScore"].(float64); ok {
			col.Value = "Score"
			col.Color = "true"
			row.AddColumn(col)
			col.Color = ""
			col.Value = fmt.Sprint(abuseConfidenceScore)
			row.AddColumn(col)
			finalResult.AddRow(row)
			row.Columns = nil
		}

		// Verifica que el campo 'domain' no sea nulo.
		if domain, ok := data["domain"].(string); ok {
			col.Value = "Domain"
			col.Color = "true"
			row.AddColumn(col)
			col.Color = ""
			col.Value = domain
			row.AddColumn(col)
			finalResult.AddRow(row)
			row.Columns = nil
		}

		if hostnames, ok := data["hostnames"].([]interface{}); ok {
			for i, hostname := range hostnames {
				if host, ok := hostname.(string); ok {
					if i == 0 {
						col.Value = "Hostnames"
						col.Color = "true"
						row.AddColumn(col)
					}

					col.Color = ""
					col.Value = host
					row.AddColumn(col)

					if i == len(hostnames)-1 {
						finalResult.AddRow(row)
						row.Columns = nil
					}
				}
			}
		}
		if lastReportedAt, ok := data["lastReportedAt"].(string); ok {
			col.Value = "Last Report"
			col.Color = "true"
			row.AddColumn(col)
			col.Color = ""
			col.Value = fmt.Sprint(lastReportedAt)
			row.AddColumn(col)
			finalResult.AddRow(row)
			row.Columns = nil
		}

		if totalReports, ok := data["totalReports"].(float64); ok {
			col.Value = "Total Reports"
			col.Color = "true"
			row.AddColumn(col)
			col.Color = ""
			col.Value = fmt.Sprint(totalReports)
			row.AddColumn(col)
			finalResult.AddRow(row)
			row.Columns = nil
		}
		if usage, ok := data["usageType"].(string); ok {
			col.Value = "Usage"
			col.Color = "true"
			row.AddColumn(col)
			col.Color = ""
			col.Value = usage
			row.AddColumn(col)
		}

	}
	finalResult.AddRow(row)
	row.Columns = nil
	bytes_final, _ := json.Marshal(finalResult)
	return bytes_final
}
