package abusedipworkflow

import (
	"encoding/json"
	"fmt"
)

func ExtractIpReport(results map[string]interface{}) []byte {
	var AbuseInfo []map[string]string

	// Asumiendo que 'commandsresult' siempre está presente y es un slice.
	for _, indivAttack := range results["commandsresult"].([]interface{}) {
		resultsjson := indivAttack.(map[string]interface{})["resultsjson"].(map[string]interface{})

		data, dataExists := resultsjson["data"].(map[string]interface{})
		if !dataExists {
			continue
		}

		abuseConfidenceScore, abuseScoreOK := data["abuseConfidenceScore"].(float64)
		domain, domainOK := data["domain"].(string)

		// Solo agregamos información al slice si tenemos al menos uno de los valores no vacíos.
		if abuseScoreOK && domainOK {
			AbuseInfo = append(AbuseInfo, map[string]string{
				fmt.Sprintf("%f", abuseConfidenceScore): domain,
			})
		}
	}

	// Agregamos la información procesada a 'results'.
	results["processedOutput"] = AbuseInfo

	// Convertimos 'results' a JSON.
	schemaBytes, _ := json.Marshal(results) // En producción, manejar este error.
	return schemaBytes
}
