package fierceworkflow

import (
	"encoding/json"
	"log"
	"strconv"

	def "information-gathering/definitions"
)

type FierceResponse = def.FierceResponse

func ExtractIpsInfo(results map[string]interface{}) []byte {
	var ipsInfo []map[string]string
	var response FierceResponse

	// analyze the result of the attack to extract the ips
	for _, indiv_attack := range results["commandsresult"].([]interface{}) {

		if indiv_attack != nil && indiv_attack.(map[string]interface{})["resultsjson"] != nil {
			resultsjson := indiv_attack.(map[string]interface{})["resultsjson"].(map[string]interface{})

			data, err := json.Marshal(resultsjson)
			if err != nil {
				log.Println(err)
			}

			err = json.Unmarshal(data, &response)
			if err != nil {
				log.Println(err)
			}
		}
	}

	// save the ip addresses in ipList avoiding duplicate
	seenIPs := make(map[string]bool)

	for i, item := range response.Found {
		if !seenIPs[item.Ip] { // check if the IP address has already been added
			ipMap := make(map[string]string)
			ipMap["ip"] = item.Ip
			ipsInfo = append(ipsInfo, map[string]string{
				"ip" + strconv.Itoa(i): item.Ip,
			})
			seenIPs[item.Ip] = true // mark the IP address as seen
		}
	}

	log.Println("IPSINFO")
	log.Println(ipsInfo)

	// Store the processed port information in the object "results".
	results["processedOutput"] = ipsInfo

	schema_bytes, _ := json.Marshal(results)
	return schema_bytes
}
