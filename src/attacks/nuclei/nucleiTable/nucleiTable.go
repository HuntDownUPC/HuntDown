package nucleitable

import (
	aa "attack-manager/AttackAbstraction"
	"encoding/json"
)

// processes the results assuming that will have hydra syntax and returns the processed results marshaled
func NucleiTableGenerator(results map[string]interface{}) []byte {
	for _, indiv_attack := range results["commandsresult"].([]interface{}) {

		if indiv_attack != nil && indiv_attack.(map[string]interface{})["resultsjson"] != nil {
			resultsjson := indiv_attack.(map[string]interface{})["resultsjson"].(map[string]interface{})
			output := internalTableGeneration(resultsjson)
			var output_schema []interface{}
			json.Unmarshal(output, &output_schema)

			indiv_attack.(map[string]interface{})["table"] = output_schema
		}
	}
	schema_bytes, _ := json.Marshal(results)
	return schema_bytes
}

func internalTableGeneration(results map[string]interface{}) []byte {
	var finalResult aa.ResultTable
	var row aa.Row

	detected, ok := results["detected"].([]interface{})
	if !ok {
		var col aa.Element
		col.Value = "NO RESULT"
		col.Color = "true"
		row.AddColumn(col)
		finalResult.AddRow(row)
	} else {
		var col aa.Element
		col.Value = "Template"
		col.Color = "true"
		row.AddColumn(col)

		col.Value = "Type"
		row.AddColumn(col)

		col.Value = "Severity"
		row.AddColumn(col)

		col.Value = "URL"
		row.AddColumn(col)

		col.Value = "Info"
		row.AddColumn(col)

		finalResult.AddRow(row)
		row.Columns = nil
		col.Color = ""
		for i, indiv_result := range detected {
			col.Value = indiv_result.(map[string]interface{})["template"].(string)
			row.AddColumn(col)

			col.Value = indiv_result.(map[string]interface{})["type"].(string)
			row.AddColumn(col)

			col.Value = indiv_result.(map[string]interface{})["severity"].(string)
			row.AddColumn(col)

			col.Value = indiv_result.(map[string]interface{})["url"].(string)
			row.AddColumn(col)

			if info, ok := indiv_result.(map[string]interface{})["info"].(string); ok {
				col.Value = info
				row.AddColumn(col)
			} else {
				col.Value = "--"
				row.AddColumn(col)
			}

			if i < len(detected)-1 {
				finalResult.AddRow(row)
				row.Columns = nil
			}
		}

	}

	finalResult.AddRow(row)
	row.Columns = nil

	bytes_final, _ := json.Marshal(finalResult)

	return bytes_final
}
