package main

import (
	nm "attacks/nuclei/nucleiTable"
	nmw "attacks/nuclei/nucleiWorkflow"
	b64 "encoding/base64"
	"encoding/json"
	"helpers/GeneralHelper"
	"strings"

	log "github.com/sirupsen/logrus"
)

type Detected struct {
	Template string `json:"template,omitempty"`
	Type     string `json:"type,omitempty"`
	Severity string `json:"severity,omitempty"`
	URL      string `json:"url,omitempty"`
	Info     string `json:"info,omitempty"` // Cambiado a string
}

type NucleiResults struct {
	Detected []Detected `json:"detected"`
}

var Name = "nuclei"

func TableGenerator(results map[string]interface{}) []byte {
	return nm.NucleiTableGenerator(results)
}

func ResultProcessor(results map[string]interface{}) []byte {
	out, _ := json.Marshal(results)
	if results == nil || results["commandsresult"] == nil {
		return out
	}

	for _, indiv_attack := range results["commandsresult"].([]interface{}) {
		resultsoutput, ok := indiv_attack.(map[string]interface{})["resultsoutput"].(string)
		if !ok {
			log.Error("Empty results output")
			return out
		}
		decoded, err := b64.StdEncoding.DecodeString(resultsoutput)
		if err != nil {
			log.Error(err)
			return out
		}
		res := parseInput(string(decoded))
		our_result := GeneralHelper.StructToMap(res)
		indiv_attack.(map[string]interface{})["resultsjson"] = our_result
	}

	schema_bytes, err := json.Marshal(results)
	if err != nil {
		log.Println("Error marshaling results")
		return []byte{}
	}
	return schema_bytes
}

func WorkflowProcessor(results map[string]interface{}) []byte {
	return nmw.ExtractNucleiReport(results)
}

func parseInput(input string) NucleiResults {
	var results NucleiResults

	if input == "" {
		return results
	}

	lines := strings.Split(input, "\n")
	for _, line := range lines {
		if line == "" {
			continue
		}

		parts := strings.Fields(line)
		if len(parts) < 4 {
			continue
		}

		cveID := strings.TrimSuffix(strings.TrimPrefix(parts[0], "["), "]")

		var info string
		if len(parts) >= 5 {
			info = strings.Join(parts[4:], " ") // Unir todas las partes desde la posición 4 hasta el final
		}

		result := Detected{
			Template: cveID,
			Type:     parts[1],
			Severity: parts[2],
			URL:      parts[3],
			Info:     info,
		}

		results.Detected = append(results.Detected, result)
	}

	return results
}
