package wpscanWorkflow

import (
	"encoding/base64"
	"encoding/json"
	"log"
)

/*
"processedOutput": [
	{
		"wpvulndb":"title"
	}
]

"processedOutput": [
    {
      "5b754676-20f5-4478-8fd3-6bc383145811": "WordPress 5.4 to 5.8 - Authenticated XSS in Block Editor"
    },
    {
      "cc23344a-5c91-414a-91e3-c46db614da8d": "WordPress < 5.8.2 - Expired DST Root CA X3 Certificate"
    },
    {
      "926cd097-b36f-4d26-9c51-0dfab11c301b": "WP < 6.0.3 - Open Redirect via wp_nonce_ays"
    }
  ]
*/

func ExtractInfo(results map[string]interface{}) []byte {
	var vulnerabilitiesInfo []map[string]string

	if base64Output, ok := results["commandsresult"].([]interface{})[0].(map[string]interface{})["resultsoutput"].(string); ok {
		outputBytes, err := base64.StdEncoding.DecodeString(base64Output)
		if err != nil {
			log.Println("Error decoding base64:", err)
			return nil
		}

		var output map[string]interface{}
		err = json.Unmarshal(outputBytes, &output)
		if err != nil {
			log.Println("Error unmarshalling JSON:", err)
			return nil
		}

		// Create a temporary map to store wordpress vulnerabilities
		Vulnerabilities := make(map[string]string)

		// Extract version info
		if version, ok := output["version"].(map[string]interface{}); ok {
			// Extract vulnerabilities within version
			if vulnerabilities, ok := version["vulnerabilities"].([]interface{}); ok {
				for _, v := range vulnerabilities {
					vulnerability, ok := v.(map[string]interface{})
					if !ok {
						continue
					}

					// Extract necessary info from each vulnerability
					if title, ok := vulnerability["title"].(string); ok {
						if references, ok := vulnerability["references"].(map[string]interface{}); ok {
							if wpvulndbArr, ok := references["wpvulndb"].([]interface{}); ok && len(wpvulndbArr) > 0 {
								if wpvulndb, ok := wpvulndbArr[0].(string); ok {
									Vulnerabilities[wpvulndb] = title
								}
							}
						}
					}
				}
			}
		}

		// Convert the Vulnerabilities map to a slice of maps
		for wpvulndb, title := range Vulnerabilities {
			vulnerabilitiesInfo = append(vulnerabilitiesInfo, map[string]string{wpvulndb: title})
		}
	}

	// Store the processed info in the object "results".
	results["processedOutput"] = vulnerabilitiesInfo

	schema_bytes, _ := json.Marshal(results)
	return schema_bytes
}
