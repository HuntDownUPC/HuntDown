package main

import (
	nm "attacks/socialscanner/socialscannerTable"
	nmw "attacks/socialscanner/socialscannerWorkflow"
	"encoding/json"
)

var Name = "socialscanner"

func TableGenerator(results map[string]interface{}) []byte {
	return nm.SocialscannerTableGenerator(results)
}

func ResultProcessor(results map[string]interface{}) []byte {
	// Hydra supports JSON output so we don't handle that
	out, _ := json.Marshal(results)
	return out
}

func WorkflowProcessor(results map[string]interface{}) []byte {
	return nmw.ExtractSocialInfo(results)
}
