package socialscannertable

import (
	aa "attack-manager/AttackAbstraction"
	"encoding/json"
)

// processes the results assuming that will have hydra syntax and returns the processed results marshaled
func SocialscannerTableGenerator(results map[string]interface{}) []byte {
	for _, indiv_attack := range results["commandsresult"].([]interface{}) {

		if indiv_attack != nil && indiv_attack.(map[string]interface{})["resultsjson"] != nil {
			resultsjson := indiv_attack.(map[string]interface{})["resultsjson"].(map[string]interface{})
			detected := resultsjson["detected"].([]interface{})
			output := internalTableGeneration(map[string]interface{}{"detected": detected})
			var output_schema []interface{}
			json.Unmarshal(output, &output_schema)

			indiv_attack.(map[string]interface{})["table"] = output_schema
		}
	}
	schema_bytes, _ := json.Marshal(results)
	return schema_bytes
}

func internalTableGeneration(results map[string]interface{}) []byte {
	var finalResult aa.ResultTable
	var col aa.Element
	var row aa.Row

	detected, ok := results["detected"].([]interface{})
	if !ok {
		col.Value = "NO RESULT"
		col.Color = "true"
		row.AddColumn(col)
	} else {
		col.Value = "Link"
		col.Color = "true"
		row.AddColumn(col)

		col.Value = "Language"
		col.Color = "true"
		row.AddColumn(col)

		col.Value = "Type"
		col.Color = "true"
		row.AddColumn(col)

		col.Value = "Status"
		col.Color = "true"
		row.AddColumn(col)

		finalResult.AddRow(row)
		row.Columns = nil
		col.Color = "false"

		for _, indiv_result := range detected {

			col.Value = indiv_result.(map[string]interface{})["link"].(string)
			row.AddColumn(col)

			col.Value = indiv_result.(map[string]interface{})["language"].(string)
			row.AddColumn(col)

			col.Value = indiv_result.(map[string]interface{})["type"].(string)
			row.AddColumn(col)

			col.Value = indiv_result.(map[string]interface{})["status"].(string)
			row.AddColumn(col)

			finalResult.AddRow(row)
			row.Columns = nil
		}
	}
	bytes_final, _ := json.Marshal(finalResult)
	return bytes_final
}
