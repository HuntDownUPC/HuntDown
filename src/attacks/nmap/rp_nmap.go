package main

import (
	nm "attacks/nmap/nmapTable"
	nmw "attacks/nmap/nmapWorkflow"
	"encoding/json"

	"reflect"

	log "github.com/sirupsen/logrus"
)

var Name = "nmap"

func TableGenerator(results map[string]interface{}) []byte {
	return nm.NmapTableGenerator(results)
}

func ResultProcessor(results map[string]interface{}) []byte {
	if results == nil || results["commandsresult"] == nil {
		return []byte{}
	}

	var portInfo []map[string]string

	for _, indiv_attack := range results["commandsresult"].([]interface{}) {
		if indiv_attack != nil && indiv_attack.(map[string]interface{})["resultsjson"] != nil {
			resultsjson := indiv_attack.(map[string]interface{})["resultsjson"].(map[string]interface{})
			resultsjsonrun := resultsjson["nmaprun"].(map[string]interface{})
			if resultsjsonrun["host"] == nil {
				continue
			}
			host := resultsjsonrun["host"].(map[string]interface{})
			if host["ports"] == nil {
				continue
			}
			ports := host["ports"].(map[string]interface{})

			portType := reflect.TypeOf(ports["port"])
			switch portType.Kind() {
			case reflect.Map:
				port := ports["port"].(map[string]interface{})
				portID := port["@portid"].(string)
				portStatus := port["state"].(map[string]interface{})["@state"].(string)

				portInfo = append(portInfo, map[string]string{
					portID: portStatus,
				})
			case reflect.Slice:
				for _, open_port := range ports["port"].([]interface{}) {
					port := open_port.(map[string]interface{})
					portID := port["@portid"].(string)
					portStatus := port["state"].(map[string]interface{})["@state"].(string)

					portInfo = append(portInfo, map[string]string{
						portID: portStatus,
					})
				}
			}
		}
	}

	// Store the processed port information in the object "results".
	results["processedOutput"] = portInfo
	schema_bytes, err := json.Marshal(results)
	if err != nil {
		log.Println("Error marshaling results")
		return []byte{}
	}

	return schema_bytes
}

func WorkflowProcessor(results map[string]interface{}) []byte {
	return nmw.ExtractPortsInfo(results)
}
