package nmapTable

import (
	aa "attack-manager/AttackAbstraction"
	"encoding/json"
	"reflect"

	log "github.com/sirupsen/logrus"
)

func NmapTableGenerator(results map[string]interface{}) []byte {
	log.Println("We are generating nmap table")
	for _, indiv_attack := range results["commandsresult"].([]interface{}) {
		tmp := indiv_attack.(map[string]interface{})["resultsjson"].(map[string]interface{})["nmaprun"].(map[string]interface{})
		output := internalTableGeneration(tmp)
		var output_schema []interface{}
		json.Unmarshal(output, &output_schema)

		indiv_attack.(map[string]interface{})["table"] = output_schema
	}
	schema_bytes, _ := json.Marshal(results)
	return schema_bytes
}

// processes the results assuming that will have nmap syntax and returns the processed results marshaled
func internalTableGeneration(results map[string]interface{}) []byte {
	auxTable := aa.NewTable()

	if aa.GetMapInterfaceField(results, "runstats", "hosts", "@up").(string) == "0" {
		var col aa.Element
		var row aa.Row

		col.Value = "0 hosts up"
		row.AddColumn(col)
		auxTable.AddRow(row)
	} else {
		var row aa.Row
		hosts := results["host"]

		//Fill Row 0
		row.AddColumnFields("true", "IP")

		//If the nmap was done with just one host, the type is map, if there are other hosts, it's a slice
		type1 := reflect.TypeOf(hosts)

		switch type1.Kind() {
		case reflect.Slice: //Several Hosts
			for _, host := range hosts.([]interface{}) {
				var col aa.Element

				col.Value = aa.GetInterfaceField(host, "address", "@addr").(string)
				row.AddColumn(col)

				col.Value = "Hostname"
				col.Color = "true"
				row.AddColumn(col)

				col.Value = host.(map[string]interface{})["hostnames"].(map[string]interface{})["hostname"].([]interface{})[0].(map[string]interface{})["@name"].(string) //.(map[string]interface{})["@addr"]
				row.AddColumn(col)

				auxTable.AddRow(row)
				row.Columns = nil

				col.Value = "Open Ports"
				col.Color = "true"
				row.AddColumn(col)

				col.Value = "Service"
				col.Color = "true"
				row.AddColumn(col)

				col.Value = "Protocol"
				col.Color = "true"
				row.AddColumn(col)

				col.Value = "Reason"
				col.Color = "true"
				row.AddColumn(col)
				col.Color = ""

				auxTable.AddRow(row)
				row.Columns = nil

				port_type := reflect.TypeOf(host.(map[string]interface{})["ports"].(map[string]interface{})["port"])
				switch port_type.Kind() {
				case reflect.Map: //Just one port TODO: what happens if 0??

					our_ports := aa.GetMapInterfaceField(host.(map[string]interface{}), "ports", "port").(map[string]interface{})
					col.Value = our_ports["@portid"].(string)
					row.AddColumn(col)

					col.Value = our_ports["service"].(map[string]interface{})["@name"].(string)
					row.AddColumn(col)

					col.Value = our_ports["@protocol"].(string)
					row.AddColumn(col)

					col.Value = our_ports["state"].(map[string]interface{})["@reason"].(string)
					row.AddColumn(col)

					auxTable.AddRow(row)
					row.Columns = nil

				case reflect.Slice:
					for _, open_port := range host.(map[string]interface{})["ports"].(map[string]interface{})["port"].([]interface{}) {

						col.Value = open_port.(map[string]interface{})["@portid"].(string)
						row.AddColumn(col)

						col.Value = open_port.(map[string]interface{})["service"].(map[string]interface{})["@name"].(string)
						row.AddColumn(col)

						col.Value = open_port.(map[string]interface{})["@protocol"].(string)
						row.AddColumn(col)

						col.Value = open_port.(map[string]interface{})["state"].(map[string]interface{})["@reason"].(string)
						row.AddColumn(col)

						auxTable.AddRow(row)
						row.Columns = nil
					}
				}
			}
		case reflect.Map: //Just one host
			var col aa.Element
			host := results["host"]

			col.Value = host.(map[string]interface{})["address"].(map[string]interface{})["@addr"].(string)
			row.AddColumn(col)

			col.Value = "Hostname"
			col.Color = "true"
			row.AddColumn(col)

			if host.(map[string]interface{})["hostnames"] != "" {
				hostname := reflect.TypeOf(host.(map[string]interface{})["hostnames"].(map[string]interface{})["hostname"])
				switch hostname.Kind() {
				case reflect.Slice: //We just pick the first hostname
					col.Value = host.(map[string]interface{})["hostnames"].(map[string]interface{})["hostname"].([]interface{})[0].(map[string]interface{})["@name"].(string)
					row.AddColumn(col)
				case reflect.Map:
					col.Value = host.(map[string]interface{})["hostnames"].(map[string]interface{})["hostname"].(map[string]interface{})["@name"].(string)
					row.AddColumn(col)
				}
				auxTable.AddRow(row)
				row.Columns = nil

				col.Value = "Open Ports"
				col.Color = "true"
				row.AddColumn(col)

				col.Value = "Service"
				col.Color = "true"
				row.AddColumn(col)

				col.Value = "Protocol"
				col.Color = "true"
				row.AddColumn(col)

				col.Value = "Reason"
				col.Color = "true"
				row.AddColumn(col)
				col.Color = ""

				auxTable.AddRow(row)
				row.Columns = nil
			}

			if host.(map[string]interface{})["ports"] != "" {
				for _, ptype := range []string{"port"} { // In the future we may consider to use extraports as well
					port_type_type := reflect.TypeOf(host.(map[string]interface{})["ports"].(map[string]interface{})[ptype])
					if port_type_type == nil {
						continue
					}
					switch port_type_type.Kind() {
					case reflect.Map:
						this_port := aa.GetMapInterfaceField(host.(map[string]interface{}), "ports", ptype)
						for _, info := range parsePortInfo(this_port.(map[string]interface{})) {
							col.Value = info
							row.AddColumn(col)
						}

						auxTable.AddRow(row)
						row.Columns = nil

					case reflect.Slice:
						for _, open_port := range aa.GetInterfaceField(host, "ports", "port").([]interface{}) {
							for _, info := range parsePortInfo(open_port.(map[string]interface{})) {
								col.Value = info
								row.AddColumn(col)
							}

							auxTable.AddRow(row)
							row.Columns = nil
						}
					}
				}
			}
		}
	}
	bytes_final, _ := json.Marshal(auxTable)
	return bytes_final
}

func parsePortInfo(port map[string]interface{}) []string {
	port_id := aa.GetInterfaceField(port, "@portid").(string)
	service := "unknown"
	tmp := aa.GetInterfaceField(port, "service", "@name")
	if tmp != nil {
		service = tmp.(string)
	}
	protocol := aa.GetInterfaceField(port, "@protocol").(string)
	reason := aa.GetInterfaceField(port, "state", "@reason").(string)
	return []string{port_id, service, protocol, reason}
}
