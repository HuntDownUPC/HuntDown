ifeq ($(strip $(HUNTDOWN_PREFIX)),)
	HUNTDOWN_PREFIX=/usr/local/huntdown
endif

ifeq ($(strip $(HUNTDOWN_CONFIG_DIR)),)
	HUNTDOWN_CONFIG_DIR=/etc/huntdown
endif

ifeq ($(strip $(HUNTDOWN_USER)),)
	HUNTDOWN_USER=huntdown
endif

ifeq ($(strip $(HUNTDOWN_GROUP)),)
	HUNTDOWN_GROUP=huntdown
endif

ifeq ($(strip $(HUNTDOWN_PROCESSOR_DIR)),)
	HUNTDOWN_PROCESSOR_DIR=$(HUNTDOWN_PREFIX)/share/processors
endif

OS := $(shell lsb_release -si)
RELEASE := $(shell lsb_release -sr)
MIN_DEBIAN := 12

ifeq ($(OS),Debian)
	ifeq ($(shell echo "$$(( $(RELEASE) < $(MIN_DEBIAN) ))"),1)
		GO_BUILD := CGO_ENABLED=0 go build 
	else
		GO_BUILD := go build
	endif
else
	GO_BUILD := go build
endif
# -gcflags='all=-N -l'
PLUGIN_FLAGS := -buildmode=plugin

DEBUG_FLAGS := -gcflags='all=-N -l' -tags="json"
