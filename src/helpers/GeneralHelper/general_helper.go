package GeneralHelper

import (
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
	"runtime/debug"
	"strconv"
	"unsafe"

	"confManager"

	log "github.com/sirupsen/logrus"
)

var ConfigEnvironment confManager.ConfigContext

func GetMapElement(msg map[string]any, key string, result interface{}) error {
	if _, ok := msg[key]; !ok {
		return errors.New("key not found")
	}

	if reflect.ValueOf(result).Kind() != reflect.Ptr {
		return errors.New("use a pointer in call to GeneralHelper.GetMapElement")
	}
	r_result := reflect.ValueOf(result).Elem()
	r_msg := reflect.ValueOf(msg[key])

	if r_msg.Kind() == r_result.Kind() {
		if r_msg.Kind() == reflect.Slice {
			for _, v := range r_msg.Interface().([]interface{}) {
				// Add a new element to the slice
				r_result.Set(reflect.Append(r_result, reflect.ValueOf(v)))
			}
		} else if r_result.Type().AssignableTo(r_msg.Type()) {
			r_result = reflect.NewAt(r_result.Type(), unsafe.Pointer(r_result.UnsafeAddr())).Elem()
			r_result.Set(r_msg)
		} else if r_msg.CanConvert(r_result.Type()) {
			r_result.Set(r_msg.Convert(r_result.Type()))
		} else {
			my_error := fmt.Sprintf("wrong type in answer: %s %s", r_msg.Kind(), r_result.Kind())
			return errors.New(my_error)
		}
		return nil
	} else if r_msg.Kind() == reflect.Float64 && r_result.Kind() == reflect.String {
		r_result = reflect.NewAt(r_result.Type(), unsafe.Pointer(r_result.UnsafeAddr())).Elem()
		tmp := strconv.Itoa(int(r_msg.Interface().(float64)))
		my_reflect := reflect.ValueOf(tmp)
		r_result.Set(my_reflect)
		return nil
	}
	my_error := fmt.Sprintf("wrong type in answer: %s %s", r_msg.Kind(), r_result.Kind())
	return errors.New(my_error)
}

func GetSession_id(msg map[string]any) (int, error) {
	var s_id string
	err := GetMapElement(msg, "session_id", &s_id)
	if err != nil {
		log.Println(string(debug.Stack()))
		return 0, errors.New("session_id not found in query")
	}
	var session_id int
	session_id, err = strconv.Atoi(s_id)
	if err != nil {
		return 0, errors.New("session_id is not integer")
	}
	return session_id, nil
}

func ArrayMapToStruct(input []map[string]interface{}, result interface{}) error {
	jsonData, err := json.Marshal(input)

	if err != nil {
		return err
	}
	err = json.Unmarshal(jsonData, &result)
	if err != nil {
		return err
	}
	return nil
}

func MapToStruct(input map[string]interface{}, result interface{}) error {
	jsonData, err := json.Marshal(input)

	if err != nil {
		return err
	}
	err = json.Unmarshal(jsonData, &result)
	if err != nil {
		return err
	}
	return nil
}

func StructToMap(obj interface{}) map[string]interface{} {
	objValue := reflect.ValueOf(obj)
	objType := objValue.Type()

	// Make sure obj is a struct
	if objType.Kind() != reflect.Struct {
		panic("Not a struct")
	}

	result := make(map[string]interface{})

	// Loop through all fields of the struct and add them to the map
	for i := 0; i < objValue.NumField(); i++ {
		field := objType.Field(i)
		value := objValue.Field(i).Interface()

		// Get the JSON name from the struct tag
		jsonName := field.Tag.Get("json")

		// Use the JSON name as the key in the map
		result[jsonName] = value
	}

	return result
}

func CastToStringSlice(items []interface{}) []string {
	var result []string
	for _, item := range items {
		if str, ok := item.(string); ok {
			result = append(result, str)
		} else {
			// handle the case where the item is not a string
		}
	}
	return result
}

func LoadConfig() {
	confManager.LoadDefaultConfig()
	mongo_host := confManager.GetValue("mongo_host")
	pulsar_host := confManager.GetValue("pulsar_host")
	pulsar_port := confManager.GetValue("pulsar_port")
	mongo_port := confManager.GetValue("mongo_port")
	mongo_user := confManager.GetValue("mongo_user")
	mongo_password := confManager.GetValue("mongo_password")

	ConfigEnvironment.DatabaseUsername = mongo_user
	ConfigEnvironment.DatabasePassword = mongo_password
	ConfigEnvironment.DatabaseHost = mongo_host
	ConfigEnvironment.DatabasePort = mongo_port
	ConfigEnvironment.PulsarHost = pulsar_host
	ConfigEnvironment.PulsarPort = pulsar_port
}
