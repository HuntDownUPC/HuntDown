package GeneralHelper

import "encoding/json"

type ErrorCodes uint8

const (
	NoSession ErrorCodes = iota
	NoDBConnection
	FieldNotFound
	InvalidFormat
	EmptyAnswer
	AlreadyExists
	NoResults
	UnknownError
)

type ErrMessage struct {
	Success   bool       `json:"success"`
	Error     string     `json:"error"`
	ErrorCode ErrorCodes `json:"error_code"`
}

func BuildErrorMessage(err error, error_code ErrorCodes) ErrMessage {
	err_msg := ErrMessage{
		Success:   false,
		Error:     err.Error(),
		ErrorCode: error_code}
	return err_msg
}

func BuildLegacyErrorMessage(err error, error_code ErrorCodes) []byte {
	err_msg := BuildErrorMessage(err, error_code)
	ret, _ := json.Marshal(err_msg)
	return []byte(ret)
}
