package GeneralHelper

import (
	"encoding/json"

	log "github.com/sirupsen/logrus"
	PulsarLib "gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
)

func SendErrorMessage(client PulsarLib.PL_Client, topic string, message ErrMessage) {
	e_msg, err := json.Marshal(message)
	if err != nil {
		log.Error(err)
		return
	}
	PulsarLib.SendMessage(client, topic, e_msg)
}
