package DatabaseHelper

import (
	"confManager"
	"errors"
	"flag"
	"fmt"
	"helpers/AttackSchema"
	"helpers/GeneralHelper"
	"log"
	"os"
	"strconv"
	"testing"
	"time"

	PulsarLib "gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
)

func TestComplexFilter(t *testing.T) {
	var pulsar_host *string
	var pulsar_port *int
	var creds *string

	default_log := "/tmp/db_helper_test.log"

	creds = flag.String("credentials", "../../../config/credentials.json", "JSON file with the credentials")
	pulsar_host = flag.String("pulsar", "", "Pulsar host: e.g. 192.168.122.168")
	pulsar_port = flag.Int("pulsar-port", 0, "Pulsar port: e.g. 6650")

	// open a file
	f, err := os.OpenFile(default_log, os.O_APPEND|os.O_CREATE|os.O_RDWR, 0666)
	if err != nil {
		fmt.Printf("error opening file: %v", err)
	}

	// don't forget to close it
	defer f.Close()
	log.SetOutput(f)

	flag.Parse()

	confManager.LoadConfigFromFile("../../../config/environment.json")
	if *pulsar_host == "" {
		*pulsar_host = confManager.GetValue("pulsar_host")
	}
	if *pulsar_host == "" {
		*pulsar_host = "localhost"
	}

	port, err := strconv.Atoi(confManager.GetValue("pulsar_port"))
	if port != 0 && *pulsar_port == 0 {
		if err != nil {
			t.Fatalf("failed to convert pulsar port: %v", err)
		}
		*pulsar_port = port
	}
	if *pulsar_port == 0 {
		*pulsar_port = 6650
	}
	pulsar_port_str := strconv.Itoa(*pulsar_port)
	client := PulsarLib.InitClient("pulsar://" + *pulsar_host + ":" + pulsar_port_str)
	defer (*client.Client).Close()

	fmt.Print("Logging in")
	session_id, err := GeneralHelper.LoginWithCredentialFile(client, *creds)
	if err != nil {
		fmt.Println("Error logging in: ", err)
		return
	}

	filters := []Filter{}
	filter, _ := BuildFilter("state", "executed", "eq")
	tmp, _ := BuildFilter("typeattack", "nmap", "eq")
	filters = append(filters, filter)
	filters = append(filters, tmp)

	results, err := GetEntryWithFilter(session_id, &client, filters)
	if err != nil {
		GeneralHelper.Logout(client, session_id)
		t.Fatalf("failed to get results: %v", err)
	}
	for _, v := range results {
		if v["state"] != "executed" {
			GeneralHelper.Logout(client, session_id)
			t.Fatalf("failed to get correct state: %v", v)
		}
		if v["typeattack"] != "nmap" {
			GeneralHelper.Logout(client, session_id)
			t.Fatalf("failed to get correct attackname: %v", v)
		}
	}
	log.Printf("[OK] filter by multiple string at the same time successful")

	success, err := validateGTE(client, session_id)
	if !success {
		GeneralHelper.Logout(client, session_id)
		t.Fatalf("failed to validate GTE: %v", err)
	}
	log.Printf("[OK] filter by multiple string and time comparison >= successful")

	success, err = validateEQ(client, session_id)
	if !success {
		GeneralHelper.Logout(client, session_id)
		t.Fatalf("failed to validate EQ: %v", err)
	}
	log.Printf("[OK] filter by multiple string and time comparison == successful")

	success, err = validateLT(client, session_id)
	if !success {
		GeneralHelper.Logout(client, session_id)
		t.Fatalf("failed to validate GTE: %v", err)
	}
	log.Printf("[OK] filter by multiple string and time comparison < successful")

	GeneralHelper.Logout(client, session_id)
}

func validateEQ(client PulsarLib.PL_Client, session_id int) (bool, error) {
	filters := []Filter{}
	filter, _ := BuildFilter("state", "executed", "eq")
	tmp, _ := BuildFilter("typeattack", "nmap", "eq")
	filters = append(filters, filter)
	filters = append(filters, tmp)

	end_time := "2024-02-09T10:49:06.421095Z"

	filter, _ = BuildFilter("results.end", end_time, "eq")
	filter_gte := append(filters, filter)
	result, err := GetEntryWithFilter(session_id, &client, filter_gte)
	if err != nil {
		GeneralHelper.Logout(client, session_id)
		return false, err
	}

	if len(result) == 0 {
		GeneralHelper.Logout(client, session_id)
		return false, errors.New("No result found on database")
	}

	var resultStruct AttackSchema.AttackResult
	err = GeneralHelper.MapToStruct(result[0]["results"].(map[string]interface{}), &resultStruct)
	if err != nil {
		GeneralHelper.Logout(client, session_id)
		return false, err
	}

	endTimeParsed, err := parseTime(end_time)
	if err != nil {
		GeneralHelper.Logout(client, session_id)
		return false, err
	}
	if !resultStruct.End.Equal(endTimeParsed) {
		GeneralHelper.Logout(client, session_id)
		return false, err
	}
	return true, nil
}

func parseTime(time_str string) (time.Time, error) {
	time_parsed, err := time.Parse("2006-01-02T15:04:05.999999Z", time_str)
	if err != nil {
		return time.Time{}, err
	}
	return time_parsed, nil
}

func validateGTE(client PulsarLib.PL_Client, session_id int) (bool, error) {
	filters := []Filter{}
	filter, _ := BuildFilter("state", "executed", "eq")
	tmp, _ := BuildFilter("typeattack", "nmap", "eq")
	filters = append(filters, filter)
	filters = append(filters, tmp)

	results, err := GetEntryWithFilter(session_id, &client, filters)
	if err != nil {
		GeneralHelper.Logout(client, session_id)
		return false, err
	}
	resultsMap := results[0]["results"].(map[string]interface{})

	first_start, err := time.Parse("2006-01-02T15:04:05.999999Z", resultsMap["start"].(string))
	if err != nil {
		GeneralHelper.Logout(client, session_id)
		return false, err
	}

	filter, _ = BuildFilter("results.start", first_start.String(), "gte")
	filter_gte := append(filters, filter)
	result, err := GetEntryWithFilter(session_id, &client, filter_gte)
	if err != nil {
		GeneralHelper.Logout(client, session_id)
		return false, err
	}

	if len(result) == 0 {
		GeneralHelper.Logout(client, session_id)
		return false, errors.New("No result found on database")
	}

	var resultStruct AttackSchema.AttackResult
	for _, v := range result {
		err = GeneralHelper.MapToStruct(v["results"].(map[string]interface{}), &resultStruct)
		if err != nil {
			GeneralHelper.Logout(client, session_id)
			return false, err
		}

		if first_start.After(resultStruct.Start) {
			GeneralHelper.Logout(client, session_id)
			return false, err
		}
	}
	return true, nil
}

func validateLT(client PulsarLib.PL_Client, session_id int) (bool, error) {
	filters := []Filter{}
	filter, _ := BuildFilter("state", "executed", "eq")
	tmp, _ := BuildFilter("typeattack", "nmap", "eq")
	filters = append(filters, filter)
	filters = append(filters, tmp)

	results, err := GetEntryWithFilter(session_id, &client, filters)
	if err != nil {
		GeneralHelper.Logout(client, session_id)
		return false, err
	}
	resultsMap := results[len(results)-1]["results"].(map[string]interface{})

	last_start, err := time.Parse("2006-01-02T15:04:05.999999Z", resultsMap["start"].(string))
	if err != nil {
		GeneralHelper.Logout(client, session_id)
		return false, err
	}

	filter, _ = BuildFilter("results.start", last_start.String(), "lt")
	filter_lt := append(filters, filter)
	result, err := GetEntryWithFilter(session_id, &client, filter_lt)
	if err != nil {
		GeneralHelper.Logout(client, session_id)
		return false, err
	}

	if len(result) == 0 {
		GeneralHelper.Logout(client, session_id)
		return false, errors.New("No result found on database")
	}

	var resultStruct AttackSchema.AttackResult
	for _, v := range result {
		err = GeneralHelper.MapToStruct(v["results"].(map[string]interface{}), &resultStruct)
		if err != nil {
			GeneralHelper.Logout(client, session_id)
			return false, err
		}

		if last_start.Equal(resultStruct.Start) || last_start.Before(resultStruct.Start) {
			GeneralHelper.Logout(client, session_id)
			return false, err
		}
	}
	return true, nil
}
