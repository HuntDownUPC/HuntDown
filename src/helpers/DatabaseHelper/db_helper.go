package DatabaseHelper

import (
	"encoding/json"
	"errors"
	"helpers/GeneralHelper"

	log "github.com/sirupsen/logrus"
	"gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
)

type Filter struct {
	Field string `json:"field"`
	Value any    `json:"value"`
	Op    string `json:"op"`
}

type DatabaseDelete struct {
	Session_id int      `json:"session_id"`
	Database   string   `json:"database"`
	Collection string   `json:"collection"`
	Field      string   `json:"field"`
	Value      any      `json:"value"`
	Filters    []Filter `json:"filters"`
}

type DatabaseInsert struct {
	Session_id int                      `json:"session_id"`
	Database   string                   `json:"database"`
	Collection string                   `json:"collection"`
	ToInsert   []map[string]interface{} `json:"to_insert"`
}

type DatabaseUpdate struct {
	Session_id int      `json:"session_id"`
	Database   string   `json:"database"`
	Collection string   `json:"collection"`
	Field      string   `json:"field"`
	Value      any      `json:"value"`
	Filters    []Filter `json:"filters"`
}

type DatabaseQuery struct {
	Session_id int      `json:"session_id"`
	Database   string   `json:"database"`
	Collection string   `json:"collection"`
	Field      string   `json:"field"`
	Filters    []Filter `json:"filters"`
}

func BuildFilter(field string, value string, operation string) (Filter, error) {
	valid_ops := []string{"$eq", "$geq", "$lte", "$ne", "$gt", "$gte", "$lt", "$exists"}

	if operation[0] != '$' {
		operation = "$" + operation
	}

	for _, op := range valid_ops {
		if op == operation {
			return Filter{Field: field, Value: value, Op: operation}, nil
		}
	}
	return Filter{}, errors.New("Invalid operation")
}

func GetEntryWithFilter(session_id int, comm_client *PulsarLib.PL_Client, filters []Filter) ([]map[string]any, error) {
	var query DatabaseQuery
	var err error

	result := []map[string]any{}
	query = DatabaseQuery{Session_id: session_id,
		Filters: filters}

	to_send := PulsarLib.BuildMessage(GeneralHelper.StructToMap(query))
	response := PulsarLib.SendRequestSync(*comm_client, "db.newQuery", to_send)

	err = json.Unmarshal(response, &result)
	if err != nil {
		log.Debug("Error parsing response", err)
		return result, err
	}
	if len(result) == 0 {
		log.Debug("No values found")
		return result, errors.New("No values found on Collection")
	}
	return result, nil
}

func GetGeneralEntryByName(session_id int, comm_client *PulsarLib.PL_Client, collection string, key string, value string) (map[string]any, error) {
	var query DatabaseQuery
	var err error

	result := map[string]any{}
	filter := Filter{Field: key, Value: value, Op: "$eq"}
	query = DatabaseQuery{Session_id: session_id,
		Database:   "general",
		Collection: collection,
		Filters:    []Filter{filter}}

	to_send := PulsarLib.BuildMessage(GeneralHelper.StructToMap(query))
	response := PulsarLib.SendRequestSync(*comm_client, "db.newQuery", to_send)
	tmp_map := []map[string]interface{}{}
	json.Unmarshal(response, &tmp_map)
	if len(tmp_map) == 0 {
		log.Debug(value, "not found in", key)
		return result, errors.New("No element " + value + " found for " + key)
	}
	response, err = json.Marshal(tmp_map[0])
	if err != nil {
		log.Debug("Error parsing response", err)
		return result, err
	}
	err = json.Unmarshal(response, &result)
	if err != nil {
		log.Debug("Error parsing response", err)
		return result, err
	}
	return result, nil
}

func GetAllGeneralCollectionEntries(session_id int, comm_client *PulsarLib.PL_Client, collection string) ([]map[string]any, error) {
	var query DatabaseQuery
	var err error

	result := []map[string]any{}
	query = DatabaseQuery{Session_id: session_id,
		Database:   "general",
		Collection: collection,
		Filters:    []Filter{}}

	to_send := PulsarLib.BuildMessage(GeneralHelper.StructToMap(query))
	response := PulsarLib.SendRequestSync(*comm_client, "db.newQuery", to_send)

	err = json.Unmarshal(response, &result)
	if err != nil {
		log.Debug("Error parsing response", err)
		return result, err
	}
	if len(result) == 0 {
		log.Debug("No values found on Collection", collection)
		return result, errors.New("No values found on Collection" + collection)
	}
	return result, nil
}

func GetAllGeneralEntriesByName(session_id int, comm_client *PulsarLib.PL_Client, collection string, key string, value string) ([]map[string]any, error) {
	var query DatabaseQuery
	var err error

	result := []map[string]any{}
	filter := Filter{Field: key, Value: value, Op: "$eq"}
	query = DatabaseQuery{Session_id: session_id,
		Database:   "general",
		Collection: collection,
		Filters:    []Filter{filter}}

	to_send := PulsarLib.BuildMessage(GeneralHelper.StructToMap(query))
	response := PulsarLib.SendRequestSync(*comm_client, "db.newQuery", to_send)

	err = json.Unmarshal(response, &result)
	if err != nil {
		log.Debug("Error parsing response", err)
		return result, err
	}
	if len(result) == 0 {
		log.Debug("ID", value, "not found in", key)
		return result, errors.New("No element " + value + " found for " + key)
	}
	return result, nil
}

func GetAllEntriesByName(session_id int, comm_client *PulsarLib.PL_Client, key string, value string) ([]map[string]any, error) {
	var query DatabaseQuery
	var err error

	result := []map[string]any{}
	filter := Filter{Field: key, Value: value, Op: "$eq"}
	query = DatabaseQuery{Session_id: session_id,
		Filters: []Filter{filter}}

	to_send := PulsarLib.BuildMessage(GeneralHelper.StructToMap(query))
	response := PulsarLib.SendRequestSync(*comm_client, "db.newQuery", to_send)

	err = json.Unmarshal(response, &result)
	if err != nil {
		log.Debug("Error parsing response", err)
		return result, err
	}
	if len(result) == 0 {
		log.Debug("ID", value, "not found in", key)
		return result, errors.New("No element " + value + " found for " + key)
	}
	return result, nil
}

func GetAllEntriesByField(session_id int, comm_client *PulsarLib.PL_Client, key string) ([]map[string]any, error) {
	var query DatabaseQuery
	var err error

	result := []map[string]any{}
	filter := Filter{Field: key, Value: true, Op: "$exists"}
	query = DatabaseQuery{Session_id: session_id,
		Filters: []Filter{filter}}

	to_send := PulsarLib.BuildMessage(GeneralHelper.StructToMap(query))
	response := PulsarLib.SendRequestSync(*comm_client, "db.newQuery", to_send)

	err = json.Unmarshal(response, &result)
	if err != nil {
		log.Debug("Error parsing response", err)
		return result, err
	}
	if len(result) == 0 {
		log.Debug("No fields found with", key)
		return result, errors.New("No element found for " + key)
	}
	return result, nil
}

func GetEntryByName(session_id int, comm_client *PulsarLib.PL_Client, key string, value string) (map[string]any, error) {
	var query DatabaseQuery
	var err error

	result := map[string]any{}
	filter := Filter{Field: key, Value: value, Op: "$eq"}
	query = DatabaseQuery{Session_id: session_id,
		Filters: []Filter{filter}}

	to_send := PulsarLib.BuildMessage(GeneralHelper.StructToMap(query))
	response := PulsarLib.SendRequestSync(*comm_client, "db.newQuery", to_send)

	tmp_map := []map[string]interface{}{}
	json.Unmarshal(response, &tmp_map)
	if len(tmp_map) == 0 {
		log.Debug("ID", value, "not found in", key)
		return result, errors.New("No element " + value + " found for " + key)
	}
	response, err = json.Marshal(tmp_map[0])
	if err != nil {
		log.Debug("Error parsing response", err)
		return result, err
	}
	err = json.Unmarshal(response, &result)
	if err != nil {
		log.Debug("Error parsing response", err)
		return result, err
	}
	return result, nil
}

func GetEntryByID(session_id int, comm_client *PulsarLib.PL_Client, element_id string) (map[string]any, error) {
	var query DatabaseQuery
	var err error
	log.Debug("Getting entry by ID")
	result := map[string]any{}
	filter := Filter{Field: "_id", Value: element_id, Op: "$eq"}
	query = DatabaseQuery{Session_id: session_id,
		Filters: []Filter{filter}}

	to_send := PulsarLib.BuildMessage(GeneralHelper.StructToMap(query))
	response := PulsarLib.SendRequestSync(*comm_client, "db.newQuery", to_send)

	tmp_map := []map[string]interface{}{}
	json.Unmarshal(response, &tmp_map)
	if len(tmp_map) == 0 {
		log.Debug("ID", element_id, "not found")
		return result, errors.New("ID " + element_id + " not found")
	}
	response, err = json.Marshal(tmp_map[0])
	if err != nil {
		log.Error("Error parsing response", err)
		return result, err
	}
	json.Unmarshal(response, &result)
	return result, nil
}

func InsertOneElement(session_id int, comm_client *PulsarLib.PL_Client, element map[string]interface{}) (string, error) {
	var arr_element []map[string]interface{} = []map[string]interface{}{element}
	var err error
	var oid_raw []interface{}
	var element_insert DatabaseInsert = DatabaseInsert{Session_id: session_id,
		ToInsert: arr_element}
	raw_element_insert := GeneralHelper.StructToMap(element_insert)
	to_send := PulsarLib.BuildMessage(raw_element_insert)
	response := PulsarLib.SendRequestSync(*comm_client, "db.insertElements", to_send)

	tmp_map := map[string]interface{}{}
	err = json.Unmarshal(response, &tmp_map)
	if err != nil {
		log.Println("Error parsing element ", err.Error())
		return "", err
	}
	err = GeneralHelper.GetMapElement(tmp_map, "ids", &oid_raw)
	if err != nil {
		log.Println("Error parsing element, ids key not found ", err.Error())
		return "", err
	}

	var oid_list []string = GeneralHelper.CastToStringSlice(oid_raw)
	if len(oid_list) > 1 {
		log.Println("Error parsing element more than one ID returned when inserting")
		return "", errors.New("Error parsing element more than one ID returned when inserting")
	}
	return oid_list[0], nil
}

func updateOneElement(session_id int, comm_client *PulsarLib.PL_Client, filters []Filter,
	key string, value any) ([]byte, error) {

	var updatedata DatabaseUpdate = DatabaseUpdate{Session_id: session_id,
		Field: key, Value: value,
		Filters: filters}
	to_send := PulsarLib.BuildMessage(GeneralHelper.StructToMap(updatedata))
	response := PulsarLib.SendRequestSync(*comm_client, "db.updateField", to_send)
	return response, nil
}

func DeleteOneElement(session_id int, comm_client *PulsarLib.PL_Client,
	key string, value string) ([]byte, error) {

	filter := Filter{Field: key, Value: value, Op: "$eq"}
	var deletedata DatabaseDelete = DatabaseDelete{Session_id: session_id,
		Filters: []Filter{filter}}

	to_send := PulsarLib.BuildMessage(GeneralHelper.StructToMap(deletedata))
	response := PulsarLib.SendRequestSync(*comm_client, "db.deleteField", to_send)
	return response, nil
}

func UpdateOneElementByKey(session_id int, comm_client *PulsarLib.PL_Client, filter map[string]interface{},
	key string, value any) ([]byte, error) {

	var filters []Filter
	for k, v := range filter {
		var one_filter Filter = Filter{Field: k, Value: v, Op: "$eq"}
		filters = append(filters, one_filter)
	}
	return updateOneElement(session_id, comm_client, filters, key, value)
}

func UpdateOneElementByID(session_id int, comm_client *PulsarLib.PL_Client, element_id string, key string, value any) ([]byte, error) {
	var filter Filter = Filter{Field: "_id", Value: element_id, Op: "$eq"}
	return updateOneElement(session_id, comm_client, []Filter{filter}, key, value)
}
