package OptionParser

import (
	"errors"
	"fmt"
	db "helpers/DatabaseHelper"
	FlagsSchema "helpers/FlagsSchema"
	"helpers/GeneralHelper"
	of "helpers/OptionsFile"
	"strings"

	"github.com/docker/docker/api/types/strslice"
	"gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
)

func ParseToolOptionFile(session_id int, comm_client *PulsarLib.PL_Client, tool string) (of.OptionsFile, error) {
	var optionsFiles of.OptionsFile
	var err error

	tmp, err := db.GetGeneralEntryByName(session_id, comm_client, "OptionsFile", "name", tool)
	if err != nil {
		return of.OptionsFile{}, fmt.Errorf("error getting OptionsFile from database: %v", err)
	}

	err = GeneralHelper.MapToStruct(tmp, &optionsFiles)
	if err != nil {
		return of.OptionsFile{}, fmt.Errorf("JSON unmarshalling failed: %v", err)
	}

	return optionsFiles, nil
}

func AssembleCommandLineString(flagsSchema FlagsSchema.Flags) (string, error) {
	var parts []string

	switch flagsSchema.Delimiter {
	case "space":
		// Assemble command line using space delimiter
		for flag, value := range flagsSchema.Flags {
			if flag == "" {
				// Handle non-flag elements
				parts = append(parts, value)
			} else {
				parts = append(parts, flag)
				if value != "" {
					parts = append(parts, value)
				}
			}
		}
	case "=", ":":
		// Assemble command line using specified delimiter
		for flag, value := range flagsSchema.Flags {
			if flag == "" {
				// Handle non-flag elements
				parts = append(parts, value)
			} else {
				if value != "" {
					parts = append(parts, flag+flagsSchema.Delimiter+value)
				} else {
					parts = append(parts, flag)
				}
			}
		}
	default:
		return "", errors.New("invalid delimiter")
	}

	return strings.Join(parts, " "), nil
}

func AssembleCommandLineStrSlice(flagsSchema FlagsSchema.Flags) (strslice.StrSlice, error) {
	var parts []string

	switch flagsSchema.Delimiter {
	case "space":
		for flag, value := range flagsSchema.Flags {
			if flag == "" {
				parts = append(parts, value)
			} else {
				parts = append(parts, flag)
				if value != "" {
					parts = append(parts, value)
				}
			}
		}
	case "=", ":":
		for flag, value := range flagsSchema.Flags {
			if flag == "" {
				parts = append(parts, value)
			} else {
				if value != "" {
					parts = append(parts, flag+flagsSchema.Delimiter+value)
				} else {
					parts = append(parts, flag)
				}
			}
		}
	default:
		return nil, errors.New("invalid delimiter")
	}

	return strslice.StrSlice(parts), nil
}
