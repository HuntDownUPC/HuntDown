package MonitorEvents

import (
	"encoding/json"
	"strings"

	log "github.com/sirupsen/logrus"
	PulsarLib "gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
)

type MonitorContext struct {
	Session_id   string
	Id           string
	Step         int
	Error        error
	PulsarClient PulsarLib.PL_Client
}

func SendMonitorError(monitor *MonitorContext, err error) {
	monitor.Error = err
	SendAndIncreaseMonitorEvent(monitor, err.Error())
}

func SendMonitorEvent(monitor *MonitorContext, text ...string) {
	log.Debug("Monitoring thing")
	response := make(map[string]interface{})
	response["session_id"] = monitor.Session_id
	response["step"] = text[0]
	response["detail"] = strings.Join(text[1:], "\n")
	response["step_num"] = monitor.Step
	response["task_id"] = monitor.Id
	if monitor.Error != nil {
		response["error"] = monitor.Error.Error()
		response["success"] = false
	} else {
		response["success"] = true
	}
	log.Println(response)
	msg, err := json.Marshal(response)
	if err != nil {
		log.Println("Error on monitoring message: ", err.Error())
		log.Println("message to send: ", msg)
	}
	PulsarLib.SendMessage(monitor.PulsarClient, "monitor", msg)
}

func SendAndIncreaseMonitorEvent(monitor *MonitorContext, text ...string) {
	monitor.Step += 1
	SendMonitorEvent(monitor, text...)
}
