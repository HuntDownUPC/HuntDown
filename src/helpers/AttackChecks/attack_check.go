package AttackChecks

import (
	"encoding/base64"
	"fmt"
	FlagsSchema "helpers/FlagsSchema"
	OptionsFileSchema "helpers/OptionsFile"
	"regexp"
)

// PerformChecks runs all the check functions sequentially and stops at the first error
func PerformChecks(parsedInput FlagsSchema.Flags, toolConfig OptionsFileSchema.OptionsFile) error {
	checks := []func(FlagsSchema.Flags, OptionsFileSchema.OptionsFile) error{
		CheckUserInputWithRegex,
		CheckOptionConflict,
		CheckOptionDependency,
	}

	for _, check := range checks {
		if err := check(parsedInput, toolConfig); err != nil {
			return err
		}
	}

	return nil
}

// CheckOptionDependency verifies the option dependencies
func CheckOptionDependency(parsedInput FlagsSchema.Flags, toolConfig OptionsFileSchema.OptionsFile) error {
	for _, option := range toolConfig.Options {
		// Check if the option's flag is present in the user input
		if _, exists := parsedInput.Flags[option.Flag]; exists {
			// Only check dependencies if the 'require' array is not empty
			if len(option.Require) > 0 {
				var missingRequirements []string
				dependencyMet := true
				for _, requiredTitle := range option.Require {
					found := false
					// Iterate over the options to find the corresponding flag for the required title
					for _, opt := range toolConfig.Options {
						if opt.Title == requiredTitle {
							if _, depExists := parsedInput.Flags[opt.Flag]; depExists {
								found = true
								break
							}
						}
					}
					if !found {
						missingRequirements = append(missingRequirements, requiredTitle)
						dependencyMet = false
					}
				}
				if !dependencyMet {
					return fmt.Errorf("error! Option '%s' requires '%v' which is missing", option.Title, missingRequirements)
				}
			}
		}
	}
	return nil
}

// CheckOptionConflict verifies the conflicts in the user input
func CheckOptionConflict(parsedInput FlagsSchema.Flags, toolConfig OptionsFileSchema.OptionsFile) error {
	for _, option := range toolConfig.Options {
		// Check if the option's flag is present in the user input
		if _, exists := parsedInput.Flags[option.Flag]; exists {
			// Only check conflicts if the 'conflict' array is not empty
			if len(option.Conflict) > 0 {
				for _, conflictingTitle := range option.Conflict {
					// Find the flag associated with the conflicting title
					for _, opt := range toolConfig.Options {
						if opt.Title == conflictingTitle {
							if _, conflictExists := parsedInput.Flags[opt.Flag]; conflictExists {
								return fmt.Errorf("error! Option '%s' conflicts with '%s'", option.Title, conflictingTitle)
							}
							break
						}
					}
				}
			}
		}
	}
	return nil
}

// CheckUserInputWithRegex checks if user input matches the expected pattern defined by regex
func CheckUserInputWithRegex(parsedInput FlagsSchema.Flags, toolConfig OptionsFileSchema.OptionsFile) error {
	// Iterate over each option in the tool configuration
	for _, option := range toolConfig.Options {
		// Check if the option requires text input
		if option.TextInput {
			// Retrieve the user input for the option
			userValue, exists := parsedInput.Flags[option.Flag]
			if !exists {
				// Try to retrieve the user input using the option title if the flag is not found
				userValue, exists = parsedInput.Flags[option.Title]
			}
			if exists {
				if option.Regex == "" {
					// No regex provided for text input option
					return nil
				}

				// Decode the base64 encoded regex
				decodedRegex, err := base64.StdEncoding.DecodeString(option.Regex)
				if err != nil {
					return fmt.Errorf("failed to decode regex for option '%s': %v", option.Title, err)
				}

				// Match the user input against the decoded regex
				matched, err := regexp.MatchString(string(decodedRegex), userValue)
				if err != nil {
					return fmt.Errorf("failed to compile regex for option '%s': %v", option.Title, err)
				}

				// If the user input does not match the regex, return an error
				if !matched {
					return fmt.Errorf("error! Option '%s' value '%s' does not match the expected pattern", option.Title, userValue)
				}
			}
		}
	}
	// Return nil if all inputs are valid
	return nil
}
