package FlagsSchema

// Flags represents the parsed selected options
type Flags struct {
	Delimiter string            `json:"delimiter"`
	Flags     map[string]string `json:"flags"`
}
