package OptionsFile

// OptionsFile structure
type OptionsFile struct {
	Name        string `json:"name"`
	OptionsId   string `json:"optionsid"`
	OptionsName string `json:"optionsname"`
	Delimiter   string `json:"delimiter"`
	Mode        struct {
		Options []string `json:"options"` 
	} `json:"mode"`
	Options     []struct {
		Title        string `json:"title"`
		MainGroup    string `json:"maingroup"`
		SubGroup     string `json:"subgroup"`
		Flag         string `json:"Flag"`
		Value        string `json:"value"`
		NeedCheckbox bool   `json:"needcheckbox"`
		Checkbox     bool   `json:"checkbox"`
		TextInput    bool   `json:"textinput"`
		Information  string `json:"information"`
		Regex        string `json:"regex"`
		Formattype        string `json:"formattype"`
		SelectOptions []string `json:"selectOptions"`
		Require      []string `json:"require"`
		Conflict     []string `json:"conflict"`
	} `json:"options"`	
}
