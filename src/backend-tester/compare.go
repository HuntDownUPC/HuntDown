package main

import "reflect"

func compare(a, b interface{}, operator string) bool {
	if operator == "==" {
		return reflect.DeepEqual(a, b)
	} else if operator == "!=" {
		return !reflect.DeepEqual(a, b)
	}

	aValue := reflect.ValueOf(a)
	bValue := reflect.ValueOf(b)

	if aValue.Kind() == bValue.Kind() {
		switch aValue.Kind() {
		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
			return compareInt(aValue.Int(), bValue.Int(), operator)
		case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
			return compareUint(aValue.Uint(), bValue.Uint(), operator)
		case reflect.Float32, reflect.Float64:
			return compareFloat(aValue.Float(), bValue.Float(), operator)
		case reflect.String:
			return compareString(aValue.String(), bValue.String(), operator)
		}
	}

	return false
}

func compareInt(a, b int64, operator string) bool {
	switch operator {
	case "<":
		return a < b
	case "<=":
		return a <= b
	case ">":
		return a > b
	case ">=":
		return a >= b
	}

	return false
}

func compareUint(a, b uint64, operator string) bool {
	switch operator {
	case "<":
		return a < b
	case "<=":
		return a <= b
	case ">":
		return a > b
	case ">=":
		return a >= b
	}

	return false
}

func compareFloat(a, b float64, operator string) bool {
	switch operator {
	case "<":
		return a < b
	case "<=":
		return a <= b
	case ">":
		return a > b
	case ">=":
		return a >= b
	}

	return false
}

func compareString(a, b string, operator string) bool {
	switch operator {
	case "<":
		return a < b
	case "<=":
		return a <= b
	case ">":
		return a > b
	case ">=":
		return a >= b
	}

	return false
}
