package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
	dbd "gitlab.com/HuntDownUPC/go-modules/db/common/v2"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"reflect"
	"strconv"
	"strings"
	"time"

	"gitlab.com/HuntDownUPC/HuntDown/src/confManager"
	dbu "gitlab.com/HuntDownUPC/go-modules/db/DBUsers/v2"
	dbw "gitlab.com/HuntDownUPC/go-modules/db/DBWrapper/v2"
)

type Message struct {
	Name    string     `json:"name"`
	Users   []dbd.User `json:"users"`
	Actions []Action   `json:"actions"`
}

type ForData struct {
	Start int
	End   int
	Step  int
}

type Action struct {
	Id                string                 `json:"id"`
	Topic             string                 `json:"topic"`
	Condition         [][]interface{}        `json:"condition"`
	For               ForData                `json:"for"`
	SkipResponse      bool                   `json:"skip_response"`
	WaitUntilResponse bool                   `json:"wait_until_response"`
	Timeout           int                    `json:"timeout"`
	Request           map[string]interface{} `json:"request"`
	Response          any                    `json:"response"`
}

func send(client PulsarLib.PL_Client, topic string, msg map[string]any, skipResponse bool) []byte {
	msgForPulsar1 := PulsarLib.BuildMessage(msg)

	if skipResponse {
		message, _ := json.Marshal(msgForPulsar1)
		PulsarLib.SendMessage(client, topic, message)
		return nil
	} else {
		return PulsarLib.SendRequestSync(client, topic, msgForPulsar1)
	}
}

func replaceVariable(placeholder string, responseMap any) interface{} {
	currentMap := responseMap
	split := strings.Split(placeholder[1:], "/")
	for i := 0; i < len(split); i++ {
		keySplit := strings.Split(split[i], "[")
		key := keySplit[0]

		mapType := reflect.TypeOf(currentMap)
		if mapType.Kind() == reflect.Map {
			var ok bool
			currentMap, ok = currentMap.(map[string]interface{})[key]
			if !ok {
				log.Println("Error! Key not found")
				break
			}
		} else {
			log.Println("Error! Type is not map")
		}

		// Parse list indexes if present
		for j := 1; j < len(keySplit); j++ {
			listKey := keySplit[j]

			keyInt, indexValid := strconv.Atoi(listKey[:len(listKey)-1])
			if indexValid != nil {
				log.Println("Error! Index is not valid")
				break
			}

			mapType := reflect.TypeOf(currentMap)
			if mapType.Kind() == reflect.Slice {
				switch currentMap.(type) {
				case []map[string]interface{}:
					currentMap = currentMap.([]map[string]interface{})[keyInt]
				default:
					currentMap = currentMap.([]interface{})[keyInt]
				}

			} else {
				log.Println("Error")
			}
		}
	}
	return currentMap
}

func replace(responseData interface{}, responseMap any) {
	if responseData == nil {
		return
	}

	switch responseData.(type) {
	case map[string]interface{}:
		for k, v := range responseData.(map[string]interface{}) {
			//if v starts with $
			if v, ok := v.(string); ok {
				if !strings.HasPrefix(v, "$") {
					continue
				}

				// Parse to correct type
				/*valueType := reflect.TypeOf(currentMap)
				switch valueType.Kind() {
				case reflect.String:
					responseData[k] = currentMap.(string)
				case reflect.Int:
					responseData[k], _ = currentMap.(int)
				case reflect.Float64:
					responseData[k], _ = currentMap.(float64)
				case reflect.Bool:
					responseData[k], _ = currentMap.(bool)
				default:
					log.Println("Error! Type not supported")
				}

				log.Println("A") //*/
				responseData.(map[string]interface{})[k] = replaceVariable(v, responseMap)
			}
		}
	case []interface{}:
		for _, v := range responseData.([]interface{}) {
			replace(v, responseMap)
		}
	}
}

// Check if element A contains all items of element B
func contains(elem1 interface{}, elem2 interface{}) bool {
	// If both are null they are the same
	if elem1 == nil && elem2 == nil {
		return true
	}

	// Return false if one of them is null as they are not the same and cannot be compared
	if elem1 == nil || elem2 == nil {
		return false
	}

	if reflect.TypeOf(elem1).Kind() != reflect.TypeOf(elem2).Kind() {
		return false
	}

	switch elem2.(type) {
	case map[string]interface{}:
		map1 := elem1.(map[string]interface{})
		map2 := elem2.(map[string]interface{})
		for k, v2 := range map2 {
			if v1, ok := map1[k]; !ok || !contains(v1, v2) {
				return false
			}
		}
		return true

	case []interface{}:
		list1 := elem1.([]interface{})
		list2 := elem2.([]interface{})
		for _, v2 := range list2 {
			found := false
			for _, v1 := range list1 {
				if contains(v1, v2) {
					found = true
					break
				}
			}
			if !found {
				return false
			}
		}
		return true

	default:
		return elem1 == elem2
	}
}

func checkConditions(condition [][]interface{}, responses map[string]interface{}) bool {
	for _, expression := range condition {
		if len(expression) != 3 {
			log.Println("Conditions must have 3 elements: first value, operator and second value")
			return false
		}

		valueA := expression[0]
		operator := expression[1]
		valueB := expression[2]

		// Parse values
		if stringA, ok := valueA.(string); ok && strings.HasPrefix(stringA, "$") {
			valueA = replaceVariable(stringA, responses)
		}

		if stringB, ok := valueB.(string); ok && strings.HasPrefix(stringB, "$") {
			valueB = replaceVariable(stringB, responses)
		}

		typeA := reflect.TypeOf(valueA)
		typeB := reflect.TypeOf(valueB)
		if typeA != typeB {
			return false // different types cannot be compared
		}

		result := compare(valueA, valueB, operator.(string))

		// We are assuming it's an AND
		if !result {
			return false
		}
	}

	return true
}

func main() {
	var testsPath *string

	testsPath = flag.String("t", "", "Specify the folder containing the tests")

	flag.Parse()

	var pulsarHostURI string
	confManager.LoadDefaultConfig()
	pulsarHost := confManager.GetValue("pulsar_host")
	pulsarPort := confManager.GetValue("pulsar_port")
	pulsarHostURI = fmt.Sprintf("pulsar://%s:%s", pulsarHost, pulsarPort)

	//Create the Pulsar Client
	client := PulsarLib.InitClient(pulsarHostURI)
	defer (*client.Client).Close()

	// Connect to the database
	var dbConn dbd.DB_Connection
	dbConn.Host = confManager.GetValue("mongo_host")
	dbConn.Username = confManager.GetValue("mongo_user")
	dbConn.Password = confManager.GetValue("mongo_password")
	dbConn.Port = confManager.GetValue("mongo_port")
	dbAdmin, _ := dbw.Connect(dbConn)

	// Open our JSON file
	files, err := ioutil.ReadDir(*testsPath)
	if err != nil {
		log.Fatal(err)
	}

	var successfulTests []string
	var failedTests []string
	testStartTime := time.Now()
	for _, file := range files {
		filename := file.Name()

		// Ignore non JSON files
		if !strings.HasSuffix(filename, ".json") {
			continue
		}

		filePath := filepath.Join(*testsPath, filename)
		jsonFile, err := os.Open(filePath)

		// if we os.Open returns an error then handle it
		if err != nil {
			log.Println(err)
		}

		log.Println("Successfully Opened " + filename)
		byteValue, _ := io.ReadAll(jsonFile)
		jsonFile.Close()

		var loadedTest Message
		json.Unmarshal(byteValue, &loadedTest)

		// Delete users (just in case)
		for _, user := range loadedTest.Users {
			dbu.DeleteUser(dbAdmin, user.Username)
			for _, role := range user.Roles {
				dbw.DeleteDatabase(dbAdmin, role.Db)
			}
		}

		// Create the needed users
		userRaw, _ := json.Marshal(loadedTest.Users)
		dbu.Add_users_direct(dbAdmin, userRaw)

		testSuccessful := true
		var responseMap = make(map[string]interface{})
		for _, action := range loadedTest.Actions {
			log.Println("Action name: " + action.Id)
			log.Println("Endpoint: " + action.Topic)

			// Check if conditions are met
			if !checkConditions(action.Condition, responseMap) {
				log.Println("Skipping action " + action.Id + " as conditions are not met")
				continue
			}

			//Replace all placeholders by their respective values
			replace(action.Request, responseMap)
			replace(action.Response, responseMap)

			//requestJson, _ := json.Marshal(action.Request)
			//log.Println("Sent request: " + string(requestJson))

			// Set up loop variables
			iteration := 0
			loopEnabled := false
			forData := action.For
			if forData.Start != 0 && forData.End != 0 {
				loopEnabled = true

				// If step is not set automatically set it to 1/-1
				if forData.Step == 0 {
					if forData.Start <= forData.End {
						forData.Step = 1
					} else {
						forData.Step = -1
					}
				}
			}

			repeat := true
			startTime := time.Now()
			timeoutReached := false
			timeout := time.Duration(action.Timeout) * time.Millisecond
			for repeat {
				// Only repeat if we wait for the response to be the expected one or if we are looping
				if !loopEnabled && !action.WaitUntilResponse {
					repeat = false
				}

				// Send request
				// The response can be null if we are skipping its response
				response := send(client, action.Topic, action.Request, action.SkipResponse)

				// Skip comparison if the action does not expect a response
				if action.SkipResponse {
					log.Println("This action does not expect a response, skipping comparison")
					continue
				} else {
					log.Println("Got response: " + string(response))
				}

				var responseData interface{}
				err := json.Unmarshal(response, &responseData)
				if err != nil {
					log.Println("Error when trying to parse the response!")
					panic(err)
				}

				// Store response into the response map
				responseMap[action.Id] = responseData

				// Compare the response with the expected one depending on if it's a dict or a list
				if contains(responseData, action.Response) {
					log.Println("Response equal to expected response")
					repeat = false
				} else {
					log.Println("Response not equal to expected response")
					log.Println(action.Response)

					if !action.WaitUntilResponse {
						testSuccessful = false
					}

					if time.Since(startTime) >= timeout {
						log.Println("Timeout reached")
						timeoutReached = true
						break
					}
				}

				//Sleep for 1 second
				if repeat {
					log.Println("Waiting 1 second before trying again...")
					time.Sleep(1 * time.Second)
				}

				if loopEnabled {
					iteration++
					// Don't repeat if it is the last iteration
					if iteration >= forData.End {
						repeat = false
					}
				}
			}

			// If the timeout has been reached stop since we cannot continue with the test
			if timeoutReached {
				break
			}
		}

		if testSuccessful {
			log.Println("Test successful")
			successfulTests = append(successfulTests, filename)
		} else {
			log.Println("Test failed")
			failedTests = append(failedTests, filename)
		}

		// Delete users
		//dbu.DeleteUser(db_admin, "test@test.com")
		log.Println("Deleting test database...")
		for _, user := range loadedTest.Users {
			dbu.DeleteUser(dbAdmin, user.Username)
			for _, role := range user.Roles {
				dbw.DeleteDatabase(dbAdmin, role.Db)
			}
		}

	}

	// Disconnect from the database
	dbw.Disconnect(dbAdmin)

	successfulTestsNumber := len(successfulTests)
	failedTestsNumber := len(failedTests)
	testsPerformed := successfulTestsNumber + failedTestsNumber
	log.Println("Number of tests performed: " + strconv.Itoa(testsPerformed))
	log.Println("Number of successful tests: " + strconv.Itoa(successfulTestsNumber) + " (" + strconv.Itoa(successfulTestsNumber*100/testsPerformed) + "%)")
	log.Println("Number of failed tests: " + strconv.Itoa(failedTestsNumber) + " (" + strconv.Itoa(failedTestsNumber*100/testsPerformed) + "%)")
	log.Println("Successful tests: " + strings.Join(successfulTests, ", "))
	log.Println("Failed tests: " + strings.Join(failedTests, ", "))
	log.Println("Total time: " + time.Since(testStartTime).String())
}
