package main

import (
	"attack-manager/AttackAbstraction"
	"os"
	"sync"

	"confManager"

	PulsarLib "gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
)

var comm_client *PulsarLib.PL_Client

/*
We create 2 goroutines that will listen for different pulsar msg topics:
 1. It expects an attack type. It will ask for all the options relatted to the commands of the attack
    Then it will wait until the options are written to the map. Once the attack is updated it will ask via pulsar
    to store it.
 2. It expects an options type. It will read the options and convert it to string and store it to the map.
*/
func main() {
	var err error
	confManager.LoadDefaultConfig()
	pulsar_host := confManager.GetValue("pulsar_host")
	pulsar_port := confManager.GetValue("pulsar_port")

	plugin_dir := confManager.GetValue("plugin_dir")
	if plugin_dir == "" {
		plugin_dir = "/usr/local/huntdown/share/processors/"
	}

	if _, err = os.Stat(plugin_dir); err != nil {
		plugin_dir = "../attacks/"
	}

	AttackAbstraction.ProcessorLoader(plugin_dir)

	//Create the Pulsar Client
	client := PulsarLib.InitClient("pulsar://" + pulsar_host + ":" + pulsar_port)
	defer (*client.Client).Close()
	comm_client = &client

	var mainwg sync.WaitGroup
	mainwg.Add(16)
	PulsarLib.WaitEvent(*comm_client, "attack-db.buildOptions", createOptions)
	PulsarLib.WaitEvent(*comm_client, "attack-db.postProcessResult", postProcessAttack)
	PulsarLib.WaitEvent(*comm_client, "ui-db.search", searchGeneralAttacks)
	PulsarLib.WaitEvent(*comm_client, "ui-db.getDashboard", getDashboard)
	PulsarLib.WaitEvent(*comm_client, "ui-db.getResult", getResult)
	PulsarLib.WaitEvent(*comm_client, "ui-db.getAttack", getAttack)
	PulsarLib.WaitEvent(*comm_client, "ui-db.getAttacks", getAttacks)
	PulsarLib.WaitEvent(*comm_client, "ui-db.getFilteredAttacks", getFilteredAttacks)
	PulsarLib.WaitEvent(*comm_client, "ui-db.updateAttackLogs", updateResultLogs)

	PulsarLib.WaitEvent(client, "ui-db.getAttackOptions", getAttackSchemaWithOptions)
	PulsarLib.WaitEvent(client, "ui-db.getOptions", getRemoteAttackOptions)
	PulsarLib.WaitEvent(client, "attack-db.getSingleOption", getGeneralOption)
	PulsarLib.WaitEvent(client, "ui-db.AttackSubmission", attackSubmission)

	PulsarLib.WaitEvent(*comm_client, "ui-db.startAttack", startAttack)
	PulsarLib.WaitEvent(client, "ui-db.deleteAttack", deleteAttack)
	PulsarLib.WaitEvent(*comm_client, "ui-db.getNotExecutedAttacks", getNotExecutedAttacks)
	mainwg.Wait()
}
