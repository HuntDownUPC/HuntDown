package main

import (
	"confManager"
	"encoding/json"
	"flag"
	"helpers/GeneralHelper"
	"os"
	"strconv"
	"testing"

	log "github.com/sirupsen/logrus"
	PulsarLib "gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
)

func TestUpdateAttackLogs(t *testing.T) {
	var pulsar_host *string
	var pulsar_port *int
	var creds *string

	creds = flag.String("credentials", "", "JSON file with the credentials")
	pulsar_host = flag.String("pulsar", "", "Pulsar host: e.g. 192.168.122.168")
	pulsar_port = flag.Int("pulsar-port", 0, "Pulsar port: e.g. 6650")

	flag.Parse()

	confManager.LoadDefaultConfig()
	if *pulsar_host == "" {
		*pulsar_host = confManager.GetValue("pulsar_host")
	}
	if *pulsar_host == "" {
		*pulsar_host = "localhost"
	}

	port, err := strconv.Atoi(confManager.GetValue("pulsar_port"))
	if port != 0 && *pulsar_port == 0 {
		if err != nil {
			t.Fatalf("failed to convert pulsar port: %v", err)
		}
		*pulsar_port = port
	}
	if *pulsar_port == 0 {
		*pulsar_port = 6650
	}
	pulsar_port_str := strconv.Itoa(*pulsar_port)
	client := PulsarLib.InitClient("pulsar://" + *pulsar_host + ":" + pulsar_port_str)
	comm_client = &client
	defer (*comm_client.Client).Close()

	log.Print("Logging in")
	session_id, err := GeneralHelper.LoginWithCredentialFile(client, *creds)
	if err != nil {
		log.Println("Error logging in: ", err)
		return
	}

	data, err := os.ReadFile("tests/test1.json")
	if err != nil {
		t.Fatalf("failed to read input file: %v", err)
	}

	parsed := make(map[string]interface{})
	if err := json.Unmarshal(data, &parsed); err != nil {
		t.Fatalf("failed to unmarshal input JSON: %v", err)
	}
	parsed["session_id"] = session_id
	msg_to_instantiation := PulsarLib.BuildMessage(parsed)
	to_send, _ := json.Marshal(msg_to_instantiation)
	updateResultLogs(to_send)
	GeneralHelper.Logout(*comm_client, session_id)
}
