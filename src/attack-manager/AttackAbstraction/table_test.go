package AttackAbstraction

import (
	"encoding/json"
	"log"
	"testing"
)

func TestTableUnique(t *testing.T) {
	var rTable ResultTable

	for _, el := range []string{"blue", "green"} {
		var row Row
		var element Element
		for _, val := range []string{"1", "2"} {
			element.Color = el
			element.Value = val
			row.AddColumn(element)
		}
		rTable.AddRow(row)
	}
	result, err := json.Marshal(rTable)
	if err != nil {
		t.Error("Parsing JSON")
	}
	log.Println(string(result))
}
