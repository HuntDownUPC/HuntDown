package AttackAbstraction

import (
	"encoding/json"
)

func ResultsToTable(results []byte) []byte {
	//results in map
	var schema map[string]interface{}
	json.Unmarshal(results, &schema)

	if _, ok := schema["processor_name"]; !ok {
		schema_bytes, _ := json.Marshal(schema)
		return schema_bytes
	}

	return ProcessTable(schema["processor_name"].(string), schema)
}
