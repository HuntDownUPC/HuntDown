package AttackAbstraction

import (
	"encoding/json"
	"os"
	"path/filepath"
	"plugin"

	log "github.com/sirupsen/logrus"
)

type Processor struct {
	Name              string
	LibName           string
	Processor         *plugin.Plugin
	ResultProcessor   plugin.Symbol
	TableGenerator    plugin.Symbol
	WorkflowProcessor plugin.Symbol
}

var ProcessorRegistry map[string]Processor

func getProcessorName(proc Processor) (string, error) {
	name_var, err := proc.Processor.Lookup("Name")
	if err != nil {
		log.Println("Error loading processor", err)
		return "", err
	}
	name := *name_var.(*string)
	return name, nil
}

func getResultProcessor(proc Processor) (plugin.Symbol, error) {
	result_processor, err := proc.Processor.Lookup("ResultProcessor")
	if err != nil {
		log.Println("Error loading processor", err)
		return nil, err
	}
	return result_processor, nil
}

func getWorkflowProcessor(proc Processor) (plugin.Symbol, error) {
	result_processor, err := proc.Processor.Lookup("WorkflowProcessor")
	if err != nil {
		log.Println("Error loading processor", err)
		return nil, err
	}
	return result_processor, nil
}

func getTableGenerator(proc Processor) (plugin.Symbol, error) {
	table_generator, err := proc.Processor.Lookup("TableGenerator")
	if err != nil {
		log.Println("Error loading processor", err)
		return nil, err
	}
	return table_generator, nil
}

func GetProcessor(name string) Processor {
	return ProcessorRegistry[name]
}

func ProcessorLoader(library_dir string) {
	ProcessorRegistry = make(map[string]Processor)
	var amount_processors int = 0

	err := filepath.Walk(library_dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() && filepath.Ext(info.Name()) == ".so" {
			var proc Processor
			full_path := path
			proc.Processor, err = plugin.Open(full_path)
			if err != nil {
				log.Println("Error Opening processor", err)
				return filepath.SkipDir
			}
			proc.Name, err = getProcessorName(proc)
			if err != nil {
				log.Println("Error getting processor name", err)
				return filepath.SkipDir
			}
			proc.ResultProcessor, err = getResultProcessor(proc)
			if err != nil {
				log.Println("Error getting result processor", err)
				return filepath.SkipDir
			}
			proc.LibName = info.Name()
			proc.TableGenerator, err = getTableGenerator(proc)
			if err != nil {
				log.Println("Error getting table generator", err)
				return filepath.SkipDir
			}
			proc.WorkflowProcessor, err = getWorkflowProcessor(proc)
			if err != nil {
				log.Println("Error getting workflow processor", err)
				return filepath.SkipDir
			}

			amount_processors++
			log.Println("Adding Procesor", proc.Name)
			ProcessorRegistry[proc.Name] = proc
		}
		return nil
	})
	if err != nil {
		log.Error("Error while walking through directory ", library_dir, " ", err)
	}
	log.Println("Loaded", amount_processors, "processors")
}

func ProcessTable(processor_name string, output map[string]interface{}) []byte {
	log.Println("Generating table for processor", processor_name)
	if _, ok := ProcessorRegistry[processor_name]; !ok {
		out, _ := json.Marshal(output)
		return out
	}
	processor := ProcessorRegistry[processor_name]
	result_processor := processor.TableGenerator.(func(map[string]interface{}) []byte)
	return result_processor(output)
}

func ProcessResult(output map[string]interface{}) []byte {
	log.Println(output)
	if _, ok := output["processor_name"]; !ok {
		log.Error("Processor name not found in output")
		return []byte{}
	}
	processor_name := output["processor_name"].(string)
	if _, ok := ProcessorRegistry[processor_name]; !ok {
		log.Println("Processor not found", processor_name)
		out, _ := json.Marshal(output)
		return out
	}
	processor := ProcessorRegistry[processor_name]
	result_processor := processor.ResultProcessor.(func(map[string]interface{}) []byte)
	return result_processor(output)
}

func ProcessAttackWorkflow(output map[string]interface{}) []byte {
	if _, ok := output["processor_name"]; !ok {
		log.Error("Processor name not found in output")
		return nil
	}
	processor_name := output["processor_name"].(string)
	log.Println("Processing attack results for workflow. Processor", processor_name)
	if _, ok := ProcessorRegistry[processor_name]; !ok {
		out, _ := json.Marshal(output)
		return out
	}
	processor := ProcessorRegistry[processor_name]
	result_processor := processor.WorkflowProcessor.(func(map[string]interface{}) []byte)
	return result_processor(output)
}
