package main

import (
	b64 "encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	as "helpers/AttackSchema"
	"helpers/DatabaseHelper"
	fs "helpers/FlagsSchema"
	GeneralHelper "helpers/GeneralHelper"
	of "helpers/OptionsFile"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	log "github.com/sirupsen/logrus"
	PulsarLib "gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
)

func getAttackSchemaWithOptions(value []byte) {
	var attack as.AttackSchema
	var session_id int
	var err error
	log.Info("Getting attack options for client")

	var message PulsarLib.MessageResponse
	json.Unmarshal(value, &message)
	log.Debug(message.Msg)

	session_id, err = GeneralHelper.GetSession_id(message.Msg)
	if err != nil {
		log.Error("Error session_id not found ", err)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoSession))
		return
	}

	var name string
	err = GeneralHelper.GetMapElement(message.Msg, "name", &name)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, []byte("{}"))
		return
	}

	attack, _ = getGeneralAttackByName(session_id, name)

	var options []of.OptionsFile
	//traverse attackchema to get all the attacks, we just have 1 attack atm, so no traverse
	for i := 0; i < len(attack.CliCommands); i++ {
		current_id := attack.CliCommands[i].OptionsId
		option, success := getGeneralAttackOptions(session_id, current_id)
		if !success {
			continue
		}
		options = append(options, option)
	}
	response, _ := json.Marshal(options)
	PulsarLib.SendMessage(*comm_client, message.Id, response)
}

func getGeneralAttackOptions(session_id int, optionsID string) (option of.OptionsFile, success bool) {
	var filter DatabaseHelper.Filter
	var err error

	success = false
	if string(optionsID) == "" {
		return
	}
	filter = DatabaseHelper.Filter{Field: "optionsid", Value: optionsID, Op: "$eq"}
	query := DatabaseHelper.DatabaseQuery{Session_id: session_id,
		Database:   "general",
		Collection: "OptionsFile",
		Filters:    []DatabaseHelper.Filter{filter}}

	to_send := PulsarLib.BuildMessage(GeneralHelper.StructToMap(query))
	response := PulsarLib.SendRequestSync(*comm_client, "db.newQuery", to_send)
	log.Println("Options: ", string(response))
	var formatted []of.OptionsFile
	err = json.Unmarshal(response, &formatted)
	if err != nil || formatted == nil || len(formatted) == 0 {
		log.Println("Error getting general attack by name ", err)
		return
	}
	option = formatted[0]
	success = true
	return
}

func getRemoteAttackOptions(msg []byte) {
	log.Println("Getting not executed attacks")
	var message PulsarLib.MessageResponse
	var session_id int
	var optionsId string
	var err error

	log.Info("Reading client options...")
	json.Unmarshal(msg, &message)
	result := message.Msg
	log.Debug(result)

	session_id, err = GeneralHelper.GetSession_id(message.Msg)
	if err != nil {
		log.Println("Error malformed attack query ", err)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoSession))
		return
	}

	err = GeneralHelper.GetMapElement(result, "optionsId", &optionsId)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.FieldNotFound))
		return
	}

	option, err := DatabaseHelper.GetEntryByID(session_id, comm_client, optionsId)
	if err != nil {
		log.Error("Error getting attack options ", err)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.EmptyAnswer))
		return
	}
	finalresult, err := json.Marshal(option)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.UnknownError))
		return
	}
	PulsarLib.SendMessage(*comm_client, message.Id, finalresult)
}

func getGeneralOption(value []byte) {
	var session_id int
	var err error
	var message PulsarLib.MessageResponse
	json.Unmarshal(value, &message)

	session_id, err = GeneralHelper.GetSession_id(message.Msg)
	if err != nil {
		log.Println("Error malformed query on attack options", err.Error())
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoSession))
		return
	}

	var optionsId string
	err = GeneralHelper.GetMapElement(message.Msg, "optionsId", &optionsId)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, []byte("{}"))
		return
	}

	option, success := getGeneralAttackOptions(session_id, optionsId)
	var response []byte
	if !success {
		response, _ = json.Marshal("{}")
	} else {
		response, _ = json.Marshal(option)
	}
	PulsarLib.SendMessage(*comm_client, message.Id, response)
}

// pre: it recieves a byte value
// post: it computes the string and store it at optionsmp map as its id as a key
func createOptions(msg []byte) {
	var message PulsarLib.MessageResponse
	var op of.OptionsFile

	log.Println("Creating Options")
	json.Unmarshal(msg, &message)
	my_query := message.Msg

	session_id, err := GeneralHelper.GetSession_id(message.Msg)
	if err != nil {
		log.Println("Error malformed query on attack options", err.Error())
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoSession))
		return
	}
	delete(my_query, "session_id")

	tmp, _ := json.Marshal(my_query)
	json.Unmarshal(tmp, &op)

	if op.Delimiter == "" {
		// tool OptionFile not compatible with new json format
		// using old string system
		var finalcommand string
		finalcommand, err = createOptionsLocal(session_id, op.OptionsId)

		if err != nil {
			PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.UnknownError))
			return
		}

		log.Println("Command sent to system: ", finalcommand)
		PulsarLib.SendMessage(*comm_client, message.Id, []byte(finalcommand))
	} else {
		// tool OptionFile compatible with new json format
		var flags fs.Flags
		flags, err = createJsonOptionsLocal(session_id, op.OptionsId)

		if err != nil {
			PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.UnknownError))
			return
		}

		flagsJSON, err := json.Marshal(flags)
		if err != nil {
			PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.UnknownError))
			return
		}

		log.Println("Command sent to system: ", flags)
		PulsarLib.SendMessage(*comm_client, message.Id, flagsJSON)
	}

}
func downloadFile(url string, localDir string) (string, error) {
	// Obtener el nombre base del archivo desde la URL
	fileName := filepath.Base(url)
	baseName := strings.TrimSuffix(fileName, filepath.Ext(fileName))
	extension := filepath.Ext(fileName)
	localPath := filepath.Join(localDir, fileName)

	// Generar un nombre único si es necesario
	counter := 1
	for fileExists(localPath) {
		localPath = filepath.Join(localDir, fmt.Sprintf("%s_%d%s", baseName, counter, extension))
		counter++
	}

	// Hacer la solicitud HTTP primero
	resp, err := http.Get(url)
	if err != nil {
		return "", fmt.Errorf("error al descargar el archivo desde %s: %v", url, err)
	}
	defer resp.Body.Close()

	// Comprobar el código de estado HTTP
	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("error: la URL %s devolvió el estado %d", url, resp.StatusCode)
	}

	// Crear el archivo en el directorio local
	file, err := os.Create(localPath)
	if err != nil {
		return "", fmt.Errorf("error al crear el archivo %s: %v", localPath, err)
	}
	defer file.Close()

	// Copiar el contenido descargado al archivo local
	_, err = io.Copy(file, resp.Body)
	if err != nil {
		return "", fmt.Errorf("error al guardar el archivo %s: %v", localPath, err)
	}

	fmt.Printf("Archivo descargado con éxito: %s\n", localPath)
	return localPath, nil // Devuelve la ruta completa del archivo
}

func processFilePath(input string, localDir string) (string, error) {
	// Verificar si la entrada es una ruta local válida
	if fileExists(input) {
		// Obtener el nombre base del archivo (solo el nombre, sin la ruta)
		fileName := filepath.Base(input)
		localPath := filepath.Join(localDir, fileName)

		// Verificar si ya existe en el directorio destino y generar un nombre único si es necesario
		baseName := strings.TrimSuffix(fileName, filepath.Ext(fileName))
		extension := filepath.Ext(fileName)
		counter := 1
		finalFileName := fileName // Inicialmente es igual al nombre original

		for fileExists(localPath) {
			// Generar un nuevo nombre único si el archivo ya existe
			finalFileName = fmt.Sprintf("%s_%d%s", baseName, counter, extension)
			localPath = filepath.Join(localDir, finalFileName)
			counter++
		}

		// Copiar el archivo al directorio destino
		inputFile, err := os.Open(input)
		if err != nil {
			return "", fmt.Errorf("error al abrir el archivo %s: %v", input, err)
		}
		defer inputFile.Close()

		outputFile, err := os.Create(localPath)
		if err != nil {
			return "", fmt.Errorf("error al crear el archivo %s: %v", localPath, err)
		}
		defer outputFile.Close()

		_, err = io.Copy(outputFile, inputFile)
		if err != nil {
			return "", fmt.Errorf("error al copiar el archivo %s: %v", input, err)
		}

		fmt.Printf("Archivo copiado con éxito: %s\n", localPath)
		return finalFileName, nil // Devuelve el nombre final del archivo (con o sin número)
	}

	// Si no es una ruta local, devuelve un error
	return "", fmt.Errorf("archivo %s no encontrado", input)
}

// fileExists verifica si un archivo existe en la ruta dada
func fileExists(path string) bool {
	_, err := os.Stat(path)
	return !os.IsNotExist(err)
}

func createOptionsLocal(session_id int, options_id string) (string, error) {
	var op of.OptionsFile
	var err error

	if options_id == "" {
		return "", nil
	}
	//validation_op contains all the options
	validation_op, err := DatabaseHelper.GetEntryByID(session_id, comm_client, options_id)
	if err != nil {
		log.Println("error getting attack options")
		err = errors.New("error getting attack options")
		return "", err
	}

	err = GeneralHelper.MapToStruct(validation_op, &op)
	if err != nil {
		log.Println("error with options format")
		err = errors.New("error with options format")
		return "", err
	}

	var finalcommand string
	for index, element := range op.Options {
		valid := true
		if len(op.Options) <= index {
			continue
		}
		if op.Options[index].Regex != "" && element.Value != "" {
			// we need to assure that the original regex is fulfilled
			regex_rule, _ := b64.StdEncoding.DecodeString(op.Options[index].Regex)
			valid, err = regexp.MatchString(string(regex_rule), element.Value)
			if err != nil {
				log.Println(err)
			}
		}
		if strings.Contains(element.Title, "Wordlist") {
			if strings.HasPrefix(element.Value, "http://") || strings.HasPrefix(element.Value, "https://") {
				// Si es una URL, descarga el archivo
				fileName, err := downloadFile(element.Value, "/etc/huntdown/wordlists")
				if err != nil {
					fmt.Printf("Error descargando el archivo: %v\n", err)
				} else {
					element.Value = filepath.Base(fileName)
					fmt.Printf("Archivo final asignado a element.Value (descargado): %s\n", fileName)
				}
			} else if strings.Contains(element.Value, "/") {
				// Si contiene barras, se asume que es una ruta local del sistema
				fileName, err := processFilePath(element.Value, "/etc/huntdown/wordlists")
				if err != nil {
					fmt.Printf("Error procesando el archivo local: %v\n", err)
				} else {
					element.Value = fileName
					fmt.Printf("Archivo final asignado a element.Value (local): %s\n", fileName)
				}
			} else {
				// Si no contiene barras, se asume que es un archivo en /etc/huntdown/wordlists
				localPath := filepath.Join("/etc/huntdown/wordlists", element.Value)
				if fileExists(localPath) {
					element.Value = element.Value
					fmt.Printf("Archivo encontrado en /etc/huntdown/wordlists: %s\n", element.Value)
				} else {
					fmt.Printf("Error: archivo %s no encontrado en /etc/huntdown/wordlists\n", element.Value)
				}
			}
		}

		if valid {
			// generate a valid json
			if element.NeedCheckbox { //The option CAN be activated
				if element.Checkbox { //The Option is Activated
					if !element.TextInput && element.Value == "" {
						// There is no input text to read nor pre written
						finalcommand += element.Flag + " "
					} else if !element.TextInput && element.Value != "" {
						// We dont let the user change the predefined value, but there is a value
						finalcommand += element.Flag + " "
						finalcommand += element.Value + " "
					} else { //this means that we also have to read an input text
						finalcommand += element.Flag + " "
						finalcommand += element.Value + " "
					}
				}
			} else { //This means that the option can't be activated and must be a text input
				if element.Flag == "" { //this means that it's not an option but a requirement
					finalcommand += element.Value + " "
				} else {
					finalcommand += element.Flag + " "
					finalcommand += element.Value + " "
				}
			}
		} else {
			log.Println("Invalid regex")
			return "", errors.New("invalid regex")
		}
	}
	log.Println("FINALCOMMAND: " + finalcommand)
	return finalcommand, nil
}

func createJsonOptionsLocal(session_id int, options_id string) (fs.Flags, error) {
	var op of.OptionsFile
	var err error

	if options_id == "" {
		return fs.Flags{}, nil
	}
	// Get the options entry by ID from the database
	validation_op, err := DatabaseHelper.GetEntryByID(session_id, comm_client, options_id)
	if err != nil {
		log.Println("error getting attack options")
		return fs.Flags{}, errors.New("error getting attack options")
	}

	// Map the validation_op to the OptionsFile structure
	err = GeneralHelper.MapToStruct(validation_op, &op)
	if err != nil {
		log.Println("error with options format")
		return fs.Flags{}, errors.New("error with options format")
	}

	flags := fs.Flags{
		Delimiter: op.Delimiter,
		Flags:     make(map[string]string),
	}

	for _, element := range op.Options {
		if strings.Contains(element.Title, "Wordlist") {
			if strings.HasPrefix(element.Value, "http://") || strings.HasPrefix(element.Value, "https://") {
				// Si es una URL, descarga el archivo
				fileName, err := downloadFile(element.Value, "/etc/huntdown/wordlists")
				if err != nil {
					fmt.Printf("Error descargando el archivo: %v\n", err)
				} else {
					element.Value = filepath.Base(fileName)
					fmt.Printf("Archivo final asignado a element.Value (descargado): %s\n", fileName)
				}
			} else if strings.Contains(element.Value, "/") {
				// Si contiene barras, se asume que es una ruta local del sistema
				fileName, err := processFilePath(element.Value, "/etc/huntdown/wordlists")
				if err != nil {
					fmt.Printf("Error procesando el archivo local: %v\n", err)
				} else {
					element.Value = fileName
					fmt.Printf("Archivo final asignado a element.Value (local): %s\n", fileName)
				}
			} else {
				// Si no contiene barras, se asume que es un archivo en /etc/huntdown/wordlists
				localPath := filepath.Join("/etc/huntdown/wordlists", element.Value)
				if fileExists(localPath) {
					element.Value = element.Value
					fmt.Printf("Archivo encontrado en /etc/huntdown/wordlists: %s\n", element.Value)
				} else {
					fmt.Printf("Error: archivo %s no encontrado en /etc/huntdown/wordlists\n", element.Value)
				}
			}
		}

		if element.NeedCheckbox {
			if element.Checkbox {
				if !element.TextInput && element.Value == "" {
					// Flag only
					flags.Flags[element.Flag] = ""
				} else {
					// Flag with value
					flags.Flags[element.Flag] = element.Value
				}
			}
		} else {
			if element.Flag == "" {
				// Non-flag element
				flags.Flags[""] = element.Value
			} else {
				// Flag with value
				flags.Flags[element.Flag] = element.Value
			}
		}
	}
	log.Debug("FLAGS ", flags.Flags)
	return flags, nil
}
