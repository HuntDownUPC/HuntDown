package main

import (
	"encoding/json"
	"fmt"
	as "helpers/AttackSchema"
	"helpers/DatabaseHelper"
	GeneralHelper "helpers/GeneralHelper"
	"reflect"
	"strconv"
	"strings"
	"time"
	"unicode"

	log "github.com/sirupsen/logrus"
	PulsarLib "gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
)

type DashboardSchema struct {
	AttacknameDashboard string `json:"attacknamedashboard"`
	Options             struct {
		Executed    string `json:"executed"`
		Executing   string `json:"executing"`
		Error       string `json:"error"`
		Avgtime     string `json:"avgtime"`
		Attackmonth string `json:"attackmonth"`
	} `json:"options"`
}

func getDashboardFromAttackGroup(session_id int, attack_group string) DashboardSchema {
	var dashboard DashboardSchema
	var err error

	tmp_map, err := DatabaseHelper.GetEntryByName(session_id, comm_client, "attacknamedashboard", attack_group)
	if err != nil {
		log.Error("Error getting dashboard ", err)
		return dashboard
	}
	err = GeneralHelper.MapToStruct(tmp_map, &dashboard)
	if err != nil {
		log.Error("Error getting dashboard ", err)
	}
	return dashboard
}

func addAttackExecutionTime(session_id int, attack_group string, duration time.Duration) {
	var dashboard DashboardSchema = getDashboardFromAttackGroup(session_id, attack_group)

	strVar := dashboard.Options.Avgtime
	var vecStr []string
	if len(strVar) <= 0 {
		vecStr = append(vecStr, strconv.Itoa(int(duration.Seconds())))
	} else {
		vecStr = strings.Split(strVar, ",")
		vecStr = append(vecStr, strconv.Itoa(int(duration.Seconds())))
	}
	condition := make(map[string]interface{})
	condition["attacknamedashboard"] = attack_group
	response, err := DatabaseHelper.UpdateOneElementByKey(session_id, comm_client, condition,
		"options.avgtime", strings.Join(vecStr, ","))
	if err != nil {
		log.Error("Error updating dashboard ", err)
		return
	}
	_ = response
	// Todo Handle Errors
}

func changeDashboardStatistics(session_id int, attack_group string, state as.AttackState, amount int) {
	var dashboard DashboardSchema = getDashboardFromAttackGroup(session_id, attack_group)

	r := reflect.ValueOf(dashboard.Options)
	upper := []rune(string(state))
	upper[0] = unicode.ToUpper(upper[0])
	new_state := string(upper)
	strVar := reflect.Indirect(r).FieldByName(new_state).String()

	var intVar int
	if len(strVar) <= 0 {
		intVar = 1
	} else {
		var err error
		intVar, err = strconv.Atoi(strVar)
		if err != nil {
			log.Fatal(err)
		}
		intVar = intVar + amount
	}
	field := fmt.Sprintf("options.%s", string(state))

	response, err := DatabaseHelper.UpdateOneElementByKey(session_id, comm_client,
		map[string]interface{}{"attacknamedashboard": attack_group}, field, strconv.Itoa(intVar))
	if err != nil {
		log.Error("Error updating dashboard ", err)
		return
	}

	_ = response
	// Todo Handle Errors
}

func getDashboard(msg []byte) {
	var message PulsarLib.MessageResponse
	var err error
	var session_id int
	var typeAttack string
	var query DatabaseHelper.DatabaseQuery

	log.Info("Database gettig dashboard values")

	json.Unmarshal(msg, &message)
	dashboard_data := message.Msg

	session_id, err = GeneralHelper.GetSession_id(dashboard_data)
	if err != nil {
		log.Error("Error malformed dashboard query ", err)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoSession))
		return
	}

	err = GeneralHelper.GetMapElement(dashboard_data, "type_of_attack", &typeAttack)
	if err != nil {
		log.Error("Error malformed dashboard query ", err)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.FieldNotFound))
		return
	}

	filter := DatabaseHelper.Filter{Field: "attacknamedashboard", Value: typeAttack, Op: "$eq"}
	query = DatabaseHelper.DatabaseQuery{Session_id: session_id,
		Filters: []DatabaseHelper.Filter{filter}}

	to_send := PulsarLib.BuildMessage(GeneralHelper.StructToMap(query))
	response := PulsarLib.SendRequestSync(*comm_client, "db.newQuery", to_send)

	tmp_map := []map[string]interface{}{}
	json.Unmarshal(response, &tmp_map)
	if len(tmp_map) == 0 {
		log.Debug("No attack found for", typeAttack)
		PulsarLib.SendMessage(*comm_client, message.Id, []byte{})
		return
	}
	response, err = json.Marshal(tmp_map[0])
	if err != nil {
		log.Error("Error getting dashboard ", err)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.EmptyAnswer))
		return
	}
	PulsarLib.SendMessage(*comm_client, message.Id, response)
}
