#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source $DIR/variables.sh

tryInstallNpmAndNode(){
    $sudo dpkg -l npm | grep -q ^ii 2> /dev/null && return
    print_header Installing NPM
    {
      $sudo apt install -y wget xz-utils npm
      #Download node LTS version
      NODE_VERSION=v18.18.0
      NODE_TAR_FILE=node-$NODE_VERSION-linux-x64.tar.xz
      rm -f $HOME/$NODE_TAR_FILE
      wget -P $HOME https://nodejs.org/dist/$NODE_VERSION/$NODE_TAR_FILE
      $sudo rm -rf /usr/local/bin/node && $sudo tar -C /usr/local --strip-components 1 -xJf $HOME/$NODE_TAR_FILE 
      export PATH=/usr/local/bin/node:$PATH;
      export PATH=$PATH:/usr/local/bin/node;
      node -v
      npm -v
    } >> "$LOG_FILE" 2>&1
}

installUI() {
    if [ ! -d $HUNTDOWN_VAR/ui ] ; then
        $sudo -u $HUNTDOWN_USER mkdir $HUNTDOWN_VAR/ui
    fi
    echo TODO: we still need to build the UI to make it more efficient
    $sudo -u $HUNTDOWN_USER cp -rfva src/Interface/* $HUNTDOWN_VAR/ui >> "$LOG_FILE" 2>&1
    $sudo -u $HUNTDOWN_USER npm install --prefix $HUNTDOWN_VAR/ui/node-pulsar >> "$LOG_FILE" 2>&1
    $sudo -u $HUNTDOWN_USER npm install --prefix $HUNTDOWN_VAR/ui/backend >> "$LOG_FILE" 2>&1
    $sudo -u $HUNTDOWN_USER npm install --prefix $HUNTDOWN_VAR/ui >> "$LOG_FILE" 2>&1
}

autostartUI(){
    print_header Setting up UI autostart
    $sudo npm install pm2 -g >> "$LOG_FILE" 2>&1
    if ! systemctl is-active --quiet pm2-$HUNTDOWN_USER; then
        $sudo env PATH="$PATH":/usr/bin /usr/local/lib/node_modules/pm2/bin/pm2 startup systemd -u $HUNTDOWN_USER --hp $HUNTDOWN_VAR >> "$LOG_FILE" 2>&1
    fi

    old_dir=$(pwd)
    cd $(cat /etc/passwd | grep ${HUNTDOWN_USER} | cut -d: -f 6)
    if ! $sudo -u $HUNTDOWN_USER pm2 show Backend > /dev/null 2>&1 ; then
        $sudo -u $HUNTDOWN_USER pm2 start --cwd $HUNTDOWN_VAR/ui/backend --name "Backend" npm -- run start-backend
    fi
    if ! $sudo -u $HUNTDOWN_USER pm2 show Frontend > /dev/null 2>&1 ; then
        $sudo -u $HUNTDOWN_USER pm2 start --cwd $HUNTDOWN_VAR/ui --name "Frontend" npm -- run start-frontend
    fi
    cd $old_dir
}

