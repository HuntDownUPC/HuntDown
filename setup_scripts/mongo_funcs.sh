#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source $DIR/variables.sh

function tryInstallMongo(){
    { $sudo apt-get -y install gnupg curl; } >> "$LOG_FILE" 2>&1
    $sudo dpkg -l mongodb-org >> "$LOG_FILE" 2>&1 | grep -q ^ii >> "$LOG_FILE" 2>&1 && return
    print_header Installing mongodb
    if [ -z "$mongo_version" ] ; then
        mongo_version="7.0"
    fi

    install_mongo
}

function install_mongo() {
    cert_filename="mongodb-server-${mongo_version}.gpg"
    out_cert="/usr/share/keyrings/$cert_filename"

    arch=""
    mongo_repo_url=""
    mongo_repo_release=""
    mongo_repo_branch=""

    if isDebian ; then
        mongo_repo_url="http://repo.mongodb.org/apt/debian"
        mongo_repo_release="bookworm"
        mongo_repo_branch="main"
    elif isUbuntu ; then
        arch="arch=amd64,arm64"
        mongo_repo_url="https://repo.mongodb.org/apt/ubuntu"
        mongo_repo_release="jammy"
        mongo_repo_branch="multiverse"
    else
        print_header "Unsupported OS to install Mongo, please do this manually"
        return
    fi

    if [ -f "$out_cert" ] ; then
        $sudo rm -f $out_cert
    fi
    curl -fsSL https://www.mongodb.org/static/pgp/server-$mongo_version.asc | \
       $sudo gpg -o ${out_cert} --dearmor
    source_file="/etc/apt/sources.list.d/mongodb-org-${mongo_version}.list"
    echo "deb [ $arch signed-by=$out_cert ] $mongo_repo_url $mongo_repo_release/mongodb-org/$mongo_version $mongo_repo_branch" | \
       $sudo tee $source_file
    $sudo apt update >> "$LOG_FILE" 2>&1
    $sudo apt-get install -y mongodb-org >> "$LOG_FILE" 2>&1
    $sudo systemctl start mongod.service

    { $sudo apt update; apt install -y mongodb-org; } >> "$LOG_FILE" 2>&1
    $sudo mkdir -p /data/db
    $sudo systemctl enable mongod 2> /dev/null
    return
}

function prepareMongo () {
    print_header Starting mongodb
    $sudo systemctl start mongod

    if [ "$os" == "Ubuntu" ] ; then
        mongo="mongosh"
    elif [ "$os" == "Debian" ] ; then
        mongo="mongosh"
    elif [ "$os" == "Kali" ] ; then
        mongo="mongosh"
    fi

    mongo_params=""
    if [ "$MONGO_HOST" != "localhost" ] ; then
        mongo_params="mongodb://$MONGO_HOST:$MONGO_PORT/admin"
    fi

    until $mongo --eval "print(\"waiting\")" &> /dev/null
        do
            sleep 1
        done

    output=$($mongo $mongo_params <<EOF
use admin
db.system.users.find({user:'$MONGO_USER'}).count()
EOF
    )

    if echo "$output" | grep "^admin> 1" > /dev/null ; then
        echo "Mongodb properly configured"
    else
        echo "Configuring Mongodb..."
        output=$($mongo $mongo_params <<EOF
use admin
db.createUser(
    {
        user:"$MONGO_USER",
        pwd: "$MONGO_PASSWORD",
        roles: [{ role: "readWrite", db: "test"},
                { role: "read", db: "reporting"}]
    }
)
EOF
)
    fi
    echo "MongoDB running successfully!"
}

function get_mongo_data() {
    if [ -z "$MONGO_USER" ] ; then
        echo -n "Enter the mongo user: "
        read -r MONGO_USER
        echo $MONGO_USER
    fi
    if [ -z "$MONGO_PASSWORD" ] ; then
        echo -n "Enter the mongo password: "
        read -sr MONGO_PASSWORD
    fi
    if [ -z "$MONGO_HOST" ] ; then
        MONGO_HOST="localhost"
    fi
    if [ -z "$MONGO_PORT" ] ; then
        MONGO_PORT="27017"
    fi
    echo ""
}

function add_mongo_users() {
    cd src/database/addUsers || exit
    { $sudo -u "$HUNTDOWN_USER" env "PATH=$PATH" go run addUsers.go -users ../users.json; } >> "$LOG_FILE" 1>&2
    cd - > /dev/null || exit
}

function create_vms() {
    cd src/database/MachinesInit || exit
    { $sudo -u "$HUNTDOWN_USER" env "PATH=$PATH" go run machineInit.go -machines "../machines.json" -config "/etc/huntdown/environment.json"; } >> "$LOG_FILE" 1>&2
    cd - > /dev/null || exit
}

function create_presets() {
    cd src/database/PresetsInit || exit
    { $sudo -u "$HUNTDOWN_USER" env "PATH=$PATH" go run presetsInit.go -presets "../presets.json" -config "/etc/huntdown/environment.json"; } >> "$LOG_FILE" 1>&2
    cd - > /dev/null || exit
}
