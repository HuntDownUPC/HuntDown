#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source $DIR/variables.sh

vagrant_install(){
    isInstalled vagrant && \
    print_ok_string "Skipping Vagrant install" && return

	print_header Installing Vagrant

	{ wget -q "$VAGRANT_URL" -O "/tmp/$VAGRANT_DEB_FILE"; \
	  $sudo dpkg -i "/tmp/$VAGRANT_DEB_FILE" ; \
	  rm -f "/tmp/$VAGRANT_DEB_FILE"; } >> "$LOG_FILE" 2>&1

	print_header Installing Libvirt plugin for vagrant
	{ $sudo apt install -y libvirt-dev; \
	  $sudo -u huntdown vagrant plugin install vagrant-libvirt; } >> "$LOG_FILE" 2>&1
	echo ""
}

