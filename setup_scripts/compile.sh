#!/bin/bash

function make_all () {
    cd src
    print_header Compiling Go Modules and main applications
    make >> "$LOG_FILE" 2>&1 
    if [ $? -ne 0 ]; then
        echo "Error while compiling Go Modules and main applications" >&2
        echo "Check ${LOG_FILE} for more information" >&2
        exit 1
    fi
    cd - > /dev/null
}

function make_install () {
    print_header Installing Go Modules and main applications
    cd src || exit
    if ! eval $sudo "HUNTDOWN_GROUP=$HUNTDOWN_GROUP HUNTDOWN_USER=$HUNTDOWN_USER PATH=$PATH" "MONGO_USER=$MONGO_USER" "MONGO_PASSWORD=$MONGO_PASSWORD" "MONGO_HOST=$MONGO_HOST" "MONGO_PORT=$MONGO_PORT"  make install >> "$LOG_FILE" 2>&1 ; then
        echo "Error while installing Go Modules and main applications" >&2
        echo "Check ${LOG_FILE} for more information" >&2
        exit 1
    fi

    (
        cd attacks || exit
        if ! make deploy >> "$LOG_FILE" 2>&1 ; then
            echo "Error while installing Go Modules and main applications" >&2
            echo "Check ${LOG_FILE} for more information" >&2
            exit 1
        fi
    )

    if ! eval $sudo "HUNTDOWN_GROUP=$HUNTDOWN_GROUP HUNTDOWN_USER=$HUNTDOWN_USER PATH=$PATH" "MONGO_USER=$MONGO_USER" "MONGO_PASSWORD=$MONGO_PASSWORD" "MONGO_HOST=$MONGO_HOST" "MONGO_PORT=$MONGO_PORT"  make install >> "$LOG_FILE" 2>&1 ; then
        echo "Error while installing Go Modules and main applications" >&2
        echo "Check ${LOG_FILE} for more information" >&2
        exit 1
    fi
    cd - > /dev/null
}
