#!/bin/bash

GO_TAR_BASE_FILE=go1.20.8.linux-amd64.tar.gz
PULSAR_VERSION="2.10.2"
APACHE_PULSAR="apache-$PULSAR_VERSION"
PULSAR_BASE_URL="https://archive.apache.org/dist/pulsar/pulsar-$PULSAR_VERSION"
PULSAR_SERVER_URL="$PULSAR_BASE_URL/apache-pulsar-$PULSAR_VERSION-bin.tar.gz"
APACHE_PULSAR_PREFIX="/usr/local/apache-pulsar-$PULSAR_VERSION"
PULSAR_USER=pulsar
PULSAR_GROUP=pulsar
PULSAR_UNIT_NAME="pulsar-standalone.service"
PULSAR_LOG_DIR="/var/log/pulsar/"
PULSAR_DATA_DIR="$APACHE_PULSAR_PREFIX/data"

VAGRANT_VERSION=$(wget https://releases.hashicorp.com/vagrant/ -q -O- | grep vagrant | head -1 | grep -Eo "vagrant_[0-9.]+" | sed 's/vagrant_//')
VAGRANT_BASE_URL="https://releases.hashicorp.com/vagrant"
VAGRANT_DEB_FILE="vagrant_${VAGRANT_VERSION}-1_amd64.deb"
VAGRANT_URL="$VAGRANT_BASE_URL/$VAGRANT_VERSION/$VAGRANT_DEB_FILE"
os=$(lsb_release -si | grep -v LSB)
release=$(lsb_release -sr | grep -v LSB)

YELLOW='\033[1;33m'
WHITE='\033[1;37m'
RED='\033[0;31m'
GREEN='\033[0;32m'
DEFAULT='\033[0m'

if [ -z $HUNTDOWN_USER ] ; then
    HUNTDOWN_USER=huntdown
fi

if [ -z $HUNTDOWN_GROUP ] ; then
    HUNTDOWN_GROUP=huntdown
fi

if [ -z $HUNTDOWN_VAR ] ; then
    HUNTDOWN_VAR="/var/huntdown"
fi

if [ -z $HUNTDOWN_PREFIX ] ; then
    HUNTDOWN_PREFIX="/usr/local/huntdown"
fi

if [ -z $HUNTDOWN_CONFIG ] ; then
    HUNTDOWN_CONFIG="/etc/huntdown"
fi

ATTACK_MANAGER_UNIT="huntdown-attack-manager.service"
INSTANTIATION_UNIT="huntdown-instantiation.service"
DATABASE_UNIT="huntdown-database.service"
WORKFLOW_UNIT="huntdown-workflow.service"

if [ -z $ENV_JSON ] ; then
    ENV_JSON=config/environment.json.in
fi

if  [ -z $LOG_FILE ] ; then
    LOG_FILE=/tmp/huntdown_install-$(date +%Y-%m-%d-%H-%M-%S).log
fi

HUNTDOWN_SERVICES=($ATTACK_MANAGER_UNIT $INSTANTIATION_UNIT $DATABASE_UNIT $WORKFLOW_UNIT)

GO=$(which go)
GO_VERSION="1.21.4"
if [ -z "$GO" ] ; then
  if [ -f "/usr/lib/go/bin/go" ] ; then
    GO="/usr/lib/go/bin/go"
    GO_VERSION=$($GO version | awk '{print $3}' | sed s/go//)
    export PATH=$PATH:/usr/lib/go/bin
  fi
else
  GO_VERSION=$($GO version | awk '{print $3}' | sed s/go//)
fi

sudo=""
if [ $UID -ne 0 ] ; then
    sudo="sudo"
fi

function isInstalled() {
    app=$1

    if [ -z "$app" ] ; then
      return 1
    fi
    dpkg -l "$app" 2> /dev/null | grep -q ^ii && return 0
    return 2
}

function print_warning_string () {
	echo -e "${WHITE}   << $* >>${DEFAULT}"
}

function print_ok_string () {
	echo -e "${GREEN}   !! $* !!${DEFAULT}"
}

function print_header () {
	echo -e "${YELLOW}   ** $* **${DEFAULT}"
}

function isDebian() {
    if [ "$os" == "Debian" ] ; then
        return 0
    fi
    return 1
}

function isKali () {
    if [ "$os" == "Kali" ] ; then
        return 0
    fi
    return 1
}

function isUbuntu () {
    if [ "$os" == "Ubuntu" ] ; then
        return 0
    fi
    return 1
}
