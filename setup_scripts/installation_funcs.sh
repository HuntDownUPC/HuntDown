#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source $DIR/variables.sh
source $DIR/environment.sh
source $DIR/mongo_funcs.sh
source $DIR/pulsar_funcs.sh

function install_jq (){
	{ sudo apt update; \
      sudo apt install -y jq >> "$LOG_FILE" 2>&1; } >> "$LOG_FILE" 2>&1
}

update_packages(){
	print_header Updating dependencies
	{ 
      $sudo apt update; \
	  $sudo apt install -y software-properties-common python3-pip; \
      #Update needrestart.conf file for silent npm installation
      $sudo sed -i "/#\$nrconf{restart} = 'i';/s/.*/\$nrconf{restart} = 'a';/" /etc/needrestart/needrestart.conf; \
      echo "Installing Docker..."; \
      $sudo apt install -y docker.io; \
      $sudo usermod -aG docker $USER; \
      echo ""
    } >> "$LOG_FILE" 2>&1
    if [ -z $GO ] ; then \
        print_header "Installing Go..."
        {
            GO_TAR_FILE=go${GO_VERSION}.linux-amd64.tar.gz; \
            rm -f $HOME/$GO_TAR_FILE; \
            wget -q -P $HOME https://go.dev/dl/$GO_TAR_FILE; \
            $sudo rm -rf /usr/lib/go && $sudo tar -C /usr/lib -xzf $HOME/$GO_TAR_FILE; \
        } >> "$LOG_FILE" 2>&1
    else
        print_ok_string "Go already installed"
    fi
    {
      export PATH=/usr/lib/go/bin:$PATH; \
      export PATH=$PATH:/usr/lib/go/bin; \
      go version; \
      $sudo apt install -y qemu-kvm libvirt-daemon-system; \
      
      if ! getent passwd $HUNTDOWN_USER > /dev/null 2>&1 ; then \
        $sudo usermod -a -G libvirt "$HUNTDOWN_USER"; \
      fi
    } >> "$LOG_FILE" 2>&1
}

update_docker_packages(){
	print_header Updating dependencies
	{ 
      $sudo apt update; \
	  $sudo apt install -y software-properties-common python3-pip; \
      #Update needrestart.conf file for silent npm installation
      $sudo sed -i "/#\$nrconf{restart} = 'i';/s/.*/\$nrconf{restart} = 'a';/" /etc/needrestart/needrestart.conf; \
      echo "Installing Docker..."; \
      $sudo apt install -y docker.io; \
      $sudo usermod -aG docker $USER; \
      echo ""; \
      echo "Installing Go..."; \
      GO_TAR_FILE=go1.20.8.linux-amd64.tar.gz; \
      rm -f $HOME/$GO_TAR_FILE; \
      wget -P $HOME https://go.dev/dl/$GO_TAR_FILE; \
      $sudo rm -rf /usr/lib/go && $sudo tar -C /usr/lib -xzf $HOME/$GO_TAR_FILE; \
      export PATH=/usr/lib/go/bin:$PATH; \
      export PATH=$PATH:/usr/lib/go/bin; \
      go version; \
      $sudo apt install -y qemu-kvm libvirt-daemon-system; \
      
      if ! getent passwd $HUNTDOWN_USER > /dev/null 2>&1 ; then \
        $sudo usermod -a -G libvirt "$HUNTDOWN_USER"; \
      fi
    } >> "$LOG_FILE" 2>&1
}


create_user_docker() {
    if ! getent passwd $HUNTDOWN_USER > /dev/null 2>&1 ; then
        print_header Adding huntdown user
        $sudo useradd -r --home-dir "$HUNTDOWN_VAR" -s /bin/false "$HUNTDOWN_USER"
        $sudo usermod -a -G $USER "$HUNTDOWN_USER"
        $sudo usermod -aG docker huntdown;
        echo "$HUNTDOWN_USER  ALL=(ALL) NOPASSWD:ALL" | $sudo tee /etc/$sudoers.d/$HUNTDOWN_USER
    fi

    if [ ! -d "$HUNTDOWN_VAR" ] ; then
        $sudo chown -R $HUNTDOWN_USER:$HUNTDOWN_USER $HUNTDOWN_VAR
    fi
}

create_user() {
    if ! getent passwd $HUNTDOWN_USER > /dev/null 2>&1 ; then
        print_header Adding huntdown user
        $sudo useradd -r --home-dir "$HUNTDOWN_VAR" -s /bin/false "$HUNTDOWN_USER"
        $sudo usermod -a -G $USER "$HUNTDOWN_USER"
    fi

    if [ -d "$HUNTDOWN_VAR" ] ; then
        $sudo chown -R $HUNTDOWN_USER:$HUNTDOWN_USER $HUNTDOWN_VAR
    fi
}

function install_jq (){
	{ $sudo apt update; \
      $sudo apt install -y jq >> "$LOG_FILE" 2>&1; } >> "$LOG_FILE" 2>&1
}

function ansible_install() {
    dpkg -l ansible 2> /dev/null | grep -q ^ii && \
       print_header Ansible already installed && return
	print_header Installing Ansible

    if [ ! -f "/usr/bin/apt-add-repository" ] ; then
        $sudo apt install -y software-properties-common >> "$LOG_FILE" 2>&1
    fi 
    { $sudo apt-add-repository -y ppa:ansible/ansible; \
      $sudo apt update; \
      $sudo apt install -y ansible; } >> "$LOG_FILE" 2>&1
    echo -e "[defaults]\nhost_key_checking = False" | $sudo -u $HUNTDOWN_USER tee $HUNTDOWN_VAR/.ansible.cfg > /dev/null
}

function welcome_message() {
	cat << EOF
Welcome to the Huntdown setup script.
This script will install all the dependencies needed to run the Huntdown.
If you need an unassisted installation setup the following environment variables:
    MONGO_USER
    MONGO_PASSWORD

And alternatively you may need to set:
    MONGO_HOST
    MONGO_PORT
If those variables are not set, the script will ask for them.
Press any key to continue or Ctrl+C to exit.
EOF
    if [ -f "$HUNTDOWN_CONFIG/environment.json" ] ; then
        echo "The installation will start automatically in 5 seconds"
        echo "Press Ctrl+C to cancel"
        for i in $(seq 1 5) ; do
            echo -n "."
            sleep 1
        done
        echo ""
    else
        read -rn 1 -s
    fi
    echo ""
    echo "Detected OS: $os"
}

function get_environment_variables () {
    if [ ! -f "$HUNTDOWN_CONFIG/environment.json" ] ; then
        echo "Previous installation not found" >&2
        return 1
    fi
    install_jq
    MONGO_USER=$(sudo cat $HUNTDOWN_CONFIG/environment.json | jq -r '.mongo_user')
    MONGO_PASSWORD=$(sudo cat $HUNTDOWN_CONFIG/environment.json | jq -r '.mongo_password')
    MONGO_HOST=$(sudo cat $HUNTDOWN_CONFIG/environment.json | jq -r '.mongo_host')
    MONGO_PORT=$(sudo cat $HUNTDOWN_CONFIG/environment.json | jq -r '.mongo_port')
    PULSAR_HOST=$(sudo cat $HUNTDOWN_CONFIG/environment.json | jq -r '.pulsar_host')
    PULSAR_PORT=$(sudo cat $HUNTDOWN_CONFIG/environment.json | jq -r '.pulsar_port')
    PULSAR_USER=$(sudo cat $HUNTDOWN_CONFIG/environment.json | jq -r '.pulsar_user')
}

function get_environment_variables () {
    if [ ! -f "$HUNTDOWN_CONFIG/environment.json" ] ; then
        echo "Previous installation not found" >&2
        return 1
    fi
    install_jq
    MONGO_USER=$($sudo cat $HUNTDOWN_CONFIG/environment.json | jq -r '.mongo_user')
    MONGO_PASSWORD=$($sudo cat $HUNTDOWN_CONFIG/environment.json | jq -r '.mongo_password')
    MONGO_HOST=$($sudo cat $HUNTDOWN_CONFIG/environment.json | jq -r '.mongo_host')
    MONGO_PORT=$($sudo cat $HUNTDOWN_CONFIG/environment.json | jq -r '.mongo_port')
    PULSAR_HOST=$($sudo cat $HUNTDOWN_CONFIG/environment.json | jq -r '.pulsar_host')
    PULSAR_PORT=$($sudo cat $HUNTDOWN_CONFIG/environment.json | jq -r '.pulsar_port')
    PULSAR_USER=$($sudo cat $HUNTDOWN_CONFIG/environment.json | jq -r '.pulsar_user')
}

function create_base_dirs () {
    print_header Creating base directories
    if [ ! -d "$HUNTDOWN_PREFIX/bin" ] ; then
        $sudo mkdir -p $HUNTDOWN_PREFIX/bin
    fi
    if [ ! -d "$HUNTDOWN_PREFIX/share/processors" ] ; then
        $sudo mkdir -p $HUNTDOWN_PREFIX/share
    fi
    if [ ! -d "$HUNTDOWN_CONFIG" ] ; then
        $sudo mkdir -p $HUNTDOWN_CONFIG
    fi
    if [ ! -d "$HUNTDOWN_VAR" ] ; then
        $sudo mkdir -p $HUNTDOWN_VAR/results        
    fi
}

function copy_base_files () {
    print_header Copying base files
    $sudo cp src/database/config.yml $HUNTDOWN_CONFIG
    $sudo cp src/instantiation/struct-definition.json $HUNTDOWN_CONFIG
}

function enable_services () {
    print_header Enabling services
    for service in $HUNTDOWN_SERVICES ; do
        $sudo systemctl enable $service >> "$LOG_FILE" 2>&1
    done
}

function start_services () {
    print_header Starting services
    for service in $HUNTDOWN_SERVICES ; do
        $sudo systemctl start $service >> "$LOG_FILE" 2>&1
    done
}

function enable_services_aws () {
    print_header Enabling services
    for service in $1 $2 $3 ; do
        $sudo systemctl enable $service >> "$LOG_FILE" 2>&1
    done
}

function start_services_aws () {
    print_header Starting services
    for service in $1 $2 $3 ; do
        $sudo systemctl start $service >> "$LOG_FILE" 2>&1
    done
}

function install_check_script() {
    service=${1:-all}
    print_header Installing check script
    $sudo install --mode=755 tools/check-$service-services.sh --target-directory $HUNTDOWN_PREFIX/bin
}