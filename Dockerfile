FROM ubuntu:24.04

RUN apt update && apt install -y nano sudo software-properties-common python3-pip wget xz-utils;

RUN wget https://go.dev/dl/go1.20.8.linux-amd64.tar.gz
RUN rm -rf /usr/lib/go && tar -C /usr/lib -xzf go1.20.8.linux-amd64.tar.gz

ENV PATH $PATH:/usr/lib/go/bin

RUN apt install -y npm

RUN wget https://nodejs.org/dist/v18.18.0/node-v18.18.0-linux-x64.tar.xz
RUN rm -rf /usr/local/bin/node && tar -C /usr/local --strip-components 1 -xJf node-v18.18.0-linux-x64.tar.xz

ENV PATH $PATH:/usr/local/bin/node

USER root
RUN mkdir -p /etc/huntdown
RUN mkdir -p /usr/local/huntdown/bin
RUN mkdir -p /usr/local/huntdown/share
RUN mkdir -p /var/huntdown

RUN mkdir huntdown
WORKDIR /huntdown

COPY . .

RUN useradd -r --home-dir /var/huntdown -s /bin/false huntdown
RUN usermod -a -G root huntdown

RUN install --mode=755 tools/check-main-services.sh --target-directory /usr/local/huntdown/bin


RUN cd src/node-pulsar && npm install

RUN cd src/Interface/backend && npm install

RUN cd src/Interface && npm install

EXPOSE 3000
EXPOSE 3001

#ENV pulsar_host 
CMD ["sh", "-c", "npm run start-backend --prefix src/Interface/backend 2>&1 | tee src/Interface/backend/backend.log & DANGEROUSLY_DISABLE_HOST_CHECK=true npm run start-frontend --prefix src/Interface 2>&1 | tee src/Interface/frontend.log && tail -f /dev/null"]
