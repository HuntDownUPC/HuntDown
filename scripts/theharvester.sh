#!/bin/bash

CONFIG_FILE="/etc/theHarvester/api-keys.yaml"
KEY_FLAG=false
THEHARVESTER_PARAMS=()

update_key() {
    local service="$1"
    local id_or_key="$2"
    local secret="$3"

    if [[ "$service" == "censys" || "$service" == "tomba" ]]; then
        sed -i "/^  $service:/!b;n;s/id:.*/id: $id_or_key/" "$CONFIG_FILE"
        sed -i "/^  $service:/!b;n;n;s/secret:.*/secret: $secret/" "$CONFIG_FILE"
    else
        sed -i "/^  $service:/!b;n;s/key:.*/key: $id_or_key/" "$CONFIG_FILE"
    fi
}

# Loop through all the arguments
for arg in "$@"; do
    if $KEY_FLAG; then
        IFS=',' read -ra ENTRIES <<< "$arg"
        for (( i=0; i<${#ENTRIES[@]}; i++ )); do
            service="${ENTRIES[$i]}"
            if [[ "$service" == "censys" || "$service" == "tomba" ]]; then
                if (( i + 2 >= ${#ENTRIES[@]} )); then
                    echo "Error: 'censys' or 'tomba' require an id and a secret."
                    exit 1
                fi
                id="${ENTRIES[$((i+1))]}"
                secret="${ENTRIES[$((i+2))]}"
                update_key "$service" "$id" "$secret"
                ((i+=2))
            else
                if (( i + 1 >= ${#ENTRIES[@]} )); then
                    echo "Error: Missing key for service '$service'."
                    exit 1
                fi
                key="${ENTRIES[$((i+1))]}"
                update_key "$service" "$key" ""
                ((i+=1))
            fi
        done
        KEY_FLAG=false
    elif [[ $arg == "-key" ]]; then
        KEY_FLAG=true
    else
        THEHARVESTER_PARAMS+=("$arg")
    fi
done

# Check if theHarvester parameters are provided
if [ ${#THEHARVESTER_PARAMS[@]} -eq 0 ]; then
    echo "No parameters were provided for theHarvester."
    exit 1
fi

# Execute theHarvester with the rest of the parameters
theHarvester "${THEHARVESTER_PARAMS[@]}"

exit 0
