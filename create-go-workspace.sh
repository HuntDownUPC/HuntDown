cat > go.work << EOF
go 1.21

use (
        ./src
        ./src/attack-manager
        ./src/attacks
        ./src/confManager
        ./src/database
        ./src/helpers
        ./src/instantiation
        ./artifacts/go-modules/PulsarLib
        ./artifacts/go-modules/db/DBUsers
        ./artifacts/go-modules/db/DBWrapper
        ./artifacts/go-modules/db/backends/mongo/MongoDBWrapper
        ./artifacts/go-modules/db/backends/mongo/UsersMongoDB
        ./artifacts/go-modules/db/common
)
EOF